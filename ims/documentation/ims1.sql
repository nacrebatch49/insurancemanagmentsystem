-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.0.41-community-nt - MySQL Community Edition (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for insurancepolicies
CREATE DATABASE IF NOT EXISTS `insurancepolicies` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `insurancepolicies`;

-- Dumping structure for table insurancepolicies.address_table
CREATE TABLE IF NOT EXISTS `address_table` (
  `address_id` int(5) NOT NULL auto_increment,
  `local_address` longtext NOT NULL,
  `pincode` int(6) NOT NULL,
  `city_id` int(6) NOT NULL,
  PRIMARY KEY  (`address_id`),
  KEY `city_id` (`city_id`),
  CONSTRAINT `city_id` FOREIGN KEY (`city_id`) REFERENCES `city_master_table` (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.address_table: ~1 rows (approximately)
/*!40000 ALTER TABLE `address_table` DISABLE KEYS */;
INSERT INTO `address_table` (`address_id`, `local_address`, `pincode`, `city_id`) VALUES
	(1, 'shikarpur', 202395, 1);
/*!40000 ALTER TABLE `address_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.admin_agent_meeting
CREATE TABLE IF NOT EXISTS `admin_agent_meeting` (
  `admin_agent_meeting_id` int(5) NOT NULL auto_increment,
  `agent_id` int(5) default '0',
  `venue` varchar(50) default NULL,
  `date` date default NULL,
  `time` time default NULL,
  PRIMARY KEY  (`admin_agent_meeting_id`),
  KEY `FK1_agent_id` (`agent_id`),
  CONSTRAINT `FK1_agent_id` FOREIGN KEY (`agent_id`) REFERENCES `agent_registration_table` (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.admin_agent_meeting: ~0 rows (approximately)
/*!40000 ALTER TABLE `admin_agent_meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_agent_meeting` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.agent_approved_table
CREATE TABLE IF NOT EXISTS `agent_approved_table` (
  `agent_approved_id` int(5) NOT NULL auto_increment,
  `agent_name` varchar(25) NOT NULL default '0',
  `agent_qualification` varchar(25) NOT NULL default '0',
  `gender` varchar(25) NOT NULL default '0',
  `agent_email_id` varchar(25) NOT NULL default '0',
  `location` varchar(25) NOT NULL default '0',
  `status_id` int(5) NOT NULL default '0',
  PRIMARY KEY  (`agent_approved_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `status_id` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.agent_approved_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `agent_approved_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent_approved_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.agent_customer_meeting
CREATE TABLE IF NOT EXISTS `agent_customer_meeting` (
  `agent_customer_meeting_id` int(11) NOT NULL auto_increment,
  `agent_id` int(11) default '0',
  `customer_id` int(11) default '0',
  `vanue` varchar(50) default NULL,
  `date` date default NULL,
  `time` time default NULL,
  PRIMARY KEY  (`agent_customer_meeting_id`),
  KEY `FK1_agent_id1` (`agent_id`),
  KEY `FK2_customer_id1` (`customer_id`),
  CONSTRAINT `FK1_agent_id1` FOREIGN KEY (`agent_id`) REFERENCES `agent_registration_table` (`agent_id`),
  CONSTRAINT `FK2_customer_id1` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.agent_customer_meeting: ~0 rows (approximately)
/*!40000 ALTER TABLE `agent_customer_meeting` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent_customer_meeting` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.agent_registration_table
CREATE TABLE IF NOT EXISTS `agent_registration_table` (
  `agent_id` int(5) NOT NULL auto_increment,
  `qualification` varchar(50) NOT NULL default '0',
  `percentage` float NOT NULL default '0',
  `year_of_passout` int(5) NOT NULL default '0',
  `dob` date NOT NULL,
  `mobile_number` bigint(20) NOT NULL default '0',
  `adhaar_card_no` bigint(20) NOT NULL default '0',
  `pancard_no` bigint(20) NOT NULL default '0',
  `date of joining` date NOT NULL,
  `agent_approved_id` int(5) NOT NULL default '0',
  `address` int(5) NOT NULL default '0',
  `login_id` int(5) default NULL,
  PRIMARY KEY  (`agent_id`),
  UNIQUE KEY `login_id` (`login_id`),
  KEY `agent_approved_id` (`agent_approved_id`),
  KEY `address` (`address`),
  CONSTRAINT `address` FOREIGN KEY (`address`) REFERENCES `address_table` (`address_id`),
  CONSTRAINT `agent_approved_id` FOREIGN KEY (`agent_approved_id`) REFERENCES `agent_approved_table` (`agent_approved_id`),
  CONSTRAINT `login_id1` FOREIGN KEY (`login_id`) REFERENCES `login_table` (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.agent_registration_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `agent_registration_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `agent_registration_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.car_insurance_master_table
CREATE TABLE IF NOT EXISTS `car_insurance_master_table` (
  `car_insurance_id` int(5) NOT NULL auto_increment,
  `coverage` mediumtext NOT NULL,
  `name` varchar(25) NOT NULL,
  `description` longtext NOT NULL,
  `rate_of_interest` double NOT NULL,
  `duration` int(11) NOT NULL,
  `payble_amount` double NOT NULL,
  `initial_amount` double NOT NULL,
  `status_id` int(11) default NULL,
  PRIMARY KEY  (`car_insurance_id`),
  KEY `status_id123` (`status_id`),
  CONSTRAINT `status_id123` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.car_insurance_master_table: ~3 rows (approximately)
/*!40000 ALTER TABLE `car_insurance_master_table` DISABLE KEYS */;
INSERT INTO `car_insurance_master_table` (`car_insurance_id`, `coverage`, `name`, `description`, `rate_of_interest`, `duration`, `payble_amount`, `initial_amount`, `status_id`) VALUES
	(1, 'fire, accedent,', 'carinurance', 'amount pay like that', 20, 120, 500000, 0, NULL),
	(2, 'accident', 'car raksha', 'car', 50, 5, 5000, 5000, 4),
	(3, 'dammage,accident', 'car crash', 'only happend by other people', 4, 15, 4500000, 2000, 5);
/*!40000 ALTER TABLE `car_insurance_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.child_care_insurance_master_table
CREATE TABLE IF NOT EXISTS `child_care_insurance_master_table` (
  `child_care_insurance_id` int(5) NOT NULL auto_increment,
  `max_limit` int(5) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '0',
  `coverage` mediumtext NOT NULL,
  `desciption` longtext NOT NULL,
  `rate_of_interest` double NOT NULL,
  `payable_amount` double NOT NULL,
  `initial_amount` double default NULL,
  `status_id` int(5) default NULL,
  `life_insurance_offers` mediumtext,
  PRIMARY KEY  (`child_care_insurance_id`),
  KEY `FK4_status_id` (`status_id`),
  CONSTRAINT `FK4_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.child_care_insurance_master_table: ~3 rows (approximately)
/*!40000 ALTER TABLE `child_care_insurance_master_table` DISABLE KEYS */;
INSERT INTO `child_care_insurance_master_table` (`child_care_insurance_id`, `max_limit`, `name`, `coverage`, `desciption`, `rate_of_interest`, `payable_amount`, `initial_amount`, `status_id`, `life_insurance_offers`) VALUES
	(1, 120, 'child plan', 'accident', 'surcha nkawCH', 20, 2000, 2000, 5, 'dfg'),
	(2, 30, 'child care', 'higher study', 'child education cover ', 4, 200000, 2000, 4, ''),
	(3, 12, 'kubduc', 'uhooio', 'gbubi', 4, 45000, 200, 5, '');
/*!40000 ALTER TABLE `child_care_insurance_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.city_master_table
CREATE TABLE IF NOT EXISTS `city_master_table` (
  `city_id` int(5) NOT NULL auto_increment,
  `city_name` varchar(25) NOT NULL,
  `state_id` int(5) NOT NULL,
  PRIMARY KEY  (`city_id`),
  KEY `state_id` (`state_id`),
  CONSTRAINT `state_id` FOREIGN KEY (`state_id`) REFERENCES `state_master_table` (`state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.city_master_table: ~1 rows (approximately)
/*!40000 ALTER TABLE `city_master_table` DISABLE KEYS */;
INSERT INTO `city_master_table` (`city_id`, `city_name`, `state_id`) VALUES
	(1, 'bul', 1);
/*!40000 ALTER TABLE `city_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.country_master_table
CREATE TABLE IF NOT EXISTS `country_master_table` (
  `country_id` int(5) NOT NULL auto_increment,
  `country_name` varchar(25) NOT NULL,
  PRIMARY KEY  (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.country_master_table: ~2 rows (approximately)
/*!40000 ALTER TABLE `country_master_table` DISABLE KEYS */;
INSERT INTO `country_master_table` (`country_id`, `country_name`) VALUES
	(1, 'india'),
	(2, 'pak');
/*!40000 ALTER TABLE `country_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_child_care_insurance_register_table
CREATE TABLE IF NOT EXISTS `customer_child_care_insurance_register_table` (
  `customer_child_care_inusrance_id` int(5) NOT NULL auto_increment,
  `child_name` varchar(50) NOT NULL default '0',
  `child_age` int(5) NOT NULL default '0',
  `birthcertificate_no` varchar(50) NOT NULL default '0',
  `parents_name` varchar(50) NOT NULL default '0',
  `date_of_insurance` date NOT NULL,
  `payment_type` varchar(50) NOT NULL default '0',
  `customer_id` int(5) NOT NULL default '0',
  `child_care_inusrance_id` int(5) NOT NULL default '0',
  PRIMARY KEY  (`customer_child_care_inusrance_id`),
  KEY `customer_id11` (`customer_id`),
  KEY `child_care_insurance_id` (`child_care_inusrance_id`),
  CONSTRAINT `child_care_insurance_id` FOREIGN KEY (`child_care_inusrance_id`) REFERENCES `child_care_insurance_master_table` (`child_care_insurance_id`),
  CONSTRAINT `customer_id11` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_child_care_insurance_register_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_child_care_insurance_register_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_child_care_insurance_register_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_claim_request_table
CREATE TABLE IF NOT EXISTS `customer_claim_request_table` (
  `claim_id` varchar(50) NOT NULL,
  `register_policy_no` varchar(50) NOT NULL,
  `nominies_name` varchar(50) default NULL,
  `email` varchar(50) NOT NULL,
  `relation` varchar(50) default NULL,
  `reason` varchar(50) NOT NULL,
  `date_of_claim` date NOT NULL,
  PRIMARY KEY  (`claim_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_claim_request_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_claim_request_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_claim_request_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_feedback_table
CREATE TABLE IF NOT EXISTS `customer_feedback_table` (
  `feedback_id` int(5) NOT NULL auto_increment,
  `feedback_name` longtext NOT NULL,
  `customer_email_id` varchar(25) NOT NULL,
  `customer_register_id` int(5) NOT NULL default '0',
  PRIMARY KEY  (`feedback_id`),
  KEY `customer_register_id` (`customer_register_id`),
  CONSTRAINT `customer_register_id` FOREIGN KEY (`customer_register_id`) REFERENCES `customer_registration_table` (`costomer_register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_feedback_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_feedback_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_feedback_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_life_insurance_register_table
CREATE TABLE IF NOT EXISTS `customer_life_insurance_register_table` (
  `customer_lifeinsurance_id` int(11) NOT NULL auto_increment,
  `insurance_name` varchar(50) NOT NULL,
  `min_limit` int(11) NOT NULL,
  `max_limit` int(11) NOT NULL,
  `date_of_policy` date NOT NULL,
  `nominies_name` varchar(50) NOT NULL,
  `nominies_email_id` varchar(50) NOT NULL,
  `relation` varchar(50) NOT NULL,
  `emi_option_type` int(5) NOT NULL,
  `register_policy_no` varchar(50) NOT NULL,
  `customer_id` int(5) NOT NULL,
  `life_insurance_id` int(5) NOT NULL,
  PRIMARY KEY  (`customer_lifeinsurance_id`),
  KEY `customer_id1` (`customer_id`),
  KEY `life_insurance_id1` (`life_insurance_id`),
  KEY `FK3_emi_option_id` (`emi_option_type`),
  CONSTRAINT `customer_id1` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`),
  CONSTRAINT `FK3_emi_option_id` FOREIGN KEY (`emi_option_type`) REFERENCES `emi_option_table` (`emi_id`),
  CONSTRAINT `life_insurance_id1` FOREIGN KEY (`life_insurance_id`) REFERENCES `life_insurance_master_table` (`life_isn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_life_insurance_register_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_life_insurance_register_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_life_insurance_register_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_registered_car_insurance_policies
CREATE TABLE IF NOT EXISTS `customer_registered_car_insurance_policies` (
  `car_insurance_id` int(11) NOT NULL auto_increment,
  `insurance_policy_no` varchar(50) NOT NULL,
  `car_number` varchar(50) NOT NULL,
  `purchage_date` date NOT NULL,
  `rc_no` varchar(50) NOT NULL,
  `nominies_name` varchar(50) NOT NULL,
  `nominies_mobile_no` bigint(20) NOT NULL,
  `nominies_relation` varchar(50) NOT NULL,
  `max_limit` int(11) NOT NULL,
  `emi_option_type` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `car_insurance_master_id` int(11) NOT NULL,
  PRIMARY KEY  (`car_insurance_id`),
  KEY `car_insurance_master_id` (`car_insurance_master_id`),
  KEY `customer_id` (`customer_id`),
  KEY `FK3_emi_option_type` (`emi_option_type`),
  CONSTRAINT `car_insurance_master_id` FOREIGN KEY (`car_insurance_master_id`) REFERENCES `car_insurance_master_table` (`car_insurance_id`),
  CONSTRAINT `customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`),
  CONSTRAINT `FK3_emi_option_type` FOREIGN KEY (`emi_option_type`) REFERENCES `emi_option_table` (`emi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_registered_car_insurance_policies: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_registered_car_insurance_policies` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_registered_car_insurance_policies` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_register_home_insuranc_table
CREATE TABLE IF NOT EXISTS `customer_register_home_insuranc_table` (
  `customer_homeinsurance_id` int(11) NOT NULL auto_increment,
  `register_policy_no` varchar(50) NOT NULL default '0',
  `home_addres` varchar(50) NOT NULL,
  `square_fit` decimal(10,0) NOT NULL,
  `home_no` varchar(50) NOT NULL,
  `max_duration` int(11) NOT NULL,
  `nominies_name` varchar(50) NOT NULL,
  `nominies_email_id` varchar(50) NOT NULL,
  `relation` varchar(50) NOT NULL,
  `date_of_policies` date NOT NULL,
  `emi_option_type1` int(5) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `home_insurance_id` int(11) NOT NULL,
  PRIMARY KEY  (`customer_homeinsurance_id`),
  KEY `FK1_customer_id` (`customer_id`),
  KEY `FK2_FK2_home_insurance_id` (`home_insurance_id`),
  KEY `FK3_emi_option_type1` (`emi_option_type1`),
  CONSTRAINT `FK1_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`),
  CONSTRAINT `FK2_FK2_home_insurance_id` FOREIGN KEY (`home_insurance_id`) REFERENCES `home_insurance_master_table` (`home_insurance_id`),
  CONSTRAINT `FK3_emi_option_type1` FOREIGN KEY (`emi_option_type1`) REFERENCES `emi_option_table` (`emi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_register_home_insuranc_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_register_home_insuranc_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_register_home_insuranc_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_registration_table
CREATE TABLE IF NOT EXISTS `customer_registration_table` (
  `costomer_register_id` int(5) NOT NULL auto_increment,
  `customer_first_name` varchar(25) NOT NULL,
  `customer_last_name` varchar(25) NOT NULL,
  `customer_email_id` varchar(25) NOT NULL,
  `customer_mobile_no` bigint(10) NOT NULL,
  `aadhar_card` double NOT NULL,
  `customer_gender` varchar(10) NOT NULL,
  `customer_dob` date NOT NULL,
  `address_id` int(5) NOT NULL,
  `agent_id` int(5) default NULL,
  `login_id` int(5) default NULL,
  `status_id` int(11) default NULL,
  PRIMARY KEY  (`costomer_register_id`),
  UNIQUE KEY `customer_email_id` (`customer_email_id`),
  UNIQUE KEY `customer_mobile_no` (`customer_mobile_no`),
  KEY `address_id` (`address_id`),
  KEY `FK2_agent_id` (`agent_id`),
  KEY `login_id` (`login_id`),
  KEY `status_id1` (`status_id`),
  CONSTRAINT `address_id` FOREIGN KEY (`address_id`) REFERENCES `address_table` (`address_id`),
  CONSTRAINT `FK2_agent_id` FOREIGN KEY (`agent_id`) REFERENCES `agent_registration_table` (`agent_id`),
  CONSTRAINT `login_id` FOREIGN KEY (`login_id`) REFERENCES `login_table` (`login_id`),
  CONSTRAINT `status_id1` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_registration_table: ~1 rows (approximately)
/*!40000 ALTER TABLE `customer_registration_table` DISABLE KEYS */;
INSERT INTO `customer_registration_table` (`costomer_register_id`, `customer_first_name`, `customer_last_name`, `customer_email_id`, `customer_mobile_no`, `aadhar_card`, `customer_gender`, `customer_dob`, `address_id`, `agent_id`, `login_id`, `status_id`) VALUES
	(1, 'banti', 'singh', 'singh@gmail.com', 8686492032, 0, 'male', '2018-06-10', 1, NULL, NULL, NULL);
/*!40000 ALTER TABLE `customer_registration_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.emi_option_table
CREATE TABLE IF NOT EXISTS `emi_option_table` (
  `emi_id` int(5) NOT NULL auto_increment,
  `pay_service_type` varchar(50) NOT NULL default '0',
  PRIMARY KEY  (`emi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.emi_option_table: ~4 rows (approximately)
/*!40000 ALTER TABLE `emi_option_table` DISABLE KEYS */;
INSERT INTO `emi_option_table` (`emi_id`, `pay_service_type`) VALUES
	(1, 'monthly'),
	(2, 'quaterly'),
	(3, 'half yearly'),
	(4, 'annually');
/*!40000 ALTER TABLE `emi_option_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.final_claim_approved_table
CREATE TABLE IF NOT EXISTS `final_claim_approved_table` (
  `claim_id` varchar(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status_id1` int(11) NOT NULL,
  `description_any` varchar(50) default NULL,
  UNIQUE KEY `claim_id` (`claim_id`),
  KEY `FK1_customer_id1` (`customer_id`),
  KEY `FK2_status_id111` (`status_id1`),
  CONSTRAINT `FK1_customer_id1` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`),
  CONSTRAINT `FK2_status_id111` FOREIGN KEY (`status_id1`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.final_claim_approved_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `final_claim_approved_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `final_claim_approved_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.home_insurance_master_table
CREATE TABLE IF NOT EXISTS `home_insurance_master_table` (
  `home_insurance_id` int(5) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '0',
  `max_limit` int(5) NOT NULL default '0',
  `coverage` mediumtext NOT NULL,
  `rate_of_interest` double NOT NULL,
  `home_insurance_offer` mediumtext,
  `initial_amount` double NOT NULL,
  `description` longtext NOT NULL,
  `total_amount_paid` double NOT NULL,
  `policy_no` varchar(50) default '0',
  `status_id` int(11) default '0',
  PRIMARY KEY  (`home_insurance_id`),
  KEY `FK1_status_is_1` (`status_id`),
  CONSTRAINT `FK1_status_is_1` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.home_insurance_master_table: ~2 rows (approximately)
/*!40000 ALTER TABLE `home_insurance_master_table` DISABLE KEYS */;
INSERT INTO `home_insurance_master_table` (`home_insurance_id`, `name`, `max_limit`, `coverage`, `rate_of_interest`, `home_insurance_offer`, `initial_amount`, `description`, `total_amount_paid`, `policy_no`, `status_id`) VALUES
	(1, 'Home suracha', 140, 'home secure offer', 20, 'good', 5000, 'pay some for your home', 600000, '115Nvc', 4),
	(2, 'Home Damage', 20, 'eathQuick', 6, 'all damage cover', 2000, 'Only for EarthQuick', 4500000, 'N111B02', 5);
/*!40000 ALTER TABLE `home_insurance_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.insurancepolicies_table
CREATE TABLE IF NOT EXISTS `insurancepolicies_table` (
  `policy_id` int(5) NOT NULL auto_increment,
  `policy_name` varchar(25) NOT NULL,
  `customer_id1` int(5) NOT NULL,
  `customer_register_policy_id` int(5) NOT NULL,
  PRIMARY KEY  (`policy_id`),
  KEY `customer_id111` (`customer_id1`),
  CONSTRAINT `customer_id111` FOREIGN KEY (`customer_id1`) REFERENCES `customer_registration_table` (`costomer_register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.insurancepolicies_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `insurancepolicies_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `insurancepolicies_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.life_insurance_master_table
CREATE TABLE IF NOT EXISTS `life_insurance_master_table` (
  `life_isn_id` int(5) NOT NULL auto_increment,
  `insurance_name` varchar(50) NOT NULL default '0',
  `duration` int(11) NOT NULL default '0',
  `insurance_policy_no` varchar(50) NOT NULL default '0',
  `rate_of_interest` double NOT NULL,
  `desciption` longtext NOT NULL,
  `total_amount_paid` double NOT NULL,
  `life_insurance_offers` mediumtext,
  `covarage` text NOT NULL,
  `status_id` int(5) default NULL,
  PRIMARY KEY  (`life_isn_id`),
  KEY `FK3_status_id` (`status_id`),
  CONSTRAINT `FK3_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.life_insurance_master_table: ~3 rows (approximately)
/*!40000 ALTER TABLE `life_insurance_master_table` DISABLE KEYS */;
INSERT INTO `life_insurance_master_table` (`life_isn_id`, `insurance_name`, `duration`, `insurance_policy_no`, `rate_of_interest`, `desciption`, `total_amount_paid`, `life_insurance_offers`, `covarage`, `status_id`) VALUES
	(1, 'life suracha', 10, '11VB4568', 20, 'life surcha kawach', 20000, 'sldfkrf', 'acci', 5),
	(2, 'life care', 20, 'N1111V01', 20, 'life care for life time', 20000, '', 'deth,MURREDER', 4),
	(3, 'life suracha', 30, '11VB4568', 14, 'family surksha', 400000, '', 'deth', 5);
/*!40000 ALTER TABLE `life_insurance_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.login_table
CREATE TABLE IF NOT EXISTS `login_table` (
  `login_id` int(5) NOT NULL auto_increment,
  `user_username` varchar(25) NOT NULL,
  `user_password` varchar(25) NOT NULL,
  `role_id` int(5) NOT NULL,
  PRIMARY KEY  (`login_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `role_table` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.login_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `login_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.payment_table
CREATE TABLE IF NOT EXISTS `payment_table` (
  `payment_id` int(5) NOT NULL auto_increment,
  `pay_amount` decimal(10,0) NOT NULL default '0',
  `date_of_emi` date NOT NULL,
  `pay_date` date NOT NULL,
  `status_id` int(5) NOT NULL,
  `policy_id` int(5) NOT NULL default '0',
  PRIMARY KEY  (`payment_id`),
  KEY `FK1_policy_id` (`policy_id`),
  KEY `FK2_status_id` (`status_id`),
  CONSTRAINT `FK1_policy_id` FOREIGN KEY (`policy_id`) REFERENCES `insurancepolicies_table` (`policy_id`),
  CONSTRAINT `FK2_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.payment_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `payment_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.role_table
CREATE TABLE IF NOT EXISTS `role_table` (
  `role_id` int(5) NOT NULL auto_increment,
  `role_name` varchar(25) NOT NULL,
  PRIMARY KEY  (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.role_table: ~3 rows (approximately)
/*!40000 ALTER TABLE `role_table` DISABLE KEYS */;
INSERT INTO `role_table` (`role_id`, `role_name`) VALUES
	(1, 'amin'),
	(2, 'customer'),
	(3, 'agent');
/*!40000 ALTER TABLE `role_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.state_master_table
CREATE TABLE IF NOT EXISTS `state_master_table` (
  `state_id` int(5) NOT NULL auto_increment,
  `state_name` varchar(25) NOT NULL,
  `country_id` int(5) NOT NULL,
  PRIMARY KEY  (`state_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `country_id` FOREIGN KEY (`country_id`) REFERENCES `country_master_table` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.state_master_table: ~1 rows (approximately)
/*!40000 ALTER TABLE `state_master_table` DISABLE KEYS */;
INSERT INTO `state_master_table` (`state_id`, `state_name`, `country_id`) VALUES
	(1, 'up', 1);
/*!40000 ALTER TABLE `state_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.status_table
CREATE TABLE IF NOT EXISTS `status_table` (
  `status_id` int(5) NOT NULL auto_increment,
  `status_name` varchar(25) NOT NULL default '0',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.status_table: ~13 rows (approximately)
/*!40000 ALTER TABLE `status_table` DISABLE KEYS */;
INSERT INTO `status_table` (`status_id`, `status_name`) VALUES
	(1, 'APPORVED'),
	(2, 'REJECT'),
	(3, 'PENDING'),
	(4, 'WORKING'),
	(5, 'NOTWORKING'),
	(6, 'LEAVE'),
	(7, 'CLAIMPASS'),
	(8, 'CLAIMREJECTED'),
	(9, 'CLAIMPENDING'),
	(10, 'FEEDBACKUNSEN'),
	(11, 'FEEDBACKSEEN'),
	(12, 'OURCUSTOMER'),
	(13, 'NOTACUSTOMER');
/*!40000 ALTER TABLE `status_table` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
