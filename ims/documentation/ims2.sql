-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.0.41-community-nt - MySQL Community Edition (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for insurancepolicies
CREATE DATABASE IF NOT EXISTS `insurancepolicies` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `insurancepolicies`;

-- Dumping structure for table insurancepolicies.address_table
CREATE TABLE IF NOT EXISTS `address_table` (
  `address_id` int(5) NOT NULL auto_increment,
  `local_address` longtext NOT NULL,
  `pincode` int(6) NOT NULL,
  `city_id` int(6) NOT NULL,
  PRIMARY KEY  (`address_id`),
  KEY `city_id` (`city_id`),
  CONSTRAINT `city_id` FOREIGN KEY (`city_id`) REFERENCES `city_master_table` (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.address_table: ~43 rows (approximately)
/*!40000 ALTER TABLE `address_table` DISABLE KEYS */;
INSERT INTO `address_table` (`address_id`, `local_address`, `pincode`, `city_id`) VALUES
	(1, 'shikarpur', 202395, 1),
	(2, 'gurudwara,ameerpet', 812005, 1),
	(3, 'gurudwara,ameerpet', 812005, 1),
	(4, 'gurudwara,ameerpet', 812005, 1),
	(5, 'gurudwara,ameerpet', 812005, 1),
	(6, 'NEAR BANK,wardha', 821006, 1),
	(7, 'newr gurudwara', 712005, 1),
	(8, 'newr gurudwara', 712005, 1),
	(9, 'newr gurudwara', 712005, 1),
	(10, 'newr gurudwara', 712005, 1),
	(11, 'newr gurudwara', 712005, 1),
	(12, 'newr gurudwara', 712005, 1),
	(13, 'newr gurudwara', 712005, 1),
	(14, 'NEAR BANK,nagpur', 821006, 1),
	(15, 'newr gurudwara', 712005, 1),
	(16, '1-9-12-121', 812005, 1),
	(17, '1-9-12-121', 812005, 1),
	(18, '1-9-12-121', 812005, 1),
	(19, 'newr gurudwara', 712005, 1),
	(20, '1-9-12-121', 812005, 1),
	(21, '1-9-12-121', 812005, 1),
	(22, '1-9-12-121', 812005, 1),
	(23, '1-9-12-121', 812005, 1),
	(24, '1-9-12-121', 812005, 1),
	(25, '1-9-12-121', 812005, 1),
	(26, '1-9-12-121', 812005, 1),
	(27, '1-9-12-121', 812005, 1),
	(28, '1-9-12-121', 812005, 1),
	(29, '1-9-12-121', 812005, 1),
	(30, '1-9-12-121', 812005, 1),
	(31, 'lvdlfdlfldskvmsk', 856589, 1),
	(32, 'lvdlfdlfldskvmsk', 856589, 1),
	(33, 'A/C SDSSSD', 856586, 1),
	(34, 'A/C SDSSSD', 856586, 1),
	(35, 'A/C SDSSSD', 856586, 1),
	(36, 'A/C SDSSSD', 856586, 1),
	(37, 'A/C SDSSSD', 856586, 1),
	(38, 'A/C SDSSSD', 856586, 1),
	(39, 'A/C SDSSSD', 856586, 1),
	(40, 'bgp vldclcmdcq', 865985, 1),
	(41, 'bhagalpur', 812005, 1),
	(42, 'DSFCSVFVDFFDF', 985654, 1),
	(43, 'hyd   djdj', 985698, 1);
/*!40000 ALTER TABLE `address_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.admin_agent_meeting
CREATE TABLE IF NOT EXISTS `admin_agent_meeting` (
  `admin_agent_meeting_id` int(5) NOT NULL auto_increment,
  `agent_id` int(5) default '0',
  `venue` varchar(50) default NULL,
  `date` date default NULL,
  `time` time default NULL,
  PRIMARY KEY  (`admin_agent_meeting_id`),
  KEY `FK1_agent_id` (`agent_id`),
  CONSTRAINT `FK1_agent_id` FOREIGN KEY (`agent_id`) REFERENCES `agent_registration_table` (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.admin_agent_meeting: ~4 rows (approximately)
/*!40000 ALTER TABLE `admin_agent_meeting` DISABLE KEYS */;
INSERT INTO `admin_agent_meeting` (`admin_agent_meeting_id`, `agent_id`, `venue`, `date`, `time`) VALUES
	(1, 3, 'nacre', '2018-06-04', '11:00:00'),
	(2, 3, 'nacre', '2018-06-04', '11:00:00'),
	(3, 3, 'nacre', '2018-06-13', '14:00:00'),
	(4, 3, 'nacre', '2018-06-13', '14:00:00');
/*!40000 ALTER TABLE `admin_agent_meeting` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.agent_approved_table
CREATE TABLE IF NOT EXISTS `agent_approved_table` (
  `agent_approved_id` int(5) NOT NULL auto_increment,
  `agent_name` varchar(25) NOT NULL default '0',
  `agent_qualification` varchar(25) NOT NULL default '0',
  `gender` varchar(25) NOT NULL default '0',
  `agent_email_id` varchar(25) NOT NULL default '0',
  `location` varchar(25) NOT NULL default '0',
  `status_id` int(5) NOT NULL default '0',
  PRIMARY KEY  (`agent_approved_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `status_id` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.agent_approved_table: ~2 rows (approximately)
/*!40000 ALTER TABLE `agent_approved_table` DISABLE KEYS */;
INSERT INTO `agent_approved_table` (`agent_approved_id`, `agent_name`, `agent_qualification`, `gender`, `agent_email_id`, `location`, `status_id`) VALUES
	(2, 'nikhil', 'btech', 'male', 'nikhilkmr214@gmail.com', 'hyd', 1),
	(4, 'rohit', 'btech', 'male', 'rohitkumar@gmail.com', 'hyd', 1);
/*!40000 ALTER TABLE `agent_approved_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.agent_customer_meeting
CREATE TABLE IF NOT EXISTS `agent_customer_meeting` (
  `agent_customer_meeting_id` int(11) NOT NULL auto_increment,
  `agent_id` int(11) default '0',
  `customer_id` int(11) default '0',
  `vanue` varchar(50) default NULL,
  `date1` date default NULL,
  `desc1` varchar(500) default NULL,
  `time1` time default NULL,
  PRIMARY KEY  (`agent_customer_meeting_id`),
  KEY `FK1_agent_id1` (`agent_id`),
  KEY `FK2_customer_id1` (`customer_id`),
  CONSTRAINT `FK1_agent_id1` FOREIGN KEY (`agent_id`) REFERENCES `agent_registration_table` (`agent_id`),
  CONSTRAINT `FK2_customer_id1` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.agent_customer_meeting: ~1 rows (approximately)
/*!40000 ALTER TABLE `agent_customer_meeting` DISABLE KEYS */;
INSERT INTO `agent_customer_meeting` (`agent_customer_meeting_id`, `agent_id`, `customer_id`, `vanue`, `date1`, `desc1`, `time1`) VALUES
	(1, 4, 4, 'police Station', '2019-02-02', 'theaf', '03:03:03');
/*!40000 ALTER TABLE `agent_customer_meeting` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.agent_registration_table
CREATE TABLE IF NOT EXISTS `agent_registration_table` (
  `agent_id` int(5) NOT NULL auto_increment,
  `percentage` float NOT NULL default '0',
  `year_of_passout` int(5) NOT NULL default '0',
  `dob` date NOT NULL,
  `mobile_number` bigint(20) NOT NULL default '0',
  `gender` varchar(50) NOT NULL default '0',
  `adhaar_card_no` bigint(20) NOT NULL default '0',
  `pancard_no` bigint(20) NOT NULL default '0',
  `date_of_joining` date NOT NULL,
  `agent_approved_id` int(5) NOT NULL default '0',
  `address` int(5) NOT NULL default '0',
  `login_id` int(5) default NULL,
  PRIMARY KEY  (`agent_id`),
  UNIQUE KEY `login_id` (`login_id`),
  KEY `agent_approved_id` (`agent_approved_id`),
  KEY `address` (`address`),
  CONSTRAINT `address` FOREIGN KEY (`address`) REFERENCES `address_table` (`address_id`),
  CONSTRAINT `agent_approved_id` FOREIGN KEY (`agent_approved_id`) REFERENCES `agent_approved_table` (`agent_approved_id`),
  CONSTRAINT `login_id1` FOREIGN KEY (`login_id`) REFERENCES `login_table` (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.agent_registration_table: ~2 rows (approximately)
/*!40000 ALTER TABLE `agent_registration_table` DISABLE KEYS */;
INSERT INTO `agent_registration_table` (`agent_id`, `percentage`, `year_of_passout`, `dob`, `mobile_number`, `gender`, `adhaar_card_no`, `pancard_no`, `date_of_joining`, `agent_approved_id`, `address`, `login_id`) VALUES
	(3, 80, 2014, '2018-06-20', 965865856, 'male', 7896585698545, 1236558, '2018-06-20', 2, 2, 44),
	(4, 20, 2018, '2018-06-22', 880340640, 'male', 69636653436, 365635354, '2018-06-22', 4, 6, 43);
/*!40000 ALTER TABLE `agent_registration_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.car_insurance_master_table
CREATE TABLE IF NOT EXISTS `car_insurance_master_table` (
  `car_insurance_id` int(5) NOT NULL auto_increment,
  `coverage` mediumtext NOT NULL,
  `name` varchar(25) NOT NULL,
  `description` longtext NOT NULL,
  `rate_of_interest` double NOT NULL,
  `duration` int(11) NOT NULL,
  `payble_amount` double NOT NULL,
  `initial_amount` double NOT NULL,
  `status_id` int(11) default NULL,
  PRIMARY KEY  (`car_insurance_id`),
  KEY `status_id123` (`status_id`),
  CONSTRAINT `status_id123` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.car_insurance_master_table: ~3 rows (approximately)
/*!40000 ALTER TABLE `car_insurance_master_table` DISABLE KEYS */;
INSERT INTO `car_insurance_master_table` (`car_insurance_id`, `coverage`, `name`, `description`, `rate_of_interest`, `duration`, `payble_amount`, `initial_amount`, `status_id`) VALUES
	(1, 'accident', 'car raksha', 'good car', 20, 12, 5000, 5000, 5),
	(2, 'cdvduysguys', 'ydsvyudsuyc', 'tvtyggggusygd', 23, 22, 400000, 3333, 5),
	(3, 'sgvxtydcvdst', 'tyftsfwyfd', 'gdcdyugfuydg', 233, 23, 1234, 2343, 4);
/*!40000 ALTER TABLE `car_insurance_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.child_care_insurance_master_table
CREATE TABLE IF NOT EXISTS `child_care_insurance_master_table` (
  `child_care_insurance_id` int(5) NOT NULL auto_increment,
  `max_limit` int(5) NOT NULL default '0',
  `name` varchar(50) NOT NULL default '0',
  `coverage` mediumtext NOT NULL,
  `desciption` longtext NOT NULL,
  `rate_of_interest` double NOT NULL,
  `payable_amount` double NOT NULL,
  `initial_amount` double default NULL,
  `status_id` int(5) default NULL,
  `life_insurance_offers` mediumtext,
  PRIMARY KEY  (`child_care_insurance_id`),
  KEY `FK4_status_id` (`status_id`),
  CONSTRAINT `FK4_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.child_care_insurance_master_table: ~2 rows (approximately)
/*!40000 ALTER TABLE `child_care_insurance_master_table` DISABLE KEYS */;
INSERT INTO `child_care_insurance_master_table` (`child_care_insurance_id`, `max_limit`, `name`, `coverage`, `desciption`, `rate_of_interest`, `payable_amount`, `initial_amount`, `status_id`, `life_insurance_offers`) VALUES
	(1, 5, 'child health', 'any deases', 'lcv;', 2, 2000, 2000, 4, 'fdgh'),
	(2, 134, 'fsadfytf', 'datdtaydyt', 'safdasd', 20, 234444, 2000, 5, 'ddfrfg');
/*!40000 ALTER TABLE `child_care_insurance_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.city_master_table
CREATE TABLE IF NOT EXISTS `city_master_table` (
  `city_id` int(5) NOT NULL auto_increment,
  `city_name` varchar(25) NOT NULL,
  `state_id` int(5) NOT NULL,
  PRIMARY KEY  (`city_id`),
  KEY `state_id` (`state_id`),
  CONSTRAINT `state_id` FOREIGN KEY (`state_id`) REFERENCES `state_master_table` (`state_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.city_master_table: ~1 rows (approximately)
/*!40000 ALTER TABLE `city_master_table` DISABLE KEYS */;
INSERT INTO `city_master_table` (`city_id`, `city_name`, `state_id`) VALUES
	(1, 'bul', 1);
/*!40000 ALTER TABLE `city_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.country_master_table
CREATE TABLE IF NOT EXISTS `country_master_table` (
  `country_id` int(5) NOT NULL auto_increment,
  `country_name` varchar(25) NOT NULL,
  PRIMARY KEY  (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.country_master_table: ~2 rows (approximately)
/*!40000 ALTER TABLE `country_master_table` DISABLE KEYS */;
INSERT INTO `country_master_table` (`country_id`, `country_name`) VALUES
	(1, 'india'),
	(2, 'pak');
/*!40000 ALTER TABLE `country_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_child_care_insurance_register_table
CREATE TABLE IF NOT EXISTS `customer_child_care_insurance_register_table` (
  `customer_child_care_inusrance_id` int(5) NOT NULL auto_increment,
  `child_name` varchar(50) NOT NULL default '0',
  `child_age` int(5) NOT NULL default '0',
  `birthcertificate_no` varchar(50) NOT NULL default '0',
  `parents_name` varchar(50) NOT NULL default '0',
  `date_of_insurance` date NOT NULL,
  `payment_type` varchar(50) NOT NULL default '0',
  `customer_id` int(5) NOT NULL default '0',
  `child_care_inusrance_id` int(5) NOT NULL default '0',
  PRIMARY KEY  (`customer_child_care_inusrance_id`),
  KEY `customer_id11` (`customer_id`),
  KEY `child_care_insurance_id` (`child_care_inusrance_id`),
  CONSTRAINT `child_care_insurance_id` FOREIGN KEY (`child_care_inusrance_id`) REFERENCES `child_care_insurance_master_table` (`child_care_insurance_id`),
  CONSTRAINT `customer_id11` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_child_care_insurance_register_table: ~1 rows (approximately)
/*!40000 ALTER TABLE `customer_child_care_insurance_register_table` DISABLE KEYS */;
INSERT INTO `customer_child_care_insurance_register_table` (`customer_child_care_inusrance_id`, `child_name`, `child_age`, `birthcertificate_no`, `parents_name`, `date_of_insurance`, `payment_type`, `customer_id`, `child_care_inusrance_id`) VALUES
	(1, 'nikhil', 2, 'fgd25', 'xcfgd', '2018-06-14', '1', 1, 1);
/*!40000 ALTER TABLE `customer_child_care_insurance_register_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_claim_request_table
CREATE TABLE IF NOT EXISTS `customer_claim_request_table` (
  `claim_id` varchar(50) NOT NULL,
  `register_policy_no` varchar(50) NOT NULL,
  `nominies_name` varchar(50) default NULL,
  `email` varchar(50) NOT NULL,
  `relation` varchar(50) default NULL,
  `reason` varchar(50) NOT NULL,
  `date_of_claim` date NOT NULL,
  `status_id` int(5) NOT NULL,
  PRIMARY KEY  (`claim_id`),
  KEY `FK1_status_id` (`status_id`),
  CONSTRAINT `FK1_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_claim_request_table: ~2 rows (approximately)
/*!40000 ALTER TABLE `customer_claim_request_table` DISABLE KEYS */;
INSERT INTO `customer_claim_request_table` (`claim_id`, `register_policy_no`, `nominies_name`, `email`, `relation`, `reason`, `date_of_claim`, `status_id`) VALUES
	('1', '12345', 'Rohit', 'rohit.ubare@gmail.com', 'Brother', 'fire', '2018-06-21', 9),
	('2', '67890', 'Ayush', 'rohit.ubare@gmail.com', 'Brother', 'flood', '2018-06-21', 9);
/*!40000 ALTER TABLE `customer_claim_request_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_feedback_table
CREATE TABLE IF NOT EXISTS `customer_feedback_table` (
  `feedback_id` int(5) NOT NULL auto_increment,
  `feedback_name` longtext NOT NULL,
  `customer_email_id` varchar(25) NOT NULL,
  `customer_register_id` int(5) NOT NULL default '0',
  PRIMARY KEY  (`feedback_id`),
  KEY `customer_register_id` (`customer_register_id`),
  CONSTRAINT `customer_register_id` FOREIGN KEY (`customer_register_id`) REFERENCES `customer_registration_table` (`costomer_register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_feedback_table: ~1 rows (approximately)
/*!40000 ALTER TABLE `customer_feedback_table` DISABLE KEYS */;
INSERT INTO `customer_feedback_table` (`feedback_id`, `feedback_name`, `customer_email_id`, `customer_register_id`) VALUES
	(2, 'shdfjkg', 'nikhilkumar@gmail.com', 1);
/*!40000 ALTER TABLE `customer_feedback_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_life_insurance_register_table
CREATE TABLE IF NOT EXISTS `customer_life_insurance_register_table` (
  `customer_lifeinsurance_id` int(11) NOT NULL auto_increment,
  `insurance_name` varchar(50) NOT NULL,
  `max_limit` int(11) NOT NULL,
  `date_of_policy` date NOT NULL,
  `nominies_name` varchar(50) NOT NULL,
  `nominies_email_id` varchar(50) NOT NULL,
  `relation` varchar(50) NOT NULL,
  `emi_option_type` int(5) NOT NULL,
  `register_policy_no` varchar(50) NOT NULL,
  `customer_id` int(5) NOT NULL,
  `life_insurance_id` int(5) NOT NULL,
  PRIMARY KEY  (`customer_lifeinsurance_id`),
  KEY `customer_id1` (`customer_id`),
  KEY `life_insurance_id1` (`life_insurance_id`),
  KEY `FK3_emi_option_id` (`emi_option_type`),
  CONSTRAINT `customer_id1` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`),
  CONSTRAINT `FK3_emi_option_id` FOREIGN KEY (`emi_option_type`) REFERENCES `emi_option_table` (`emi_id`),
  CONSTRAINT `life_insurance_id1` FOREIGN KEY (`life_insurance_id`) REFERENCES `life_insurance_master_table` (`life_isn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_life_insurance_register_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_life_insurance_register_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_life_insurance_register_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_registered_car_insurance_policies
CREATE TABLE IF NOT EXISTS `customer_registered_car_insurance_policies` (
  `car_insurance_id` int(11) NOT NULL auto_increment,
  `insurance_policy_no` varchar(50) NOT NULL,
  `car_number` varchar(50) NOT NULL,
  `purchage_date` date NOT NULL,
  `rc_no` varchar(50) NOT NULL,
  `nominies_name` varchar(50) NOT NULL,
  `nominies_mobile_no` bigint(20) NOT NULL,
  `nominies_relation` varchar(50) NOT NULL,
  `max_limit` int(11) NOT NULL,
  `emi_option_type` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `car_insurance_master_id` int(11) NOT NULL,
  PRIMARY KEY  (`car_insurance_id`),
  KEY `car_insurance_master_id` (`car_insurance_master_id`),
  KEY `customer_id` (`customer_id`),
  KEY `FK3_emi_option_type` (`emi_option_type`),
  CONSTRAINT `car_insurance_master_id` FOREIGN KEY (`car_insurance_master_id`) REFERENCES `car_insurance_master_table` (`car_insurance_id`),
  CONSTRAINT `customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`),
  CONSTRAINT `FK3_emi_option_type` FOREIGN KEY (`emi_option_type`) REFERENCES `emi_option_table` (`emi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_registered_car_insurance_policies: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_registered_car_insurance_policies` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_registered_car_insurance_policies` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_register_home_insuranc_table
CREATE TABLE IF NOT EXISTS `customer_register_home_insuranc_table` (
  `customer_homeinsurance_id` int(11) NOT NULL auto_increment,
  `register_policy_no` varchar(50) NOT NULL default '0',
  `home_addres` varchar(50) NOT NULL,
  `square_fit` decimal(10,0) NOT NULL,
  `home_no` varchar(50) NOT NULL,
  `max_duration` int(11) NOT NULL,
  `nominies_name` varchar(50) NOT NULL,
  `nominies_email_id` varchar(50) NOT NULL,
  `relation` varchar(50) NOT NULL,
  `date_of_policies` date NOT NULL,
  `emi_option_type1` int(5) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `home_insurance_id` int(11) NOT NULL,
  PRIMARY KEY  (`customer_homeinsurance_id`),
  KEY `FK1_customer_id` (`customer_id`),
  KEY `FK2_FK2_home_insurance_id` (`home_insurance_id`),
  KEY `FK3_emi_option_type1` (`emi_option_type1`),
  CONSTRAINT `FK1_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`),
  CONSTRAINT `FK2_FK2_home_insurance_id` FOREIGN KEY (`home_insurance_id`) REFERENCES `home_insurance_master_table` (`home_insurance_id`),
  CONSTRAINT `FK3_emi_option_type1` FOREIGN KEY (`emi_option_type1`) REFERENCES `emi_option_table` (`emi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_register_home_insuranc_table: ~2 rows (approximately)
/*!40000 ALTER TABLE `customer_register_home_insuranc_table` DISABLE KEYS */;
INSERT INTO `customer_register_home_insuranc_table` (`customer_homeinsurance_id`, `register_policy_no`, `home_addres`, `square_fit`, `home_no`, `max_duration`, `nominies_name`, `nominies_email_id`, `relation`, `date_of_policies`, `emi_option_type1`, `customer_id`, `home_insurance_id`) VALUES
	(1, '12345', 'hyd', 500, '123', 5, 'Rohit', 'rohit.ubare@gmail.com', 'Brother', '2018-06-21', 4, 3, 1),
	(2, '67890', 'hyd', 700, '678', 5, 'Ayush', 'ayush.shetaki', 'Brother', '2018-06-21', 4, 4, 1);
/*!40000 ALTER TABLE `customer_register_home_insuranc_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.customer_registration_table
CREATE TABLE IF NOT EXISTS `customer_registration_table` (
  `costomer_register_id` int(5) NOT NULL auto_increment,
  `customer_first_name` varchar(25) NOT NULL,
  `customer_last_name` varchar(25) NOT NULL,
  `customer_email_id` varchar(35) NOT NULL,
  `customer_mobile_no` bigint(10) NOT NULL,
  `adhar_no` bigint(20) NOT NULL,
  `customer_gender` varchar(10) NOT NULL,
  `customer_dob` date NOT NULL,
  `address_id` int(5) NOT NULL,
  `agent_id` int(5) default NULL,
  `login_id` int(5) default NULL,
  `status_id` int(11) default NULL,
  PRIMARY KEY  (`costomer_register_id`),
  UNIQUE KEY `customer_email_id` (`customer_email_id`),
  UNIQUE KEY `customer_mobile_no` (`customer_mobile_no`),
  KEY `address_id` (`address_id`),
  KEY `FK2_agent_id` (`agent_id`),
  KEY `login_id` (`login_id`),
  KEY `status_id1` (`status_id`),
  CONSTRAINT `address_id` FOREIGN KEY (`address_id`) REFERENCES `address_table` (`address_id`),
  CONSTRAINT `login_id` FOREIGN KEY (`login_id`) REFERENCES `login_table` (`login_id`),
  CONSTRAINT `status_id1` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.customer_registration_table: ~6 rows (approximately)
/*!40000 ALTER TABLE `customer_registration_table` DISABLE KEYS */;
INSERT INTO `customer_registration_table` (`costomer_register_id`, `customer_first_name`, `customer_last_name`, `customer_email_id`, `customer_mobile_no`, `adhar_no`, `customer_gender`, `customer_dob`, `address_id`, `agent_id`, `login_id`, `status_id`) VALUES
	(1, 'nikhiln', 'kumar', 'nikhilkmr214@gmail.com', 9852141476, 123456789586, 'male', '2019-03-02', 41, 3, 41, 12),
	(2, 'Abhishek', 'Sharma', 'abhi.asharma18@gmail.com', 7020881685, 234134532345, 'male', '1995-07-10', 18, 4, 44, 12),
	(3, 'banti', 'singh', 'singh@gmail.com', 9822212707, 987456321654, 'male', '2018-06-21', 3, 3, NULL, 13),
	(4, 'nikhil', 'kumar', 'vamsee.com@gmail.com', 9822210707, 987654258369, 'male', '2018-06-21', 5, 4, NULL, 13),
	(5, 'niks', 'kumar', 'nid@gmail.com', 5555555552, 55555444444, 'male', '2018-06-28', 6, 6, 44, 13),
	(6, 'srivani', 'ksr', 'srivani.kasturi@gmail.com', 9658569856, 123456789586, 'Female', '2018-06-20', 43, 3, 45, 12);
/*!40000 ALTER TABLE `customer_registration_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.emi_option_table
CREATE TABLE IF NOT EXISTS `emi_option_table` (
  `emi_id` int(5) NOT NULL auto_increment,
  `pay_service_type` varchar(50) NOT NULL default '0',
  PRIMARY KEY  (`emi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.emi_option_table: ~4 rows (approximately)
/*!40000 ALTER TABLE `emi_option_table` DISABLE KEYS */;
INSERT INTO `emi_option_table` (`emi_id`, `pay_service_type`) VALUES
	(1, 'monthly'),
	(2, 'quaterly'),
	(3, 'half yearly'),
	(4, 'annually');
/*!40000 ALTER TABLE `emi_option_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.final_claim_approved_table
CREATE TABLE IF NOT EXISTS `final_claim_approved_table` (
  `claim_id` varchar(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status_id1` int(11) NOT NULL,
  `description_any` varchar(50) default NULL,
  `payAmount` int(100) default NULL,
  UNIQUE KEY `claim_id` (`claim_id`),
  KEY `FK1_customer_id1` (`customer_id`),
  KEY `FK2_status_id111` (`status_id1`),
  CONSTRAINT `FK1_customer_id1` FOREIGN KEY (`customer_id`) REFERENCES `customer_registration_table` (`costomer_register_id`),
  CONSTRAINT `FK2_status_id111` FOREIGN KEY (`status_id1`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.final_claim_approved_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `final_claim_approved_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `final_claim_approved_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.home_insurance_master_table
CREATE TABLE IF NOT EXISTS `home_insurance_master_table` (
  `home_insurance_id` int(5) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '0',
  `max_limit` int(5) NOT NULL default '0',
  `coverage` mediumtext NOT NULL,
  `rate_of_interest` double NOT NULL,
  `home_insurance_offer` mediumtext,
  `initial_amount` double NOT NULL,
  `description` longtext NOT NULL,
  `total_amount_paid` double NOT NULL,
  `policy_no` varchar(50) default '0',
  `status_id` int(11) default '0',
  PRIMARY KEY  (`home_insurance_id`),
  KEY `FK1_status_is_1` (`status_id`),
  CONSTRAINT `FK1_status_is_1` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.home_insurance_master_table: ~3 rows (approximately)
/*!40000 ALTER TABLE `home_insurance_master_table` DISABLE KEYS */;
INSERT INTO `home_insurance_master_table` (`home_insurance_id`, `name`, `max_limit`, `coverage`, `rate_of_interest`, `home_insurance_offer`, `initial_amount`, `description`, `total_amount_paid`, `policy_no`, `status_id`) VALUES
	(1, 'Home suracha', 140, 'home secure offer', 30, 'good', 5000, 'pay some for your home', 600432, '115Nvc', 4),
	(2, 'Home Damage', 20, 'eathQuick', 5, 'all damage cover', 2000, 'Only for EarthQuick', 4500000, 'N111B02', 5),
	(3, 'qwer', 23, 'sddfffgg', 20, 'dfg', 3445, 'edddffsww', 23, '1223344', 5);
/*!40000 ALTER TABLE `home_insurance_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.insurancepolicies_table
CREATE TABLE IF NOT EXISTS `insurancepolicies_table` (
  `policy_id` int(5) NOT NULL auto_increment,
  `policy_name` varchar(25) NOT NULL,
  `customer_id1` int(5) NOT NULL,
  `customer_register_policy_id` int(5) NOT NULL,
  PRIMARY KEY  (`policy_id`),
  KEY `customer_id111` (`customer_id1`),
  CONSTRAINT `customer_id111` FOREIGN KEY (`customer_id1`) REFERENCES `customer_registration_table` (`costomer_register_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.insurancepolicies_table: ~3 rows (approximately)
/*!40000 ALTER TABLE `insurancepolicies_table` DISABLE KEYS */;
INSERT INTO `insurancepolicies_table` (`policy_id`, `policy_name`, `customer_id1`, `customer_register_policy_id`) VALUES
	(1, 'child', 1, 1),
	(2, 'Home', 3, 12345),
	(3, 'Home', 4, 67890);
/*!40000 ALTER TABLE `insurancepolicies_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.life_insurance_master_table
CREATE TABLE IF NOT EXISTS `life_insurance_master_table` (
  `life_isn_id` int(5) NOT NULL auto_increment,
  `insurance_name` varchar(50) NOT NULL default '0',
  `duration` int(11) NOT NULL default '0',
  `insurance_policy_no` varchar(50) NOT NULL default '0',
  `rate_of_interest` double NOT NULL,
  `desciption` longtext NOT NULL,
  `total_amount_paid` double NOT NULL,
  `life_insurance_offers` mediumtext,
  `covarage` text NOT NULL,
  `status_id` int(5) default NULL,
  PRIMARY KEY  (`life_isn_id`),
  KEY `FK3_status_id` (`status_id`),
  CONSTRAINT `FK3_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.life_insurance_master_table: ~6 rows (approximately)
/*!40000 ALTER TABLE `life_insurance_master_table` DISABLE KEYS */;
INSERT INTO `life_insurance_master_table` (`life_isn_id`, `insurance_name`, `duration`, `insurance_policy_no`, `rate_of_interest`, `desciption`, `total_amount_paid`, `life_insurance_offers`, `covarage`, `status_id`) VALUES
	(1, 'life suracha', 10, '11VB4568', 20, 'life surcha kawach', 20000, 'sldfkrf', 'acci', 5),
	(2, 'life care', 20, 'N1111V01', 20, 'life care for life time GGGG', 20000, '', 'deth,MURREDER', 4),
	(3, 'life suracha', 30, '11VB4568', 14, 'family surksha', 400000, '', 'deth', 5),
	(4, 'suracha', 12, '11vnk7', 10, 'good health good life good sure', 50000, 'good offer', 'accident', 5),
	(5, 'asdd', 23, '11VB4568', 32, 'dfffgg', 200000, '', 'deedf', 5),
	(6, 'raksha kawach', 12, '11VNH56', 12, 'GOOD', 10, 'GOOD OFFERS', 'ACCIDENT', 4);
/*!40000 ALTER TABLE `life_insurance_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.login_table
CREATE TABLE IF NOT EXISTS `login_table` (
  `login_id` int(5) NOT NULL auto_increment,
  `user_username` varchar(25) NOT NULL,
  `user_password` varchar(25) NOT NULL,
  `role_id` int(5) NOT NULL,
  PRIMARY KEY  (`login_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `role_table` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.login_table: ~5 rows (approximately)
/*!40000 ALTER TABLE `login_table` DISABLE KEYS */;
INSERT INTO `login_table` (`login_id`, `user_username`, `user_password`, `role_id`) VALUES
	(41, 'niku1476', '12365456', 1),
	(42, 'nini5865', '0', 2),
	(43, 'nihy5469', '0', 2),
	(44, 'badre456', '0', 3),
	(45, 'srks6985', '3963647', 2);
/*!40000 ALTER TABLE `login_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.payment_table
CREATE TABLE IF NOT EXISTS `payment_table` (
  `payment_id` int(5) NOT NULL auto_increment,
  `pay_amount` decimal(10,0) NOT NULL default '0',
  `date_of_emi` date NOT NULL,
  `pay_date` date NOT NULL,
  `status_id` int(5) NOT NULL,
  `policy_id` int(5) NOT NULL default '0',
  PRIMARY KEY  (`payment_id`),
  KEY `FK1_policy_id` (`policy_id`),
  KEY `FK2_status_id` (`status_id`),
  CONSTRAINT `FK1_policy_id` FOREIGN KEY (`policy_id`) REFERENCES `insurancepolicies_table` (`policy_id`),
  CONSTRAINT `FK2_status_id` FOREIGN KEY (`status_id`) REFERENCES `status_table` (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.payment_table: ~5 rows (approximately)
/*!40000 ALTER TABLE `payment_table` DISABLE KEYS */;
INSERT INTO `payment_table` (`payment_id`, `pay_amount`, `date_of_emi`, `pay_date`, `status_id`, `policy_id`) VALUES
	(1, 1000, '2018-06-21', '2018-06-21', 1, 2),
	(2, 1000, '2018-06-21', '2018-06-21', 1, 2),
	(3, 1000, '2018-06-21', '2018-06-21', 1, 2),
	(4, 500, '2018-06-21', '2018-06-21', 1, 3),
	(5, 5, '2018-06-21', '2018-06-21', 1, 3);
/*!40000 ALTER TABLE `payment_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.role_table
CREATE TABLE IF NOT EXISTS `role_table` (
  `role_id` int(5) NOT NULL auto_increment,
  `role_name` varchar(25) NOT NULL,
  PRIMARY KEY  (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.role_table: ~3 rows (approximately)
/*!40000 ALTER TABLE `role_table` DISABLE KEYS */;
INSERT INTO `role_table` (`role_id`, `role_name`) VALUES
	(1, 'amin'),
	(2, 'customer'),
	(3, 'agent');
/*!40000 ALTER TABLE `role_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.state_master_table
CREATE TABLE IF NOT EXISTS `state_master_table` (
  `state_id` int(5) NOT NULL auto_increment,
  `state_name` varchar(25) NOT NULL,
  `country_id` int(5) NOT NULL,
  PRIMARY KEY  (`state_id`),
  KEY `country_id` (`country_id`),
  CONSTRAINT `country_id` FOREIGN KEY (`country_id`) REFERENCES `country_master_table` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.state_master_table: ~1 rows (approximately)
/*!40000 ALTER TABLE `state_master_table` DISABLE KEYS */;
INSERT INTO `state_master_table` (`state_id`, `state_name`, `country_id`) VALUES
	(1, 'up', 1);
/*!40000 ALTER TABLE `state_master_table` ENABLE KEYS */;

-- Dumping structure for table insurancepolicies.status_table
CREATE TABLE IF NOT EXISTS `status_table` (
  `status_id` int(5) NOT NULL auto_increment,
  `status_name` varchar(25) NOT NULL default '0',
  PRIMARY KEY  (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table insurancepolicies.status_table: ~13 rows (approximately)
/*!40000 ALTER TABLE `status_table` DISABLE KEYS */;
INSERT INTO `status_table` (`status_id`, `status_name`) VALUES
	(1, 'APPROVED'),
	(2, 'REJECT'),
	(3, 'PENDING'),
	(4, 'WORKING'),
	(5, 'NOTWORKING'),
	(6, 'LEAVE'),
	(7, 'CLAIMPASS'),
	(8, 'CLAIMREJECTED'),
	(9, 'CLAIMPENDING'),
	(10, 'FEEDBACKUNSEN'),
	(11, 'FEEDBACKSEEN'),
	(12, 'OURCUSTOMER'),
	(13, 'NOTACUSTOMER');
/*!40000 ALTER TABLE `status_table` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
