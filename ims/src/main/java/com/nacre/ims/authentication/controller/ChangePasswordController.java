package com.nacre.ims.authentication.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.authentication.service.AuthenticationService;
import com.nacre.ims.authentication.serviceimpl.AuthenticationServiceImpl;
@WebServlet("/ChangePassword")
public class ChangePasswordController extends HttpServlet {
	/**
	 * @author Nikhilesh 
	 */
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException  {
		AuthenticationService service=null;
		PrintWriter pw=res.getWriter();
		res.setContentType("text/html");
		HttpSession ses=req.getSession();
		
		int count=0;
	    service=new AuthenticationServiceImpl();
	    
        int id=(int) ses.getAttribute("id");
        int loginId=(int) ses.getAttribute("loginId");
        String roleName=(String) ses.getAttribute("rolename");
        String newpswd=req.getParameter("newpassword");
        System.out.println(id+" "+loginId+"  "+roleName+"  "+newpswd);
        
        try {
        	count=service.changePassword(newpswd,roleName,id,loginId);
          	if(count>0) {
          		if(roleName.equals("agent")) {
          			res.sendRedirect("/ims/agent/pages/home.jsp");
          		}else if(roleName.equals("amin")) {
          			res.sendRedirect("/ims/admin/pages/home.jsp");
          		}else if(roleName.equals("customer")) {
          			res.sendRedirect("/ims/customer/pages/home.jsp");
          		}else{
          			res.sendRedirect("");
          		}
          	}
        }catch(Exception e) {
        	
        	e.printStackTrace();
        }
        
        
       
	}
	                 
public void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException, IOException{
    doGet(req, res);

 }


}
