package com.nacre.ims.authentication.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.authentication.service.AuthenticationService;
import com.nacre.ims.authentication.serviceimpl.AuthenticationServiceImpl;
import com.nacre.ims.bo.LoginBO;
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {

	/**
	 * @author Nikhilesh
	 */
	private static final long serialVersionUID = 1L;
	
	 public LoginController() {
	
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter pw=null;
		resp.setContentType("text/html");
		pw=resp.getWriter();
		AuthenticationService service=null;
			
		
		String user=req.getParameter("username");
		String pass=req.getParameter("password");
		
	
		
		LoginBO bo=null;;
		try{
			HttpSession session=req.getSession();
			service=new AuthenticationServiceImpl();
			RequestDispatcher rd=null;
			bo=service.authenticate(user, pass);
			
			String rolename=bo.getRole_name();
			int id=bo.getId();
			int loginid=bo.getLoginid();
			session.setAttribute("id", id);
		
			session.setAttribute("loginId",loginid);
			session.setAttribute("rolename",rolename);
			if(rolename.equalsIgnoreCase("agent")) {
				resp.sendRedirect("/ims/agent/pages/home.jsp");
			}
			else if(rolename.contentEquals("customer")) {
				resp.sendRedirect("/ims/customer/pages/home.jsp");
				
			}else if(rolename.equalsIgnoreCase("amin")) {
				resp.sendRedirect("/ims/admin/pages/home.jsp");
				
			}else {
				session.setAttribute("msg", "Please give proper credential");
				resp.sendRedirect("/ims/commons/pages/Login.jsp");
				
			}
				
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
		
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	doGet(req, resp);
	}

}
