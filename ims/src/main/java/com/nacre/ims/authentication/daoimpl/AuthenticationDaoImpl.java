package com.nacre.ims.authentication.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nacre.ims.admin.dbutils.AdminDbUtils;
import com.nacre.ims.authentication.dao.AuthenticationDao;
import com.nacre.ims.authentication.dbutils.AuthenticationDbUtils;
import com.nacre.ims.bo.LoginBO;
import com.nacre.ims.commonsutil.ConnectionUtil;

public class AuthenticationDaoImpl implements AuthenticationDao {
	Connection con=null;
	LoginBO bo=null;
	
	@Override
	public LoginBO authenticate(String user, String pass) throws SQLException {
		PreparedStatement ps=null;
		int count=0;
		String role_name="";
		int login_id=0;
		int id=0;
		try {
		con=ConnectionUtil.getConnection();
		ps=con.prepareStatement(AuthenticationDbUtils.AUTHENTICATION);
		
		ps.setString(1, user);
		ps.setString(2, pass);
		
	   ResultSet rs=ps.executeQuery();
	   if(rs.next()) {
		   role_name=rs.getString(1);
		   login_id=rs.getInt(2);
		   
	   }
	   if(role_name.equalsIgnoreCase("agent")) {
		   
		   PreparedStatement ps1=con.prepareStatement(AuthenticationDbUtils.AUTHENTICATION_GET_AGENT_ID);
		   ps1.setInt(1, login_id);
		   ResultSet rs1=ps1.executeQuery();
		   while(rs1.next()) {
			   bo=new LoginBO();
			   
			   bo.setId(rs1.getInt(1));
			   bo.setRole_name(role_name);
			   bo.setLoginid(login_id);
			  // bo.setId(login_id);
		   }
		   
	   }else if(role_name.equalsIgnoreCase("customer")) {
		   PreparedStatement ps2=con.prepareStatement(AuthenticationDbUtils.AUTHENTICATION_GET_CUSTOMER_ID);
		   ps2.setInt(1, login_id);
		   ResultSet rs2=ps2.executeQuery();
		   while(rs2.next()) {
			   bo=new LoginBO();
			   bo.setId(rs2.getInt(1));
			   bo.setRole_name(role_name);
			   bo.setLoginid(login_id);
			  // bo.setId(login_id);
		   }
		   
	   }else if(role_name.equalsIgnoreCase("amin")){
		   PreparedStatement ps3=con.prepareStatement(AdminDbUtils.GETTING_LOGIN_ID);
		   ps3.setString(1, user);
		   ps3.setString(2, pass);
		   ResultSet rs2=ps3.executeQuery();
		   while(rs2.next()) {
			   bo=new LoginBO();
			   bo.setLoginid(login_id);
			   bo.setRole_name(role_name);
			  // bo.setId(login_id);
		   }
		   
	   }else {
		   bo=new LoginBO();
		   bo.setId(0);
		   bo.setRole_name("");
		  // bo.setId(0);
	   }
	   
		}catch(Exception e) {
			e.printStackTrace();
		}
		return bo;
		
		
		
	}

	@Override
	public int changePassword(String newPass,String rolename,int costomer_register_id,int loginId) throws SQLException {
		int count=0;
		

		try { 
		con=ConnectionUtil.getConnection();
		if(rolename.equals("customer")) {
			
		PreparedStatement ps=null;
		ps=con.prepareStatement(AuthenticationDbUtils.AUTHENTICATION_CHANGEPASSWORD_FOR_CUSTOMER);
		ps.setString(1, newPass);
		ps.setInt(2, loginId);
	    count=ps.executeUpdate();
		return count;
	}
		else if(rolename.equals("agent")) {
		System.out.println("agent");
			PreparedStatement ps2=null;
			ps2=con.prepareStatement(AuthenticationDbUtils.AUTHENTICATION_CHANGEPASSWORD_FOR_AGENT);
		    ps2.setString(1, newPass);
			ps2.setInt(2, loginId);
			count=ps2.executeUpdate();
			return count;
		}
		else if(rolename.equals("amin")) {
			//System.out.println("agent");
				PreparedStatement ps2=null;
				ps2=con.prepareStatement(AuthenticationDbUtils.CHANGE_PASSWORD_FOR_ADMIN);
			    ps2.setString(1, newPass);
				ps2.setInt(2, loginId);
				count=ps2.executeUpdate();
				return count;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return count;
		
}
	
	

	@Override
	public int forgotPassword1(String newPass, String email) {
          int count=0;
          int k=0;
          int i=0;
		try { 
		con=ConnectionUtil.getConnection();
		PreparedStatement ps3=null;
		ps3=con.prepareStatement("update login_table set login_table.user_password=? where login_table.user_username=?");
		ps3.setString(1, newPass);
		ps3.setString(2, email);
		i=ps3.executeUpdate();
		return i;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return i;
	
	}

	@Override
	public String forgotPassword(String username) {
		con=ConnectionUtil.getConnection();
		try {
			PreparedStatement ps5 =con.prepareStatement("select role_table.role_name from role_table,login_table where login_table.user_username=? and role_table.role_id=login_table.role_id");
			ps5.setString(1, username);
			ResultSet rs5=ps5.executeQuery();
			if(rs5.next()) {
				System.out.println(rs5.getString(1));
				if(rs5.getString(1).equals("agent")) {
					PreparedStatement ps6=con.prepareStatement("select agent_approved_table.agent_email_id from agent_approved_table,login_table,agent_registration_table where login_table.user_username=? and agent_registration_table.login_id=login_table.login_id and agent_approved_table.agent_approved_id=agent_registration_table.agent_approved_id");
					ps6.setString(1, username);
					ResultSet rs6=ps6.executeQuery();
					if(rs6.next()) {
						return rs6.getString(1);
					}
						
					}else if(rs5.getString(1).equalsIgnoreCase("customer")) {
						PreparedStatement ps6=con.prepareStatement("select customer_registration_table.customer_email_id from login_table,customer_registration_table where customer_registration_table=login_table.login_id and login_table.user_username=?");
						ps6.setString(1, username);
						ResultSet rs6=ps6.executeQuery();
						if(rs6.next()) {
							return rs6.getString(1);
						}	
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
