package com.nacre.ims.authentication.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nacre.ims.authentication.service.AuthenticationService;
import com.nacre.ims.authentication.serviceimpl.AuthenticationServiceImpl;
import com.nacre.ims.commonsutil.SendMail;
@WebServlet("/ForgotController")
public class ForgotPasswordController extends HttpServlet {

	/**
	 * @author Nikhilesh
	 */
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Inside ForgetController");
        boolean isSend=false;         
      
		String email=request.getParameter("email");
			AuthenticationService service=new AuthenticationServiceImpl();
			String val=service.ForgotPassword(email);
			
		if(val!=null) {
		  String link="http://localhost:9658/ims/commons/pages/ForgotPassword2.jsp?"+email;
		isSend=SendMail.send("your Forgot link is",val,"nikhilkmr214@gmail.com", "nikhilkmr214@",link);
		}
		if(isSend) {
			response.sendRedirect("/ims/commons/pages/Login.jsp");
		}
		
		
		
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
