package com.nacre.ims.authentication.dao;

import java.sql.SQLException;

import com.nacre.ims.bo.LoginBO;

public interface AuthenticationDao {
	public LoginBO authenticate(String user, String pass) throws SQLException;
	public int changePassword(String newPass,String rolename,int costomer_register_id,int loginId) throws SQLException;
	public int forgotPassword1(String newPass, String email);
	public String forgotPassword(String username);
	
	
}
