package com.nacre.ims.authentication.serviceimpl;

import java.sql.SQLException;

import com.nacre.ims.authentication.dao.AuthenticationDao;
import com.nacre.ims.authentication.daoimpl.AuthenticationDaoImpl;
import com.nacre.ims.authentication.service.AuthenticationService;
import com.nacre.ims.bo.LoginBO;

public class AuthenticationServiceImpl implements AuthenticationService {
	AuthenticationDao dao=new AuthenticationDaoImpl();

	@Override
	public LoginBO authenticate(String user, String pass) throws SQLException {
	LoginBO bo=null;
		
	bo=dao.authenticate(user, pass);
		System.out.println("bo="+bo);
		return bo ;

	}

	@Override
	public int changePassword(String newPass,String rolename,int costomer_register_id,int loginId) throws SQLException {
	     int result=0;
	     result=dao.changePassword(newPass,rolename,costomer_register_id,loginId);
		return result;
	}

	@Override
	public int ForgotPassword1(String newPass, String email) {
		int result=0;
		result=dao.forgotPassword1(newPass,email);
		
		return result;
	}
	@Override
	public String ForgotPassword(String username) {
		String result=null;
		result=dao.forgotPassword(username);
		
		return result;
	}

	

	

}
