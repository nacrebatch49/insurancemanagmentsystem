package com.nacre.ims.authentication.dbutils;

public class AuthenticationDbUtils {
	public static final String AUTHENTICATION="select role_table.role_name,login_table.login_id from role_table,login_table where role_table.role_id=login_table.role_id and login_table.user_username=? and login_table.user_password=?"; 
	public static final String AUTHENTICATION_GET_AGENT_ID="select agent_registration_table.agent_id from agent_registration_table where agent_registration_table.login_id=?";
	public static final String AUTHENTICATION_GET_CUSTOMER_ID="select customer_registration_table.costomer_register_id from customer_registration_table where customer_registration_table.login_id=?"; 
    public static final String AUTHENTICATION_CHANGEPASSWORD_FOR_CUSTOMER="UPDATE login_table SET login_table.user_password=? WHERE login_table.login_id=?";
    public static final String AUTHENTICATION_CHANGEPASSWORD_FOR_AGENT="UPDATE login_table SET login_table.user_password=?  WHERE login_table.login_id=?";
    public static final String AUTHENTICATION_FORGOTPASSWORD_FOR_CUSTOMER="UPDATE login_table,customer_registration_table set login_table.user_password=? where login_table.login_id=customer_registration_table.login_id and customer_registration_table.customer_email_id=?";
    public static final String AUTHENTICATION_FORGOTPASSWORD_FOR_AGENT="update agent_approved_table,agent_registration_table,login_table set login_table.user_password=? where agent_approved_table.agent_approved_id=agent_registration_table.agent_approved_id and agent_registration_table.login_id=login_table.login_id and agent_approved_table.agent_email_id=?";
    public static final String CHANGE_PASSWORD_FOR_ADMIN="update login_table set login_table.user_password=? where login_table.login_id=?";
}
