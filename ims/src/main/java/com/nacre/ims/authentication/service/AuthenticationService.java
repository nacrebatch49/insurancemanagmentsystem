package com.nacre.ims.authentication.service;

import java.sql.SQLException;

import com.nacre.ims.bo.LoginBO;

public interface AuthenticationService {
	public LoginBO authenticate(String user,String pass) throws SQLException;
	public int changePassword(String newPass,String rolename,int costomer_register_id,int loginId) throws SQLException;
	public int ForgotPassword1(String newPass, String email);
	public String ForgotPassword( String email);
	
	
}