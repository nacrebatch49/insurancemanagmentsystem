package com.nacre.ims.authentication.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.authentication.service.AuthenticationService;
import com.nacre.ims.authentication.serviceimpl.AuthenticationServiceImpl;

@WebServlet("/ForgotPasswordController2")
public class ForgotPasswordController2 extends HttpServlet {
	
	AuthenticationService service=null;

	/**
	 * @author Nikhilesh
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession ses=req.getSession();
		String email=ses.getAttribute("email").toString();
		System.out.println(email);
		String newPass=req.getParameter("password");
		
		System.out.println(newPass);
		service=new AuthenticationServiceImpl();
		
		int count=service.ForgotPassword1(newPass,email);
		if(count>0)
		ses.setAttribute("msg", "your password is changed");
		else
			ses.setAttribute("msg", "your password is not changed");	
		res.sendRedirect("/ims/commons/pages/Login.jsp");
		
		
		
		
		
		
		
		
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
	doGet(req, res);
	}


}
