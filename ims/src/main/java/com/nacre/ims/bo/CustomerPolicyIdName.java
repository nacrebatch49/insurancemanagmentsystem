package com.nacre.ims.bo;

public class CustomerPolicyIdName {
	/*
	 * G Venu
	 */
	private String policy_Name;
	private int policy_Id;

	public String getPolicy_Name() {
		return policy_Name;
	} 

	public void setPolicy_Name(String policy_Name) {
		this.policy_Name = policy_Name;
	}

	public int getPolicy_Id() {
		return policy_Id;
	}

	public void setPolicy_Id(int policy_Id) {
		this.policy_Id = policy_Id;
	}

	@Override
	public String toString() {
		return "CustomerPolicyIdName [policy_Name=" + policy_Name + ", policy_Id=" + policy_Id + "]";
	}

}
