package com.nacre.ims.bo;

public class New_Policies_Bo {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "New_Policies_Bo [name=" + name + "]";
	}
	
}
