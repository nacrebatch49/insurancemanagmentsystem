package com.nacre.ims.bo;

public class ViewCustomerTable {
private String fullname;
private String customer_email_id;
private String customer_mobile_no;
private String customer_gender;
private String customer_dob;
private String address;
private String policy_name;
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getCustomer_email_id() {
		return customer_email_id;
	}
	public void setCustomer_email_id(String customer_email_id) {
		this.customer_email_id = customer_email_id;
	}
	public String getCustomer_mobile_no() {
		return customer_mobile_no;
	}
	public void setCustomer_mobile_no(String customer_mobile_no) {
		this.customer_mobile_no = customer_mobile_no;
	}
	public String getCustomer_gender() {
		return customer_gender;
	}
	public void setCustomer_gender(String customer_gender) {
		this.customer_gender = customer_gender;
	}
	public String getCustomer_dob() {
		return customer_dob;
	}
	public void setCustomer_dob(String customer_dob) {
		this.customer_dob = customer_dob;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPolicy_name() {
		return policy_name;
	}
	public void setPolicy_name(String policy_name) {
		this.policy_name = policy_name;
	}
	@Override
	public String toString() {
		return "ViewCustomerTable [fullname=" + fullname + ", customer_email_id=" + customer_email_id
				+ ", customer_mobile_no=" + customer_mobile_no + ", customer_gender=" + customer_gender
				+ ", customer_dob=" + customer_dob + ", address=" + address + ", policy_name=" + policy_name + "]";
	}
	
}
