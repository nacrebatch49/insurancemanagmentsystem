package com.nacre.ims.bo;

import java.io.Serializable;

public class CustomerViewMyPolicyBO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int policy_id;
	private String policy_name;
	private int customer_register_policy_id;

	public int getPolicy_id() {
		return policy_id;
	}

	public void setPolicy_id(int policy_id) {
		this.policy_id = policy_id;
	}

	public String getPolicy_name() {
		return policy_name;
	}

	public void setPolicy_name(String policy_name) {
		this.policy_name = policy_name;
	}

	public int getCustomer_register_policy_id() {
		return customer_register_policy_id;
	}

	public void setCustomer_register_policy_id(int customer_register_policy_id) {
		this.customer_register_policy_id = customer_register_policy_id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "CustomerViewMyPolicyBO [policy_id=" + policy_id + ", policy_name=" + policy_name
				+ ", customer_register_policy_id=" + customer_register_policy_id + "]";
	}
	

}
