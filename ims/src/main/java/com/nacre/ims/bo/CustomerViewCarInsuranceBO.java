package com.nacre.ims.bo;

public class CustomerViewCarInsuranceBO {
	
	private int insurance_policy_no;
	private String car_number;
	private String purchage_date;
	private String rc_no;
	private String nominies_name;
	private long nominies_mobile_no;
	private String nominies_relation;
	private int max_limit;
	private String emi_option_type;
	private int totalAmt;

	public int getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(int totalAmt) {
		this.totalAmt = totalAmt;
	}

	public int getInsurance_policy_no() {
		return insurance_policy_no;
	}

	public void setInsurance_policy_no(int insurance_policy_no) {
		this.insurance_policy_no = insurance_policy_no;
	}

	public String getCar_number() {
		return car_number;
	}

	public void setCar_number(String car_number) {
		this.car_number = car_number;
	}

	public String getPurchage_date() {
		return purchage_date;
	}

	public void setPurchage_date(String purchage_date) {
		this.purchage_date = purchage_date;
	}

	public String getRc_no() {
		return rc_no;
	}

	public void setRc_no(String rc_no) {
		this.rc_no = rc_no;
	}

	public String getNominies_name() {
		return nominies_name;
	}

	public void setNominies_name(String nominies_name) {
		this.nominies_name = nominies_name;
	}

	public long getNominies_mobile_no() {
		return nominies_mobile_no;
	}

	public void setNominies_mobile_no(long nominies_mobile_no) {
		this.nominies_mobile_no = nominies_mobile_no;
	}

	public String getNominies_relation() {
		return nominies_relation;
	}

	public void setNominies_relation(String nominies_relation) {
		this.nominies_relation = nominies_relation;
	}

	public int getMax_limit() {
		return max_limit;
	}

	public void setMax_limit(int max_limit) {
		this.max_limit = max_limit;
	}

	public String getEmi_option_type() {
		return emi_option_type;
	}

	public void setEmi_option_type(String emi_option_type) {
		this.emi_option_type = emi_option_type;
	}
	@Override
	public String toString() {
		return "CustomerViewCarInsuranceBO [insurance_policy_no=" + insurance_policy_no + ", car_number=" + car_number
				+ ", purchage_date=" + purchage_date + ", rc_no=" + rc_no + ", nominies_name=" + nominies_name
				+ ", nominies_mobile_no=" + nominies_mobile_no + ", nominies_relation=" + nominies_relation
				+ ", max_limit=" + max_limit + ", emi_option_type=" + emi_option_type + ", totalAmt=" + totalAmt + "]";
	}


}
