package com.nacre.ims.bo;

import java.sql.Date;

public class ViweallCustomerdetailsBO {
	private String costomer_register_id;
	private String customer_first_name;
	private String customer_last_name;
	private String customer_email_id;
	private long customer_mobile_no;
	private String customer_gender;
	private Date customer_dob;
	private String local_address;
	private int pincode;
	private String city_name ;
	private String state_name;
	private String country_name;
	
	
	
	public String getCostomer_register_id() {
		return costomer_register_id;
	}
	public void setCostomer_register_id(String costomer_register_id) {
		this.costomer_register_id = costomer_register_id;
	}
	public String getCustomer_first_name() {
		return customer_first_name;
	}
	public void setCustomer_first_name(String customer_first_name) {
		this.customer_first_name = customer_first_name;
	}
	public String getCustomer_last_name() {
		return customer_last_name;
	}
	public void setCustomer_last_name(String customer_last_name) {
		this.customer_last_name = customer_last_name;
	}
	public String getCustomer_email_id() {
		return customer_email_id;
	}
	public void setCustomer_email_id(String customer_email_id) {
		this.customer_email_id = customer_email_id;
	}
	public long getCustomer_mobile_no() {
		return customer_mobile_no;
	}
	public void setCustomer_mobile_no(long l) {
		this.customer_mobile_no = l;
	}
	public String getCustomer_gender() {
		return customer_gender;
	}
	public void setCustomer_gender(String customer_gender) {
		this.customer_gender = customer_gender;
	}
	public Date getCustomer_dob() {
		return customer_dob;
	}
	public void setCustomer_dob(Date customer_dob) {
		this.customer_dob = customer_dob;
	}
	public String getLocal_address() {
		return local_address;
	}
	public void setLocal_address(String i) {
		this.local_address = i;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public String getState_name() {
		return state_name;
	}
	public void setState_name(String state_name) {
		this.state_name = state_name;
	}
	public String getCountry_name() {
		return country_name;
	}
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}
	@Override
	public String toString() {
		return "ViweallCustomerdetailsBO [costomer_register_id=" + costomer_register_id + ", customer_first_name="
				+ customer_first_name + ", customer_last_name=" + customer_last_name + ", customer_email_id="
				+ customer_email_id + ", customer_mobile_no=" + customer_mobile_no + ", customer_gender="
				+ customer_gender + ", customer_dob=" + customer_dob + ", local_address=" + local_address + ", pincode="
				+ pincode + ", city_name=" + city_name + ", state_name=" + state_name + ", country_name=" + country_name
				+ "]";
	}
	
}