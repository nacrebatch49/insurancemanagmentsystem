package com.nacre.ims.bo;

public class LoginBO {
	/**
	 * @author Nikhilesh 
	 */
	private int id;
	private int loginid;
	private String role_name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLoginid() {
		return loginid;
	}

	public void setLoginid(int loginid) {
		this.loginid = loginid;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	@Override
	public String toString() {
		return "LoginBO [id=" + id + ", loginid=" + loginid + ", role_name=" + role_name + "]";
	}

}
