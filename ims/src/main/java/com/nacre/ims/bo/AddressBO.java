package com.nacre.ims.bo;

public class AddressBO {
	/**
	 * @author Nikhilesh 
	 */
	
	private String local_address;
	private int pincode;
	private int city_id;
	
	public String getLocal_address() {
		return local_address;
	}
	public void setLocal_address(String local_address) {
		this.local_address = local_address;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public int getCity_id() {
		return city_id;
	}
	public void setCity_id(int city_id) {
		this.city_id = city_id;
	}


}
