package com.nacre.ims.bo;

import java.util.Arrays;
import java.util.Date;

public class CustomerMeetingShceduleBO {
	private String venue;
	private String desc;
	private String date;
	private String time;
	private String[] email;
	
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String[] getEmail() {
		return email;
	}
	public void setEmail(String[] email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "CustomerMeetingShceduleBO [venue=" + venue + ", desc=" + desc + ", date=" + date + ", time=" + time
				+ ", email=" + Arrays.toString(email) + "]";
	}
	
	
	
}
