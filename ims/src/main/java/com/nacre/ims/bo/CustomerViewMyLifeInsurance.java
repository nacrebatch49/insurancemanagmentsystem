package com.nacre.ims.bo;

public class CustomerViewMyLifeInsurance {
	private String insuranceName;
	private int maxLimit;
	private String nomineeName;
	private String nomineeRelation;
	private String emailId;
	private String emiType;
	private String dateOfPolicy;
	private int registerPolicyNumber;
	private int totalAmt;

	public int getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(int totalAmt) {
		this.totalAmt = totalAmt;
	}

	public String getInsuranceName() {
		return insuranceName;
	}

	public void setInsuranceName(String insuranceName) {
		this.insuranceName = insuranceName;
	}

	public int getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(int maxLimit) {
		this.maxLimit = maxLimit;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public String getNomineeRelation() {
		return nomineeRelation;
	}

	public void setNomineeRelation(String nomineeRelation) {
		this.nomineeRelation = nomineeRelation;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getEmiType() {
		return emiType;
	}

	public void setEmiType(String emiType) {
		this.emiType = emiType;
	}

	public String getDateOfPolicy() {
		return dateOfPolicy;
	}

	public void setDateOfPolicy(String dateOfPolicy) {
		this.dateOfPolicy = dateOfPolicy;
	}

	public int getRegisterPolicyNumber() {
		return registerPolicyNumber;
	}

	public void setRegisterPolicyNumber(int registerPolicyNumber) {
		this.registerPolicyNumber = registerPolicyNumber;
	}

	@Override
	public String toString() {
		return "CustomerViewMyLifeInsurance [insuranceName=" + insuranceName + ", maxLimit=" + maxLimit
				+ ", nomineeName=" + nomineeName + ", nomineeRelation=" + nomineeRelation + ", emailId=" + emailId
				+ ", emiType=" + emiType + ", dateOfPolicy=" + dateOfPolicy + ", registerPolicyNumber="
				+ registerPolicyNumber + "]";
	}

}
