package com.nacre.ims.bo;

public class AgentApprovedBO {
	
	/**
	 * @author Nikhilesh
	 */
	
	private String agent_name;
	private String agent_qualification;
	private String agent_email_id;
	private String agent_gender;
	private String location;
	
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	public String getAgent_qualification() {
		return agent_qualification;
	}
	public void setAgent_qualification(String agent_qualification) {
		this.agent_qualification = agent_qualification;
	}
	public String getAgent_email_id() {
		return agent_email_id;
	}
	public void setAgent_email_id(String agent_email_id) {
		this.agent_email_id = agent_email_id;
	}
	public String getAgent_gender() {
		return agent_gender;
	}
	public void setAgent_gender(String agent_gender) {
		this.agent_gender = agent_gender;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	@Override
	public String toString() {
		return "AgentApprovedBO [agent_name=" + agent_name + ", agent_qualification=" + agent_qualification
				+ ", agent_email_id=" + agent_email_id + ", agent_gender=" + agent_gender + ", location=" + location
				+ "]";
	}
	

	
	
}