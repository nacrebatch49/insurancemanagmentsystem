package com.nacre.ims.bo;

public class CustomerViewChildCareInsuranceBO {
	private String child_name;
	private int child_age;
	private String birthcertificate_no;
	private String parents_name;
	private String date_of_insurance;
	private String payment_type;
	private int totalAmt;
	private int registerpolicy;

	public int getRegisterpolicy() {
		return registerpolicy;
	}

	public void setRegisterpolicy(int registerpolicy) {
		this.registerpolicy = registerpolicy;
	}

	public int getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(int totalAmt) {
		this.totalAmt = totalAmt;
	}

	public String getChild_name() {
		return child_name;
	}

	public void setChild_name(String child_name) {
		this.child_name = child_name;
	}

	public int getChild_age() {
		return child_age;
	}

	public void setChild_age(int child_age) {
		this.child_age = child_age;
	}

	public String getBirthcertificate_no() {
		return birthcertificate_no;
	}

	public void setBirthcertificate_no(String birthcertificate_no) {
		this.birthcertificate_no = birthcertificate_no;
	}

	public String getParents_name() {
		return parents_name;
	}

	public void setParents_name(String parents_name) {
		this.parents_name = parents_name;
	}

	public String getDate_of_insurance() {
		return date_of_insurance;
	}

	public void setDate_of_insurance(String date_of_insurance) {
		this.date_of_insurance = date_of_insurance;
	}

	public String getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	@Override
	public String toString() {
		return "CustomerViewChildCareInsuranceBO [child_name=" + child_name + ", child_age=" + child_age
				+ ", birthcertificate_no=" + birthcertificate_no + ", parents_name=" + parents_name
				+ ", date_of_insurance=" + date_of_insurance + ", payment_type=" + payment_type + ", totalAmt="
				+ totalAmt + ", registerpolicy=" + registerpolicy + "]";
	}

}
