package com.nacre.ims.bo;

public class CustomerCarInsuranceRegisterTableBO {

	private String insurancePolicyNo;
	private String carNo;
	private String purchaseDate;
	private String rcNo;
	private String nomineeName;
	private long nomineeMobileNo;
	private String nomineeRelation;
	private int maxLimit;
	private int emiOptionType;
	private int customerId;
	private int carInsuranceMasterId;
	private String register_policy_id;
	private int cust_id;
	

	public String getInsurancePolicyNo() {
		return insurancePolicyNo;
	}

	public void setInsurancePolicyNo(String insurancePolicyNo) {
		this.insurancePolicyNo = insurancePolicyNo;
	}

	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getRcNo() {
		return rcNo;
	}

	public void setRcNo(String rcNo) {
		this.rcNo = rcNo;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public long getNomineeMobileNo() {
		return nomineeMobileNo;
	}

	public void setNomineeMobileNo(long nomineeMobileNo) {
		this.nomineeMobileNo = nomineeMobileNo;
	}

	public String getNomineeRelation() {
		return nomineeRelation;
	}

	public void setNomineeRelation(String nomineeRelation) {
		this.nomineeRelation = nomineeRelation;
	}

	public int getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(int maxLimit) {
		this.maxLimit = maxLimit;
	}

	public int getEmiOptionType() {
		return emiOptionType;
	}

	public void setEmiOptionType(int emiOptionType) {
		this.emiOptionType = emiOptionType;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getCarInsuranceMasterId() {
		return carInsuranceMasterId;
	}

	public void setCarInsuranceMasterId(int carInsuranceMasterId) {
		this.carInsuranceMasterId = carInsuranceMasterId;
	}
	

	public String getRegister_policy_id() {
		return register_policy_id;
	}

	public void setRegister_policy_id(String register_policy_id) {
		this.register_policy_id = register_policy_id;
	}

	public int getCust_id() {
		return cust_id;
	}

	public void setCust_id(int cust_id) {
		this.cust_id = cust_id;
	}

	@Override
	public String toString() {
		return "CustomerCarInsuranceRegisterTableBO [insurancePolicyNo=" + insurancePolicyNo + ", carNo=" + carNo
				+ ", purchaseDate=" + purchaseDate + ", rcNo=" + rcNo + ", nomineeName=" + nomineeName
				+ ", nomineeMobileNo=" + nomineeMobileNo + ", nomineeRelation=" + nomineeRelation + ", maxLimit="
				+ maxLimit + ", emiOptionType=" + emiOptionType + ", customerId=" + customerId
				+ ", carInsuranceMasterId=" + carInsuranceMasterId + ", register_policy_id=" + register_policy_id
				+ ", cust_id=" + cust_id + "]";
	}
	
	

}
