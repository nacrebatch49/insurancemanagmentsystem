package com.nacre.ims.bo;

public class CustomerViewMyHomeInsurance {
	

	private int registerNumber;
	private String homeAdd;
	private float square_feet;
	private String home_no;
	private int maxDur;
	private String nominies_name;
	private String nominies_email_id;
	private String relation;
	private String date_of_policies;
	private String emi_option_type1;
	private int totalAmt;

	public int getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(int totalAmt) {
		this.totalAmt = totalAmt;
	}

	public int getRegisterNumber() {
		return registerNumber;
	}

	public void setRegisterNumber(int registerNumber) {
		this.registerNumber = registerNumber;
	}

	public String getHomeAdd() {
		return homeAdd;
	}

	public void setHomeAdd(String homeAdd) {
		this.homeAdd = homeAdd;
	}

	public float getSquare_feet() {
		return square_feet;
	}

	public void setSquare_feet(float square_feet) {
		this.square_feet = square_feet;
	}

	public String getHome_no() {
		return home_no;
	}

	public void setHome_no(String home_no) {
		this.home_no = home_no;
	}

	public int getMaxDur() {
		return maxDur;
	}

	public void setMaxDur(int maxDur) {
		this.maxDur = maxDur;
	}

	public String getNominies_name() {
		return nominies_name;
	}

	public void setNominies_name(String nominies_name) {
		this.nominies_name = nominies_name;
	}

	public String getNominies_email_id() {
		return nominies_email_id;
	}

	public void setNominies_email_id(String nominies_email_id) {
		this.nominies_email_id = nominies_email_id;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getDate_of_policies() {
		return date_of_policies;
	}

	public void setDate_of_policies(String date_of_policies) {
		this.date_of_policies = date_of_policies;
	}

	public String getEmi_option_type1() {
		return emi_option_type1;
	}

	public void setEmi_option_type1(String emi_option_type1) {
		this.emi_option_type1 = emi_option_type1;
	}
	@Override
	public String toString() {
		return "CustomerViewMyHomeInsurance [registerNumber=" + registerNumber + ", homeAdd=" + homeAdd
				+ ", square_feet=" + square_feet + ", home_no=" + home_no + ", maxDur=" + maxDur + ", nominies_name="
				+ nominies_name + ", nominies_email_id=" + nominies_email_id + ", relation=" + relation
				+ ", date_of_policies=" + date_of_policies + ", emi_option_type1=" + emi_option_type1 + ", totalAmt="
				+ totalAmt + "]";
	}

}
