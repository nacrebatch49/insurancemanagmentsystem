package com.nacre.ims.bo;

import java.sql.Date;

import com.nacre.ims.dto.Address;

public class CustomerRegistrationBO {
	/**
	 * @author Nikhilesh 
	 */
	private String customer_first_name;
	private String customer_last_name;
    private String customer_email_id;
    private long customer_mobile_no;
    private String customer_gender;
    private Date customer_dob;
    private long customer_adhar_no;
    private String Address;
    private int city_Id;
    private int pincode;
    private int agent_id;
    private int status_id;
    private String username;
    private String password;

	  
   public String getCustomer_first_name() {
		return customer_first_name;
	}
	public void setCustomer_first_name(String customer_first_name) {
		this.customer_first_name = customer_first_name;
	}
	public String getCustomer_last_name() {
		return customer_last_name;
	}
	public void setCustomer_last_name(String customer_last_name) {
		this.customer_last_name = customer_last_name;
	}
	public String getCustomer_email_id() {
		return customer_email_id;
	}
	public void setCustomer_email_id(String customer_email_id) {
		this.customer_email_id = customer_email_id;
	}
	public long getCustomer_mobile_no() {
		return customer_mobile_no;
	}
	public void setCustomer_mobile_no(long customer_mobile_no) {
		this.customer_mobile_no = customer_mobile_no;
	}
	public String getCustomer_gender() {
		return customer_gender;
	}
	public void setCustomer_gender(String customer_gender) {
		this.customer_gender = customer_gender;
	}
	public Date getCustomer_dob() {
		return customer_dob;
	}
	public void setCustomer_dob(Date customer_dob) {
		this.customer_dob = customer_dob;
	}
	public long getCustomer_adhar_no() {
		return customer_adhar_no;
	}
	public void setCustomer_adhar_no(long customer_adhar_no) {
		this.customer_adhar_no = customer_adhar_no;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public int getCity_Id() {
		return city_Id;
	}
	public void setCity_Id(int city_Id) {
		this.city_Id = city_Id;
	}
	public int getAgent_id() {
		return agent_id;
	}
	public void setAgent_id(int agent_id) {
		this.agent_id = agent_id;
	}
/*	public int getLogin_id() {
		return login_id;
	}
	public void setLogin_id(int login_id) {
		this.login_id = login_id;
	}*/
	public int getStatus_id() {
		return status_id;
	}
	public void setStatus_id(int status_id) {
		this.status_id = status_id;
	}
	/*public Address getAddress_id() {
		return address_id;
	}
	public void setAddress_id(Address address_id) {
		this.address_id = address_id;
	}*/
	
	public int getPincode() {
		return pincode;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	@Override
	public String toString() {
		return "CustomerRegistrationBO [customer_first_name=" + customer_first_name + ", customer_last_name="
				+ customer_last_name + ", customer_email_id=" + customer_email_id + ", customer_mobile_no="
				+ customer_mobile_no + ", customer_gender=" + customer_gender + ", customer_dob=" + customer_dob
				+ ", customer_adhar_no=" + customer_adhar_no + ", Address=" + Address + ", city_Id=" + city_Id
				+ ", pincode=" + pincode + ", status_id=" + status_id + ", username=" + username + ", password="
				+ password + "]";
	}




}
