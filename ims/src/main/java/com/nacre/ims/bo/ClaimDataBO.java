package com.nacre.ims.bo;

import java.io.Serializable;
/*
  @author Rohit Ubare
*/
public class ClaimDataBO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String pName;
	private String ROI;
	private String totalAmount;
	private String nName;
	private String nEmail;
	private String relation;
	private String claimID;
	private String custID;
	private String statID;
	private String payAmount;
	private String des;

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getROI() {
		return ROI;
	}

	public void setROI(String rOI) {
		ROI = rOI;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getnName() {
		return nName;
	}

	public void setnName(String nName) {
		this.nName = nName;
	}

	public String getnEmail() {
		return nEmail;
	}

	public void setnEmail(String nEmail) {
		this.nEmail = nEmail;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getClaimID() {
		return claimID;
	}

	public void setClaimID(String claimID) {
		this.claimID = claimID;
	}

	public String getCustID() {
		return custID;
	}

	public void setCustID(String custID) {
		this.custID = custID;
	}

	public String getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getStatID() {
		return statID;
	}

	public void setStatID(String statID) {
		this.statID = statID;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ClaimDataBO [pName=" + pName + ", ROI=" + ROI + ", totalAmount=" + totalAmount + ", nName=" + nName
				+ ", nEmail=" + nEmail + ", relation=" + relation + ", claimID=" + claimID + ", custID=" + custID
				+ ", statID=" + statID + ", payAmount=" + payAmount + ", des=" + des + "]";
	}

	

	

}
