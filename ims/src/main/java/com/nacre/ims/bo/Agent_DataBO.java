package com.nacre.ims.bo;

import java.util.Date;

public class Agent_DataBO {
	
	private int agent_Id;
	private String qualification;
	private float percentage;
	private int year_Of_Passout;
	private  Date dob;
	private long mobile_Number;
	private long adhaar_Card_No;
	private long pancard_No;
	private Date date_Of_Joining;
	private int agent_Approved_Id;
	private int address;
	private int login_Id;
	public int getAgent_Id() {
		return agent_Id;
	}
	public void setAgent_Id(int agent_Id) {
		this.agent_Id = agent_Id;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public float getPercentage() {
		return percentage;
	}
	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	public int getYear_Of_Passout() {
		return year_Of_Passout;
	}
	public void setYear_Of_Passout(int year_Of_Passout) {
		this.year_Of_Passout = year_Of_Passout;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public long getMobile_Number() {
		return mobile_Number;
	}
	public void setMobile_Number(long mobile_Number) {
		this.mobile_Number = mobile_Number;
	}
	public long getAdhaar_Card_No() {
		return adhaar_Card_No;
	}
	public void setAdhaar_Card_No(long adhaar_Card_No) {
		this.adhaar_Card_No = adhaar_Card_No;
	}
	public long getPancard_No() {
		return pancard_No;
	}
	public void setPancard_No(long pancard_No) {
		this.pancard_No = pancard_No;
	}
	public Date getDate_Of_Joining() {
		return date_Of_Joining;
	}
	public void setDate_Of_Joining(Date date_Of_Joining) {
		this.date_Of_Joining = date_Of_Joining;
	}
	public int getAgent_Approved_Id() {
		return agent_Approved_Id;
	}
	public void setAgent_Approved_Id(int agent_Approved_Id) {
		this.agent_Approved_Id = agent_Approved_Id;
	}
	public int getAddress() {
		return address;
	}
	public void setAddress(int address) {
		this.address = address;
	}
	public int getLogin_Id() {
		return login_Id;
	}
	public void setLogin_Id(int login_Id) {
		this.login_Id = login_Id;
	}
	
	
	@Override
	public String toString() {
		return "Customer_DataBO [agent_Id=" + agent_Id + ", qualification=" + qualification + ", percentage="
				+ percentage + ", year_Of_Passout=" + year_Of_Passout + ", dob=" + dob + ", mobile_Number="
				+ mobile_Number + ", adhaar_Card_No=" + adhaar_Card_No + ", pancard_No=" + pancard_No
				+ ", date_Of_Joining=" + date_Of_Joining + ", agent_Approved_Id=" + agent_Approved_Id + ", address="
				+ address + ", login_Id=" + login_Id + "]";
	}
	
	
	

}
