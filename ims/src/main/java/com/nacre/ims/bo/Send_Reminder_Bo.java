package com.nacre.ims.bo;

public class Send_Reminder_Bo {

	private String name;
	private Long mobileNo;
	private String emailId;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	@Override
	public String toString() {
		return "Send_Reminder_Bo [name=" + name + ", mobileNo=" + mobileNo + ", emailId=" + emailId + "]";
	}
	
}
