package com.nacre.ims.bo;

import java.sql.Date;

public class CustomerLifeRegisterBo {
	@Override
	public String toString() {
		return "CustomerLifeRegisterBo [insuranceName=" + insuranceName + ", maxlimit=" + maxlimit + ", nomineeName="
				+ nomineeName + ", nomineeEmail=" + nomineeEmail + ", nomineeRelation=" + nomineeRelation
				+ ", paymenttype=" + paymenttype + ", register_policy_id=" + register_policy_id + ", date_of_policy="
				+ date_of_policy + ", cust_id=" + cust_id + ", LifeInsuranceid=" + LifeInsuranceid + "]";
	}
	private String insuranceName;
	private int maxlimit;
	private String nomineeName;
	private String nomineeEmail;
	private String nomineeRelation;
	private int paymenttype;
	private String register_policy_id;
	private Date date_of_policy;
	private int cust_id;
    private int LifeInsuranceid;
	public String getInsuranceName() {
		return insuranceName;
	}
	public void setInsuranceName(String insuranceName) {
		this.insuranceName = insuranceName;
	}
	public int getMaxlimit() {
		return maxlimit;
	}
	public void setMaxlimit(int maxlimit) {
		this.maxlimit = maxlimit;
	}
	public String getNomineeName() {
		return nomineeName;
	}
	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}
	public String getNomineeEmail() {
		return nomineeEmail;
	}
	public void setNomineeEmail(String nomineeEmail) {
		this.nomineeEmail = nomineeEmail;
	}
	public String getNomineeRelation() {
		return nomineeRelation;
	}
	public void setNomineeRelation(String nomineeRelation) {
		this.nomineeRelation = nomineeRelation;
	}
	public int getPaymenttype() {
		return paymenttype;
	}
	public void setPaymenttype(int paymenttype) {
		this.paymenttype = paymenttype;
	}
	public String getRegister_policy_id() {
		return register_policy_id;
	}
	public void setRegister_policy_id(String register_policy_id) {
		this.register_policy_id = register_policy_id;
	}
	public Date getDate_of_policy() {
		return date_of_policy;
	}
	public void setDate_of_policy(Date date_of_policy) {
		this.date_of_policy = date_of_policy;
	}
	public int getCust_id() {
		return cust_id;
	}
	public void setCust_id(int cust_id) {
		this.cust_id = cust_id;
	}
	public int getLifeInsuranceid() {
		return LifeInsuranceid;
	}
	public void setLifeInsuranceid(int lifeInsuranceid) {
		LifeInsuranceid = lifeInsuranceid;
	}
}
