package com.nacre.ims.bo;

public class Send_Rem_Dely_Pay_Bo {

	private String name;
	private Long mobileNo;
	private String emailId;
	private Double pay_amount;
	public Double getPay_amount() {
		return pay_amount;
	}
	public void setPay_amount(Double pay_amount) {
		this.pay_amount = pay_amount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(Long mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	@Override
	public String toString() {
		return "Send_Rem_Dely_Pay_Bo [name=" + name + ", mobileNo=" + mobileNo + ", emailId=" + emailId
				+ ", pay_amount=" + pay_amount + "]";
	}
	
	

}
