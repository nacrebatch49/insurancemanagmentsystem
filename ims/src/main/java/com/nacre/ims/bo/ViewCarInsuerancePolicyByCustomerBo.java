package com.nacre.ims.bo;

public class ViewCarInsuerancePolicyByCustomerBo {
	
	private int carIncId;
	public int getCarIncId() {
		return carIncId;
	}
	public void setCarIncId(int carIncId) {
		this.carIncId = carIncId;
	}
	private String policyName;
	private String policyDescription;
	private String coverage;
	private int duration;
	private double initialAmount;
	private double paybleAmount;
	private double rateofInterest;
	
	public String getPolicyName() {
		return policyName;
	}
	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}
	public String getPolicyDescription() {
		return policyDescription;
	}
	public void setPolicyDescription(String policyDescription) {
		this.policyDescription = policyDescription;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public double getInitialAmount() {
		return initialAmount;
	}
	public void setInitialAmount(double initialAmount) {
		this.initialAmount = initialAmount;
	}
	public double getPaybleAmount() {
		return paybleAmount;
	}
	public void setPaybleAmount(double paybleAmount) {
		this.paybleAmount = paybleAmount;
	}
	public double getRateofInterest() {
		return rateofInterest;
	}
	public void setRateofInterest(double rateofInterest) {
		this.rateofInterest = rateofInterest;
	}

}
