package com.nacre.ims.bo;

/**
 * @author Ajit
 *
 */
public class EditHomeInsuranceBO {

	private String policyName;
	private String coverage;
	private String description;
	private int duration;
	private double initialAmount;
	private double totalamount;
	private double rateOfInterest;
	private String policyOffer;
	private String policyNumber;
	
	public String getPolicyName() {
		return policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	public String getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public double getInitialAmount() {
		return initialAmount;
	}

	public void setInitialAmount(double initialAmount) {
		this.initialAmount = initialAmount;
	}

	public double getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}

	public double getRateOfInterest() {
		return rateOfInterest;
	}

	public void setRateOfInterest(double rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public String getPolicyOffer() {
		return policyOffer;
	}

	public void setPolicyOffer(String policyOffer) {
		this.policyOffer = policyOffer;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	@Override
	public String toString() {
		return "EditHomeInsuranceBO [policyName=" + policyName + ", coverage=" + coverage + ", description="
				+ description + ", duration=" + duration + ", initialAmount=" + initialAmount + ", totalamount="
				+ totalamount + ", rateOfInterest=" + rateOfInterest + ", policyOffer=" + policyOffer
				+ ", policyNumber=" + policyNumber + "]";
	}

	



}
