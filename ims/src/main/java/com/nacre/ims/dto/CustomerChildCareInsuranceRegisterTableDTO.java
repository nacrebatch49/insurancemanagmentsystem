package com.nacre.ims.dto;

public class CustomerChildCareInsuranceRegisterTableDTO {

	private String child_Name;
	private int child_Age;
	private String birthCertificate_No;
	private String parents_Name;
	private String date_of_Insurance;
	private String payment_Type;
	private int childCareInusranceId;

	public String getChild_Name() {
		return child_Name;
	}

	public void setChild_Name(String child_Name) {
		this.child_Name = child_Name;
	}

	public int getChild_Age() {
		return child_Age;
	}

	public void setChild_Age(int child_Age) {
		this.child_Age = child_Age;
	}

	public String getBirthCertificate_No() {
		return birthCertificate_No;
	}

	public void setBirthCertificate_No(String birthCertificate_No) {
		this.birthCertificate_No = birthCertificate_No;
	}

	public String getParents_Name() {
		return parents_Name;
	}

	public void setParents_Name(String parents_Name) {
		this.parents_Name = parents_Name;
	}

	public String getDate_of_Insurance() {
		return date_of_Insurance;
	}

	public void setDate_of_Insurance(String date_of_Insurance) {
		this.date_of_Insurance = date_of_Insurance;
	}

	public String getPayment_Type() {
		return payment_Type;
	}

	public void setPayment_Type(String payment_Type) {
		this.payment_Type = payment_Type;
	}
    
	
	public int getChildCareInusranceId() {
		return childCareInusranceId;
	}

	public void setChildCareInusranceId(int childCareInusranceId) {
		this.childCareInusranceId = childCareInusranceId;
	}

	@Override
	public String toString() {
		return "CustomerChildCareInsuranceRegisterTableDTO [child_Name=" + child_Name + ", child_Age=" + child_Age
				+ ", birthCertificate_No=" + birthCertificate_No + ", parents_Name=" + parents_Name
				+ ", date_of_Insurance=" + date_of_Insurance + ", payment_Type=" + payment_Type
				+ ", childCareInusranceId=" + childCareInusranceId + "]";
	}

	
	

}
