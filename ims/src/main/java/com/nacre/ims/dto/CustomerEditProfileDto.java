package com.nacre.ims.dto;

import java.io.Serializable;
import java.util.Date;

public class CustomerEditProfileDto implements Serializable {

	private String customer_first_name;
	private String customer_last_name;
	private long customer_mobile_no;
	private int city_id;
	private String local_address;
	private int pincode;
	private int costomer_register_id;


	

	public String getCustomer_first_name() {
		return customer_first_name;
	}

	public void setCustomer_first_name(String customer_first_name) {
		this.customer_first_name = customer_first_name;
	}

	public String getCustomer_last_name() {
		return customer_last_name;
	}

	public void setCustomer_last_name(String customer_last_name) {
		this.customer_last_name = customer_last_name;
	}

	public long getCustomer_mobile_no() {
		return customer_mobile_no;
	}

	public void setCustomer_mobile_no(long customer_mobile_no) {
		this.customer_mobile_no = customer_mobile_no;
	}

	public int getCity_id() {
		return city_id;
	}

	public void setCity_id(int city_id) {
		this.city_id = city_id;
	}

	public String getLocal_address() {
		return local_address;
	}

	public void setLocal_address(String local_address) {
		this.local_address = local_address;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public int getCostomer_register_id() {
		return costomer_register_id;
	}

	public void setCostomer_register_id(int costomer_register_id) {
		this.costomer_register_id = costomer_register_id;
	}

	@Override
	public String toString() {
		return "CustomerEditProfileDto [customer_first_name=" + customer_first_name + ", customer_last_name="
				+ customer_last_name + ", customer_mobile_no=" + customer_mobile_no + ", city_id=" + city_id
				+ ", local_address=" + local_address + ", pincode=" + pincode + ",costomer_register_id="+costomer_register_id+"]";
	}

}
