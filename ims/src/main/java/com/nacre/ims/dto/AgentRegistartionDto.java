package com.nacre.ims.dto;

import java.io.Serializable;
import java.util.Date;

public class AgentRegistartionDto implements Serializable {
	private String agent_name;
	
	private float percentage;
	private int year_of_passout;
	private String dob;
	private long mobile_number;
	private String gender;
	private long adhaar_card_no;
	private long pancard_no;
	private String date_of_joining;
	private String mailid;
	
	public String getMailid() {
		return mailid;
	}
	public void setMailid(String mailid) {
		this.mailid = mailid;
	}

	private String agent_approved_id;
	
	private String local_address;
	private int pincode;
	

	private  int city_id;
	private String user_username;
	private String user_password;
	
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	public float getPercentage() {
		return percentage;
	}
	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	public int getYear_of_passout() {
		return year_of_passout;
	}
	public void setYear_of_passout(int year_of_passout) {
		this.year_of_passout = year_of_passout;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public long getMobile_number() {
		return mobile_number;
	}
	public void setMobile_number(long mobile_number) {
		this.mobile_number = mobile_number;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getAdhaar_card_no() {
		return adhaar_card_no;
	}
	public void setAdhaar_card_no(long adhaar_card_no) {
		this.adhaar_card_no = adhaar_card_no;
	}
	public long getPancard_no() {
		return pancard_no;
	}
	public void setPancard_no(long pancard_no) {
		this.pancard_no = pancard_no;
	}
	public String getDate_of_joining() {
		return date_of_joining;
	}
	public void setDate_of_joining(String date_of_joining) {
		this.date_of_joining = date_of_joining;
	}
	
	public String getAgent_approved_id() {
		return agent_approved_id;
	}
	public void setAgent_approved_id(String agent_approved_id) {
		this.agent_approved_id = agent_approved_id;
	}
	
	public String getLocal_address() {
		return local_address;
	}
	public void setLocal_address(String local_address) {
		this.local_address = local_address;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public int getCity_id() {
		return city_id;
	}
	public void setCity_id(int city_id) {
		this.city_id = city_id;
	}
	public String getUser_username() {
		return user_username;
	}
	public void setUser_username(String user_username) {
		this.user_username = user_username;
	}
	public String getUser_password() {
		return user_password;
	}
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	
	@Override
	public String toString() {
		return "AgentRegistartionDto [percentage=" + percentage + ", year_of_passout=" + year_of_passout + ", dob="
				+ dob + ", mobile_number=" + mobile_number + ", gender=" + gender + ", adhaar_card_no=" + adhaar_card_no
				+ ", pancard_no=" + pancard_no + ", date_of_joining=" + date_of_joining + ", agent_approved_id=" + agent_approved_id + ", local_address="
				+ local_address + ", city_id=" + city_id + ", user_username=" + user_username + ", user_password="
				+ user_password + "]";
	}

	
	
}
