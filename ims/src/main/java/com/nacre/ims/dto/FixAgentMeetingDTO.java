package com.nacre.ims.dto;

import java.sql.Time;
import java.util.Arrays;
import java.sql.Date;

public class FixAgentMeetingDTO {
	private int[] agentId;
	private String venue;
	private Date date;
	private Time time;
	public int[] getAgentId() {
		return agentId;
	}
	public void setAgentId(int[] agentId) {
		this.agentId = agentId;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Time getTime() {
		return time;
	}
	public void setTime(Time time) {
		this.time = time;
	}
	
	@Override
	public String toString() {
		return "FixAgentMeetingDTO [agentId=" + Arrays.toString(agentId) + ", venue=" + venue + ", date=" + date
				+ ", time=" + time + "]";
	}
	
	
	
}
