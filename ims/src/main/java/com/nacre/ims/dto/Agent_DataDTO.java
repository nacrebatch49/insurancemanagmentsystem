package com.nacre.ims.dto;

import java.util.Date;

/**
 * 
 * @author soma shelke
 *
 */

public class Agent_DataDTO {

	private int agent_Id;
	private String agent_name;
	private String agent_qualification;
	private String gender;
	
	private String agent_emai_id;
	private long mobile_Number;
	private long adhaar_Card_No;
	private long pancard_No;
	
	private Date date_Of_Joining;
	private String location;
	private String local_address;
	private long pincode;
	
	private String city_name;
	private String state_name;
	private String country_name;
	private int count_No_Of_Customer;
	public int getAgent_Id() {
		return agent_Id;
	}
	public void setAgent_Id(int agent_Id) {
		this.agent_Id = agent_Id;
	}
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	public String getAgent_qualification() {
		return agent_qualification;
	}
	public void setAgent_qualification(String agent_qualification) {
		this.agent_qualification = agent_qualification;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getAgent_emai_id() {
		return agent_emai_id;
	}
	public void setAgent_emai_id(String agent_emai_id) {
		this.agent_emai_id = agent_emai_id;
	}
	public long getMobile_Number() {
		return mobile_Number;
	}
	public void setMobile_Number(long mobile_Number) {
		this.mobile_Number = mobile_Number;
	}
	public long getAdhaar_Card_No() {
		return adhaar_Card_No;
	}
	public void setAdhaar_Card_No(long adhaar_Card_No) {
		this.adhaar_Card_No = adhaar_Card_No;
	}
	public long getPancard_No() {
		return pancard_No;
	}
	public void setPancard_No(long pancard_No) {
		this.pancard_No = pancard_No;
	}
	public Date getDate_Of_Joining() {
		return date_Of_Joining;
	}
	public void setDate_Of_Joining(Date date_Of_Joining) {
		this.date_Of_Joining = date_Of_Joining;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLocal_address() {
		return local_address;
	}
	public void setLocal_address(String local_address) {
		this.local_address = local_address;
	}
	public long getPincode() {
		return pincode;
	}
	public void setPincode(long pincode) {
		this.pincode = pincode;
	}
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public String getState_name() {
		return state_name;
	}
	public void setState_name(String state_name) {
		this.state_name = state_name;
	}
	public String getCountry_name() {
		return country_name;
	}
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}
	public int getCount_No_Of_Customer() {
		return count_No_Of_Customer;
	}
	public void setCount_No_Of_Customer(int count_No_Of_Customer) {
		this.count_No_Of_Customer = count_No_Of_Customer;
	}
	
	@Override
	public String toString() {
		return "Agent_DataDTO [agent_Id=" + agent_Id + ", agent_name=" + agent_name + ", agent_qualification="
				+ agent_qualification + ", gender=" + gender + ", agent_emai_id=" + agent_emai_id + ", mobile_Number="
				+ mobile_Number + ", adhaar_Card_No=" + adhaar_Card_No + ", pancard_No=" + pancard_No
				+ ", date_Of_Joining=" + date_Of_Joining + ", location=" + location + ", local_address=" + local_address
				+ ", pincode=" + pincode + ", city_name=" + city_name + ", state_name=" + state_name + ", country_name="
				+ country_name + ", count_No_Of_Customer=" + count_No_Of_Customer + "]";
	}
}
