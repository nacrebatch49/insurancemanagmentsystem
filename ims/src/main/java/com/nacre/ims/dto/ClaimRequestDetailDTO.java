package com.nacre.ims.dto;

import java.io.Serializable;
/*
  @author Rohit Ubare
*/
public class ClaimRequestDetailDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String fName;
	private String lName;
	private String policyNo;
	private String nominiName;
	private String nominiEmail;
	private String relation;

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getNominiName() {
		return nominiName;
	}

	public void setNominiName(String nominiName) {
		this.nominiName = nominiName;
	}

	public String getNominiEmail() {
		return nominiEmail;
	}

	public void setNominiEmail(String nominiEmail) {
		this.nominiEmail = nominiEmail;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}
	
	

	@Override
	public String toString() {
		return "ClaimRequestDetailDTO [fName=" + fName + ", lName=" + lName + ", policyNo=" + policyNo + ", nominiName="
				+ nominiName + ", nominiEmail=" + nominiEmail + ", relation=" + relation + "]";
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
