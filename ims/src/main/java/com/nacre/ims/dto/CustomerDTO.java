package com.nacre.ims.dto;

import java.sql.Date;

public class CustomerDTO {
	private String costomer_register_id;
	private String customer_first_name;
	private String customer_last_name;
	private String customer_email_id;
	private int customer_mobile_no;
	private String customer_gender;
	private Date customer_dob;
	private String address_id;
	private int agent_id;
	private int login_id;
	public String getCostomer_register_id() {
		return costomer_register_id;
	}
	public void setCostomer_register_id(String costomer_register_id) {
		this.costomer_register_id = costomer_register_id;
	}
	public String getCustomer_first_name() {
		return customer_first_name;
	}
	public void setCustomer_first_name(String customer_first_name) {
		this.customer_first_name = customer_first_name;
	}
	public String getCustomer_last_name() {
		return customer_last_name;
	}
	public void setCustomer_last_name(String customer_last_name) {
		this.customer_last_name = customer_last_name;
	}
	public String getCustomer_email_id() {
		return customer_email_id;
	}
	public void setCustomer_email_id(String customer_email_id) {
		this.customer_email_id = customer_email_id;
	}
	public int getCustomer_mobile_no() {
		return customer_mobile_no;
	}
	public void setCustomer_mobile_no(int customer_mobile_no) {
		this.customer_mobile_no = customer_mobile_no;
	}
	public String getCustomer_gender() {
		return customer_gender;
	}
	public void setCustomer_gender(String customer_gender) {
		this.customer_gender = customer_gender;
	}
	public Date getCustomer_dob() {
		return customer_dob;
	}
	public void setCustomer_dob(Date customer_dob) {
		this.customer_dob = customer_dob;
	}
	public String getAddress_id() {
		return address_id;
	}
	public void setAddress_id(String address_id) {
		this.address_id = address_id;
	}
	public int getAgent_id() {
		return agent_id;
	}
	public void setAgent_id(int agent_id) {
		this.agent_id = agent_id;
	}
	public int getLogin_id() {
		return login_id;
	}
	public void setLogin_id(int login_id) {
		this.login_id = login_id;
	}
	@Override
	public String toString() {
		return "CustomerDTO [costomer_register_id=" + costomer_register_id + ", customer_first_name="
				+ customer_first_name + ", customer_last_name=" + customer_last_name + ", customer_email_id="
				+ customer_email_id + ", customer_mobile_no=" + customer_mobile_no + ", customer_gender="
				+ customer_gender + ", customer_dob=" + customer_dob + ", address_id=" + address_id + ", agent_id="
				+ agent_id + ", login_id=" + login_id + "]";
	}
	
}