package com.nacre.ims.dto;

import java.io.Serializable;
/*
  @author Rohit Ubare
*/
public class ClaimDataDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String pNo;
	private String nName;
	private String nEmail;
	private String relation;
	private String pName;
	private String totalAmount;
	private String ROI;
	private String GST;
	private String payAmount;
	private String statusId;
	private String des;
	private String claimID;
	private String custID;

	public String getpNo() {
		return pNo;
	}

	public void setpNo(String pNo) {
		this.pNo = pNo;
	}

	public String getnName() {
		return nName;
	}

	public void setnName(String nName) {
		this.nName = nName;
	}

	public String getnEmail() {
		return nEmail;
	}

	public void setnEmail(String nEmail) {
		this.nEmail = nEmail;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getROI() {
		return ROI;
	}

	public void setROI(String rOI) {
		ROI = rOI;
	}

	public String getGST() {
		return GST;
	}

	public void setGST(String gST) {
		GST = gST;
	}

	public String getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getClaimID() {
		return claimID;
	}

	public void setClaimID(String claimID) {
		this.claimID = claimID;
	}

	public String getCustID() {
		return custID;
	}

	public void setCustID(String custID) {
		this.custID = custID;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ClaimDataDTO [pNo=" + pNo + ", nName=" + nName + ", nEmail=" + nEmail + ", relation=" + relation
				+ ", pName=" + pName + ", totalAmount=" + totalAmount + ", ROI=" + ROI + ", GST=" + GST + ", payAmount="
				+ payAmount + ", statusId=" + statusId + ", des=" + des + ", claimID=" + claimID + ", custID=" + custID
				+ "]";
	}

	
	
	
}
