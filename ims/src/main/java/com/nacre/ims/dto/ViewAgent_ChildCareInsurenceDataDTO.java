/**
 * 
 */
package com.nacre.ims.dto;

/**
 * @author vishal patil
 *
 */
public class ViewAgent_ChildCareInsurenceDataDTO {
	private String name;
	private int max_limit;
	private String coverage;
	private String desciption;
	private double rate_of_interest;
	private double payable_amount;
	private double initial_amount;
	private String life_insurance_offers;
	private String status;
	private int childid;
	
	public int getChildid() {
		return childid;
	}
	public void setChildid(int childid) {
		this.childid = childid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMax_limit() {
		return max_limit;
	}
	public void setMax_limit(int max_limit) {
		this.max_limit = max_limit;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getDesciption() {
		return desciption;
	}
	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}
	public double getRate_of_interest() {
		return rate_of_interest;
	}
	public void setRate_of_interest(double rate_of_interest) {
		this.rate_of_interest = rate_of_interest;
	}
	public double getPayable_amount() {
		return payable_amount;
	}
	public void setPayable_amount(double payable_amount) {
		this.payable_amount = payable_amount;
	}
	public double getInitial_amount() {
		return initial_amount;
	}
	public void setInitial_amount(double initial_amount) {
		this.initial_amount = initial_amount;
	}
	public String getLife_insurance_offers() {
		return life_insurance_offers;
	}
	public void setLife_insurance_offers(String life_insurance_offers) {
		this.life_insurance_offers = life_insurance_offers;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "ViewAgent_ChildCareInsurenceDataDTO [name=" + name + ", max_limit=" + max_limit + ", coverage=" + coverage
				+ ", desciption=" + desciption + ", rate_of_interest=" + rate_of_interest + ", payable_amount="
				+ payable_amount + ", initial_amount=" + initial_amount + ", life_insurance_offers="
				+ life_insurance_offers + ", status=" + status + "]";
	}
	
}
