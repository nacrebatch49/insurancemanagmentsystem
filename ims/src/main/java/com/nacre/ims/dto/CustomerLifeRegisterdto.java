package com.nacre.ims.dto;

public class CustomerLifeRegisterdto {
	private String insuranceName;
	private int maxlimit;
	private String nomineeName;
	private String nomineeEmail;
	private String nomineeRelation;
	private int paymenttype;
	private int cust_id;

	private int LifeInsuranceid;

	public int getLifeInsuranceid() {
		return LifeInsuranceid;
	}

	public void setLifeInsuranceid(int lifeInsuranceid) {
		LifeInsuranceid = lifeInsuranceid;
	}

	public String getInsuranceName() {
		return insuranceName;
	}

	public void setInsuranceName(String insuranceName) {
		this.insuranceName = insuranceName;
	}

	public int getMaxlimit() {
		return maxlimit;
	}

	public void setMaxlimit(int maxlimit) {
		this.maxlimit = maxlimit;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public String getNomineeEmail() {
		return nomineeEmail;
	}

	public void setNomineeEmail(String nomineeEmail) {
		this.nomineeEmail = nomineeEmail;
	}

	public String getNomineeRelation() {
		return nomineeRelation;
	}

	public void setNomineeRelation(String nomineeRelation) {
		this.nomineeRelation = nomineeRelation;
	}

	public int getPaymenttype() {
		return paymenttype;
	}

	public void setPaymenttype(int paymenttype) {
		this.paymenttype = paymenttype;
	}

	public int getCust_id() {
		return cust_id;
	}

	public void setCust_id(int cust_id) {
		this.cust_id = cust_id;
	}

}
