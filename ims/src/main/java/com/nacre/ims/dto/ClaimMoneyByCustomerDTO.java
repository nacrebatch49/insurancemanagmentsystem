package com.nacre.ims.dto;

import java.io.Serializable;
import java.sql.Date;

public class ClaimMoneyByCustomerDTO implements Serializable {
	
	private String policId;
	private String nomineeName;
	private String email;
	private String relation;
	private String reason;
	private  Date dateOfClaim;
	public String getPolicId() {
		return policId;
	}
	public void setPolicId(String policId) {
		this.policId = policId;
	}
	public String getNomineeName() {
		return nomineeName;
	}
	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getDateOfClaim() {
		return dateOfClaim;
	}
	public void setDateOfClaim(Date dateOfClaim) {
		this.dateOfClaim = dateOfClaim;
	}
	@Override
	public String toString() {
		return "ClaimMoneyByCustomerDTO [policId=" + policId + ", nomineeName=" + nomineeName + ", email=" + email
				+ ", relation=" + relation + ", reason=" + reason + ", dateOfClaim=" + dateOfClaim + "]";
	}
}
