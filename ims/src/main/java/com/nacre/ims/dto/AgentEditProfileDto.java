package com.nacre.ims.dto;



import java.io.Serializable;
import java.util.Date;

public class AgentEditProfileDto implements Serializable {
	private String agent_name;
	private long mobile_number;
	private String qualification;
	private int year_of_passout;
	private float percentage;
	
	private int city_id;
	private String local_address;
	private int pincode;
	private int agent_id;

	

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public long getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(long mobile_number) {
		this.mobile_number = mobile_number;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public int getYear_of_passout() {
		return year_of_passout;
	}

	public void setYear_of_passout(int year_of_passout) {
		this.year_of_passout = year_of_passout;
	}

	public float getPercentage() {
		return percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}

	
	public int getCity_id() {
		return city_id;
	}

	public void setCity_id(int city_id) {
		this.city_id = city_id;
	}

	public String getLocal_address() {
		return local_address;
	}

	public void setLocal_address(String local_address) {
		this.local_address = local_address;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	public int getAgent_id() {
		return agent_id;
	}

	public void setAgent_id(int agent_id) {
		this.agent_id = agent_id;
	}

	@Override
	public String toString() {
		return "AgentViewProfileDto [agent_name=" + agent_name + ", mobile_number=" + mobile_number
				+ ", qualification=" + qualification + ", year_of_passout=" + year_of_passout + ", percentage="
				+ percentage + ", city_id="+ city_id + ", local_address=" + local_address + ", pincode=" + pincode + "]";
	}

	
}

