package com.nacre.ims.dto;

public class CustomerCarInsuranceRegisterTableDTO {

	private String insurancePolicyNo;
	private String carNo;
	private String purchaseDate;
	private String rcNo;
	private String nomineeName;
	private long nomineeMobileNo;
	private String nomineeRelation;
	private int maxLimit;
	private int emiOptionType;
	private int customerId;
	private int carInsuranceMasterId;

	public String getInsurancePolicyNo() {
		return insurancePolicyNo;
	}

	public void setInsurancePolicyNo(String insurancePolicyNo) {
		this.insurancePolicyNo = insurancePolicyNo;
	}

	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getRcNo() {
		return rcNo;
	}

	public void setRcNo(String rcNo) {
		this.rcNo = rcNo;
	}

	public String getNomineeName() {
		return nomineeName;
	}

	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}

	public long getNomineeMobileNo() {
		return nomineeMobileNo;
	}

	public void setNomineeMobileNo(long nomineeMobileNo) {
		this.nomineeMobileNo = nomineeMobileNo;
	}

	public String getNomineeRelation() {
		return nomineeRelation;
	}

	public void setNomineeRelation(String nomineeRelation) {
		this.nomineeRelation = nomineeRelation;
	}

	public int getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(int maxLimit) {
		this.maxLimit = maxLimit;
	}

	public int getEmiOptionType() {
		return emiOptionType;
	}

	public void setEmiOptionType(int emiOptionType) {
		this.emiOptionType = emiOptionType;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getCarInsuranceMasterId() {
		return carInsuranceMasterId;
	}

	public void setCarInsuranceMasterId(int carInsuranceMasterId) {
		this.carInsuranceMasterId = carInsuranceMasterId;
	}

	@Override
	public String toString() {
		return "CustomerCarInsuranceRegisterTableDTO [insurancePolicyNo=" + insurancePolicyNo + ", carNo=" + carNo
				+ ", purchaseDate=" + purchaseDate + ", rcNo=" + rcNo + ", nomineeName=" + nomineeName
				+ ", nomineeMobileNo=" + nomineeMobileNo + ", nomineeRelation=" + nomineeRelation + ", maxLimit="
				+ maxLimit + ", emiOptionType=" + emiOptionType + ", customerId=" + customerId
				+ ", carInsuranceMasterId=" + carInsuranceMasterId + "]";
	}
	
	

}
