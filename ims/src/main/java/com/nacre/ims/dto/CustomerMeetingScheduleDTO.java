package com.nacre.ims.dto;

public class CustomerMeetingScheduleDTO {
	
	private String venue;
	private String desc;
	private String date;
	private String time;
	private String[] email;
	public String[] getEmail() {
		return email;
	}
	public void setEmail(String[] email) {
		this.email = email;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	@Override
	public String toString() {
		return "customer_meeting_schedule [venue=" + venue + ", desc=" + desc + ", date=" + date + ", time=" + time
				+ "]";
	}

}
