package com.nacre.ims.dto;

public class AgentApprovalDTO {
	private int id;
	private String name;
	private String qualification;
	private String gender;
	private String email;
	private String location;
	private int status_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getStatus_id() {
		return status_id;
	}

	public void setStatus_id(int status_id) {
		this.status_id = status_id;
	}

	@Override
	public String toString() {
		return "AgentApprovalDTO [id=" + id + ", name=" + name + ", qualification=" + qualification + ", gender="
				+ gender + ", email=" + email + ", location=" + location + ", status_id=" + status_id + "]";
	}

}
