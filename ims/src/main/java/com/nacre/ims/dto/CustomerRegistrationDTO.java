package com.nacre.ims.dto;

import java.sql.Date;

public class CustomerRegistrationDTO {
	
	/**
	 * @author Nikhilesh 
	 */
	private String firstName;
	   private String lastName;
	   private String email;
	   private long mobileNo;
	   private Date dob;
	   private String gender;
	   private long addharNo;
	   private String address;
	   private int city_id;
	   private int pincode;
	 
	  private int loginId;
	// private Address adressId;
	  private int agentId;

	  
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(long mobileNo) {
		this.mobileNo = mobileNo;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getAddharNo() {
		return addharNo;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setAddharNo(long addharNo) {
		this.addharNo = addharNo;
	}
	   
	   public int getCity_id() {
		return city_id;
	}
	public void setCity_id(int city_id) {
		this.city_id = city_id;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	/*public long getAgentId() {
		return agentId;
	}
	public void setAgentId(long agentId) {
		this.agentId = agentId;
	}*/
	public int getLoginId() {
		return loginId;
	}
	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}
	
public int getAgentId() {
		return agentId;
	}
	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}
@Override
public String toString() {
	return "CustomerRegistrationDTO [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
			+ ", mobileNo=" + mobileNo + ", dob=" + dob + ", gender=" + gender + ", addharNo=" + addharNo + ", address="
			+ address + ", city_id=" + city_id + ", pincode=" + pincode + ", loginId=" + loginId + ", agentId="
			+ agentId + "]";
}
}
