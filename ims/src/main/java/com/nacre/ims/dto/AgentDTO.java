package com.nacre.ims.dto;

import java.sql.Date;

public class AgentDTO {
	private String Agent_id;
	private String qualification;
	private String percentage;
	private String year_of_passout;
	private Date dob;
	private int mobile_number;
	private String adhaar_card_no;
	private String pancard_no;
	private Date dateofjoining;

}
