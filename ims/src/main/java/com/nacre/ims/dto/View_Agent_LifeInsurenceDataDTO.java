/**
 * 
 */
package com.nacre.ims.dto;

/**
 * @author vishal patil
 *
 */
public class View_Agent_LifeInsurenceDataDTO {
	private String name;
	private String insurance_policy_no;
	private String desciption;
	private String covarage;
	private int duration;
	private double rate_of_interest;
	private double total_amount_paid;
	private String life_insurance_offers;
	private String status;
	private int lifeid;
	public int getLifeid() {
		return lifeid;
	}
	public void setLifeid(int lifeid) {
		this.lifeid = lifeid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInsurance_policy_no() {
		return insurance_policy_no;
	}
	public void setInsurance_policy_no(String insurance_policy_no) {
		this.insurance_policy_no = insurance_policy_no;
	}
	public String getDesciption() {
		return desciption;
	}
	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}
	public String getCovarage() {
		return covarage;
	}
	public void setCovarage(String covarage) {
		this.covarage = covarage;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public double getRate_of_interest() {
		return rate_of_interest;
	}
	public void setRate_of_interest(double rate_of_interest) {
		this.rate_of_interest = rate_of_interest;
	}
	public double getTotal_amount_paid() {
		return total_amount_paid;
	}
	public void setTotal_amount_paid(double total_amount_paid) {
		this.total_amount_paid = total_amount_paid;
	}
	public String getLife_insurance_offers() {
		return life_insurance_offers;
	}
	public void setLife_insurance_offers(String life_insurance_offers) {
		this.life_insurance_offers = life_insurance_offers;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "View_Agent_LifeInsurenceDataDTO [name=" + name + ", insurance_policy_no=" + insurance_policy_no
				+ ", desciption=" + desciption + ", covarage=" + covarage + ", duration=" + duration
				+ ", rate_of_interest=" + rate_of_interest + ", total_amount_paid=" + total_amount_paid
				+ ", life_insurance_offers=" + life_insurance_offers + ", status=" + status + "]";
	}
	
}
