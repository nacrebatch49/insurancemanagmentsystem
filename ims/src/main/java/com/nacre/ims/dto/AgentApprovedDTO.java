package com.nacre.ims.dto;

public class AgentApprovedDTO {
	/**
	 * @author Nikhilesh 
	 */
	private String agentName;
	private String qualification;
	private String email;
	private String gender;
	private String location;
	
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	@Override
	public String toString() {
		return "AgentApprovedDTO [agentName=" + agentName + ", qualification=" + qualification + ", email=" + email
				+ ", gender=" + gender + ", location=" + location + "]";
	}

}
