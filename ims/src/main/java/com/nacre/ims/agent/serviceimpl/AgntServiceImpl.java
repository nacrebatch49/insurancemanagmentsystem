package com.nacre.ims.agent.serviceimpl;


import java.util.List;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.beanutils.BeanUtils;
import com.nacre.ims.bo.AgentApprovedBO;
import com.nacre.ims.dto.AgentApprovedDTO;
import com.nacre.ims.agent.dao.AgentDao;
import com.nacre.ims.agent.daoimpl.AgentDaoImpl;
import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.bo.AgentApprovalBO;
import com.nacre.ims.dto.AgentApprovalDTO;
import com.nacre.ims.bo.AgentApprovedBO;
import com.nacre.ims.dto.AgentApprovedDTO;

import com.nacre.ims.bean.AgentViewProfileBean;
import com.nacre.ims.bean.MyCustomerDetailsByAgent;
import com.nacre.ims.bo.AgentEditProfileBo;
import com.nacre.ims.bo.AgentRegistartionBo;
import com.nacre.ims.bo.CustomerMeetingScheduleBO;
import com.nacre.ims.bo.CustomerMeetingShceduleBO;
import com.nacre.ims.bo.GetChildCareInsurenceDataBO;
import com.nacre.ims.bo.GetLifeInsurenceDataBO;
import com.nacre.ims.bo.ViewAgent_ChildCareInsurenceDataBO;
import com.nacre.ims.bo.View_Agent_LifeInsurenceDataBO;
import com.nacre.ims.dto.AgentEditProfileDto;
import com.nacre.ims.dto.AgentRegistartionDto;
import com.nacre.ims.dto.CustomerMeetingScheduleDTO;
import com.nacre.ims.dto.CustomrMeetingSchduleDTO;
import com.nacre.ims.dto.GetChildCareInsurenceDataDTO;
import com.nacre.ims.dto.ViewAgent_ChildCareInsurenceDataDTO;
import com.nacre.ims.dto.View_Agent_LifeInsurenceDataDTO;
import com.nacre.ims.bo.GetHomeInsuranceBO;
import com.nacre.ims.dto.GetHomeInsuranceDTO;
import com.nacre.ims.bo.ViewCarInsuerancePolicyByAgentBo;
import com.nacre.ims.dto.ViewCarInsurancePolicyByAgentDTO;
public class AgntServiceImpl implements AgentService {

	public AgntServiceImpl(){
		System.out.println("AgntServiceImpl.AgntServiceImpl()");
	}
	
	
	
	AgentDao dao=new AgentDaoImpl();
	AgentApprovedBO bo=null;
	int result=0;

	@Override
	public int agentApprove(AgentApprovedDTO dto) throws SQLException {
		bo=new AgentApprovedBO();
		bo.setAgent_name(dto.getAgentName());
		bo.setAgent_qualification(dto.getQualification());
		bo.setAgent_email_id(dto.getEmail());
		bo.setAgent_gender(dto.getGender());
        bo.setLocation(dto.getLocation());	
       
        result=dao.agentApprove(bo);
		return result;
	}

	@Override
	public List<View_Agent_LifeInsurenceDataDTO> agentFetchLifeInsurence() throws Exception {
		View_Agent_LifeInsurenceDataDTO dto=null;
		List<View_Agent_LifeInsurenceDataDTO> list=new ArrayList();
		
		List<View_Agent_LifeInsurenceDataBO> bo1=null;
		//set property DTO to BO
		bo1=dao.agentFetchLifeInsurence();
		for(View_Agent_LifeInsurenceDataBO bo2:bo1){
			dto=new View_Agent_LifeInsurenceDataDTO();
			dto.setName(bo2.getName());
			dto.setDuration(bo2.getDuration());
			dto.setInsurance_policy_no(bo2.getInsurance_policy_no());
			dto.setRate_of_interest(bo2.getRate_of_interest());
			dto.setDesciption(bo2.getDesciption());
			dto.setTotal_amount_paid(bo2.getTotal_amount_paid());
			dto.setLife_insurance_offers(bo2.getLife_insurance_offers());
			dto.setCovarage(bo2.getCovarage());
			dto.setStatus(bo2.getStatus());
			dto.setLifeid(bo2.getLifeid());
			list.add(dto);
		}
		
		return list;
	}

	@Override
	public List<ViewAgent_ChildCareInsurenceDataDTO> agentFetchChildInsurence() throws Exception {
		ViewAgent_ChildCareInsurenceDataDTO dto=null;
		List<ViewAgent_ChildCareInsurenceDataDTO> list=new ArrayList<>();
		List<ViewAgent_ChildCareInsurenceDataBO> bo1=null;
		bo1=dao.agentFetchChildCareInsurence();
		for(ViewAgent_ChildCareInsurenceDataBO bo2:bo1){
			dto=new ViewAgent_ChildCareInsurenceDataDTO();
			BeanUtils.copyProperties(dto,bo2);
			list.add(dto);
			System.out.println(dto);
		}
		return list;
	}
	

	
	@Override
	public List<ViewCarInsurancePolicyByAgentDTO> getAllCarPolicy() {
		List<ViewCarInsurancePolicyByAgentDTO> listdto = new ArrayList();
		List<ViewCarInsuerancePolicyByAgentBo> list = dao.getAllCarPolicy();
		for (ViewCarInsuerancePolicyByAgentBo bo : list) {
			ViewCarInsurancePolicyByAgentDTO dto = new ViewCarInsurancePolicyByAgentDTO();
			dto.setCarIncId(bo.getCarIncId());
			dto.setPolicyName(bo.getPolicyName());
			dto.setCoverage(bo.getCoverage());
			dto.setPolicyDescription(bo.getPolicyDescription());
			dto.setRateofInterest(bo.getRateofInterest());
			dto.setDuration(bo.getDuration());
			dto.setPaybleAmount(bo.getPaybleAmount());
			dto.setInitialAmount(bo.getInitialAmount());
			listdto.add(dto);

		}
		return listdto;
	}
	

	GetHomeInsuranceDTO getHomeInsuranceDTO=null;
	GetHomeInsuranceBO getHomeInsuranceBO=null;
    
    @Override
	public List<GetHomeInsuranceDTO> getHomeInsurence() {
		List<GetHomeInsuranceDTO> listDto=null;
		List<GetHomeInsuranceBO> listBo=null; 
		GetHomeInsuranceDTO getHomeInsuranceDTO=null;
		
			
			listDto=new ArrayList<>();
			listBo=dao.getHomeInsurence();
			for (GetHomeInsuranceBO getHomeInsuranceBO : listBo) {
				getHomeInsuranceDTO=new GetHomeInsuranceDTO();
				getHomeInsuranceDTO.setPolicyName(getHomeInsuranceBO.getPolicyName());
				getHomeInsuranceDTO.setCoverage(getHomeInsuranceBO.getCoverage());
				getHomeInsuranceDTO.setDescription(getHomeInsuranceBO.getDescription());
				getHomeInsuranceDTO.setDuration(getHomeInsuranceBO.getDuration());
				getHomeInsuranceDTO.setInitialAmount(getHomeInsuranceBO.getInitialAmount());
				getHomeInsuranceDTO.setTotalamount(getHomeInsuranceBO.getTotalamount());
				getHomeInsuranceDTO.setRateOfInterest(getHomeInsuranceBO.getRateOfInterest());
				getHomeInsuranceDTO.setPolicyOffer(getHomeInsuranceBO.getPolicyOffer());
				getHomeInsuranceDTO.setPolicyNumber(getHomeInsuranceBO.getPolicyNumber());
				getHomeInsuranceDTO.setHomeInsuranceId(getHomeInsuranceBO.getHomeInsuranceId());
				listDto.add(getHomeInsuranceDTO);
			}
			
		
		return listDto;
	}
    
    List<MyCustomerDetailsByAgent> agent=null;
	MyCustomerDetailsByAgent customerdetails=new MyCustomerDetailsByAgent();
	@Override
	public List<MyCustomerDetailsByAgent> getMyCustomerDetails(int agent_id) throws ClassNotFoundException, SQLException {
		 /*SqlDate.sqlToUtilDate(customerdetails.getDob());*/
		String customerlist="";
		agent=dao.getMyCustomerDetails(agent_id);
		for(int i=0;i<agent.size();i++){
		
		agent.get(i).setPolicies(customerlist);
		}
		System.out.println("service"+agent);
		return agent; 
	}

	
	/*@Override
	public int fixingSchedulMeeting(CustomrMeetingSchduleDTO dto,int agent_id,int customer_id) throws Exception {
		
		dao=new AgentDaoImpl();
		CustomerMeetingScheduleBO bo=null;
		bo=new CustomerMeetingScheduleBO();
		
		bo.setVenue(dto.getVenue());
		bo.setDesc(dto.getDesc());
		bo.setDate(dto.getDate());
		bo.setTime(dto.getTime());
		bo.setEmail(dto.getEmail());
		return dao.agentSchedul(bo,agent_id,customer_id);
	}*/
	
	@Override
	public AgentViewProfileBean agentViewProfileService(int agent_id) throws ClassNotFoundException, SQLException {
		AgentViewProfileBean bean = new AgentViewProfileBean();
		bean = dao.agentViewProfile(agent_id);
		return bean;
	}

	@Override
	public String editProfile(AgentEditProfileDto dto) throws ClassNotFoundException, SQLException {
		AgentEditProfileBo bo = null;
		int count = 0;
		bo = new AgentEditProfileBo();
		bo.setAgent_name(dto.getAgent_name());
		bo.setQualification(dto.getQualification());
		bo.setPercentage(dto.getPercentage());
		bo.setYear_of_passout(dto.getYear_of_passout());
		bo.setMobile_number(dto.getMobile_number());
		bo.setLocal_address(dto.getLocal_address());
		bo.setPincode(dto.getPincode());
		bo.setCity_id(dto.getCity_id());
		
		bo.setAgent_id(dto.getAgent_id());
		count = dao.EditProfile(bo);
		if (count == 0) {
			return "profile is not updated";
		} else {

			return "Profile is updated sucessfully";
		}
	}

	@Override
	public int fixingSchedulMeeting(CustomerMeetingScheduleDTO dto, int agent_id) throws Exception {
		dao=new AgentDaoImpl();
		CustomerMeetingScheduleBO bo=null;
		bo=new CustomerMeetingScheduleBO();
		
		bo.setVenue(dto.getVenue());
		bo.setDesc(dto.getDesc());
		bo.setDate(dto.getDate());
		bo.setTime(dto.getTime());
		bo.setEmail(dto.getEmail());
		return dao.agentSchedul(bo,agent_id);
	}
	@Override
	public String checkStatus(int agent_approved_id) throws SQLException {
		System.out.println("AgntServiceImpl.checkStatus()");
		AgentDao dao=new AgentDaoImpl();
		String check_status=dao.checkStatus(agent_approved_id);
		System.out.println("service"+check_status);
		System.out.println(agent_approved_id);
		return check_status;
	}

	@Override
	public List<String> getAgentData(int agent_approve_id) throws SQLException {
		List<String> list=new ArrayList<>();
		AgentDao dao=new AgentDaoImpl();
		list=dao.getAgentData(agent_approve_id);
		return list;
	}

	@Override
	public String agentRegister(AgentRegistartionDto dto) throws SQLException {
		System.out.println("service");
		AgentDao dao=new AgentDaoImpl();
		AgentRegistartionBo bo=new AgentRegistartionBo();
		bo.setAgent_name(dto.getAgent_name());
		bo.setPercentage(dto.getPercentage());
		bo.setYear_of_passout(dto.getYear_of_passout());
		bo.setDob(dto.getDob());
		bo.setMobile_number(dto.getMobile_number());
		bo.setGender(dto.getGender());
		bo.setAdhaar_card_no(dto.getAdhaar_card_no());
		bo.setPancard_no(dto.getPancard_no());
		bo.setDate_of_joining(dto.getDate_of_joining());
		bo.setAgent_approved_id(dto.getAgent_approved_id());
		bo.setLocal_address(dto.getLocal_address());
		bo.setCity_id(dto.getCity_id());
		bo.setUser_username(dto.getUser_username());
		bo.setUser_password(dto.getUser_password());
		bo.setPincode(dto.getPincode());
		bo.setMailId(dto.getMailid());
		int count=dao.agentRegister(bo);
		
		if(count==0) {
			return "rec not inserted";
		}
		else {
			return "rec inserted";
		}
		
	}

}
	
