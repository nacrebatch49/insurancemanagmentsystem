package com.nacre.ims.agent.controller;
/**
 * @author Ajit
 *
 */
import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;



@WebServlet("/viewHomeInsuranceByAgentController")
public class ViewHomeInsuranceByAgentController extends HttpServlet {
	
		private static final long serialVersionUID = 1L;
	       
	    /**
	     * @see HttpServlet#HttpServlet()
	     */
	    public ViewHomeInsuranceByAgentController() {
	       
	        // TODO Auto-generated constructor stub
	    }
	
		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			AgentService agentService=null;
			HttpSession session=null;
			try {
			  agentService= new AgntServiceImpl(); 
			  session=req.getSession();
			  session.setAttribute("data", agentService.getHomeInsurence());
			  resp.sendRedirect("/ims/agent/pages/homeinsurancebyagent.jsp");
			  System.out.println(agentService.getHomeInsurence());
			
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
	
		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			doGet(request, response);
		}
	
	}

	