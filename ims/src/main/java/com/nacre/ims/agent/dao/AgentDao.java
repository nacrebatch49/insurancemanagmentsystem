package com.nacre.ims.agent.dao;


import java.util.List;
import java.sql.SQLException;
import java.util.List;
import com.nacre.ims.bo.AgentApprovedBO;
import com.nacre.ims.bo.ViewAgent_ChildCareInsurenceDataBO;
import com.nacre.ims.bo.View_Agent_LifeInsurenceDataBO;
import com.nacre.ims.bean.AgentViewProfileBean;
import com.nacre.ims.bean.MyCustomerDetailsByAgent;
import com.nacre.ims.bo.AgentEditProfileBo;
import com.nacre.ims.bo.AgentRegistartionBo;
import com.nacre.ims.bo.CustomerMeetingScheduleBO;
import com.nacre.ims.bo.ViewCarInsuerancePolicyByAgentBo;
import com.nacre.ims.bo.GetHomeInsuranceBO;
import java.util.List;


public interface AgentDao {
	public int agentApprove(AgentApprovedBO bo) throws SQLException;
	public AgentViewProfileBean agentViewProfile(int agent_id) throws SQLException, ClassNotFoundException;
	public int EditProfile(AgentEditProfileBo bo) throws ClassNotFoundException, SQLException;
	public List<View_Agent_LifeInsurenceDataBO> agentFetchLifeInsurence() throws Exception;
	public List<ViewAgent_ChildCareInsurenceDataBO> agentFetchChildCareInsurence() throws Exception;
	public    List<ViewCarInsuerancePolicyByAgentBo>  getAllCarPolicy();
	public List<GetHomeInsuranceBO> getHomeInsurence();
	public List<MyCustomerDetailsByAgent> getMyCustomerDetails(int agent_id) throws  SQLException, ClassNotFoundException;
	public int agentSchedul(CustomerMeetingScheduleBO bo,int agent_id) throws Exception;
	public String checkStatus(int agent_approve_id) throws SQLException;

	public List<String> getAgentData(int agent_approve_id) throws SQLException;
	
	public int agentRegister(AgentRegistartionBo bo) throws SQLException;
}

