/**
 * 
 */
package com.nacre.ims.agent.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;


/**
 * @author vishal patil
 *
 */
@WebServlet("/viewagent_childinsurencedatacontroller")
public class ViewAgent_ChildInsurenceDataController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		AgentService service=new AgntServiceImpl();
		HttpSession session=req.getSession();
			try {
				//System.out.println(service.agentFetchLifeInsurence());
				session.setAttribute("list",service.agentFetchChildInsurence());
			resp.sendRedirect("/ims/agent/pages/agent_childcareinsurence.jsp");
			} catch (Exception e) {
				
				e.printStackTrace();
			}	
			
		
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
