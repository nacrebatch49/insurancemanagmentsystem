package com.nacre.ims.agent.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;
import com.nacre.ims.commonsutil.SendMail;
import com.nacre.ims.dto.AgentApprovedDTO;
@WebServlet("/AgentApprovedController")
public class AgentApprovedController extends HttpServlet {

	/**
	 * @author Nikhilesh Wani
	 */
	AgentService service=null;
	AgentApprovedDTO dto=null;
	int count=0;
	
	private static final long serialVersionUID = 1L;
	public AgentApprovedController() {
		
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	PrintWriter pw=null;
	resp.setContentType("text/html");
	pw=resp.getWriter();
	
	HttpSession session=req.getSession();
	String agentName=req.getParameter("fullname");
	String agentQualification=req.getParameter("qualification");
	String emailId=req.getParameter("email");
	String agentGender=req.getParameter("gen");
	String location=req.getParameter("loc");
	dto=new AgentApprovedDTO();
	
	dto.setAgentName(agentName);
	dto.setQualification(agentQualification);
	dto.setEmail(emailId);
	dto.setGender(agentGender);
	dto.setLocation(location);
	
	try {
		service=new AgntServiceImpl();
		
		count=service.agentApprove(dto);
		System.out.println(count);
		if(count>0) {
			SendMail.send("Regarding approval id", emailId, "nikhilkmr214@gmail.com", "nikhilkmr214@", "your approval id is "+count+" please confirm your approval then register");
			session.setAttribute("msg","You are registered please wait for approval");
			resp.sendRedirect("/ims/commons/pages/Login.jsp");
		}
	
		
	}catch(Exception e) {
		e.printStackTrace();
	}
	
	

	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	doGet(req, resp);
	}

}
