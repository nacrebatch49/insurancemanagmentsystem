package com.nacre.ims.agent.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;

@WebServlet("/GetAgentInfo")
public class GetAgentData extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		AgentService service=null;
		service=new AgntServiceImpl();
		List<String> list=new ArrayList<>();
		HttpSession ses=req.getSession(true);
		int agent_approve_id=(int) ses.getAttribute("agent_approve_id");
		try {
			list=service.getAgentData(agent_approve_id);
			ses.setAttribute("agent_name", list.get(0));
			ses.setAttribute("emailId", list.get(1));
			System.out.println(list);
			Gson g=new Gson();
			String s=g.toJson(list);
			System.out.println(s);
			res.getWriter().println(s);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
