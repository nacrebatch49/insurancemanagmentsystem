/**
 * 
 */
package com.nacre.ims.agent.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;
import com.nacre.ims.dto.ViewCarInsurancePolicyByAgentDTO;

/**
 * @author Barkha
 *
 */
@WebServlet("/getCarInsuranceDataControllerByAgent")
public class GetCarInsuranceDataByAgentController extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside view details");
		resp.setContentType("text/html");
		AgentService service=new AgntServiceImpl();
		List<ViewCarInsurancePolicyByAgentDTO> list=service.getAllCarPolicy();
		System.out.println(list);
		HttpSession ses=req.getSession();
		ses.setAttribute("list", list);
		resp.sendRedirect("/ims/agent/pages/ViewCarInsue.jsp");
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
