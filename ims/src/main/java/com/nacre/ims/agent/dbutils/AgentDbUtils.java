package com.nacre.ims.agent.dbutils;

public class AgentDbUtils {
	public static final String INSERT_AGENT_DETAILS_FOR_APPROVED="insert into agent_approved_table (agent_approved_table.agent_name,agent_approved_table.agent_qualification,agent_approved_table.agent_email_id,agent_approved_table.gender,agent_approved_table.location,agent_approved_table.status_id)values(?,?,?,?,?,(select status_table.status_id from status_table where status_table.status_name='PENDING'))";
	public static final String getCarDetailsPolicy="select car.name,car.coverage,car.description,car.rate_of_interest,car.duration,car.payble_amount,car.initial_amount, car.car_insurance_id from car_insurance_master_table car where car.status_id=(select status_table.status_id from status_table where status_table.status_name='WORKING')";
    public static final String GET_HOME_POLIVY_BY_AGENT="select name,max_limit,coverage,description,total_amount_paid,rate_of_interest,initial_amount,home_insurance_offer,policy_no,home_insurance_id from home_insurance_master_table where status_id=( select status_id from status_table where status_name='WORKING')";
    public static final String GET_MYCUSTOMERDETAILSBYAGENT="select concat(cust.customer_first_name , cust.customer_last_name) as fullname,cust.customer_email_id,cust.customer_mobile_no,cust.customer_gender,cust.customer_dob,CONCAT(CONCAT(CONCAT(CONCAT(add1.local_address , city.city_name),state.state_name),cou.country_name) ,add1.pincode) as address,inc.policy_name from customer_registration_table cust,address_table add1,city_master_table city,state_master_table state,country_master_table cou,insurancepolicies_table inc where cust.agent_id=? and cust.address_id=add1.address_id and add1.city_id=city.city_id and city.state_id=state.state_id and state.country_id=cou.country_id and cust.costomer_register_id=inc.customer_id1";
    public static final String GET_SCHEDULEMEETINGCUSTOMER="INSERT INTO agent_customer_meeting(vanue,desc1,date1,time1,agent_id,customer_id) values(?,?,?,?,?,(select customer_registration_table.costomer_register_id from customer_registration_table where customer_registration_table.customer_email_id=? ))";
    public static final String SELECT_AGENT = "select ap.agent_name,ap.agent_qualification,"
		+ "ap.agent_email_id,ar.percentage,ar.year_of_passout,ar.dob,"
		+ "ar.mobile_number,ar.gender,adhaar_card_no,ar.pancard_no,"
		+ "ar.date_of_joining,add1.local_address,add1.pincode,city.city_name,"
		+ "state.state_name,country.country_name from agent_approved_table ap,"
		+ "agent_registration_table ar,address_table add1,city_master_table city,"
		+ "state_master_table state,country_master_table country "
		+ "where ap.agent_approved_id=ar.agent_approved_id " + "and add1.address_id=ar.address "
		+ "and city.city_id=add1.city_id " + "and state.state_id=city.state_id "
		+ "and country.country_id=state.state_id " + "and ar.agent_id=?";


public static final String UPDATE_AGENT = "update agent_approved_table ap,agent_registration_table ar,address_table add1 set "
		+ "ap.agent_name=?,ap.agent_qualification=?,ar.percentage=?,ar.year_of_passout=?,ar.mobile_number=?,add1.local_address=?,add1.city_id=?,add1.pincode=? "
		+ "where add1.address_id=ar.address and " + "ap.agent_approved_id=ar.agent_approved_id  and "
		+ "ar.agent_id=?";
public static final String CHECK_STATUS = "select status_table.status_name from status_table,agent_approved_table where agent_approved_table.status_id=status_table.status_id and agent_approved_table.agent_approved_id=?";

public static final String GET_AGENT_DATA = "select agent_approved_table.agent_name,agent_approved_table.agent_email_id,agent_approved_table.agent_qualification from agent_approved_table where agent_approved_table.agent_approved_id=?";
public static final String REGISTER_AGENT = "insert into agent_registration_table(agent_registration_table.percentage,agent_registration_table.year_of_passout,agent_registration_table.dob,agent_registration_table.mobile_number,agent_registration_table.gender,agent_registration_table.adhaar_card_no,agent_registration_table.pancard_no,agent_registration_table.date_of_joining,agent_registration_table.address,agent_registration_table.login_id,agent_registration_table.agent_approved_id) values(?,?,?,?,?,?,?,?,?,?,(select agent_approved_table.agent_approved_id from agent_approved_table where agent_approved_table.agent_email_id=?))";
public static final String INSERT_ADDRESS="insert into address_table(address_table.local_address,address_table.pincode,address_table.city_id) values(?,?,?)";
public static final String INSERT_LOGINDETAILS="insert into login_table(login_table.user_username,login_table.user_password,login_table.role_id) values(?,?,?)";


}

