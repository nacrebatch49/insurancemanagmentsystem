package com.nacre.ims.agent.daoimpl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.nacre.ims.admin.dbutils.AdminDbUtils;
import java.sql.SQLException;
import java.sql.Time;
import com.nacre.ims.agent.dao.AgentDao;
import com.nacre.ims.agent.dbutils.AgentDbUtils;
import java.util.ArrayList;
import java.util.List;
import com.nacre.ims.admin.dbutils.AdminDbUtils;

import com.nacre.ims.bo.AgentApprovedBO;
import com.nacre.ims.bo.AgentEditProfileBo;
import com.nacre.ims.bo.AgentRegistartionBo;
import com.nacre.ims.bo.CustomerMeetingScheduleBO;
import com.nacre.ims.bo.ViewAgent_ChildCareInsurenceDataBO;
import com.nacre.ims.bo.View_Agent_LifeInsurenceDataBO;
import com.nacre.ims.commonsutil.ConnectionUtil;
import com.nacre.ims.commonsutil.SqlDate;
import com.nacre.ims.dto.ViewAgent_ChildCareInsurenceDataDTO;
import com.nacre.ims.agent.dao.AgentDao;
import com.nacre.ims.agent.dbutils.AgentDbUtils;
import com.nacre.ims.bean.AgentViewProfileBean;
import com.nacre.ims.bean.MyCustomerDetailsByAgent;
import com.nacre.ims.bo.ViewCarInsuerancePolicyByAgentBo;
import com.nacre.ims.bo.GetHomeInsuranceBO;


public class AgentDaoImpl implements AgentDao {

	Connection con = null;
	
	@Override
	public List<View_Agent_LifeInsurenceDataBO> agentFetchLifeInsurence() throws Exception {
		AgentDao dao=new AgentDaoImpl();
		List<View_Agent_LifeInsurenceDataBO> list=new ArrayList<>();
		Connection con=null;
		PreparedStatement ps=null;
		int count=0;
		con=ConnectionUtil.getConnection();
		ps=con.prepareStatement(AdminDbUtils.SELECT_LIFE_INSURENCE_QUEERY);
		ResultSet rs=ps.executeQuery();
		while(rs.next()){
			View_Agent_LifeInsurenceDataBO bo=new View_Agent_LifeInsurenceDataBO();
			bo.setName(rs.getString("insurance_name"));
			bo.setDuration(rs.getInt("duration"));
			bo.setInsurance_policy_no(rs.getString("insurance_policy_no"));
			bo.setRate_of_interest(rs.getDouble("rate_of_interest"));
			bo.setDesciption(rs.getString("desciption"));
			bo.setTotal_amount_paid(rs.getDouble("total_amount_paid"));
			bo.setLife_insurance_offers(rs.getString("life_insurance_offers"));
			bo.setCovarage(rs.getString("covarage"));
			bo.setLifeid(rs.getInt("life_isn_id"));
			
			//bo.setStatus(rs.getString("status"));
			
			list.add(bo);
		
		}
		
		return list;

	}
	
	@Override
	public int agentApprove(AgentApprovedBO bo) throws SQLException {
	
	Connection con=null;
	int count=0;
	con=ConnectionUtil.getConnection();
	PreparedStatement ps=con.prepareStatement(AgentDbUtils.INSERT_AGENT_DETAILS_FOR_APPROVED,Statement.RETURN_GENERATED_KEYS);
	
	
	
	ps.setString(1, bo.getAgent_name());
	ps.setString(2, bo.getAgent_qualification());
	ps.setString(3, bo.getAgent_email_id());
	ps.setString(4, bo.getAgent_gender());
	ps.setString(5, bo.getLocation());
      
	ps.executeUpdate();
	ResultSet rs4=ps.getGeneratedKeys();
	if(rs4.next()) {
		count=rs4.getInt(1);
	}

		return count;
	}

	@Override
	public List<ViewAgent_ChildCareInsurenceDataBO> agentFetchChildCareInsurence() throws Exception {
		AgentDao dao=new AgentDaoImpl();
		List<ViewAgent_ChildCareInsurenceDataBO> list=new ArrayList<>();
		Connection con=null;
		PreparedStatement ps=null;
		int count=0;
		con=ConnectionUtil.getConnection();
		ps=con.prepareStatement(AdminDbUtils.SELECT_ChilCARE_INSURENCE_QUEERY);
		ResultSet rs=ps.executeQuery();
		while(rs.next()){
			ViewAgent_ChildCareInsurenceDataBO bo=new ViewAgent_ChildCareInsurenceDataBO();
			bo.setMax_limit(rs.getInt("max_limit"));
			bo.setName(rs.getString("name"));
			bo.setCoverage(rs.getString("coverage"));
			bo.setDesciption(rs.getString("desciption"));
			bo.setRate_of_interest(rs.getDouble("rate_of_interest"));
			bo.setPayable_amount(rs.getDouble("payable_amount"));
		    bo.setInitial_amount(rs.getDouble("initial_amount"));
		    bo.setLife_insurance_offers(rs.getString("life_insurance_offers"));
		    bo.setChildid(rs.getInt("child_care_insurance_id"));
		    //bo.setStatus(rs.getString("status"));
		    list.add(bo);
		}
		
		return list;
	}

	@Override
	public    List<ViewCarInsuerancePolicyByAgentBo>  getAllCarPolicy() {
		  List<ViewCarInsuerancePolicyByAgentBo> list=new ArrayList();
		Connection con = null;
		con=ConnectionUtil.getConnection();
		PreparedStatement ps = null;
		
		try {
			ps=con.prepareStatement(AgentDbUtils.getCarDetailsPolicy);
		   ResultSet rs=	ps.executeQuery();
		 
		   while(rs.next()) {
			   ViewCarInsuerancePolicyByAgentBo bo= new ViewCarInsuerancePolicyByAgentBo();
			   
			   bo.setPolicyName(rs.getString(1));
			   bo.setCoverage(rs.getString(2));
			   bo.setPolicyDescription(rs.getString(3));
			   bo.setRateofInterest(rs.getDouble(4));
			   bo.setDuration(rs.getInt(5));
			   bo.setPaybleAmount(rs.getDouble(6));
			   bo.setInitialAmount(rs.getDouble(7));
			   bo.setCarIncId(rs.getInt(8));
			   list.add(bo);
		   }
		   
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}


	@Override
	public List<GetHomeInsuranceBO> getHomeInsurence() {
		con = ConnectionUtil.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		GetHomeInsuranceBO getHomeInsuranceBO = null;
		List<GetHomeInsuranceBO> listBo = null;

		try {
				ps = con.prepareStatement(AgentDbUtils.GET_HOME_POLIVY_BY_AGENT);
				rs=ps.executeQuery();
				
				listBo = new ArrayList<>();
				while (rs.next()) {
					getHomeInsuranceBO = new GetHomeInsuranceBO();
					getHomeInsuranceBO.setPolicyName(rs.getString(1));
					getHomeInsuranceBO.setDuration(rs.getInt(2));
					getHomeInsuranceBO.setCoverage(rs.getString(3));
					getHomeInsuranceBO.setDescription(rs.getString(4));
					getHomeInsuranceBO.setTotalamount(rs.getDouble(5));
					getHomeInsuranceBO.setRateOfInterest(rs.getDouble(6));
					getHomeInsuranceBO.setInitialAmount(rs.getDouble(7));
					getHomeInsuranceBO.setPolicyOffer(rs.getString(8));
					getHomeInsuranceBO.setPolicyNumber(rs.getString(9));
					getHomeInsuranceBO.setHomeInsuranceId(rs.getInt(10));
					listBo.add(getHomeInsuranceBO);
				}
			
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return listBo;
	}
	
	  @Override
			public List<MyCustomerDetailsByAgent> getMyCustomerDetails(int agent_id) throws ClassNotFoundException, SQLException {
		
			  PreparedStatement ps=null;
				ResultSet rs=null;
				MyCustomerDetailsByAgent customerDetails=null;
		
				List<MyCustomerDetailsByAgent> list=new ArrayList<MyCustomerDetailsByAgent>();
				con=ConnectionUtil.getConnection();
				String query=AgentDbUtils.GET_MYCUSTOMERDETAILSBYAGENT;
				ps=con.prepareStatement(query);
				ps.setInt(1, agent_id);
				rs=ps.executeQuery();
				
				
				while(rs.next()){
					
					System.out.println("record found");
					customerDetails=new MyCustomerDetailsByAgent();
					//System.out.println(rs.getString(1)+" "+rs.getString(2)+" "+rs.getLong(3)+" "+rs.getString(4)+" "+rs.getString(5)+" "+rs.getString(6)+" "+rs.getDate(7));
				    customerDetails.setFirst_name(rs.getString("fullname"));
				    customerDetails.setEmail(rs.getString("customer_email_id"));
				    customerDetails.setMobile_no(rs.getLong("customer_mobile_no"));
				    customerDetails.setGender(rs.getString("customer_gender"));
				    customerDetails.setDob(rs.getDate("customer_dob"));
				    customerDetails.setAddress(rs.getString("address"));
				    customerDetails.setPolicies(rs.getString("policy_name"));
				    System.out.println(rs.getDate("customer_dob"));
				   
				    
				    
				    list.add(customerDetails);
				}
				
				System.out.println("all record"+list);
				
				
				return list;
			}
		  
		  @Override
			public int agentSchedul(CustomerMeetingScheduleBO bo, int agent_id) throws Exception {

				
				Connection connection=null;
				PreparedStatement preparedstatement=null;
				connection=ConnectionUtil.getConnection();
				String date=bo.getDate();
				java.sql.Date date1=SqlDate.date(date);
				String time=bo.getTime();
				Time time1= SqlDate.time(time);
				String email="";
				
				int result=0;
				preparedstatement=connection.prepareStatement(AgentDbUtils.GET_SCHEDULEMEETINGCUSTOMER );
				
				for(String e:bo.getEmail()){
				preparedstatement.setString(1, bo.getVenue());
				preparedstatement.setString(2, bo.getDesc());
				preparedstatement.setDate(3, date1);
				preparedstatement.setTime(4, time1);
				preparedstatement.setInt(5, agent_id);
				preparedstatement.setString(6, e);
				
				result=result+preparedstatement.executeUpdate();
				}
				return result;
			}
			
		  @Override

			public AgentViewProfileBean agentViewProfile(int agent_id) throws SQLException, ClassNotFoundException {
				Connection con=null;
				 String query=null;
				 PreparedStatement ps=null;
				 ResultSet rs=null;
				 AgentViewProfileBean bean=new AgentViewProfileBean();
				 query=AgentDbUtils.SELECT_AGENT;
				 con=ConnectionUtil.getConnection();
				 ps=con.prepareStatement(query);
				 ps.setInt(1, agent_id);
				 rs=ps.executeQuery();
				 if(rs.next()){
					 bean.setAgent_name(rs.getString(1));
					 bean.setQualification(rs.getString(2));
					 bean.setAgent_email_id(rs.getString(3));
					 bean.setPercentage(rs.getFloat(4));
					 bean.setYear_of_passout(rs.getInt(5));
					 bean.setDob(rs.getDate(6));
					 bean.setMobile_number(rs.getLong(7));
					 bean.setGender(rs.getString(8));
					 bean.setAdhaar_card_no(rs.getLong(9));
					 bean.setPancard_no(rs.getLong(10));
					 bean.setDate_of_joining(rs.getDate(11));
				     bean.setLocal_address(rs.getString(12));
				     bean.setPincode(rs.getInt(13));
				     bean.setCity_name(rs.getString(14));
				     bean.setState_name(rs.getString(15));
				     bean.setCountry_name(rs.getString(16));
				 }
				
				
				return bean;
			}

			@Override
			public int EditProfile(AgentEditProfileBo bo) throws ClassNotFoundException, SQLException {
				Connection con=null;
				 String query=null;
				 PreparedStatement ps=null;
				 int count=0;
				 AgentViewProfileBean bean=new AgentViewProfileBean();
				 query=AgentDbUtils.UPDATE_AGENT;
				 con=ConnectionUtil.getConnection();
				 ps=con.prepareStatement(query);
				 
				 ps.setString(1,bo.getAgent_name());
				 ps.setString(2, bo.getQualification());
				 ps.setFloat(3, bo.getPercentage());
				 ps.setInt(4, bo.getYear_of_passout());
				 ps.setLong(5, bo.getMobile_number());
				 ps.setString(6, bo.getLocal_address());
				 ps.setInt(7, bo.getCity_id());
				 ps.setInt(8, bo.getPincode());
				 ps.setInt(9, bo.getAgent_id());
				count=ps.executeUpdate();
				return count;
			}

			@Override
			public String checkStatus(int agent_approve_id) throws SQLException {
				System.out.println("AgentDaoImpl.checkStatus()");
				Connection con = null;
				PreparedStatement ps = null;
				int count = 0;
				ResultSet rs = null;
				String check_status = "";
				con = ConnectionUtil.getConnection();
				ps = con.prepareStatement(AgentDbUtils.CHECK_STATUS);
				ps.setInt(1, agent_approve_id);
				rs = ps.executeQuery();
				if (rs.next()) {
					check_status = rs.getString(1);
				}
				System.out.println(agent_approve_id);
				System.out.println("dao" + check_status);

				return check_status;

			}

			@Override
			public List<String> getAgentData(int agent_approve_id) throws SQLException {

				Connection con = null;
				PreparedStatement ps = null;
				int count = 0;
				ResultSet rs = null;
				List<String> list = new ArrayList<>();
				con = ConnectionUtil.getConnection();
				ps = con.prepareStatement(AgentDbUtils.GET_AGENT_DATA);
				ps.setInt(1, agent_approve_id);
				rs = ps.executeQuery();
				if (rs.next()) {
					list.add(rs.getString(1));
					list.add(rs.getString(2));
					list.add(rs.getString(3));
				}

				return list;

			}

			@Override
			public int agentRegister(AgentRegistartionBo bo) throws SQLException {
				System.out.println("dao");
				Connection con = null;
				PreparedStatement ps, ps1, ps2 = null;
				int count = 0;
				ResultSet rs, rs1, rs2 = null;
				List<String> list = new ArrayList<>();
				con = ConnectionUtil.getConnection();
				System.out.println(bo);
				ps = con.prepareStatement(AgentDbUtils.INSERT_LOGINDETAILS, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, bo.getUser_username());
				ps.setString(2, bo.getUser_password());
				ps.setInt(3, 3);
				ps.executeUpdate();
				rs = ps.getGeneratedKeys();
				int login_id = 0;
				if (rs.next()) {
					login_id = rs.getInt(1);
				}
				ps1 = con.prepareStatement(AgentDbUtils.INSERT_ADDRESS, Statement.RETURN_GENERATED_KEYS);
				ps1.setString(1, bo.getLocal_address());
				ps1.setInt(2, bo.getPincode());
				ps1.setInt(3, bo.getCity_id());
				ps1.executeUpdate();
				rs1 = ps1.getGeneratedKeys();
				int address_id = 0;
				if (rs1.next()) {
					address_id = rs1.getInt(1);
				}
				ps2 = con.prepareStatement(AgentDbUtils.REGISTER_AGENT, Statement.RETURN_GENERATED_KEYS);
				ps2.setFloat(1, bo.getPercentage());
				ps2.setInt(2, bo.getYear_of_passout());
				String dob = bo.getDob();
				Date date1 = SqlDate.date(dob);

				ps2.setDate(3, date1);
				ps2.setLong(4, bo.getMobile_number());
				ps2.setString(5, bo.getGender());
				ps2.setLong(6, bo.getAdhaar_card_no());
				ps2.setLong(7, bo.getPancard_no());
				String doj = bo.getDate_of_joining();
				Date date2 = SqlDate.date(dob);

				ps2.setDate(8, date2);

				ps2.setInt(9, address_id);
				ps2.setInt(10, login_id);
				ps2.setString(11, bo.getMailId());
				int count1 = ps2.executeUpdate();

				System.out.println(bo);
				return count1;
			}

		}

