package com.nacre.ims.agent.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;

@WebServlet("/checkController")
public class AgentApprovalStatus extends HttpServlet {
	public AgentApprovalStatus() {
		System.out.println("AgentApprovalStatus.AgentApprovalStatus()");
	}
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		System.out.println("AgentApprovalStatus.doGet()");
		res.setContentType("application/json");
		int agent_approve_id=Integer.parseInt(req.getParameter("approveid"));
		HttpSession ses=req.getSession(true);
		ses.setAttribute("agent_approve_id", agent_approve_id);
	AgentService service=null;
	service=new AgntServiceImpl();
	try {
		String check_status=service.checkStatus(agent_approve_id);
		System.out.println(agent_approve_id);
		System.out.println(check_status);
		Gson g=new Gson();
		String str=g.toJson(check_status);
		res.getWriter().println(str);
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
		
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
