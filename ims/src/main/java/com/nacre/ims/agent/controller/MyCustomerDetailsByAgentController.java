package com.nacre.ims.agent.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;
import com.nacre.ims.bean.MyCustomerDetailsByAgent;



@WebServlet("/MyCustomerDetailsByAgentControllerurl")
public class MyCustomerDetailsByAgentController extends HttpServlet {
	public MyCustomerDetailsByAgentController(){
		System.out.println("MyCustomerDetailsByAgentController.MyCustomerDetailsByAgentController()");
	}
	AgentService service;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession ses=null;
		PrintWriter pw=null;
		pw=res.getWriter();
		res.setContentType("text/html");
		
		ses=req.getSession();
		int agent_id=Integer.parseInt(ses.getAttribute("id").toString());
		service=new AgntServiceImpl();
		List<MyCustomerDetailsByAgent> agent=null;
		try {
			agent=service. getMyCustomerDetails(agent_id);
			System.out.println(agent);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ses.setAttribute("mycustomer",agent );
		System.out.println(agent);
		
		res.sendRedirect("/ims/agent/pages/viewcustomerdatatable.jsp");
		
		
		
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req,res);
	}
}