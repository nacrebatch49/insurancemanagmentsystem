package com.nacre.ims.agent.service;

import java.sql.SQLException;
import com.nacre.ims.dto.AgentApprovedDTO;
import java.util.List;
import com.nacre.ims.dto.ViewCarInsurancePolicyByAgentDTO;
import com.nacre.ims.bean.AgentViewProfileBean;
import com.nacre.ims.bean.MyCustomerDetailsByAgent;
import com.nacre.ims.dto.AgentEditProfileDto;
import com.nacre.ims.dto.AgentRegistartionDto;
import com.nacre.ims.dto.CustomerMeetingScheduleDTO;
import com.nacre.ims.dto.CustomrMeetingSchduleDTO;
import com.nacre.ims.dto.GetHomeInsuranceDTO;

import com.nacre.ims.dto.ViewAgent_ChildCareInsurenceDataDTO;
import com.nacre.ims.dto.View_Agent_LifeInsurenceDataDTO;

public interface AgentService {
	public int agentApprove(AgentApprovedDTO dto) throws SQLException;

	public List<View_Agent_LifeInsurenceDataDTO> agentFetchLifeInsurence()throws Exception;
	public List<ViewAgent_ChildCareInsurenceDataDTO> agentFetchChildInsurence() throws Exception;
	public List<GetHomeInsuranceDTO> getHomeInsurence();
	public    List<ViewCarInsurancePolicyByAgentDTO>  getAllCarPolicy();;
	public List<MyCustomerDetailsByAgent> getMyCustomerDetails(int agent_id) throws ClassNotFoundException,SQLException;
	public int fixingSchedulMeeting(CustomerMeetingScheduleDTO dto,int agent_id)throws Exception;
	public AgentViewProfileBean agentViewProfileService(int agent_id) throws ClassNotFoundException, SQLException;

	public String editProfile(AgentEditProfileDto dto) throws ClassNotFoundException, SQLException;
	public String checkStatus(int agent_approved_id) throws SQLException;

	public List<String> getAgentData(int agent_approve_id) throws SQLException;

	public String agentRegister(AgentRegistartionDto dto) throws SQLException;
		
	/*public int fixingSchedulMeeting(CustomrMeetingSchduleDTO dto, int agent_id, int customer_id) throws Exception;
*/
	
	

}

