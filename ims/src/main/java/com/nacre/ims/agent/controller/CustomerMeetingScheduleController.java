package com.nacre.ims.agent.controller;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;
import com.nacre.ims.commonsutil.SendMail;
import com.nacre.ims.dto.CustomerMeetingScheduleDTO;
@WebServlet("/CustomerMeetingScheduleControllerurl")
public class CustomerMeetingScheduleController extends HttpServlet {
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession ses=req.getSession();
		//int id=Integer.parseInt(ses.getAttribute("id").toString());
		String venue =null;
		String desc=null;
		String date =null;
		String time =null;
		AgentService service=null;
		int agnt_id=Integer.parseInt(ses.getAttribute("id").toString());
		
		String[] s=req.getParameterValues("vehicle");
		System.out.println(Arrays.toString(s));
	try{	
		venue=req.getParameter("venue");
		desc=req.getParameter("description");
		date=req.getParameter("date");
		time=req.getParameter("time");
		

		CustomerMeetingScheduleDTO dto=new CustomerMeetingScheduleDTO();
		dto.setVenue(venue);
		dto.setDesc(desc);
		dto.setDate(date);
		dto.setTime(time);
		dto.setEmail(s);
		service=new AgntServiceImpl();
		int result=service.fixingSchedulMeeting(dto,agnt_id);
		boolean b=false;
		String msg="Your meeting is secheduled with your agent on "+date+" timming is"+time+" ,venue is"+venue+ " This mail is send by nacre Software service dont forward anything" ; 
		if(result>0){
			for(String s1:s){
				b=SendMail.send(desc, s1, "nikhilkmr214@gmail.com", "nikhilkmr214@", msg);
			}
		}
		else{
			
		}
		if(b) {
			res.sendRedirect("/ims/MyCustomerDetailsByAgentControllerurl");
		}
		
	}catch(Exception e){
		e.printStackTrace();
		
	}
		
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
