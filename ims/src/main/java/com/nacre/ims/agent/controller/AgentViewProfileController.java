
package com.nacre.ims.agent.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;
import com.nacre.ims.bean.AgentViewProfileBean;

@WebServlet("/agentViewProfileController")
public class AgentViewProfileController extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		AgentService service=null;
		RequestDispatcher rd=null;
		service=new AgntServiceImpl();
		
		
		HttpSession ses=req.getSession();
	
		int agent_id=Integer.parseInt(ses.getAttribute("id").toString());
		try {
			AgentViewProfileBean bean=service.agentViewProfileService(agent_id);
			
			ses.setAttribute("viewinfo", bean);
			System.out.println("controller"+bean);
			
			res.sendRedirect("/ims/agent/pages/agentViewProfile.jsp");
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}
}