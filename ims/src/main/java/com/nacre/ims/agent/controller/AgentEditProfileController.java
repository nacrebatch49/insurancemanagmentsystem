package com.nacre.ims.agent.controller;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;
import com.nacre.ims.dto.AgentEditProfileDto;
@WebServlet("/agentEditController")
public class AgentEditProfileController extends HttpServlet {
public AgentEditProfileController() {
	System.out.println("AgentEditProfileController.AgentEditProfileController()");
}
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String agent_name,qualification, local_address= null;
		long mobile_number = 0;
		int year_of_passout, pincode = 0;
		int city_id=0;
		float percentage = 0f;
		/*int agent_id=0;
		HttpSession ses=req.getSession(true);
		agent_id=ses.getAttribute("");*/
		int agent_id=2;
		
		AgentService service=null;
		AgentEditProfileDto dto=null;
		
		
		agent_name=req.getParameter("name");
		qualification=req.getParameter("qualification");
		percentage=Float.parseFloat(req.getParameter("percentage"));
		year_of_passout=Integer.parseInt(req.getParameter("yop"));
		mobile_number=Long.parseLong(req.getParameter("mobileno"));
		local_address=req.getParameter("address");
		pincode=Integer.parseInt(req.getParameter("pincode"));
		city_id=Integer.parseInt(req.getParameter("city"));
		
		
		dto=new AgentEditProfileDto();
		dto.setAgent_name(agent_name);
		dto.setQualification(qualification);
		dto.setPercentage(percentage);
		dto.setYear_of_passout(year_of_passout);
		dto.setMobile_number(mobile_number);
		dto.setLocal_address(local_address);
		dto.setPincode(pincode);
		dto.setCity_id(city_id);
		
		dto.setAgent_id(agent_id);
		System.out.println(dto);
		
		service=new AgntServiceImpl();
		try {
			String result=service.editProfile(dto);
			req.setAttribute("result", result);
			System.out.println(result);
			res.sendRedirect("/ims/agentViewProfileController");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		

	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
