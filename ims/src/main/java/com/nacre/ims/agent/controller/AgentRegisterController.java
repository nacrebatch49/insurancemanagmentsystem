package com.nacre.ims.agent.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;
import com.nacre.ims.commonsutil.SendMail;
import com.nacre.ims.dto.AgentRegistartionDto;

@WebServlet("/AgentRegisterController")

public class AgentRegisterController extends HttpServlet {
public AgentRegisterController() {
	System.out.println("AgentRegisterController.AgentRegisterController()");
}
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		AgentRegistartionDto dto = null;
		AgentService service = null;
		HttpSession ses=req.getSession(true);
		String agent_name = (String) ses.getAttribute("agent_name");
		float percentage = 0f;
		int year_of_passout = 0;
		String dob = null;
		long mobile_number = 0;
		String gender = null;
		long adhaar_card_no = 0;
		long pancard_no = 0;
		String date_of_joining = null;
		int address = 0;
		 
		int login_id = 0;
		String local_address = null;
		int city_id = 0;
		String user_username = null;
		String user_password = null;
		int pincode= 0;
		
		String emailId=(String) ses.getAttribute("emailId");
		percentage = Float.parseFloat(req.getParameter("percentage1"));
		year_of_passout = Integer.parseInt(req.getParameter("yop"));
		dob = req.getParameter("dob");
		mobile_number = Long.parseLong(req.getParameter("mobileno"));
		gender = req.getParameter("doj");
		adhaar_card_no = Long.parseLong(req.getParameter("adhar"));
		pancard_no = Long.parseLong(req.getParameter("pan"));
		date_of_joining = req.getParameter("doj");

		local_address = req.getParameter("address");
		city_id = Integer.parseInt(req.getParameter("city"));
		System.out.println(agent_name);
		String str1=agent_name.substring(0, 3);
		String str2=Long.toString(mobile_number).substring(7, 10);
		user_username = str1 + str2;
		System.out.println(user_username);
		Integer pass = (int) (Math.random() * 100000);
		user_password = pass.toString();
		
		pincode=Integer.parseInt(req.getParameter("pincode"));
		dto = new AgentRegistartionDto();
		dto.setAgent_name(agent_name);
		dto.setPercentage(percentage);
		dto.setYear_of_passout(year_of_passout);
		dto.setDob(dob);
		dto.setMobile_number(mobile_number);
		dto.setGender(gender);
		dto.setAdhaar_card_no(adhaar_card_no);
		dto.setPancard_no(pancard_no);
		dto.setDate_of_joining(date_of_joining);
		dto.setAgent_approved_id(emailId);
		dto.setLocal_address(local_address);
		dto.setCity_id(city_id);
		dto.setUser_username(user_username);
		dto.setUser_password(user_password);
		dto.setPincode(pincode);
		dto.setMailid(emailId);
		service=new AgntServiceImpl();
		try {
		
			String result=service.agentRegister(dto);
			//mailSent(user_username,user_password,emailId);
			SendMail.send("username and password", emailId, "nikhilkmr214@gmail.com", "nikhilkmr214@", "your username "+user_username+" and password is "+user_password);
			ses.setAttribute("msg", "Check your mail for approval id");
			res.sendRedirect("/ims/commons/pages/Login.jsp");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		doGet(req, res);
	}
	
	
	
	
	
	

}
