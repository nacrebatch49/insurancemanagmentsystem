package com.nacre.ims.customer.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.nacre.ims.bo.ClaimMoneyByCustomerBO;
import com.nacre.ims.commonsutil.ConnectionUtil;
import com.nacre.ims.customer.dao.ClaimMoneyByCustomerDAO;
import com.nacre.ims.customer.dbutils.CustomerDbUtils;

public class ClaimMoneyByCustomerDAOImpl implements ClaimMoneyByCustomerDAO {

		Connection connection=null;
		PreparedStatement preparedStatement=null;
		
		
	@Override
	public int updateClaimMoneyByCustomerDao(ClaimMoneyByCustomerBO bo) {
		String claimId=null;
		int result =0;
		try {
		connection=ConnectionUtil.getConnection();
			preparedStatement=connection.prepareStatement(CustomerDbUtils.INSERT_INTO_CLAIMMONEYBYCUSTOMER);
			preparedStatement.setString(1, claimId);
			preparedStatement.setString(2, bo.getPolicId());
			preparedStatement.setString(3, bo.getNomineeName());
			preparedStatement.setString(4, bo.getEmail());
			preparedStatement.setString(5, bo.getRelation());
			preparedStatement.setString(6, bo.getReason());
			preparedStatement.setDate(7, bo.getDateOfClaim());
			
			result=preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return result;
	}
	private static String generateClaimId(String claimid){
		
		return claimid;
		
	}
}
