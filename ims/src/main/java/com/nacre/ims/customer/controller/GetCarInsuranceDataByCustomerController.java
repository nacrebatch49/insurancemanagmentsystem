/**
 * 
 */
package com.nacre.ims.customer.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.customer.serviceimpl.CustomerServiceImpl;

import com.nacre.ims.dto.ViewCarInsurancePolicyByCustomerDTO;

/**
 * @author Barkha
 *
 */
@WebServlet("/getCarInsuranceDataControllerByCustomer")
public class GetCarInsuranceDataByCustomerController extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside view details");
		resp.setContentType("text/html");
		CustomerService service=new CustomerServiceImpl();
		List<ViewCarInsurancePolicyByCustomerDTO> list=service.getAllCarPolicy();
		System.out.println(list);
		HttpSession ses=req.getSession();
		ses.setAttribute("list", list);
		resp.sendRedirect("/ims/customer/pages/CarInsueCustomer.jsp");
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
