package com.nacre.ims.customer.dao;

import com.nacre.ims.bo.ClaimMoneyByCustomerBO;

public interface ClaimMoneyByCustomerDAO {
	public int updateClaimMoneyByCustomerDao(ClaimMoneyByCustomerBO bo);

}
