package com.nacre.ims.customer.serviceimpl;

import com.nacre.ims.bo.ClaimMoneyByCustomerBO;
import com.nacre.ims.customer.dao.ClaimMoneyByCustomerDAO;
import com.nacre.ims.customer.daoimpl.ClaimMoneyByCustomerDAOImpl;
import com.nacre.ims.customer.service.ClaimMoneyByCustomerService;
import com.nacre.ims.dto.ClaimMoneyByCustomerDTO;

public class ClaimMoneyByCustomerServiceImpl implements ClaimMoneyByCustomerService {
	ClaimMoneyByCustomerDAO dao=null;
	ClaimMoneyByCustomerBO bo=null;
	@Override
	public int updateClaimMoneyByCustomerService(ClaimMoneyByCustomerDTO dto) {
		dao=new ClaimMoneyByCustomerDAOImpl();
		bo=new ClaimMoneyByCustomerBO();
		//Coverting dato class to bo class
		
		bo.setPolicId(dto.getPolicId());
		bo.setNomineeName(dto.getNomineeName());
		bo.setEmail(dto.getEmail());
		bo.setRelation(dto.getRelation());
		bo.setReason(dto.getReason());
		bo.setDateOfClaim(dto.getDateOfClaim());
		
		int result=dao.updateClaimMoneyByCustomerDao(bo);
		return result;
	}

}
