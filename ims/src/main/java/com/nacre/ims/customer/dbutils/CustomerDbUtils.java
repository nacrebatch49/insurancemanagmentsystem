package com.nacre.ims.customer.dbutils;


public class CustomerDbUtils {
	
	public static final String INSERT_CHILDCARE_DETAILS = "INSERT INTO customer_child_care_insurance_register_table(child_name,child_age,birthcertificate_no,parents_name,date_of_insurance,payment_type,customer_id,child_care_inusrance_id) values(?,?,?,?,?,?,?,?) ";
	public static final String INSERT_CHILDCARE_POLICY="insert into insurancepolicies_table (insurancepolicies_table.policy_name,insurancepolicies_table.customer_id1,insurancepolicies_table.customer_register_policy_id)values(?,?,?)";
	public static final String INSERT_CAR_DETAILS = "INSERT INTO customer_registered_car_insurance_policies(insurance_policy_no,car_number,purchage_date,rc_no,nominies_name,nominies_mobile_no,nominies_relation,max_limit,emi_option_type,customer_id,car_insurance_master_id) values(?,?,?,?,?,?,?,?,?,?,?) ";

	public static final String INSERT_INTO_CLAIMMONEYBYCUSTOMER="INSERT INTO CUSTOMER_CLAIM_REQUEST_TABLE(register_policy_no,nominies_name,email,relation,reason,date_of_claim) VALUES(?,?,?,?,?,?)";
	
	public static final String str="select customer_dob,customer_email_id from customer_registration_table where costomer_register_id=?" ;
    public static final String str1="insert into customer_life_insurance_register_table(insurance_name,max_limit,nominies_name,nominies_email_id,relation,emi_option_type,register_policy_no,date_of_policy,customer_id,life_insurance_id)values(?,?,?,?,?,?,?,?,?,?)";
	public static final String str2="select life_isn_id from life_insurance_master_table";
    public static final String str3="insert into insurancepolicies_table (policy_name,customer_id1,customer_register_policy_id) values(?,?,?)";
	public static final String str4="select life_insurance_master_table.`total_amount_paid` from life_insurance_master_table";
//    public static final String getCarDetailsPolicy="select car.name,car.coverage,car.description,car.rate_of_interest,car.duration,car.payble_amount,car.initial_amount, car.car_insurance_id from car_insurance_master_table car where car.status_id=(select status_table.status_id from status_table where status_table.status_name='WORKING')";
  //  public static final String GET_HOME_POLIVY_BY_CUSTOMER="select name,max_limit,coverage,description,total_amount_paid,rate_of_interest,initial_amount,home_insurance_offer,policy_no,home_insurance_id from home_insurance_master_table where status_id=( select status_id from status_table where status_name='WORKING')";
/*
	public static final String str="select customer_dob,customer_email_id from customer_registration_table where costomer_register_id=?" ;
    public static final String str1="insert into customer_life_insurance_register_table(insurance_name,max_limit,nominies_name,nominies_email_id,relation,emi_option_type,register_policy_no,date_of_policy,customer_id,life_insurance_id)values(?,?,?,?,?,?,?,?,?,?)";
	public static final String str2="select life_isn_id from life_insurance_master_table";
    public static final String str3="insert into insurancepolicies_table (policy_name,customer_id1,customer_register_policy_id) values(?,?,?)";
	public static final String str4="select life_insurance_master_table.`total _amount_paid` from life_insurance_master_table";*/
	
	public static final String GET_ALL_CUSTOMER_POLICY_IDS="select policy_name,customer_register_policy_id from insurancepolicies_table where customer_id1=?";
	public static final String Get_DOB = "select customer_dob,customer_email_id from customer_registration_table where costomer_register_id=?";
	public static final String SET_CUSTOMER_REGISTER= "insert into customer_life_insurance_register_table(insurance_name,max_limit,nominies_name,nominies_email_id,relation,emi_option_type,register_policy_no,date_of_policy,customer_id,life_insurance_id)values(?,?,?,?,?,?,?,?,?,?)";
	public static final String SET_INSURANCE_POLICIES= "insert into insurancepolicies_table (policy_name,customer_id1,customer_register_policy_id) values(?,?,?)";
	public static final String GET_AMOUNT= "select life_insurance_master_table.`total_amount_paid` from life_insurance_master_table";
	public static final String getCarDetailsPolicy = "select car.name,car.coverage,car.description,car.rate_of_interest,car.duration,car.payble_amount,car.initial_amount, car.car_insurance_id from car_insurance_master_table car where car.status_id=(select status_table.status_id from status_table where status_table.status_name='WORKING')";
	public static final String GET_HOME_POLIVY_BY_CUSTOMER = "select name,max_limit,coverage,description,total_amount_paid,rate_of_interest,initial_amount,home_insurance_offer,policy_no,home_insurance_id from home_insurance_master_table where status_id=( select status_id from status_table where status_name='WORKING')";
    public static final String Get_CUSTOMER_POLICIES="select policy_id,policy_name,customer_register_policy_id from insurancepolicies_table";
    public static final String GET_CUSTOMER_LIFE_POLICY="select d.insurance_name,max_limit,nominies_name,nominies_email_id,relation,register_policy_no,date_of_policy,pay_service_type,m.`total_amount_paid` from customer_life_insurance_register_table d,emi_option_table e,life_insurance_master_table m where d.emi_option_type=e.emi_id and d.life_insurance_id=m.life_isn_id";
    public static final String GET_CUSTOMER_CAR_POLICY="select insurance_policy_no,car_number,purchage_date,rc_no,nominies_name,nominies_mobile_no,nominies_relation,max_limit,pay_service_type,payble_amount from customer_registered_car_insurance_policies d,emi_option_table e,car_insurance_master_table m where d.car_insurance_master_id=m.car_insurance_id and d.emi_option_type=e.emi_id;";
    public static final String GET_CUSTOMER_CHILD_POLICY="select child_name,child_age,birthcertificate_no,parents_name,date_of_insurance,pay_service_type,payable_amount,register_policy from child_care_insurance_master_table m,customer_child_care_insurance_register_table d,emi_option_table e where e.emi_id=d.payment_type and d.child_care_inusrance_id=m.child_care_insurance_id";
	public static final String GET_CUSTOMER_HOME_POLICY="select register_policy_no,home_addres,square_fit,home_no,max_duration,nominies_name,nominies_email_id,relation,date_of_policies,total_amount_paid,pay_service_type from home_insurance_master_table m,customer_register_home_insuranc_table d,emi_option_table e where e.emi_id=d.emi_option_type1 and m.home_insurance_id=d.home_insurance_id";

	public static final String SELECT_CUSTOMER = "select c.customer_first_name,c.customer_last_name,"
			+ "c.customer_email_id,c.customer_mobile_no,c.customer_gender,c.customer_dob,"
			+ "c.adhar_no,add1.local_address,add1.pincode,city.city_name,"
			+ "state.state_name,country.country_name from customer_registration_table c,"
			+ "address_table add1,country_master_table country,state_master_table state,city_master_table city "
			+ "where add1.address_id=c.address_id and city.city_id=add1.city_id "
			+ "and state.state_id=city.state_id and country.country_id=state.country_id "
			+ "and c.costomer_register_id=?";
	public static final String update_customer = "update customer_registration_table c,address_table add1 set c.customer_first_name=?,c.customer_last_name=?,c.customer_mobile_no=?,add1.local_address=?,add1.city_id=?,add1.pincode=? where c.address_id=add1.address_id and c.costomer_register_id=?";

	public static final String INSERT_LOGIN_DETAILS="insert into login_table(login_table.user_username,login_table.user_password,login_table.role_id) values(?,?,(select role_table.role_id from role_table where role_table.role_name='customer'))";
	public static final String INSERT_ADDRESS_DETAILS="insert into address_table (address_table.local_address,address_table.pincode,address_table.city_id) values(?,?,?)";
	public static final String INSERT_CUSTOMER_REGISTRATION="insert into customer_registration_table(customer_registration_table.customer_first_name,customer_registration_table.customer_last_name,customer_registration_table.customer_email_id,customer_registration_table.customer_mobile_no,customer_registration_table.customer_gender,customer_registration_table.customer_dob,customer_registration_table.adhar_no,customer_registration_table.address_id,customer_registration_table.agent_id,customer_registration_table.login_id,customer_registration_table.status_id) values(?,?,?,?,?,?,?,?,?,?,(select status_table.status_id from status_table where status_table.status_name='OURCUSTOMER' ))";
}
 