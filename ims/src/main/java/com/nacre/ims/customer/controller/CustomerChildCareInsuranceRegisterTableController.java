package com.nacre.ims.customer.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.customer.serviceimpl.CustomerServiceImpl;
import com.nacre.ims.dto.CustomerChildCareInsuranceRegisterTableDTO;

@WebServlet("/childcareinsuranceurl")
public class CustomerChildCareInsuranceRegisterTableController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		System.out.println("inside controller class");
		HttpSession ses =null;
		            ses = req.getSession();

		// general settings

		res.setContentType("text/html");
		PrintWriter pw = null;
		pw = res.getWriter();
		String childName = null, birthCertificate_No = null, parentName = null, date_Of_Insurance = null,
				paymentType = null;
		int childAge = 0, childCareInusranceId = 0;
		int result = 0;

		RequestDispatcher rd = null;
		// reading form data

		childName = req.getParameter("childName");
		childAge = Integer.parseInt(req.getParameter("childAge"));
		birthCertificate_No = req.getParameter("birthCertificate");
		parentName = req.getParameter("parentName");
		date_Of_Insurance = req.getParameter("dateInsurance");
		paymentType = req.getParameter("pay");
		 childCareInusranceId=Integer.parseInt(req.getParameter("childcareid"));
		 
		 System.out.println("childcareid........."+childCareInusranceId);

		//childCareInusranceId = 1;

		// setting static customer_id

		int cust_id = 1;

		// creating DTO Object

		CustomerChildCareInsuranceRegisterTableDTO childCareDto = null;
		childCareDto = new CustomerChildCareInsuranceRegisterTableDTO();

		// setting values to dto object

		childCareDto.setChild_Name(childName);
		childCareDto.setChild_Age(childAge);
		childCareDto.setBirthCertificate_No(birthCertificate_No);
		childCareDto.setParents_Name(parentName);
		childCareDto.setDate_of_Insurance(date_Of_Insurance);
		childCareDto.setPayment_Type(paymentType);
		childCareDto.setChildCareInusranceId(childCareInusranceId);

		// creating service object

		CustomerService childCareService = new CustomerServiceImpl();

		System.out.println("dto values............" + childCareDto);

		try {
			result = childCareService.insertIntoCustomerChildCareInsuranceDetails(childCareDto, cust_id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (result != 0) {

			res.sendRedirect("/ims/customer/pages/home.jsp");
			//rd.forward(req, res);
		} else {
			res.sendRedirect("/ims/customer/pages/childcareinsurance.jsp");
		}
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
