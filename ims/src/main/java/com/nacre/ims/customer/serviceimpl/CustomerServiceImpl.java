package com.nacre.ims.customer.serviceimpl;

import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.nacre.ims.bean.CustomerViewProfileBean;

//import org.apache.commons.beanutils.BeanUtils;

import com.nacre.ims.bo.CustomerCarInsuranceRegisterTableBO;
import com.nacre.ims.bo.CustomerChildCareInsuranceRegisterTableBO;
import com.nacre.ims.bo.CustomerEditProfileBo;
import com.nacre.ims.bo.CustomerLifeRegisterBo;
import com.nacre.ims.bo.CustomerRegistrationBO;
import com.nacre.ims.bo.CustomerViewCarInsuranceBO;
import com.nacre.ims.bo.CustomerViewChildCareInsuranceBO;
import com.nacre.ims.bo.CustomerViewMyHomeInsurance;
import com.nacre.ims.bo.CustomerViewMyLifeInsurance;
import com.nacre.ims.bo.CustomerViewMyPolicyBO;
import com.nacre.ims.bo.GetHomeInsuranceBO;
import com.nacre.ims.bo.ViewCarInsuerancePolicyByCustomerBo;
import com.nacre.ims.bo.ViewCustomer_ChildCareInsurenceDataBO;
import com.nacre.ims.bo.View_Customer_LifeInsurenceDataBO;
import com.nacre.ims.commonsutil.SendMail;
import com.nacre.ims.customer.dao.CustomerDao;
import com.nacre.ims.customer.daoimpl.CustomerDaoImpl;
import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.dto.CustomerCarInsuranceRegisterTableDTO;
import com.nacre.ims.dto.CustomerChildCareInsuranceRegisterTableDTO;
import com.nacre.ims.dto.CustomerEditProfileDto;
import com.nacre.ims.dto.CustomerLifeRegisterdto;
import com.nacre.ims.dto.CustomerRegistrationDTO;
import com.nacre.ims.dto.GetHomeInsuranceDTO;
import com.nacre.ims.dto.LoginDTO;
import com.nacre.ims.dto.ViewCarInsurancePolicyByCustomerDTO;
import com.nacre.ims.dto.ViewCustomer_ChildCareInsurenceDataDTO;
import com.nacre.ims.dto.View_Customer_LifeInsurenceDataDTO;

public class CustomerServiceImpl implements CustomerService {

	// CustomerDao dao = new CustomerDaoImpl();

	@Override
	public int insertIntoCustomerChildCareInsuranceDetails(CustomerChildCareInsuranceRegisterTableDTO childCareDto,
			int cust_id) throws SQLException {

		int count = 0;

		// creating BO object

		CustomerChildCareInsuranceRegisterTableBO childCareBo = null;

		childCareBo = new CustomerChildCareInsuranceRegisterTableBO();

		// setting bo object values

		childCareBo.setChild_Name(childCareDto.getChild_Name());
		childCareBo.setChild_Age(childCareDto.getChild_Age());
		childCareBo.setBirthCertificate_No(childCareDto.getBirthCertificate_No());
		childCareBo.setParents_Name(childCareDto.getParents_Name());
		childCareBo.setDate_of_Insurance(childCareDto.getDate_of_Insurance());
		childCareBo.setPayment_Type(childCareDto.getPayment_Type());
		childCareBo.setChildCareInusranceId(childCareDto.getChildCareInusranceId());

		// creating dao object

		CustomerDao childCareDao = null;

		childCareDao = new CustomerDaoImpl();

		System.out.println("bo values............" + childCareBo);

		count = childCareDao.insertIntoCustomerChildCareInsuranceDetails(childCareBo, cust_id);

		return count;
	}

	@Override
	public int insertIntoCustomerCarInsuranceDetails(CustomerCarInsuranceRegisterTableDTO carDto) throws SQLException {

		int count = 0;

		// creating BO object

		CustomerCarInsuranceRegisterTableBO carBo = null;

		carBo = new CustomerCarInsuranceRegisterTableBO();

		// setting bo object values

		/*
		 * carBo.setInsurancePolicyNo(carDto.getInsurancePolicyNo());
		 * carBo.setCarNo(carDto.getCarNo());
		 * carBo.setPurchaseDate(carDto.getPurchaseDate());
		 * carBo.setRcNo(carDto.getRcNo());
		 * carBo.setNomineeName(carDto.getNomineeName());
		 * carBo.setNomineeMobileNo(carDto.getNomineeMobileNo());
		 * carBo.setNomineeRelation(carDto.getNomineeRelation());
		 * carBo.setMaxLimit(carDto.getMaxLimit());
		 */

		// setting bo object values from dto object using
		// BeanUtils.copyProperties(destination,source); method

		try {

			BeanUtils.copyProperties(carBo, carDto);

		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}

		// creating dao object

		CustomerDao carDao = null;

		carDao = new CustomerDaoImpl();

		count = carDao.insertIntoCustomerCarInsuranceDetails(carBo);
		return count;
	}
	//CustomerRegistrationBO bo = null;

	@Override
	public int customerRegistrationService(CustomerRegistrationDTO dto, LoginDTO ldto) throws SQLException {
		int count = 0;
		CustomerRegistrationBO bo = null;
		bo = new CustomerRegistrationBO();
		bo.setCustomer_first_name(dto.getFirstName());
		bo.setCustomer_last_name(dto.getLastName());
		bo.setCustomer_email_id(dto.getEmail());
		bo.setCustomer_mobile_no(dto.getMobileNo());
		bo.setCustomer_gender(dto.getGender());
		bo.setCustomer_dob(dto.getDob());
		bo.setCustomer_adhar_no(dto.getAddharNo());
		bo.setAddress(dto.getAddress());
		bo.setCity_Id(dto.getCity_id());
		// bo.setLogin_id(dto.getLoginId());
		bo.setPincode(dto.getPincode());
		// bo.setUsername(username);
		// bo.setPassword(password);
		bo.setAgent_id(dto.getAgentId());

		bo.setUsername(ldto.getUsername());
		bo.setPassword(ldto.getPassword());

		count = dao.customerRegistration(bo);

		return count;
	}

	@Override
	public int getYear(int i) {
		CustomerDao dao = null;
		Date d = null;
		java.util.Date d1 = null;
		int year = 0;
		// create objects
		dao = new CustomerDaoImpl();
		d1 = new java.util.Date();
		d = (Date) dao.getYear(i);
		// d = dao.getYear(i);
		year = d1.getYear() - d.getYear();
		return year;
		
	}

	@Override
	public int registerLife(CustomerLifeRegisterdto dto) {
		CustomerLifeRegisterBo bo = null;
		CustomerDao dao = null;
		float payment = 0.0f;
		// create the objects
		bo = new CustomerLifeRegisterBo();
		dao = new CustomerDaoImpl();
		bo.setInsuranceName(dto.getInsuranceName());
		bo.setMaxlimit(dto.getMaxlimit());
		bo.setNomineeName(dto.getNomineeName());
		bo.setNomineeEmail(dto.getNomineeEmail());
		bo.setNomineeRelation(dto.getNomineeRelation());
		bo.setPaymenttype(dto.getPaymenttype());
		int i = (int) (Math.random() * 6123123);
		bo.setRegister_policy_id(String.valueOf(i));
		bo.setDate_of_policy(new Date((new java.util.Date()).getTime()));
		bo.setCust_id(dto.getCust_id());
		bo.setLifeInsuranceid(dto.getLifeInsuranceid());
		Object[] obj = dao.registerLife(bo);
		System.out.println(Arrays.toString(obj));

		bo.setNomineeRelation(dto.getNomineeRelation());
		bo.setPaymenttype(dto.getPaymenttype());
		//int i = (int) (Math.random() * 6123123);
		bo.setRegister_policy_id(String.valueOf(i));
		bo.setDate_of_policy(new Date((new java.util.Date()).getTime()));
		bo.setCust_id(dto.getCust_id());
		bo.setLifeInsuranceid(dto.getLifeInsuranceid());
		//Object[] obj = dao.registerLife(bo);
		System.out.println(Arrays.toString(obj));

		int result = (int) obj[0];
		payment = result / bo.getMaxlimit();
		if (bo.getPaymenttype() == 1) {
			payment = payment * 1;
		} else if (bo.getPaymenttype() == 2) {
			payment = payment * 3;
		} else if (bo.getPaymenttype() == 3) {
			payment = payment * 6;
		} else {
			payment = payment * 12;
		}
		// sending the email
		boolean b = SendMail.send("Registered successfully for Life Insurance", (String) obj[1],
				"vishnuarrora@gmail.com", "9981836213",
				"Hello, Your registration for life insurance has been succedded, your policy id is "
						+ bo.getRegister_policy_id()
						+ " and payment has to be made according to the payment mode with in time. Payment is" + payment
						+ "0Rs");
		System.out.println(b);
		return result;
	}

	@Override
	public List<View_Customer_LifeInsurenceDataDTO> customerFetchLifeInsurence() throws Exception {
		CustomerDao dao = null;
		View_Customer_LifeInsurenceDataDTO dto = null;
		List<View_Customer_LifeInsurenceDataDTO> list = new ArrayList();

		List<View_Customer_LifeInsurenceDataBO> bo1 = null;
		// set property DTO to BO
		dao = new CustomerDaoImpl();
		bo1 = dao.customerFetchLifeInsurence();
		for (View_Customer_LifeInsurenceDataBO bo2 : bo1) {
			dto = new View_Customer_LifeInsurenceDataDTO();
			dto.setName(bo2.getName());
			dto.setDuration(bo2.getDuration());
			dto.setInsurance_policy_no(bo2.getInsurance_policy_no());
			dto.setRate_of_interest(bo2.getRate_of_interest());
			dto.setDesciption(bo2.getDesciption());
			dto.setTotal_amount_paid(bo2.getTotal_amount_paid());
			dto.setLife_insurance_offers(bo2.getLife_insurance_offers());
			dto.setCovarage(bo2.getCovarage());
			dto.setStatus(bo2.getStatus());
			dto.setLifeid(bo2.getLifeid());
			list.add(dto);
		}

		return list;
	}

	@Override
	public List<ViewCustomer_ChildCareInsurenceDataDTO> customerFetchChildInsurence() throws Exception {
		CustomerDao dao = null;
		ViewCustomer_ChildCareInsurenceDataDTO dto = null;
		List<ViewCustomer_ChildCareInsurenceDataDTO> list = new ArrayList<>();
		List<ViewCustomer_ChildCareInsurenceDataBO> bo1 = null;
		dao = new CustomerDaoImpl();
		bo1 = dao.customerFetchChildCareInsurence();
		for (ViewCustomer_ChildCareInsurenceDataBO bo2 : bo1) {
			dto = new ViewCustomer_ChildCareInsurenceDataDTO();
			BeanUtils.copyProperties(dto, bo2);
			list.add(dto);
			System.out.println(dto);
		}
		return list;
	}

	public List<ViewCarInsurancePolicyByCustomerDTO> getAllCarPolicy() {
		CustomerDao dao = new CustomerDaoImpl();
		List<ViewCarInsurancePolicyByCustomerDTO> listdto = new ArrayList();
		List<ViewCarInsuerancePolicyByCustomerBo> list = dao.getAllCarPolicy();
		for (ViewCarInsuerancePolicyByCustomerBo bo : list) {
			ViewCarInsurancePolicyByCustomerDTO dto = new ViewCarInsurancePolicyByCustomerDTO();
			dto.setCarIncId(bo.getCarIncId());
			dto.setPolicyName(bo.getPolicyName());
			dto.setCoverage(bo.getCoverage());
			dto.setPolicyDescription(bo.getPolicyDescription());
			dto.setRateofInterest(bo.getRateofInterest());
			dto.setDuration(bo.getDuration());
			dto.setPaybleAmount(bo.getPaybleAmount());
			dto.setInitialAmount(bo.getInitialAmount());
			listdto.add(dto);

		}
		return listdto;

	}

	CustomerDao dao = new CustomerDaoImpl();
	GetHomeInsuranceDTO getHomeInsuranceDTO = null;
	GetHomeInsuranceBO getHomeInsuranceBO = null;

	@Override
	public List<GetHomeInsuranceDTO> getHomeInsurence() {
		List<GetHomeInsuranceDTO> listDto = null;
		List<GetHomeInsuranceBO> listBo = null;

		listDto = new ArrayList<>();
		listBo = dao.getHomeInsurence();
		for (GetHomeInsuranceBO getHomeInsuranceBO : listBo) {
			getHomeInsuranceDTO = new GetHomeInsuranceDTO();
			getHomeInsuranceDTO.setPolicyName(getHomeInsuranceBO.getPolicyName());
			getHomeInsuranceDTO.setCoverage(getHomeInsuranceBO.getCoverage());
			getHomeInsuranceDTO.setDescription(getHomeInsuranceBO.getDescription());
			getHomeInsuranceDTO.setDuration(getHomeInsuranceBO.getDuration());
			getHomeInsuranceDTO.setInitialAmount(getHomeInsuranceBO.getInitialAmount());
			getHomeInsuranceDTO.setTotalamount(getHomeInsuranceBO.getTotalamount());
			getHomeInsuranceDTO.setRateOfInterest(getHomeInsuranceBO.getRateOfInterest());
			getHomeInsuranceDTO.setPolicyOffer(getHomeInsuranceBO.getPolicyOffer());
			getHomeInsuranceDTO.setPolicyNumber(getHomeInsuranceBO.getPolicyNumber());
			getHomeInsuranceDTO.setHomeInsuranceId(getHomeInsuranceBO.getHomeInsuranceId());
			listDto.add(getHomeInsuranceDTO);
		}

		return listDto;
	}

	@Override
	public CustomerViewProfileBean customerViewProfileService(int customer_id)
			throws ClassNotFoundException, SQLException {
		CustomerViewProfileBean bean = new CustomerViewProfileBean();
		bean = dao.CustomerViewProfile(customer_id);
		return bean;
	}

	@Override
	public String customerEditProfile(CustomerEditProfileDto dto) throws ClassNotFoundException, SQLException {
		CustomerEditProfileBo bo = null;
		bo = new CustomerEditProfileBo();
		bo.setCustomer_first_name(dto.getCustomer_first_name());
		bo.setCustomer_last_name(dto.getCustomer_last_name());
		bo.setCustomer_mobile_no(dto.getCustomer_mobile_no());
		bo.setLocal_address(dto.getLocal_address());
		bo.setCity_id(dto.getCity_id());
		bo.setPincode(dto.getPincode());
		bo.setCostomer_register_id(dto.getCostomer_register_id());

		int count = dao.customerEditProfle(bo);
		if (count == 0) {
			return "profile is not updated";
		} else {

			return "Profile is updated sucessfully";
		}
	}
	@Override
	public List GetMyPolicies(int cust_id) {
		CustomerDao dao=null;
		List<CustomerViewMyPolicyBO> list=null;
		List listbo=new ArrayList();
		List<CustomerViewMyLifeInsurance>  life=new ArrayList();
		List<CustomerViewMyHomeInsurance> home=new ArrayList();
		List<CustomerViewCarInsuranceBO> car=new ArrayList();
		List<CustomerViewChildCareInsuranceBO> child=new ArrayList();
		//create the objects
		dao=new CustomerDaoImpl();
		list=dao.viewMyPolicyDAO();
		System.out.println("Entered into service");
		for(CustomerViewMyPolicyBO bo:list) {

			if((bo.getPolicy_name()).equalsIgnoreCase("Life Insurance")){
	    CustomerViewMyLifeInsurance bo1=dao.getCustomerLifeInsurance(bo.getCustomer_register_policy_id());
			 life.add(bo1);
			}
			else if((bo.getPolicy_name()).equalsIgnoreCase("Home Insurance")) {
		 CustomerViewMyHomeInsurance bo2=dao.getCustomerHomeInsurance(bo.getCustomer_register_policy_id());
		    home.add(bo2);
			}
			else if((bo.getPolicy_name()).equalsIgnoreCase("Car Insurance")) {
		  CustomerViewCarInsuranceBO bo3=dao.getCustomerCarInsurance(bo.getCustomer_register_policy_id());
		  car.add(bo3);
			}
			else if((bo.getPolicy_name()).equalsIgnoreCase("Child Insurance")) {
		CustomerViewChildCareInsuranceBO bo4=dao.getCustomerChildInsurance(bo.getCustomer_register_policy_id());
		child.add(bo4);
			}
			
		}
		listbo.add(life);
		listbo.add(home);
		listbo.add(car);
		listbo.add(child);
		return listbo;
	}


}
