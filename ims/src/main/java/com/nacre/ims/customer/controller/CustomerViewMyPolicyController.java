package com.nacre.ims.customer.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.customer.serviceimpl.CustomerServiceImpl;

@WebServlet("/viewmypolicy")
public class CustomerViewMyPolicyController extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CustomerService service = new CustomerServiceImpl();
		HttpSession session=req.getSession(false);
		int i=(int) session.getAttribute("id");
		try {
		List listdto=service.GetMyPolicies(i);
		if(listdto!=null) {
		req.setAttribute("ViewMyPolicies",listdto);
		RequestDispatcher rd=req.getRequestDispatcher("customer/pages/viewmypolicies.jsp");
		rd.forward(req, resp);
		}}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
