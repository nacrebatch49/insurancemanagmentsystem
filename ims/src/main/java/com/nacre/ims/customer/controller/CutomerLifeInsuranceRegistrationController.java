package com.nacre.ims.customer.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.customer.serviceimpl.CustomerServiceImpl;
import com.nacre.ims.dto.CustomerLifeRegisterdto;

@WebServlet("/customerLifeReg")
public class CutomerLifeInsuranceRegistrationController extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    		CustomerLifeRegisterdto register=null;
    		CustomerService service=null;
    		HttpSession session=null;
    	    int result;
    	    String data=null;
    		//creating objects
    	    session=req.getSession();
    		register=new CustomerLifeRegisterdto();
    		service= new CustomerServiceImpl();
    		register.setInsuranceName(req.getParameter("litype"));
    		register.setMaxlimit(Integer.parseInt(req.getParameter("maxlimit")));
    		register.setNomineeName(req.getParameter("nomineename"));
    		register.setNomineeEmail(req.getParameter("nomineeEmail"));
    		register.setNomineeRelation(req.getParameter("nomineerelation"));
    		register.setPaymenttype(Integer.parseInt(req.getParameter("pay")));
    		register.setLifeInsuranceid(Integer.parseInt(req.getParameter("id")));
    		//register.setCust_id(session.getAttribute("id");
    		register.setCust_id(Integer.parseInt(session.getAttribute("id").toString()));
    		System.out.println("ids"+register.getCust_id()+"  "+ register.getLifeInsuranceid());
    		result=service.registerLife(register);
    		//System.out.println("ids"+register.getCust_id()+"  "+ register.getLifeInsuranceid());
    		HttpSession ses=req.getSession();
    		
    		if(result<0) {
    			data="Not registered successfully";
    		}
    		else {
    			data="Life Insurance Resgistered Successfully";
    		}
    		ses.setAttribute("register", data);
    		/*RequestDispatcher rd=req.getRequestDispatcher("customer/pages/home.jsp");
    		rd.forward(req, resp);*/
    		resp.sendRedirect("/ims/customer/pages/home.jsp");
    		
    	
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	doGet(req, resp);
	}

}
