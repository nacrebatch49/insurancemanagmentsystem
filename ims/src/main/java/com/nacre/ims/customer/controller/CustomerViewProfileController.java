package com.nacre.ims.customer.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;
import com.nacre.ims.bean.AgentViewProfileBean;
import com.nacre.ims.bean.CustomerViewProfileBean;
import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.customer.serviceimpl.CustomerServiceImpl;

@WebServlet("/CustomerViewProfileController")
public class CustomerViewProfileController extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		System.out.println("Inside Get Profile Controller");
		CustomerService service = null;
		
		service = new CustomerServiceImpl();
		
		  HttpSession ses=req.getSession();
		 int id=Integer.parseInt(ses.getAttribute("id").toString());
		 System.out.println("id="+id);
		int customer_id = id;
		try {
	
			CustomerViewProfileBean bean = service.customerViewProfileService(customer_id);
			System.out.println(customer_id);

			ses.setAttribute("viewinfo", bean);
			System.out.println("controller" + bean);

			res.sendRedirect("/ims/customer/pages/customerViewProfile.jsp");

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		doGet(req, res);
	}

}
