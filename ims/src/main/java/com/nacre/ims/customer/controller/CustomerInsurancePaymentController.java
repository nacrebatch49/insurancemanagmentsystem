package com.nacre.ims.customer.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.customer.service.CustomerService;

public class CustomerInsurancePaymentController extends HttpServlet {

	/**
	 * G Venu
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter pw=null;
		HttpSession session=null;
		CustomerService service=null;
		int custId=0;
		//get the printwriter
		pw=res.getWriter();
		//set the content type
		res.setContentType("text/html");
		//get the previous session
		session=req.getSession(true);
		//get the customer id from the session
		Object obj=session.getAttribute("custId");
		custId=(int)obj;
		//use service
		 
		
		
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
