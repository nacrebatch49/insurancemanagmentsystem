package com.nacre.ims.customer.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nacre.ims.customer.service.Customerfeedbackservice;
import com.nacre.ims.customer.serviceimpl.CustomerfeedbackserviceImpl;

@WebServlet("/customerfeedbackcontroller")
public class Customerfeedbackcontroller extends HttpServlet {
	public Customerfeedbackcontroller(){
		System.out.println("Customerfeedbackcontroller.Customerfeedbackcontroller()");
	}
	PrintWriter pw=null;
	String email=null;
	String msg=null;
	int result;
	Customerfeedbackservice service;
	
	RequestDispatcher rd;
	static int reg_id=1;
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		System.out.println("inside doget()");
		try{
		System.out.println("Customerfeedbackcontroller.doGet()");
		res.setContentType("text/html");
		service=new CustomerfeedbackserviceImpl();
		pw=res.getWriter();
		email=req.getParameter("email");
		msg=req.getParameter("description");
		
		System.out.println(email);
		System.out.println(msg);
		
		
		 result=service.sendFeedback(msg,email,reg_id);
		 System.out.println(result);
		if(result>0){
		
			res.sendRedirect("/ims/customer/pages/home.jsp");
     
		}
		else{
			rd=req.getRequestDispatcher("/customer/pages/error.jsp");
			rd.forward(req, res);
		}
		}catch(Exception e){
			e.printStackTrace();
		}
     
		
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
