package com.nacre.ims.customer.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.google.gson.Gson;
import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.customer.serviceimpl.CustomerServiceImpl;
@WebServlet("/getDob")
public class GetCustomerDob extends HttpServlet {
	@Override
	public  void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	 CustomerService service=null;
  	 HttpSession session=null;
     int i=0,result=0;
     Gson gson=null;
	 //creating the objects
	 service=new CustomerServiceImpl();
	 resp.setContentType("application/json");
	 session=req.getSession(true);
	 i=(int) session.getAttribute("id");
	 System.out.println(i);
	 result=service.getYear(i);
	 result=result*12;
	 gson=new Gson();
	 gson.toJson(result);
	 System.out.println(result);
	 resp.getWriter().print(result);
	 
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
     doGet(req, resp);
	}

}
