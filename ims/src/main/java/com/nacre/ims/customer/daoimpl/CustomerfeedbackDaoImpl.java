package com.nacre.ims.customer.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


import com.nacre.ims.commonsutil.ConnectionUtil;
import com.nacre.ims.customer.dao.CustomerfeedbackDao;

public class CustomerfeedbackDaoImpl implements CustomerfeedbackDao {
 private static final String SEND_FEEDBACK="insert into customer_feedback_table(feedback_name,customer_email_id,customer_register_id)values(?,?,?)";
/* private static final String SEND_FEEDBACK="insert into customer_feedback_table(feedback_name,customer_email_id)values(?,?)where customer_register_id=1";*/
	int result=0;
 PreparedStatement ps=null;
 Connection con=null;
 @Override
	public int sendFeedback(String msg,String email,int reg_id) {
	 
	 
		try {
			System.out.println("Inside DAOImpl try block.....");
			System.out.println("email="+email);
			System.out.println("msg="+msg);
			System.out.println("reg_id="+reg_id);
			con= ConnectionUtil.getConnection();
			ps=con.prepareStatement(SEND_FEEDBACK);
			ps.setString(1,msg);
			ps.setString(2,email);
			ps.setInt(3,reg_id);
			result=ps.executeUpdate();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return result;
	}

}
