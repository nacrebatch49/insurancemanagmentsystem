package com.nacre.ims.customer.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.customer.serviceimpl.CustomerServiceImpl;
import com.nacre.ims.dto.CustomerEditProfileDto;

@WebServlet("/CustomerEditProfileController")
public class CustomerEditProfileController extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String customer_first_name,customer_last_name,local_address;
		 long customer_mobile_no=0;
		 int city_id,pincode=0;
		 /*int costomer_register_id=0;
			HttpSession ses=req.getSession(true);
			costomer_register_id=ses.getAttribute("");*/
		 int costomer_register_id=1;
		CustomerService service=null;
		
		CustomerEditProfileDto dto=null;
		
		customer_first_name=req.getParameter("firstname");
		customer_last_name=req.getParameter("lastname");
		customer_mobile_no=Long.parseLong(req.getParameter("mobileno"));
		local_address=req.getParameter("address");
		city_id=Integer.parseInt(req.getParameter("city"));
		pincode=Integer.parseInt(req.getParameter("pincode"));
		dto=new CustomerEditProfileDto();
		dto.setCustomer_first_name(customer_first_name);
		dto.setCustomer_last_name(customer_last_name);
		dto.setCustomer_mobile_no(customer_mobile_no);
		dto.setLocal_address(local_address);
		dto.setCity_id(city_id);
		dto.setPincode(pincode);
		dto.setCostomer_register_id(costomer_register_id);
		
		service=new CustomerServiceImpl();
		try {
			String result=service.customerEditProfile(dto);
			req.setAttribute("result", result);
			System.out.println(result);
			res.sendRedirect("/ims/CustomerViewProfileController");
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
		
	}
	

}
