package com.nacre.ims.customer.service;

import java.sql.SQLException;
import com.nacre.ims.dto.CustomerRegistrationDTO;
import com.nacre.ims.dto.LoginDTO;
import java.util.List;
import com.nacre.ims.dto.CustomerCarInsuranceRegisterTableDTO;
import com.nacre.ims.dto.CustomerChildCareInsuranceRegisterTableDTO;
import com.nacre.ims.bean.CustomerViewProfileBean;
import com.nacre.ims.dto.CustomerEditProfileDto;
import com.nacre.ims.dto.CustomerLifeRegisterdto;
import com.nacre.ims.dto.ViewCustomer_ChildCareInsurenceDataDTO;
import com.nacre.ims.dto.View_Customer_LifeInsurenceDataDTO;
import com.nacre.ims.dto.GetHomeInsuranceDTO;

import com.nacre.ims.dto.ViewCarInsurancePolicyByCustomerDTO;

public interface CustomerService {
	public int customerRegistrationService(CustomerRegistrationDTO dto, LoginDTO ldto) throws SQLException;
	public int getYear(int i);

	public int registerLife(CustomerLifeRegisterdto dto);

	public List<View_Customer_LifeInsurenceDataDTO> customerFetchLifeInsurence() throws Exception;

	public List<ViewCustomer_ChildCareInsurenceDataDTO> customerFetchChildInsurence() throws Exception;

	public List<GetHomeInsuranceDTO> getHomeInsurence();

	public List<ViewCarInsurancePolicyByCustomerDTO> getAllCarPolicy();

	public int insertIntoCustomerChildCareInsuranceDetails(CustomerChildCareInsuranceRegisterTableDTO childCareDto,
			int cust_id) throws SQLException;

	public int insertIntoCustomerCarInsuranceDetails(CustomerCarInsuranceRegisterTableDTO CarDto) throws SQLException;


//    public    List<ViewCarInsurancePolicyByCustomerDTO>  getAllCarPolicy();
    
    public CustomerViewProfileBean customerViewProfileService(int customer_id) throws ClassNotFoundException, SQLException;

	public String customerEditProfile(CustomerEditProfileDto dto) throws ClassNotFoundException, SQLException;

	public List GetMyPolicies(int cust_id) ;
   }
