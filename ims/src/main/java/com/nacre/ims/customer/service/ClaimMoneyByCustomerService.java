package com.nacre.ims.customer.service;

import com.nacre.ims.dto.ClaimMoneyByCustomerDTO;

public interface ClaimMoneyByCustomerService {
	
	public int updateClaimMoneyByCustomerService(ClaimMoneyByCustomerDTO dto);

}
