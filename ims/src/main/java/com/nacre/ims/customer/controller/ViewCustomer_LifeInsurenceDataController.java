/**
 * 
 */
package com.nacre.ims.customer.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.nacre.ims.agent.service.AgentService;
import com.nacre.ims.agent.serviceimpl.AgntServiceImpl;
import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.customer.serviceimpl.CustomerServiceImpl;


/**
 * @author vishal patil
 *
 */
@WebServlet("/viewcustomer_lifeinsurencedatacontroller")
public class ViewCustomer_LifeInsurenceDataController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CustomerService service=new CustomerServiceImpl();
		HttpSession session=req.getSession();
			try {
				//System.out.println(service.agentFetchLifeInsurence());
				session.setAttribute("list",service.customerFetchLifeInsurence());
			resp.sendRedirect("/ims/customer/pages/customer_lifeinsurence.jsp");
			} catch (Exception e) {
				
				e.printStackTrace();
			}	
			
		
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
