package com.nacre.ims.customer.controller;
/**
 * @author Ajit
 *
 */
import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.customer.serviceimpl.CustomerServiceImpl;



@WebServlet("/viewHomeInsuranceByCustomerController")
public class ViewHomeInsuranceByCustomerController extends HttpServlet {
	
		private static final long serialVersionUID = 1L;
	       
	    /**
	     * @see HttpServlet#HttpServlet()
	     */
	    public ViewHomeInsuranceByCustomerController() {
	       
	        // TODO Auto-generated constructor stub
	    }
	
		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			CustomerService customerService=null;
			HttpSession session=null;
			try {
				customerService= new CustomerServiceImpl(); 
			  session=req.getSession();
			  session.setAttribute("data", customerService.getHomeInsurence());
			  resp.sendRedirect("/ims/customer/pages/homeinsurancebycustomer.jsp");
			  System.out.println(customerService.getHomeInsurence());
			
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
	
		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			doGet(request, response);
		}
	
	}

	