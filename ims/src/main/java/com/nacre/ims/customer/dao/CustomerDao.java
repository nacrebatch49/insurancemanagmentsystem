package com.nacre.ims.customer.dao;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.nacre.ims.bean.CustomerViewProfileBean;
import com.nacre.ims.bo.CustomerCarInsuranceRegisterTableBO;
import com.nacre.ims.bo.CustomerChildCareInsuranceRegisterTableBO;
import com.nacre.ims.bo.CustomerEditProfileBo;
import com.nacre.ims.bo.CustomerLifeRegisterBo;
import com.nacre.ims.bo.CustomerRegistrationBO;
import com.nacre.ims.bo.CustomerViewCarInsuranceBO;
import com.nacre.ims.bo.CustomerViewChildCareInsuranceBO;
import com.nacre.ims.bo.CustomerViewMyHomeInsurance;
import com.nacre.ims.bo.CustomerViewMyLifeInsurance;
import com.nacre.ims.bo.CustomerViewMyPolicyBO;
import com.nacre.ims.bo.GetHomeInsuranceBO;
import com.nacre.ims.bo.ViewCarInsuerancePolicyByCustomerBo;
import com.nacre.ims.bo.ViewCustomer_ChildCareInsurenceDataBO;
import com.nacre.ims.bo.View_Customer_LifeInsurenceDataBO;

public interface CustomerDao {
	public Date getYear(int i);

	public Object[] registerLife(CustomerLifeRegisterBo bo);

	public List<View_Customer_LifeInsurenceDataBO> customerFetchLifeInsurence() throws Exception;

	public List<ViewCustomer_ChildCareInsurenceDataBO> customerFetchChildCareInsurence() throws Exception;

	public List<GetHomeInsuranceBO> getHomeInsurence();

	public List<ViewCarInsuerancePolicyByCustomerBo> getAllCarPolicy();

	public int insertIntoCustomerChildCareInsuranceDetails(CustomerChildCareInsuranceRegisterTableBO childCareBo,
			int cust_id) throws SQLException;

	public int insertIntoCustomerCarInsuranceDetails(CustomerCarInsuranceRegisterTableBO carBo) throws SQLException;

	/*
	 * public Object[] registerLife(CustomerLifeRegisterBo bo); public void
	 * getCustomerPaymentInfo(int custId)throws Exception; public
	 * List<View_Customer_LifeInsurenceDataBO> customerFetchLifeInsurence()throws
	 * Exception; public List<ViewCustomer_ChildCareInsurenceDataBO>
	 * customerFetchChildCareInsurence()throws Exception; public
	 * List<GetHomeInsuranceBO> getHomeInsurence();
	 * 
	 * 
	 * public List<ViewCarInsuerancePolicyByCustomerBo> getAllCarPolicy();
	 */

	public CustomerViewProfileBean CustomerViewProfile(int customer_id) throws ClassNotFoundException, SQLException;

	public int customerEditProfle(CustomerEditProfileBo bo) throws ClassNotFoundException, SQLException;

	public int customerRegistration(CustomerRegistrationBO bo) throws SQLException;

	public List<CustomerViewMyPolicyBO> viewMyPolicyDAO();

	public CustomerViewMyLifeInsurance getCustomerLifeInsurance(int registerpolicy);

	public CustomerViewMyHomeInsurance getCustomerHomeInsurance(int registerpolicy);

	public CustomerViewCarInsuranceBO getCustomerCarInsurance(int registerpolicy);

	public CustomerViewChildCareInsuranceBO getCustomerChildInsurance(int registerpolicy);

}
