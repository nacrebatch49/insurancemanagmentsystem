package com.nacre.ims.customer.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nacre.ims.customer.service.ClaimMoneyByCustomerService;
import com.nacre.ims.customer.serviceimpl.ClaimMoneyByCustomerServiceImpl;
import com.nacre.ims.dto.ClaimMoneyByCustomerDTO;

@WebServlet("/claimMoneyUrl")
public class ClaimMoneyByCustomerController extends HttpServlet {
	
    public ClaimMoneyByCustomerController() {
    }
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=null;
		
		 String policyId=null;
		 String nomineeName=null;
		 String email=null;
		 String relation=null;
		 String reason=null;
		 String dateOfClaim=null;
		 ClaimMoneyByCustomerDTO dto=null;
		ClaimMoneyByCustomerService service=null;
		java.util.Date date2 = null;
		java.sql.Date date3 = null;
		//General Setting
		response.setContentType("text/html");
		pw=response.getWriter();
		//getting all value from from form
		policyId=request.getParameter("policyNo");
		nomineeName=request.getParameter("nomineeName");
		email=request.getParameter("nomineeEmail");
		relation=request.getParameter("nomineeRelation");
		reason=request.getParameter("nomineeReason");
		dateOfClaim=request.getParameter("nomineeClaimDate");
		DateFormat formatter;
		
		formatter = new SimpleDateFormat("YYYY-MM-DD");
		try {
			 date2 = formatter.parse(dateOfClaim);
			 date3=new java.sql.Date(date2.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//set All value to DTO class
		dto.setPolicId(policyId);
		dto.setNomineeName(nomineeName);
		dto.setEmail(email);
		dto.setRelation(relation);
		dto.setReason(reason);
		dto.setDateOfClaim(date3);
		
		//create service class object
		service=new ClaimMoneyByCustomerServiceImpl();
		int result=service.updateClaimMoneyByCustomerService(dto);
		
		if(result>0){
			pw.println("Record inserted properly");
		}else{
			pw.println(" Record not inserted ");
		}
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
