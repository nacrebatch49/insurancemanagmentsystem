package com.nacre.ims.customer.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.customer.serviceimpl.CustomerServiceImpl;
import com.nacre.ims.dto.CustomerCarInsuranceRegisterTableDTO;

@WebServlet("/carinsuarncepolicyurl")
public class CustomerCarInsuranceRegisterTableController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		// general settings

		res.setContentType("text/html");
		PrintWriter pw = null;
		pw = res.getWriter();
		int result = 0;

		// creating Session object

		HttpSession ses = null;
		ses = req.getSession();

		String insurancePolicyNo = null, carNo = null, purchaseDate = null, rcNo = null, nomineeName = null,
				nomineeRelation = null;
		long mobileNo = 0;
		int maxLimit = 0;

		// reading form data
		insurancePolicyNo = req.getParameter("insurancePolicyNo");
		carNo = req.getParameter("carNo");
		purchaseDate = req.getParameter("purchaseDate");
		rcNo = req.getParameter("rcNo");
		nomineeName = req.getParameter("nomineeName");
		mobileNo = Long.parseLong(req.getParameter("nomineeMobileNo"));
		nomineeRelation = req.getParameter("nomineeRelation");
		maxLimit = Integer.parseInt(req.getParameter("maxLimit"));

		RequestDispatcher rd = null;

		// setting static values to foreign key columns

		int emiOptionType = 1, customerId = 1, carInsuranceMasterId = 1;

		// creating dto object

		CustomerCarInsuranceRegisterTableDTO carDto = null;
		carDto = new CustomerCarInsuranceRegisterTableDTO();

		carDto.setInsurancePolicyNo(insurancePolicyNo);
		carDto.setCarNo(carNo);
		carDto.setPurchaseDate(purchaseDate);
		carDto.setRcNo(rcNo);
		carDto.setNomineeName(nomineeName);
		carDto.setNomineeMobileNo(mobileNo);
		carDto.setNomineeRelation(nomineeRelation);
		carDto.setMaxLimit(maxLimit);
		carDto.setEmiOptionType(emiOptionType);
		carDto.setCustomerId(customerId);
		carDto.setCarInsuranceMasterId(carInsuranceMasterId);

		// creating service interface implementation class object

		CustomerService carService = null;
		carService = new CustomerServiceImpl();

		try {
			result = carService.insertIntoCustomerCarInsuranceDetails(carDto);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (result > 0) {
			rd = req.getRequestDispatcher("/customer/pages/home.jsp");
			rd.forward(req, res);
		} else {
			rd = req.getRequestDispatcher("/customer/pages/carinsurancepolicy.jsp");
			rd.forward(req, res);
		}

	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
