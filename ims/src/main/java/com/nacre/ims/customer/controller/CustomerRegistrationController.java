package com.nacre.ims.customer.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.commonsutil.SendMail;
import com.nacre.ims.customer.service.CustomerService;
import com.nacre.ims.customer.serviceimpl.CustomerServiceImpl;
import com.nacre.ims.dto.CustomerRegistrationDTO;
import com.nacre.ims.dto.LoginDTO;

@WebServlet("/CustomerRegistrationController")
public class CustomerRegistrationController extends HttpServlet {
	CustomerService service=null;
	
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
public CustomerRegistrationController() {
		
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CustomerRegistrationDTO dto=null;
	    LoginDTO ldto=null;
	    
		PrintWriter pw=null;
		resp.setContentType("text/html");
		pw=resp.getWriter();
		HttpSession session=req.getSession();
		

	//	int loginid =Integer.parseInt(session.getAttribute("loginId").toString());
		//int agentId=Integer.parseInt(session.getAttribute("agentid").toString());
		int costomer_register_id=0;
		
		String firstname=req.getParameter("first_name");
		String lastname=req.getParameter("last_name");
		String email=req.getParameter("email");
		long mobileno=Long.parseLong(req.getParameter("mobile"));
		String gender=req.getParameter("gender");
		Date dob=Date.valueOf(req.getParameter("dob"));
		long addharno=Long.parseLong(req.getParameter("adhar"));
		String address=req.getParameter("address");
		int agentid=Integer.parseInt(req.getParameter("agentno"));
		int cityId=Integer.parseInt(req.getParameter("city"));
		int pincode=Integer.parseInt(req.getParameter("pincode"));
		
		
		String username=firstname.substring(0, 2)+lastname.substring(0,2)+Long.toString(mobileno).substring(Long.toString(mobileno).length()-5, Long.toString(mobileno).length()-1);
		String password= Integer.toString((int)(Math.random()*10000000));
		System.out.println(username);
		System.out.println(password);
		
		dto=new CustomerRegistrationDTO();
		dto.setFirstName(firstname);
		dto.setLastName(lastname);
		dto.setEmail(email);
		dto.setMobileNo(mobileno);
		dto.setCity_id(cityId);
		dto.setPincode(pincode);
		dto.setGender(gender);
		dto.setDob(dob);
		dto.setAddharNo(addharno);
		dto.setAgentId(agentid);
		//dto.setLoginId(loginid);
		dto.setAddress(address);
		ldto=new LoginDTO();
		ldto.setUsername(username);
		ldto.setPassword(password);
		boolean issnd=false;	
try {
	
	service=new CustomerServiceImpl();
	costomer_register_id=service.customerRegistrationService(dto,ldto);
	session.setAttribute("costomer_register_id", costomer_register_id);
	
	if(costomer_register_id==0) {
		session.setAttribute("msg", "User not register please try again");
		resp.sendRedirect("/ims/commons/pages/Login.jsp");
	}
	{
		issnd=SendMail.send("your user name and password is",email, "vishnuwork247@gmail.com", "Vikky@123", "your username is:"+username+" and password is:"+password);
	}
	
		session.setAttribute("msg", "Thanku for Registration please check your mail");
		resp.sendRedirect("/ims/commons/pages/Login.jsp");
	
	
	
}catch(Exception e){
	e.printStackTrace();
	
}
		
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	doGet(req, resp);
	
	}
	

}
