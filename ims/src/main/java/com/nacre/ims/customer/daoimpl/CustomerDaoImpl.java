package com.nacre.ims.customer.daoimpl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.nacre.ims.admin.dbutils.AdminDbUtils;
import com.nacre.ims.agent.dao.AgentDao;
import com.nacre.ims.agent.daoimpl.AgentDaoImpl;
import com.nacre.ims.bean.CustomerViewProfileBean;
import com.nacre.ims.bo.CustomerCarInsuranceRegisterTableBO;
import com.nacre.ims.bo.CustomerChildCareInsuranceRegisterTableBO;
import com.nacre.ims.bo.CustomerEditProfileBo;
import com.nacre.ims.bo.CustomerLifeRegisterBo;
import com.nacre.ims.bo.CustomerRegistrationBO;
import com.nacre.ims.bo.CustomerViewCarInsuranceBO;
import com.nacre.ims.bo.CustomerViewChildCareInsuranceBO;
import com.nacre.ims.bo.CustomerViewMyHomeInsurance;
import com.nacre.ims.bo.CustomerViewMyLifeInsurance;
import com.nacre.ims.bo.CustomerViewMyPolicyBO;
import com.nacre.ims.bo.GetHomeInsuranceBO;
import com.nacre.ims.bo.ViewCarInsuerancePolicyByCustomerBo;
import com.nacre.ims.bo.ViewCustomer_ChildCareInsurenceDataBO;
import com.nacre.ims.bo.View_Customer_LifeInsurenceDataBO;
import com.nacre.ims.commonsutil.ConnectionUtil;
import com.nacre.ims.commonsutil.SqlDate;
import com.nacre.ims.customer.dao.CustomerDao;
import com.nacre.ims.customer.dbutils.CustomerDbUtils;

public class CustomerDaoImpl implements CustomerDao {

	private static String email = "";

	Connection con = null;
	PreparedStatement ps = null;
	// private static String email = "";

	@Override
	public Date getYear(int i) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Date d = null;
		con = ConnectionUtil.getConnection();
		try {
			ps = con.prepareStatement(CustomerDbUtils.Get_DOB);
			ps.setInt(1, 1);
			rs = ps.executeQuery();
			if (rs.next()) {
				System.out.println("entered in db");
				d = rs.getDate(1);
				email = rs.getString(2);
				System.out.println("email is " + email);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return d;
	}

	@Override
	public Object[] registerLife(CustomerLifeRegisterBo bo) {

		Connection con = null;
		Statement st1 = null;
		PreparedStatement ps = null, ps1 = null;
		ResultSet rs2 = null;
		Object[] obj = null;
		int i = 0, lastresult = 0, totalamount = 0;

		con = ConnectionUtil.getConnection();
		try {
			System.out.println("bo is"+bo);
			ps = con.prepareStatement(CustomerDbUtils.SET_CUSTOMER_REGISTER);
			ps.setString(1, bo.getInsuranceName());
			ps.setInt(2, bo.getMaxlimit());
			ps.setString(3, bo.getNomineeName());
			ps.setString(4, bo.getNomineeEmail());
			ps.setString(5, bo.getNomineeRelation());
			ps.setInt(6, bo.getPaymenttype());
			ps.setString(7, bo.getRegister_policy_id());
			ps.setDate(8, bo.getDate_of_policy());
			ps.setInt(9, bo.getCust_id());
			ps.setInt(10, bo.getLifeInsuranceid());
			i = ps.executeUpdate();
			if (i > 0) {
				ps1 = con.prepareStatement(CustomerDbUtils.SET_INSURANCE_POLICIES);
				ps1.setString(1, "Life Insurance");
				ps1.setInt(2, bo.getCust_id());
				ps1.setString(3, bo.getRegister_policy_id());
				lastresult = ps1.executeUpdate();
			}
			if (lastresult > 0) {
				st1 = con.createStatement();
				rs2 = st1.executeQuery(CustomerDbUtils.GET_AMOUNT);
				while (rs2.next()) {
					totalamount = rs2.getInt(1);
				}
				obj = new Object[2];
				obj[0] = totalamount;
				obj[1] = email;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return obj;
	}

	@Override
	/*public void getCustomerPaymentInfo(int custId) throws Exception {
		
		 * G Venu
		  
		Connection connection=null;
		Statement statement=null;
		ResultSet resultSet=null;
		PreparedStatement preparedStatement=null;
		CustomerPolicyIdName customerPolicyIdNameBO=null;
		List<CustomerPolicyIdName> listCustPolicyId=null;
		//get the connection
		connection=ConnectionUtil.getConnection();
		//create preparedStatement object
		statement=connection.createStatement();
		//execute the query
		resultSet=statement.executeQuery(CustomerDbUtils.GET_ALL_CUSTOMER_POLICY_IDS);
		//create arrayList for storing all customer policy id's and names
		listCustPolicyId=new ArrayList<CustomerPolicyIdName>();
		while(resultSet.next()) {
			customerPolicyIdNameBO=new CustomerPolicyIdName();
			customerPolicyIdNameBO.setPolicy_Name(resultSet.getString(1));
			customerPolicyIdNameBO.setPolicy_Id(resultSet.getInt(2));
			listCustPolicyId.add(customerPolicyIdNameBO);
		}
		//check the policy type according to type get the data from appropriate table 
		listCustPolicyId.forEach(list->{
			//for home policy
			if(list.getPolicy_Name().equalsIgnoreCase("home")) {
				//preparedStatement=connection.prepareStatement("");
				
			}
			//for childCare policy
			if(list.getPolicy_Name().equalsIgnoreCase("childCare")) {
				
			}
			//for Life policy
			if(list.getPolicy_Name().equalsIgnoreCase("life")) {
				
			}
			//for vehicle policy
			if(list.getPolicy_Name().equalsIgnoreCase("car")) {
	
			}
		});
	}
	
*/
	public List<GetHomeInsuranceBO> getHomeInsurence() {
		con = ConnectionUtil.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		GetHomeInsuranceBO getHomeInsuranceBO = null;
		List<GetHomeInsuranceBO> listBo = null;

		try {
			ps = con.prepareStatement(CustomerDbUtils.GET_HOME_POLIVY_BY_CUSTOMER);
			rs = ps.executeQuery();

			listBo = new ArrayList<>();
			while (rs.next()) {
				getHomeInsuranceBO = new GetHomeInsuranceBO();
				getHomeInsuranceBO.setPolicyName(rs.getString(1));
				getHomeInsuranceBO.setDuration(rs.getInt(2));
				getHomeInsuranceBO.setCoverage(rs.getString(3));
				getHomeInsuranceBO.setDescription(rs.getString(4));
				getHomeInsuranceBO.setTotalamount(rs.getDouble(5));
				getHomeInsuranceBO.setRateOfInterest(rs.getDouble(6));
				getHomeInsuranceBO.setInitialAmount(rs.getDouble(7));
				getHomeInsuranceBO.setPolicyOffer(rs.getString(8));
				getHomeInsuranceBO.setPolicyNumber(rs.getString(9));
				getHomeInsuranceBO.setHomeInsuranceId(rs.getInt(10));
				listBo.add(getHomeInsuranceBO);
			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return listBo;
	}

	@Override
	public List<View_Customer_LifeInsurenceDataBO> customerFetchLifeInsurence() throws Exception {
		AgentDao dao = new AgentDaoImpl();
		List<View_Customer_LifeInsurenceDataBO> list = new ArrayList<>();
		Connection con = null;
		PreparedStatement ps = null;
		int count = 0;
		con = ConnectionUtil.getConnection();
		ps = con.prepareStatement(AdminDbUtils.SELECT_LIFE_INSURENCE_QUEERY);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			View_Customer_LifeInsurenceDataBO bo = new View_Customer_LifeInsurenceDataBO();
			bo.setName(rs.getString("insurance_name"));
			bo.setDuration(rs.getInt("duration"));
			bo.setInsurance_policy_no(rs.getString("insurance_policy_no"));
			bo.setRate_of_interest(rs.getDouble("rate_of_interest"));
			bo.setDesciption(rs.getString("desciption"));
			bo.setTotal_amount_paid(rs.getDouble("total_amount_paid"));
			bo.setLife_insurance_offers(rs.getString("life_insurance_offers"));
			bo.setCovarage(rs.getString("covarage"));
			bo.setLifeid(rs.getInt("life_isn_id"));

			// bo.setStatus(rs.getString("status"));

			list.add(bo);

		}

		return list;
	}

	@Override
	public List<ViewCustomer_ChildCareInsurenceDataBO> customerFetchChildCareInsurence() throws Exception {
		AgentDao dao = new AgentDaoImpl();
		List<ViewCustomer_ChildCareInsurenceDataBO> list = new ArrayList<>();
		Connection con = null;
		PreparedStatement ps = null;
		int count = 0;
		con = ConnectionUtil.getConnection();
		ps = con.prepareStatement(AdminDbUtils.SELECT_ChilCARE_INSURENCE_QUEERY);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ViewCustomer_ChildCareInsurenceDataBO bo = new ViewCustomer_ChildCareInsurenceDataBO();
			bo.setMax_limit(rs.getInt("max_limit"));
			bo.setName(rs.getString("name"));
			bo.setCoverage(rs.getString("coverage"));
			bo.setDesciption(rs.getString("desciption"));
			bo.setRate_of_interest(rs.getDouble("rate_of_interest"));
			bo.setPayable_amount(rs.getDouble("payable_amount"));
			bo.setInitial_amount(rs.getDouble("initial_amount"));
			bo.setLife_insurance_offers(rs.getString("life_insurance_offers"));
			bo.setChildid(rs.getInt("child_care_insurance_id"));
			// bo.setStatus(rs.getString("status"));
			list.add(bo);
		}

		return list;
	}

	public List<ViewCarInsuerancePolicyByCustomerBo> getAllCarPolicy() {
		List<ViewCarInsuerancePolicyByCustomerBo> list = new ArrayList();
		Connection con = null;
		con = ConnectionUtil.getConnection();
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement(CustomerDbUtils.getCarDetailsPolicy);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				ViewCarInsuerancePolicyByCustomerBo bo = new ViewCarInsuerancePolicyByCustomerBo();

				bo.setPolicyName(rs.getString(1));
				bo.setCoverage(rs.getString(2));
				bo.setPolicyDescription(rs.getString(3));
				bo.setRateofInterest(rs.getDouble(4));
				bo.setDuration(rs.getInt(5));
				bo.setPaybleAmount(rs.getDouble(6));
				bo.setInitialAmount(rs.getDouble(7));
				bo.setCarIncId(rs.getInt(8));
				list.add(bo);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;

	}

	/*
	 * dinesh
	 */
	@Override
	public int insertIntoCustomerChildCareInsuranceDetails(CustomerChildCareInsuranceRegisterTableBO childCareBo,
			int cust_id) throws SQLException {

		Connection con = null;
		PreparedStatement ps = null;
		con = ConnectionUtil.getConnection();
		System.out.println("INSERT_CHILDCARE_DETAILS query.........." + CustomerDbUtils.INSERT_CHILDCARE_DETAILS);
		ps = con.prepareStatement(CustomerDbUtils.INSERT_CHILDCARE_DETAILS,Statement.RETURN_GENERATED_KEYS);

		String d = childCareBo.getDate_of_Insurance();

		// changing the String date to sql type date

		Date date = SqlDate.date(d);

		// setting values to PreparedStatement

		ps.setString(1, childCareBo.getChild_Name());
		ps.setInt(2, childCareBo.getChild_Age());
		ps.setString(3, childCareBo.getBirthCertificate_No());
		ps.setString(4, childCareBo.getParents_Name());
		ps.setDate(5, date);
		ps.setString(6, childCareBo.getPayment_Type());
		ps.setInt(7, cust_id);
		ps.setInt(8, childCareBo.getChildCareInusranceId());

		// process the result

		int result = ps.executeUpdate();
		ResultSet rs2=null;
		int key=0;
		int result1=0;
		if(result>0) {
			rs2=ps.getGeneratedKeys();
			if(rs2.next()) {
				key=rs2.getInt(1);
			}
			PreparedStatement ps2=con.prepareStatement(CustomerDbUtils.INSERT_CHILDCARE_POLICY);
			ps2.setString(1, "child");
			ps2.setInt(2, cust_id);
			ps2.setInt(3, key);
			result1=ps2.executeUpdate();
			
		}

		return result1;
	}

	@Override
	public int insertIntoCustomerCarInsuranceDetails(CustomerCarInsuranceRegisterTableBO carBo) throws SQLException {

		Connection con = null;
		PreparedStatement ps = null, ps1 = null;
		ResultSet rs2 = null;
		Statement st1 = null;
		con = ConnectionUtil.getConnection();
		ps = con.prepareStatement(CustomerDbUtils.INSERT_CAR_DETAILS);
		int result = 0;
		int lastresult = 0, totalAmt = 0;
		// converting String date to Date type date

		String purchaseDate = carBo.getPurchaseDate();
		Date purchaseDate1 = SqlDate.date(purchaseDate);

		// setting values to PreparedStatement

		ps.setString(1, carBo.getInsurancePolicyNo());
		ps.setString(2, carBo.getCarNo());
		ps.setDate(3, purchaseDate1);
		ps.setString(4, carBo.getRcNo());
		ps.setString(5, carBo.getNomineeName());
		ps.setLong(6, carBo.getNomineeMobileNo());
		ps.setString(7, carBo.getNomineeRelation());
		ps.setInt(8, carBo.getMaxLimit());
		ps.setInt(9, carBo.getEmiOptionType());
		ps.setInt(10, carBo.getCustomerId());
		ps.setInt(11, carBo.getCarInsuranceMasterId());

		// process the result

		result = ps.executeUpdate();

		if (result > 0) {

			ps1 = con.prepareStatement(CustomerDbUtils.SET_INSURANCE_POLICIES);
			ps1.setString(1, "car Insurance");
			ps1.setInt(2, carBo.getCust_id());
			ps1.setString(3, carBo.getRegister_policy_id());
			lastresult = ps1.executeUpdate();
		}
		if (lastresult > 0) {
			st1 = con.createStatement();
			rs2 = st1.executeQuery(CustomerDbUtils.GET_AMOUNT);
			while (rs2.next()) {
				totalAmt = rs2.getInt(1);
			}

			
		}
		return result;
	}

	@Override
	public CustomerViewProfileBean CustomerViewProfile(int customer_id) throws ClassNotFoundException, SQLException {

		Connection con = null;
		String query = null;
		PreparedStatement ps = null;
		ResultSet rs = null, rs2 = null;
		CustomerViewProfileBean bean = new CustomerViewProfileBean();
		query = CustomerDbUtils.SELECT_CUSTOMER;
		con = ConnectionUtil.getConnection();
		ps = con.prepareStatement(query);
		ps.setInt(1, customer_id);
		rs = ps.executeQuery();
		if (rs.next()) {
			bean.setCustomer_first_name(rs.getString(1));
			bean.setCustomer_last_name(rs.getString(2));
			bean.setCustomer_email_id(rs.getString(3));

			bean.setCustomer_mobile_no(rs.getLong(4));
			bean.setCustomer_gender(rs.getString(5));
			bean.setCustomer_dob(rs.getDate(6));
			bean.setAdhar_no(rs.getLong(7));
			bean.setLocal_address(rs.getString(8));
			bean.setPincode(rs.getInt(9));
			bean.setCity_name(rs.getString(10));
			bean.setState_name(rs.getString(11));
			bean.setCountry_name(rs.getString(12));
		}
		System.out.println(bean.getAdhar_no());
		return bean;

	}

	@Override
	public int customerEditProfle(CustomerEditProfileBo bo) throws ClassNotFoundException, SQLException {
		Connection con = null;
		String query = null;
		PreparedStatement ps = null;
		int count = 0;

		CustomerViewProfileBean bean = new CustomerViewProfileBean();
		query = CustomerDbUtils.update_customer;
		con = ConnectionUtil.getConnection();
		ps = con.prepareStatement(query);

		ps.setString(1, bo.getCustomer_first_name());
		ps.setString(2, bo.getCustomer_last_name());
		ps.setLong(3, bo.getCustomer_mobile_no());
		ps.setString(4, bo.getLocal_address());
		ps.setInt(5, bo.getCity_id());
		ps.setInt(6, bo.getPincode());
		ps.setInt(7, bo.getCostomer_register_id());
		count = ps.executeUpdate();
		return count;
	}

	@Override
	public int customerRegistration(CustomerRegistrationBO bo) throws SQLException {
		int login = 0;
		int addressId = 0;
		int count = 0;
		int loginId = 0;
		int costomer_register_id = 0;
		try {
			con = ConnectionUtil.getConnection();
			ps = con.prepareStatement(CustomerDbUtils.INSERT_LOGIN_DETAILS, PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, bo.getUsername());
			ps.setString(2, bo.getPassword());
			login = ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			while (rs.next()) {
				loginId = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (login > 0) {
			try {
				ps = con.prepareStatement(CustomerDbUtils.INSERT_ADDRESS_DETAILS,
						PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, bo.getAddress());
				ps.setInt(2, bo.getPincode());
				ps.setInt(3, bo.getCity_Id());
				count = ps.executeUpdate();
				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					addressId = rs.getInt(1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			if (count > 0) {
				ps = con.prepareStatement(CustomerDbUtils.INSERT_CUSTOMER_REGISTRATION,
						PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, bo.getCustomer_first_name());
				ps.setString(2, bo.getCustomer_last_name());
				ps.setString(3, bo.getCustomer_email_id());
				ps.setLong(4, bo.getCustomer_mobile_no());
				ps.setString(5, bo.getCustomer_gender());
				ps.setDate(6, bo.getCustomer_dob());
				ps.setLong(7, bo.getCustomer_adhar_no());
				ps.setInt(8, addressId);
				ps.setInt(9, bo.getAgent_id());
				ps.setInt(10, loginId);
				count = ps.executeUpdate();

				ResultSet rs = ps.getGeneratedKeys();
				while (rs.next()) {
					costomer_register_id = rs.getInt(1);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return costomer_register_id;
		
	}
	@Override

	public List<CustomerViewMyPolicyBO> viewMyPolicyDAO() {
		List<CustomerViewMyPolicyBO> list = new ArrayList<>();

		Connection con;
		try {
			con = ConnectionUtil.getConnection();
			PreparedStatement pst = con.prepareStatement(CustomerDbUtils.Get_CUSTOMER_POLICIES);
			ResultSet rs = pst.executeQuery();

			while (rs.next()) {
				CustomerViewMyPolicyBO bo = new CustomerViewMyPolicyBO();
				bo.setPolicy_id(rs.getInt(1));
				bo.setPolicy_name(rs.getString(2));
				bo.setCustomer_register_policy_id(rs.getInt(3));
				list.add(bo);

			}
		}  catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("list in daoimpl" + list);
		return list;

	}

@Override
	public CustomerViewMyLifeInsurance getCustomerLifeInsurance(int registerpolicy) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		CustomerViewMyLifeInsurance bo = null;
		// get the objects
		connection = ConnectionUtil.getConnection();
		bo=new CustomerViewMyLifeInsurance();
		try {
			preparedStatement = connection.prepareStatement(CustomerDbUtils.GET_CUSTOMER_LIFE_POLICY);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				bo.setInsuranceName(resultSet.getString(1));
				bo.setMaxLimit(resultSet.getInt(2));
				bo.setNomineeName(resultSet.getString(3));
				bo.setEmailId(resultSet.getString(4));
				bo.setNomineeRelation(resultSet.getString(5));
				bo.setRegisterPolicyNumber(resultSet.getInt(6));
				bo.setDateOfPolicy(resultSet.getString(7));
				bo.setEmiType(resultSet.getString(8));
				bo.setTotalAmt(resultSet.getInt(9));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bo;
	}

	@Override
	public CustomerViewMyHomeInsurance getCustomerHomeInsurance(int registerpolicy) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		CustomerViewMyHomeInsurance bo = null;
		// get the objects
		connection = ConnectionUtil.getConnection();
		try {
			preparedStatement = connection.prepareStatement(CustomerDbUtils.GET_CUSTOMER_HOME_POLICY);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				bo = new CustomerViewMyHomeInsurance();
				bo.setRegisterNumber(resultSet.getInt(1));
				bo.setHomeAdd(resultSet.getString(2));
				bo.setSquare_feet(resultSet.getFloat(3));
				bo.setHome_no(resultSet.getString(4));
				bo.setMaxDur(resultSet.getInt(5));
				bo.setNominies_name(resultSet.getString(6));
				bo.setNominies_email_id(resultSet.getString(7));
				bo.setRelation(resultSet.getString(8));
				bo.setDate_of_policies(resultSet.getString(9));
				bo.setTotalAmt(resultSet.getInt(10));
				bo.setEmi_option_type1(resultSet.getString(11));
		}}
			catch (Exception e) {
			e.printStackTrace();
		}
		return bo;
	}

	@Override
	public CustomerViewCarInsuranceBO getCustomerCarInsurance(int registerpolicy) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		CustomerViewCarInsuranceBO bo = null;
		// get the objects
		connection = ConnectionUtil.getConnection();
		try {
			preparedStatement = connection.prepareStatement(CustomerDbUtils.GET_CUSTOMER_CAR_POLICY);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				bo = new CustomerViewCarInsuranceBO();
				bo.setInsurance_policy_no(resultSet.getInt(1));
				bo.setCar_number(resultSet.getString(2));
				bo.setPurchage_date(resultSet.getString(3));
				bo.setRc_no(resultSet.getString(4));
				bo.setNominies_name(resultSet.getString(5));
				bo.setNominies_mobile_no(resultSet.getLong(6));
				bo.setNominies_relation(resultSet.getString(7));
				bo.setMax_limit(resultSet.getInt(8));
				bo.setEmi_option_type(resultSet.getString(9));
				bo.setTotalAmt(resultSet.getInt(10));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bo;
	}

	@Override
	public CustomerViewChildCareInsuranceBO getCustomerChildInsurance(int registerpolicy) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		CustomerViewChildCareInsuranceBO bo = null;
		// get the objects

		connection = ConnectionUtil.getConnection();
		try {
			preparedStatement = connection.prepareStatement(CustomerDbUtils.GET_CUSTOMER_CHILD_POLICY);
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				bo=new CustomerViewChildCareInsuranceBO();
				bo.setChild_name(resultSet.getString(1));
				bo.setChild_age(resultSet.getInt(2));
				bo.setBirthcertificate_no(resultSet.getString(3));
				bo.setParents_name(resultSet.getString(4));
				bo.setDate_of_insurance(resultSet.getString(5));
				bo.setPayment_type(resultSet.getString(6));
				bo.setTotalAmt(resultSet.getInt(7));
				bo.setRegisterpolicy(resultSet.getInt(8));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bo;
	}

}
	
