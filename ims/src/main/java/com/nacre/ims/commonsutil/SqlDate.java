package com.nacre.ims.commonsutil;

import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class SqlDate {
	public static Date date(String date){
		//System.out.println("inside date");
		System.out.println("string date........"+date);
	    String str=date;  
	    Date date1=Date.valueOf(str);  
	    System.out.println(date);
		return date1;  
	}  

	public static Time time(String time) throws ParseException{
		SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
		long l=sdf.parse(time).getTime();
		
		Time t=new Time(l);
		return t;
	}
}
