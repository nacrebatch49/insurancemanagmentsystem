package com.nacre.ims.commonsutil.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.nacre.ims.commonsutil.ConnectionUtil;
import com.nacre.ims.commonsutil.dao.CommonsUtilDao;


public class CommonsUtilDaoImpl implements CommonsUtilDao {

	@Override
	public Map<Integer, String> getCountry() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		Connection con =ConnectionUtil.getConnection();
		
		try {
		PreparedStatement pst = con.prepareStatement("select *  from country_master_table");
		
		ResultSet rs = pst.executeQuery();
		
		while (rs.next()) {
			System.out.println(rs.getInt(1));
			System.out.println(rs.getString(2));
			map.put(rs.getInt(1), rs.getString(2));
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return map;

	}

	@Override
	public Map<Integer, String> getState(int country) throws Exception {
		Map<Integer, String> map = new HashMap<Integer, String>();
		Connection con = ConnectionUtil.getConnection();
		
try {
			PreparedStatement pst = con.prepareStatement("select * from state_master_table where country_id=?");
			pst.setInt(1, country);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getInt(1));
				System.out.println(rs.getString(2));
				map.put(rs.getInt(1), rs.getString(2));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
		
	}
	

	@Override
	public Map<Integer, String> getCity(int state) throws Exception {
		Map<Integer, String> map = new HashMap<Integer, String>();
		Connection con = ConnectionUtil.getConnection();
		try {
			PreparedStatement pst = con.prepareStatement("select*  from city_master_table where state_id=?");
			pst.setInt(1, state);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getInt(1));
				System.out.println(rs.getString(2));
				map.put(rs.getInt(1), rs.getString(2));

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;

}
		
}
