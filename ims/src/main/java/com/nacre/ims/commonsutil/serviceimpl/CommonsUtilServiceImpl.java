package com.nacre.ims.commonsutil.serviceimpl;


import java.util.Map;

import com.nacre.ims.commonsutil.dao.CommonsUtilDao;
import com.nacre.ims.commonsutil.daoimpl.CommonsUtilDaoImpl;
import com.nacre.ims.commonsutil.service.CommonsUtilService;

public class CommonsUtilServiceImpl implements CommonsUtilService {
	CommonsUtilDao dao=null;

	@Override
	public Map<Integer, String> getCountry() {
		dao=new CommonsUtilDaoImpl();
		Map map=dao.getCountry();
		return map;
	
	}

	@Override
	public Map<Integer, String> getState(int country) throws Exception {
		dao = new CommonsUtilDaoImpl();
		Map map=dao.getState(country);
		return map;
	}

	@Override
	public Map<Integer, String> getCity(int state) throws Exception {
		dao = new CommonsUtilDaoImpl();
		Map map=dao.getCity(state);
		return map;
	
	}

}

