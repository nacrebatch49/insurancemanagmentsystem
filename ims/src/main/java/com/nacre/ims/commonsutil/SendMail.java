package com.nacre.ims.commonsutil;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail {

	public static boolean send(String subject,String to,String from,String password,String msg){  
        //Get properties object
        boolean b = false;
        Properties props = new Properties();    
        props.put("mail.smtp.host", "smtp.gmail.com");    
        /*props.put("mail.smtp.socketFactory.port", "465");    
        props.put("mail.smtp.socketFactory.class",    
                  "javax.net.ssl.SSLSocketFactory"); */
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.auth", "true");    
        props.put("mail.smtp.port", "587");    
        //get Session   
        Session session = Session.getInstance(props,    
         new javax.mail.Authenticator() {    
         protected PasswordAuthentication getPasswordAuthentication() {    
         return new PasswordAuthentication(from,password);  
         }
        });    
        //compose message    
        try {    
         MimeMessage message = new MimeMessage(session);    
         message.setFrom(new InternetAddress(from));
         message.setRecipient(Message.RecipientType.TO,new InternetAddress(to));    
         message.setSubject(subject);    
         message.setText(msg);    
         //send message  
         Transport.send(message);  
         b = true;
         System.out.println("message sent successfully");    
        } catch (MessagingException e) {
        	b=false;
        	e.printStackTrace();}    
           return b;
  }  
	}



