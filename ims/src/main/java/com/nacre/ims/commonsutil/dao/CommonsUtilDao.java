package com.nacre.ims.commonsutil.dao;

import java.util.Map;

public interface CommonsUtilDao {
	public Map<Integer, String> getCountry() ;
	public Map<Integer, String> getState(int country) throws Exception;
	public Map<Integer, String> getCity(int state) throws Exception;

}
