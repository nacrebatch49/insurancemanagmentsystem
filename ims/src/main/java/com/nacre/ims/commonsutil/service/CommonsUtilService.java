package com.nacre.ims.commonsutil.service;

import java.util.Map;

public interface CommonsUtilService {
	public Map<Integer,String> getCountry();
	public Map<Integer,String> getState(int country) throws Exception;
	public Map<Integer,String> getCity(int state) throws Exception;
}
