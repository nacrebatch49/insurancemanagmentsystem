package com.nacre.ims.commonsutil;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.nacre.ims.commonsutil.service.CommonsUtilService;
import com.nacre.ims.commonsutil.serviceimpl.CommonsUtilServiceImpl;
@WebServlet("/GetCountry")
public class GetCountry extends HttpServlet {
	CommonsUtilService service=null;
	/**
	 * @author Nikhilesh 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside servlet");
		resp.setContentType("application/json");
		service=new CommonsUtilServiceImpl();
	
	//	Map<Integer,String> map=new HashMap<>();
	     Map map;
        map = service.getCountry();
        Gson g=new Gson();
		String s=g.toJson(map);
		System.out.println(s);
		resp.getWriter().write(s);
		//resp.flushBuffer();

	
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
