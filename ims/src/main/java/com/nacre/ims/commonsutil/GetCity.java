package com.nacre.ims.commonsutil;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.nacre.ims.commonsutil.service.CommonsUtilService;
import com.nacre.ims.commonsutil.serviceimpl.CommonsUtilServiceImpl;
@WebServlet("/GetCity")
public class GetCity extends HttpServlet {
	CommonsUtilService service=null;

	/**
	 * @author Nikhilesh
	 */
	private static final long serialVersionUID = 1L;
	public GetCity() {
		System.out.println("GetCityController.GetCityController()");
	}

	

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside servlet city");
		resp.setContentType("application/json");
		int sid=Integer.valueOf(req.getParameter("stateId1"));
		
		System.out.println("state id .........................."+sid);
		service=new CommonsUtilServiceImpl();
	//	Map<Integer,String> map=new HashMap<>();
	   Map map;
	try {
		map = service.getCity(sid);
	
		Gson g=new Gson();
		String s=g.toJson(map);
		System.out.println(s);
		resp.getWriter().write(s);
	//	resp.flushBuffer();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
