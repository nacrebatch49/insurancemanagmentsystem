package com.nacre.ims.bean;

public class State {
	/**
	 * @author Nikhilesh 
	 */
	
	private String StateId;
	private String StateName;

	public String getStateId() {
		return StateId;
	}

	public void setStateId(String stateId) {
		StateId = stateId;
	}

	public String getStateName() {
		return StateName;
	}

	public void setStateName(String stateName) {
		StateName = stateName;
	}

	@Override
	public String toString() {
		return "State [StateId=" + StateId + ", StateName=" + StateName + "]";
	}

}
