package com.nacre.ims.bean;

import java.util.Date;

public class CustomerViewProfileBean {
	private String customer_first_name;
	private String customer_last_name;
	private String customer_email_id;
	private String customer_password;
	private long customer_mobile_no;
	private String customer_gender;
	private Date customer_dob;
	private long adhar_no;
	private String country_name;
	private String state_name;
	private String city_name;
	private String local_address;
	private int pincode;

	public String getCustomer_first_name() {
		return customer_first_name;
	}

	public void setCustomer_first_name(String customer_first_name) {
		this.customer_first_name = customer_first_name;
	}

	public String getCustomer_last_name() {
		return customer_last_name;
	}

	public void setCustomer_last_name(String customer_last_name) {
		this.customer_last_name = customer_last_name;
	}

	public String getCustomer_email_id() {
		return customer_email_id;
	}

	public void setCustomer_email_id(String customer_email_id) {
		this.customer_email_id = customer_email_id;
	}

	public String getCustomer_password() {
		return customer_password;
	}

	public void setCustomer_password(String customer_password) {
		this.customer_password = customer_password;
	}

	public long getCustomer_mobile_no() {
		return customer_mobile_no;
	}

	public void setCustomer_mobile_no(long customer_mobile_no) {
		this.customer_mobile_no = customer_mobile_no;
	}

	public String getCustomer_gender() {
		return customer_gender;
	}

	public void setCustomer_gender(String customer_gender) {
		this.customer_gender = customer_gender;
	}

	public Date getCustomer_dob() {
		return customer_dob;
	}

	public void setCustomer_dob(Date customer_dob) {
		this.customer_dob = customer_dob;
	}

	public long getAdhar_no() {
		return adhar_no;
	}

	public void setAdhar_no(long adhar_no) {
		this.adhar_no = adhar_no;
	}

	public String getCountry_name() {
		return country_name;
	}

	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}

	public String getState_name() {
		return state_name;
	}

	public void setState_name(String state_name) {
		this.state_name = state_name;
	}

	public String getCity_name() {
		return city_name;
	}

	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}

	public String getLocal_address() {
		return local_address;
	}

	public void setLocal_address(String local_address) {
		this.local_address = local_address;
	}

	public int getPincode() {
		return pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	@Override
	public String toString() {
		return "CustomerViewProfileBean [customer_first_name=" + customer_first_name + ", customer_last_name="
				+ customer_last_name + ", customer_email_id=" + customer_email_id + ", customer_password="
				+ customer_password + ", customer_mobile_no=" + customer_mobile_no + ", customer_gender="
				+ customer_gender + ", customer_dob=" + customer_dob + ", adhar_no=" + adhar_no + ", country_name="
				+ country_name + ", state_name=" + state_name + ", city_name=" + city_name + ", local_address="
				+ local_address + ", pincode=" + pincode + "]";
	}

}
