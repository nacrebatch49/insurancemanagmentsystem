package com.nacre.ims.bean;

import java.util.Date;

public class MyCustomerDetailsByAgent  {
	public MyCustomerDetailsByAgent(){
		System.out.println("MyCustomerDetailsByAgent.MyCustomerDetailsByAgent()");
	}

	private String first_name;
	private String email;
	private long mobile_no;
	private String address;;
	private String policies;
	private String gender;
	private Date dob;
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(long mobile_no) {
		this.mobile_no = mobile_no;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPolicies() {
		return policies;
	}
	public void setPolicies(String policies) {
		this.policies = policies;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	@Override
	public String toString() {
		return "MyCustomerDetailsByAgent [first_name=" + first_name + ", email=" + email + ", mobile_no=" + mobile_no
				+ ", address=" + address + ", policies=" + policies + ", gender=" + gender + ", dob="
				+ dob + "]";
	}
	
	
	
}

	

