package com.nacre.ims.bean;

public class Country {
	
	/**
	 * @author Nikhilesh 
	 */
	private String CountryId;
	private String CountryName;

	public String getCountryId() {
		return CountryId;
	}

	public void setCountryId(String countryId) {
		CountryId = countryId;
	}

	public String getCountryName() {
		return CountryName;
	}

	public void setCountryName(String countryName) {
		CountryName = countryName;
	}

	@Override
	public String toString() {
		return "Country [CountryId=" + CountryId + ", CountryName=" + CountryName + "]";
	}

}
