package com.nacre.ims.bean;



import java.util.Date;

public class AgentViewProfileBean {
	private String agent_name;

	private long mobile_number;
	private String agent_email_id;
	private String gender;
	private Date dob;
	private Date date_of_joining;
	private long adhaar_card_no;
	private long pancard_no;
	private String qualification;
	private String university;
	private int year_of_passout;
	private float percentage;
	private String country_name;
	private String state_name;
	private String city_name;
	private String local_address;
	private int pincode;
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	public long getMobile_number() {
		return mobile_number;
	}
	public void setMobile_number(long mobile_number) {
		this.mobile_number = mobile_number;
	}
	public String getAgent_email_id() {
		return agent_email_id;
	}
	public void setAgent_email_id(String agent_email_id) {
		this.agent_email_id = agent_email_id;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public Date getDate_of_joining() {
		return date_of_joining;
	}
	public void setDate_of_joining(Date date_of_joining) {
		this.date_of_joining = date_of_joining;
	}
	public long getAdhaar_card_no() {
		return adhaar_card_no;
	}
	public void setAdhaar_card_no(long adhaar_card_no) {
		this.adhaar_card_no = adhaar_card_no;
	}
	public long getPancard_no() {
		return pancard_no;
	}
	public void setPancard_no(long pancard_no) {
		this.pancard_no = pancard_no;
	}
	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public String getUniversity() {
		return university;
	}
	public void setUniversity(String university) {
		this.university = university;
	}
	public int getYear_of_passout() {
		return year_of_passout;
	}
	public void setYear_of_passout(int year_of_passout) {
		this.year_of_passout = year_of_passout;
	}
	public float getPercentage() {
		return percentage;
	}
	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	public String getCountry_name() {
		return country_name;
	}
	public void setCountry_name(String country_name) {
		this.country_name = country_name;
	}
	public String getState_name() {
		return state_name;
	}
	public void setState_name(String state_name) {
		this.state_name = state_name;
	}
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public String getLocal_address() {
		return local_address;
	}
	public void setLocal_address(String local_address) {
		this.local_address = local_address;
	}
	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	@Override
	public String toString() {
		return "AgentViewProfileBean [agent_name=" + agent_name + ", mobile_number=" + mobile_number + ", agent_email_id="
				+ agent_email_id + ", gender=" + gender + ", dob=" + dob + ", date_of_joining=" + date_of_joining
				+ ", adhaar_card_no=" + adhaar_card_no + ", pancard_no=" + pancard_no + ", qualification="
				+ qualification + ", university=" + university + ", year_of_passout=" + year_of_passout
				+ ", percentage=" + percentage + ", country_name=" + country_name + ", state_name=" + state_name
				+ ", city_name=" + city_name + ", local_address=" + local_address + ", pincode=" + pincode + "]";
	}

	
}

