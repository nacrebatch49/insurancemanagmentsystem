package com.nacre.ims.admin.controller;
/**
 * @author Ajit
 *
 */
import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;



@WebServlet("/gethomeinsurancecontroller")
public class ViewHomeInsuranceController extends HttpServlet {
	
		private static final long serialVersionUID = 1L;
	       
	    /**
	     * @see HttpServlet#HttpServlet()
	     */
	    public ViewHomeInsuranceController() {
	       
	        // TODO Auto-generated constructor stub
	    }
	
		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			AdminService adminService=null;
			HttpSession session=null;
			try {
			  adminService= new AdminServiceImpl(); 
			  session=req.getSession();
			  session.setAttribute("data", adminService.getHomeInsurence());
			  resp.sendRedirect("/ims/admin/pages/homeinsurance.jsp");
			  System.out.println(adminService.getHomeInsurence());
			
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
	
		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			doGet(request, response);
		}
	
	}

	