package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;
import com.nacre.ims.dto.Agent_DataDTO;


@WebServlet("/get_Agent_DataController")
public class Get_Agent_DataController extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter out = res.getWriter();
		// simple settings
		res.setContentType("text/html");
		HttpSession ses=req.getSession();

		AdminService service = new AdminServiceImpl();
		

		try {
			// call service class method
			List<Agent_DataDTO> agentData = service.fetchAgentData();
			System.out.println(agentData);
			
				System.out.println("Data is found..");
			ses.setAttribute("agentData", agentData);
			res.sendRedirect("/ims/admin/pages/agentViewByAdmin111.jsp");
			//rd.forward(req, res);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
