package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;
import com.nacre.ims.dto.FixAgentMeetingDTO;

@WebServlet("/fixAgentMeetingController")
public class FixAgentMeetingController extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		System.out.println("FixAgentMeetingController.doGet()");
		PrintWriter pw = res.getWriter();
		// pw.println("soma shelke ............");

		String venue = req.getParameter("venue");
		String date1 = req.getParameter("date");
		String time = req.getParameter("time");
		
		String[] agentId = req.getParameterValues("agentids");
		
		System.out.println(Arrays.toString(agentId));


		System.out.println("------------------------------------------------");
		//System.out.println(venue + "  " + date1 + " " + time + "  " + agentId.toString());
		System.out.println("------------------------------------------------");
		System.out.println("FixAgentMeetingController.doGet()");
		FixAgentMeetingDTO dto = new FixAgentMeetingDTO();
		Date mettingTime = null;
		Date meetDate = null;
		// convert String[] into integer
		// array.............................................
		int size = agentId.length;
		int[] agentIds = new int[size];
		for (int i = 0; i < agentIds.length; i++) {
			agentIds[i] = Integer.parseInt(agentId[i]);
		}
		// convert time in simple date
		// format...............................................
		SimpleDateFormat timeFormat = new SimpleDateFormat("hh:MM");
		try {
			mettingTime = timeFormat.parse(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println("meeting time is :: " + mettingTime.toString());

		// convert date in simple date
		// format................................................
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = (Date) formatter.parse(req.getParameter("date"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println("meeting date is :: " + meetDate);
		// set DTO
		// propereties.................................................................
		dto.setAgentId(agentIds);
		dto.setVenue(venue);
		dto.setDate(new java.sql.Date(date.getTime()));

		dto.setTime(new Time(mettingTime.getTime()));
		System.out.println("DTO IS -:- " + dto);

		// create Service class
		// object...........................................................
		AdminService service = new AdminServiceImpl();

		// call service class method..
		int count = service.fixAgentMeeting(dto);

		if (count != 0) {
			res.sendRedirect("/get_Agent_DataController");
		}
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}
}
