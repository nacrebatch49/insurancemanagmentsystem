package com.nacre.ims.admin.controller;

/**
 * @author Ajit
 *
 */
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;
import com.nacre.ims.dto.EditHomeInsuranceDTO;

@WebServlet("/edithomeinsurancecontroller")
public class EditHomeInsuranceController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditHomeInsuranceController() {

		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		EditHomeInsuranceDTO editHomeInsuranceDTO = null;
		AdminService adminService = null;
		RequestDispatcher requestDispatcher = null;
		try {
			String policyName = req.getParameter("policyname");
			String policyCoverage = req.getParameter("policycoverage");
			String policyDescription = req.getParameter("policydescription");
			int policyDuration = Integer.parseInt(req.getParameter("policyduration"));
			double initialAmount = Double.parseDouble(req.getParameter("initialamount"));
			double totalAmount = Double.parseDouble(req.getParameter("totalamount"));
			double policyRate = Double.parseDouble(req.getParameter("policyrate"));
			String policyOffer = req.getParameter("policyoffer");
			String policyNumber = req.getParameter("policynumber");

			editHomeInsuranceDTO = new EditHomeInsuranceDTO();
			editHomeInsuranceDTO.setPolicyName(policyName);
			editHomeInsuranceDTO.setCoverage(policyCoverage);
			editHomeInsuranceDTO.setDescription(policyDescription);
			editHomeInsuranceDTO.setDuration(policyDuration);
			editHomeInsuranceDTO.setInitialAmount(initialAmount);
			editHomeInsuranceDTO.setTotalamount((totalAmount));
			editHomeInsuranceDTO.setRateOfInterest(policyRate);
			editHomeInsuranceDTO.setPolicyOffer(policyOffer);
			editHomeInsuranceDTO.setPolicyNumber(policyNumber);
			System.out.println(editHomeInsuranceDTO);

			adminService = new AdminServiceImpl();
			int data = adminService.editHomeInsurence(editHomeInsuranceDTO);

			if (data > 0) {
				
				  requestDispatcher =req.getRequestDispatcher("gethomeinsurancecontroller");
				  requestDispatcher.forward(req, resp); 
				
				System.out.println("data inserted");
			} else {
				
				 requestDispatcher =req.getRequestDispatcher("gethomeinsurancecontroller");
				 requestDispatcher.forward(req, resp);
				 
				System.out.println("data not inserted");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
