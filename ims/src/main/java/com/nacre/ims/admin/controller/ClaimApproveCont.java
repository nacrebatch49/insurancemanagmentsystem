package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.nacre.ims.admin.service.AdminClaimService;
import com.nacre.ims.admin.serviceimpl.AdminClaimServiceImpl;
import com.nacre.ims.dto.ClaimDataDTO;

/*
  @author Rohit Ubare
*/

@WebServlet("/claimApp")
public class ClaimApproveCont extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	AdminClaimService adminClaim = null;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter writer = resp.getWriter();
		resp.setContentType("application/json");
		adminClaim = new AdminClaimServiceImpl();
		

		String pNo = req.getParameter("no");
		String nName = req.getParameter("nName");
		String nEmail = req.getParameter("nEmail");
		String relation = req.getParameter("relation");
		
		ClaimDataDTO ddto = new ClaimDataDTO();
		ddto.setpNo(pNo);
		ddto.setnName(nName);
		ddto.setnEmail(nEmail);
		ddto.setRelation(relation);
		
		ClaimDataDTO rdto = new ClaimDataDTO();
		try {
			rdto = adminClaim.getClaimResult(ddto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String grdto =  gson.toJson(rdto);
		writer.println(grdto);
		
		HttpSession session = req.getSession();
		session.setAttribute("resultDTO", rdto);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
