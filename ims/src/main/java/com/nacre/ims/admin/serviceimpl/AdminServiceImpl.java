package com.nacre.ims.admin.serviceimpl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.util.Map;
import org.apache.commons.beanutils.BeanUtils;
import com.nacre.ims.admin.dao.AdminDao;
import com.nacre.ims.admin.dao.Send_Reminders_DAO;
import com.nacre.ims.admin.daoimpl.AdminDaoImpl;
import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.daoimpl.Send_Reminders_DAO_Impl;
import com.nacre.ims.admin.service.AdminService;

import com.nacre.ims.bo.AgentApprovalBO;
import com.nacre.ims.dto.AgentApprovalDTO;
import com.nacre.ims.bo.AddCarInsuranceBO;
import com.nacre.ims.dto.AddCarInsuranceDTO;
import com.nacre.ims.bo.AddChildCareInsurenceBO;
import com.nacre.ims.bo.AddHomeInsuranceBO;
import com.nacre.ims.bo.AddLifeInsurenceBO;
import com.nacre.ims.bo.EditChildCareInsurenceBO;
import com.nacre.ims.bo.EditHomeInsuranceBO;
import com.nacre.ims.bo.EditLifeInsurenceBO;
import com.nacre.ims.bo.GetChildCareInsurenceDataBO;
import com.nacre.ims.bo.GetLifeInsurenceDataBO;
import com.nacre.ims.bo.Send_Rem_Before_1_Day_Bo;
import com.nacre.ims.bo.Send_Rem_Dely_Pay_Bo;
import com.nacre.ims.dto.AddChildCareInsurenceDTO;
import com.nacre.ims.dto.AddHomeInsuranceDTO;
import com.nacre.ims.dto.AddLifeInsurenceDTO;
import com.nacre.ims.dto.Agent_DataDTO;
import com.nacre.ims.dto.EditChildCareInsurenceDTO;
import com.nacre.ims.dto.EditHomeInsuranceDTO;
import com.nacre.ims.dto.EditLifeInsurenceDTO;
import com.nacre.ims.dto.FixAgentMeetingDTO;
import com.nacre.ims.dto.GetChildCareInsurenceDataDTO;
import com.nacre.ims.dto.GetHomeInsuranceDTO;
import com.nacre.ims.dto.GetLifeInsurenceDataDTO;


public class AdminServiceImpl implements AdminService {
	AdminDao dao = new AdminDaoImpl();
	AddCarInsuranceBO addCarInsuranceBO = null;

    AddHomeInsuranceBO addHomeInsuranceBO=null;
    AddHomeInsuranceDTO addHomeInsuranceDTO=null;
    GetHomeInsuranceDTO getHomeInsuranceDTO=null;
    com.nacre.ims.bo.GetHomeInsuranceBO GetHomeInsuranceBO=null;
    EditHomeInsuranceBO editHomeInsuranceBO=null;
 
    @Override
	public List<AgentApprovalDTO> getApprovalData() throws SQLException {
		/*
		 * G Venu 
		 */
		AdminDao dao=null;
		List<AgentApprovalDTO> listdto=new ArrayList<AgentApprovalDTO>();
		List<AgentApprovalBO> listbo=null;
		//create the objects
		dao=new AdminDaoImpl();
		listbo=dao.getApprovalData();
		listbo.forEach(bo->{
			AgentApprovalDTO dto=new AgentApprovalDTO();
			try {
				BeanUtils.copyProperties(dto,bo );
				listdto.add(dto);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		});
		

		return listdto;
	}

	@Override
	public String getApprovedOrReject(int[] i,String btnVal) throws SQLException {
		/*
		 * G Venu
		 */
		AdminDao dao=null;
		//create object for daoimpl class
		dao=new AdminDaoImpl();
		//send the int array to dao
		int res=dao.getApprovedOrRejectStatus(i,btnVal);
		if(res>0) {
			return res+" Records updated";
		}
		else {
			return "Updation failed";
		}
	}
	@Override
	public int addCarInsurance(AddCarInsuranceDTO addCarInsuranceDTO) {
		AddCarInsuranceBO bo = new AddCarInsuranceBO();
		try {
			BeanUtils.copyProperties(bo, addCarInsuranceDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dao.addCarInsurance(bo);
	}

	@Override
	public List<AddCarInsuranceDTO> getAllCarPolicy() {
		List<AddCarInsuranceDTO> listdto = new ArrayList();
		List<AddCarInsuranceBO> list = dao.getAllCarPolicy();
		for (AddCarInsuranceBO bo : list) {
			AddCarInsuranceDTO dto = new AddCarInsuranceDTO();
			dto.setCarIncId(bo.getCarIncId());
			dto.setPolicyName(bo.getPolicyName());
			dto.setCoverage(bo.getCoverage());
			dto.setPolicyDescription(bo.getPolicyDescription());
			dto.setRateofInterest(bo.getRateofInterest());
			dto.setDuration(bo.getDuration());
			dto.setPaybleAmount(bo.getPaybleAmount());
			dto.setInitialAmount(bo.getInitialAmount());
			listdto.add(dto);

		}
		return listdto;
	}

	@Override
	public int editCarInsurance(AddCarInsuranceDTO addCarInsuranceDTO) {
		AddCarInsuranceBO bo = new AddCarInsuranceBO();
		try {
			BeanUtils.copyProperties(bo, addCarInsuranceDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dao.editCarInsurance(bo);
	}

	@Override
	public int deleteCarInsurance(int id) {

		return dao.deleteCarInsurance(id);
	}

	@Override
	public int addlifeinsurence(AddLifeInsurenceDTO dto) throws Exception {
		AddLifeInsurenceBO bo = new AddLifeInsurenceBO();
		int output = 0;

		// set property DTO to BO
		BeanUtils.copyProperties(bo, dto);

		/*
		 * bo.setName(dto.getName());
		 * bo.setInsurance_policy_no(dto.getInsurance_policy_no());
		 * bo.setDesciption(dto.getDesciption()); bo.setCovarage(dto.getCovarage());
		 * bo.setDuration(dto.getDuration());
		 * bo.setRate_of_interest(dto.getRate_of_interest());
		 * bo.setTotal_amount_paid(dto.getTotal_amount_paid());
		 * bo.setLife_insurance_offers(dto.getLife_insurance_offers());
		 */
		return output = dao.addlifeinsurence(bo);
	}

	@Override
	public int addchildcareinsurence(AddChildCareInsurenceDTO dto) throws Exception {
		AddChildCareInsurenceBO bo = new AddChildCareInsurenceBO();
		int output = 0;
		BeanUtils.copyProperties(bo, dto);
		return output = dao.addchildcareinsurence(bo);
	}

	@Override
	public List<GetLifeInsurenceDataDTO> fetchLifeInsurence() throws Exception {
		GetLifeInsurenceDataDTO dto = null;
		List<GetLifeInsurenceDataDTO> list = new ArrayList();

		List<GetLifeInsurenceDataBO> bo1 = null;
		// set property DTO to BO
		bo1 = dao.fetchLifeInsurence();
		for (GetLifeInsurenceDataBO bo2 : bo1) {
			dto = new GetLifeInsurenceDataDTO();
			dto.setName(bo2.getName());
			dto.setDuration(bo2.getDuration());
			dto.setInsurance_policy_no(bo2.getInsurance_policy_no());
			dto.setRate_of_interest(bo2.getRate_of_interest());
			dto.setDesciption(bo2.getDesciption());
			dto.setTotal_amount_paid(bo2.getTotal_amount_paid());
			dto.setLife_insurance_offers(bo2.getLife_insurance_offers());
			dto.setCovarage(bo2.getCovarage());
			dto.setStatus(bo2.getStatus());
			dto.setLifeid(bo2.getLifeid());
			list.add(dto);
		}

		return list;
	}

	@Override
	public List<GetChildCareInsurenceDataDTO> fetchChildCareInsurence() throws Exception {
		GetChildCareInsurenceDataDTO dto = null;
		List<GetChildCareInsurenceDataDTO> list = new ArrayList<>();
		List<GetChildCareInsurenceDataBO> bo1 = null;
		bo1 = dao.fetchChildCareInsurence();
		for (GetChildCareInsurenceDataBO bo2 : bo1) {
			dto = new GetChildCareInsurenceDataDTO();
			BeanUtils.copyProperties(dto, bo2);
			list.add(dto);
			System.out.println(dto);
		}
		return list;
	}

	@Override
	public int editLifeInsurence(EditLifeInsurenceDTO dto) throws Exception {

		EditLifeInsurenceBO bo = new EditLifeInsurenceBO();
		int output = 0;

		// set property DTO to BO
		BeanUtils.copyProperties(bo, dto);

		return output = dao.editLifeInsurence(bo);
	}

	@Override
	public int editchildcareinsurence(EditChildCareInsurenceDTO dto) throws Exception {
		EditChildCareInsurenceBO bo = new EditChildCareInsurenceBO();
		int output = 0;
		BeanUtils.copyProperties(bo, dto);
		return output = dao.editchildcareinsurence(bo);
	}

	@Override
	public int deleteLifeInsurance(int id) {

		return dao.deleteLifeInsurence(id);
	}

	@Override
	public int deleteChildCareInsurance(int id) {

		return dao.deleteChildCareInsurence(id);

	}
	
	@Override
	public int addHomeInsurence(AddHomeInsuranceDTO addHomeInsuranceDTO) {
		addHomeInsuranceBO=new AddHomeInsuranceBO();
		try {
			BeanUtils.copyProperties(addHomeInsuranceBO, addHomeInsuranceDTO);
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return dao.addHomeInsurence(addHomeInsuranceBO);
	}
	@Override
	public List<GetHomeInsuranceDTO> getHomeInsurence() {
		List<GetHomeInsuranceDTO> listDto=null;
		List<com.nacre.ims.bo.GetHomeInsuranceBO> listBo=null; 
		
			
			listDto=new ArrayList<>();
			listBo=dao.getHomeInsurence();
			for (com.nacre.ims.bo.GetHomeInsuranceBO getHomeInsuranceBO : listBo) {
				getHomeInsuranceDTO=new GetHomeInsuranceDTO();
				getHomeInsuranceDTO.setPolicyName(getHomeInsuranceBO.getPolicyName());
				getHomeInsuranceDTO.setCoverage(getHomeInsuranceBO.getCoverage());
				getHomeInsuranceDTO.setDescription(getHomeInsuranceBO.getDescription());
				getHomeInsuranceDTO.setDuration(getHomeInsuranceBO.getDuration());
				getHomeInsuranceDTO.setInitialAmount(getHomeInsuranceBO.getInitialAmount());
				getHomeInsuranceDTO.setTotalamount(getHomeInsuranceBO.getTotalamount());
				getHomeInsuranceDTO.setRateOfInterest(getHomeInsuranceBO.getRateOfInterest());
				getHomeInsuranceDTO.setPolicyOffer(getHomeInsuranceBO.getPolicyOffer());
				getHomeInsuranceDTO.setPolicyNumber(getHomeInsuranceBO.getPolicyNumber());
				getHomeInsuranceDTO.setHomeInsuranceId(getHomeInsuranceBO.getHomeInsuranceId());
				listDto.add(getHomeInsuranceDTO);
			}
			
		
		return listDto;
	}
	@Override
	public int editHomeInsurence(EditHomeInsuranceDTO editHomeInsuranceDTO) {
		editHomeInsuranceBO=new EditHomeInsuranceBO();
		try {
			BeanUtils.copyProperties(editHomeInsuranceBO, editHomeInsuranceDTO);
		} catch (IllegalAccessException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return dao.editHomeInsurence(editHomeInsuranceBO);
	}
	@Override
	public int deleteHomeInsurance(int id) {
		
		return dao.deleteHomeInsurance(id);
	}
	
	@Override
	public List viewAllCustomerData() throws SQLException {
		//create dao class object
		AdminDao dao=new AdminDaoImpl();
		List list=dao.getAllCustomer();
		System.out.println(list);
		return list;
	}
	@Override
	public int removeCustomer(int id) {
		//create dao class object
		AdminDao dao=new AdminDaoImpl();
		int count=0;
		//call DAO class method
		try{
			count=dao.removeCustomer(id);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}
	

	
	
	@Override
	public List<Agent_DataDTO> fetchAgentData() {
		AdminDao dao1 = new AdminDaoImpl();
		
		try{
		List list=dao1.fetchAgentData();
		 //Agent_DataDTO dto=(Agent_DataDTO) list.get(1);
		return list;
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@Override
	public int deleteAgent(int id) {
		AdminDao dao1 = new AdminDaoImpl();
		int count=dao1.deleteAgent(id);
		return count;
	}

	
	@Override
	public int fixAgentMeeting(FixAgentMeetingDTO dto) {
		AdminDao dao1 = new AdminDaoImpl();
		int count=0;
		try{
			count=dao1.fixAgentMeeting(dto);
		}catch (Exception e) {
			e.printStackTrace();
		}	
		return count;
	}
	
	@Override
	public Map sendReminder() throws Exception {
		Send_Reminders_DAO dao=new Send_Reminders_DAO_Impl();
		Map<Integer,Send_Rem_Before_1_Day_Bo> m1=dao.sendReminder();	
		System.out.println("Service new Polices Class"+m1);
		return m1;
	}

	@Override
	public Map sendRemDely() throws Exception {
		Send_Reminders_DAO dao=new Send_Reminders_DAO_Impl();
		Map<Integer,Send_Rem_Dely_Pay_Bo> m1=dao.sendRemDely();	
		System.out.println("Service dely payment method Class"+m1);
		return m1;
	}

	@Override
	public Map sendRem1Day() throws Exception {
		Send_Reminders_DAO dao=new Send_Reminders_DAO_Impl();
		Map<Integer,Send_Rem_Before_1_Day_Bo> m1=dao.sendRem1day();	
		System.out.println("Service Send reminder before 1 Day method Class"+m1);
		return m1;
	}
	
	
}

	