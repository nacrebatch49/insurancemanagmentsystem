package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.nacre.ims.admin.service.AdminClaimService;
import com.nacre.ims.admin.serviceimpl.AdminClaimServiceImpl;
import com.nacre.ims.dto.ClaimRequestDetailDTO;

/*
  @author Rohit Ubare
*/

@WebServlet("/claimReq")
public class ClaimRequestCont extends HttpServlet {

	private static final long serialVersionUID = 1L;

	AdminClaimService adminClaim = null;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		adminClaim = new AdminClaimServiceImpl();
		PrintWriter writer = resp.getWriter();
		resp.setContentType("application/json");

		List<ClaimRequestDetailDTO> claimName = null;

		try {
			claimName = adminClaim.getClaimName();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		String reqList = gson.toJson(claimName);
		writer.println(reqList);

		HttpSession session = req.getSession();
		session.setAttribute("list", claimName);
		System.out.println(claimName);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
