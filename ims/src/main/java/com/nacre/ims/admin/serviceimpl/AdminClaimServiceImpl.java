package com.nacre.ims.admin.serviceimpl;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.nacre.ims.admin.dao.AdminClaimDAO;
import com.nacre.ims.admin.daoimpl.AdminClaimDAOImpl;
import com.nacre.ims.admin.service.AdminClaimService;
import com.nacre.ims.bo.ClaimDataBO;
import com.nacre.ims.bo.ClaimRequestDetailBO;
import com.nacre.ims.dto.ClaimDataDTO;
import com.nacre.ims.dto.ClaimRequestDetailDTO;
/*
  @author Rohit Ubare
*/
public class AdminClaimServiceImpl implements AdminClaimService {

	AdminClaimDAO adminClaim = null;

	@Override
	public int getClaimNotification() throws Exception {
		adminClaim = new AdminClaimDAOImpl();
		return adminClaim.getClaimNotification();
	}

	@Override
	public List<ClaimRequestDetailDTO> getClaimName() throws Exception {
		ClaimRequestDetailDTO dto = null;
		ClaimRequestDetailBO bo = null;
		List<ClaimRequestDetailDTO> claimName = new LinkedList<>();
		adminClaim = new AdminClaimDAOImpl();
		List<ClaimRequestDetailBO> list = adminClaim.getClaimName();

		ListIterator<ClaimRequestDetailBO> li = list.listIterator();

		while (li.hasNext()) {
			dto = new ClaimRequestDetailDTO();
			bo = (ClaimRequestDetailBO) li.next();

			dto.setfName(bo.getfName());
			dto.setlName(bo.getlName());
			dto.setPolicyNo(bo.getPolicyNo());
			dto.setNominiName(bo.getNominiName());
			dto.setNominiEmail(bo.getNominiEmail());
			dto.setRelation(bo.getRelation());

			claimName.add(dto);
		}

		return claimName;
	}

	@Override
	public ClaimDataDTO getClaimResult(ClaimDataDTO ddto) throws Exception {
		ClaimDataDTO rdto = new ClaimDataDTO();
		ClaimDataBO bo = null;
		adminClaim = new AdminClaimDAOImpl();
		bo = adminClaim.getClaimData(ddto.getpNo());

		float totalAmount = 0, roi = 0, gst = 0, payAmount = 0;
		String statusId = "CLAIMREJECTED";

		if (ddto.getnName().equalsIgnoreCase(bo.getnName()) && ddto.getnEmail().equalsIgnoreCase(bo.getnEmail())
				&& ddto.getRelation().equalsIgnoreCase(bo.getRelation())) {
			totalAmount = Float.parseFloat(bo.getTotalAmount());
			roi = Float.parseFloat(bo.getROI());
			gst = 5.0f;

			payAmount = totalAmount + (totalAmount * (roi / 100));
			payAmount = payAmount - (payAmount * (gst / 100));
			statusId = "CLAIMPASS";
		}

		rdto.setpNo(ddto.getpNo());
		rdto.setpName(bo.getpName());
		rdto.setnName(ddto.getnName());
		rdto.setnEmail(ddto.getnEmail());
		rdto.setRelation(ddto.getRelation());
		rdto.setTotalAmount(Float.toString(totalAmount));
		rdto.setROI(Float.toString(roi));
		rdto.setGST(Float.toString(gst));
		rdto.setPayAmount(Float.toString(payAmount));
		rdto.setStatusId(statusId);
		rdto.setCustID(bo.getCustID());
		rdto.setClaimID(bo.getClaimID());

		return rdto;
	}

	@Override
	public int setFinalClaimReport(ClaimDataDTO dto) throws Exception {

		adminClaim = new AdminClaimDAOImpl();
		ClaimDataBO bo = new ClaimDataBO();
		int statusID = adminClaim.getStatusID(dto.getStatusId());

		bo.setClaimID(dto.getClaimID());
		bo.setCustID(dto.getCustID());
		bo.setStatID(Integer.toString(statusID));
		bo.setDes(dto.getDes());
		bo.setPayAmount(dto.getPayAmount());

		int count = adminClaim.setFinalClaimReport(bo);

		if (count > 0) {
			return adminClaim.updateClaimRequest(Integer.parseInt(dto.getClaimID()), Integer.parseInt(dto.getpNo()),
					statusID);
		}
		return 0;
	}

}
