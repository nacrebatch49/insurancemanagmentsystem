/**
 * 
 */
package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;
import com.nacre.ims.dto.AddCarInsuranceDTO;

/**
 * @author Barkha
 *
 */
@WebServlet("/getCarInsuranceDataController")
public class GetCarInsuranceDataController extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside view details");
		resp.setContentType("text/html");
		AdminService service=new AdminServiceImpl();
		List<AddCarInsuranceDTO> list=service.getAllCarPolicy();
		System.out.println(list);
		HttpSession ses=req.getSession();
		ses.setAttribute("list", list);
		resp.sendRedirect("/ims/admin/pages/CarInsue.jsp");
		
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
