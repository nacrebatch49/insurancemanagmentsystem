package com.nacre.ims.admin.dao;

import java.sql.SQLException;
import java.util.List;
import com.nacre.ims.bo.AddCarInsuranceBO;
import com.nacre.ims.bo.AddChildCareInsurenceBO;
import com.nacre.ims.bo.AddHomeInsuranceBO;
import com.nacre.ims.bo.AddLifeInsurenceBO;
import com.nacre.ims.bo.EditChildCareInsurenceBO;
import com.nacre.ims.bo.EditHomeInsuranceBO;
import com.nacre.ims.bo.EditLifeInsurenceBO;
import com.nacre.ims.bo.GetChildCareInsurenceDataBO;
import com.nacre.ims.bo.GetHomeInsuranceBO;
import com.nacre.ims.bo.GetLifeInsurenceDataBO;
import com.nacre.ims.bo.ViweallCustomerdetailsBO;
import com.nacre.ims.dto.Agent_DataDTO;
import com.nacre.ims.dto.FixAgentMeetingDTO;
import com.nacre.ims.bo.AgentApprovalBO;



public interface AdminDao {
public List<AgentApprovalBO> getApprovalData()throws SQLException;
public int getApprovedOrRejectStatus(int a[],String btnVal)throws SQLException;
	public int addCarInsurance(AddCarInsuranceBO addCarInsuranceBO);
	public    List<AddCarInsuranceBO>  getAllCarPolicy();
	public int editCarInsurance(AddCarInsuranceBO addCarInsuranceBO);
	public int deleteCarInsurance(int id);
	public int addlifeinsurence(AddLifeInsurenceBO bo)throws Exception;
 
	public int addchildcareinsurence(AddChildCareInsurenceBO bo)throws Exception;
    
	public List<GetLifeInsurenceDataBO> fetchLifeInsurence()throws Exception;
	
	public List<GetChildCareInsurenceDataBO> fetchChildCareInsurence()throws Exception;

	public int editLifeInsurence(EditLifeInsurenceBO bo)throws Exception;

	public int editchildcareinsurence(EditChildCareInsurenceBO bo)throws Exception;

	public int deleteLifeInsurence(int id);
	public int addHomeInsurence(AddHomeInsuranceBO addHomeInsuranceBO);
	public List<GetHomeInsuranceBO> getHomeInsurence();
	public int editHomeInsurence(EditHomeInsuranceBO editHomeInsuranceBO);
	public int deleteHomeInsurance(int id);

	public int deleteChildCareInsurence(int id);
	public List<ViweallCustomerdetailsBO> getAllCustomer() throws SQLException;

	public int removeCustomer(int id);
    public List<Agent_DataDTO> fetchAgentData();
	
	public int deleteAgent(int id);
	
	public int fixAgentMeeting(FixAgentMeetingDTO dto);
	
}
 