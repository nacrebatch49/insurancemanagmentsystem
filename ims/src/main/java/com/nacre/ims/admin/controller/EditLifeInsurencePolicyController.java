/**
 * 
 */
package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;
import com.nacre.ims.dto.EditLifeInsurenceDTO;

/**
 * @author vishal patil
 *
 */
@WebServlet("/editlifeinsurencepolicycontroller")
public class EditLifeInsurencePolicyController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside servlet");
		PrintWriter pw =resp.getWriter();
		resp.setContentType("text/html;charset=UTF-8");
		EditLifeInsurenceDTO dto=null;
		AdminService service=null;
		 try{
			 String name=req.getParameter("policyname");
			 String insurance_policy_no=req.getParameter("policyno");
			 String desciption=req.getParameter("policydescription");
			 String covarage=req.getParameter("policycoverage");
			 String duration=req.getParameter("policyduration");
			 String rate_of_interest=req.getParameter("policyrate");
			 String total_amount_paid=req.getParameter("policyamount");
			 String life_insurance_offers=req.getParameter("policyoffer");
			// String status=req.getParameter("policystatus");
			 
			 dto=new EditLifeInsurenceDTO();
			 dto.setName(name);
			 dto.setInsurance_policy_no(insurance_policy_no);
			 dto.setDesciption(desciption);
			 dto.setCovarage(covarage);
			 dto.setDuration(Integer.parseInt(duration));
			 dto.setRate_of_interest(Double.parseDouble(rate_of_interest));
			 dto.setTotal_amount_paid(Double.parseDouble(total_amount_paid));
			 dto.setLife_insurance_offers(life_insurance_offers);
			// dto.setStatus(status);
			 System.out.println("editdto="+dto);
			 
			 service=new AdminServiceImpl();
			 int data=service.editLifeInsurence(dto);
			 RequestDispatcher rd=null;
			 if (data > 0) {
					System.out.println("Recored inserted......");
					rd = req.getRequestDispatcher("getlifeinsurencedatacontroller");

					rd.forward(req, resp);
				}
		 }
		 catch(Exception e){
			 e.printStackTrace();
			 
		 }
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		doGet(req, resp);
	}

}
