/**
 * 
 */
package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;
import com.nacre.ims.dto.EditChildCareInsurenceDTO;

/**
 * @author vishal patil
 *
 */
@WebServlet("/editchildcareinsurencecontroller")
public class EditChildCareInsurenceController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside servlet");
		PrintWriter pw =resp.getWriter();
		resp.setContentType("text/html;charset=UTF-8");
		EditChildCareInsurenceDTO dto=null;
		AdminService service=null;
		
		try {
			String policyName=req.getParameter("policyname");
			String policyCoverage=req.getParameter("policycoverage");
			String policyDescription=req.getParameter("policydescription");
			int policyDuration=Integer.parseInt(req.getParameter("policyduration"));
			double initialAmount=Double.parseDouble(req.getParameter("policyinitialamount"));
			double totalAmount=Double.parseDouble(req.getParameter("policyamount"));
			double policyRate=Double.parseDouble(req.getParameter("policyrate"));
			String policyOffer=req.getParameter("policyoffer");
			
			dto= new EditChildCareInsurenceDTO();
			dto.setName(policyName);
			dto.setCoverage(policyCoverage);
		    dto.setDesciption(policyDescription);
			dto.setMax_limit(policyDuration);
			dto.setInitial_amount(initialAmount);
			dto.setPayable_amount(totalAmount);
			dto.setRate_of_interest(policyRate);
			dto.setLife_insurance_offers(policyOffer);
			
			System.out.println(dto);
			
			service= new AdminServiceImpl();
			 int data=service.editchildcareinsurence(dto);
			 RequestDispatcher rd=null;
			 if (data > 0) {
					System.out.println("Recored inserted......");
					rd = req.getRequestDispatcher("getchildcareinsurencedatacontroller");

					rd.forward(req, resp);
				}
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		doGet(req, resp);
	}

}
