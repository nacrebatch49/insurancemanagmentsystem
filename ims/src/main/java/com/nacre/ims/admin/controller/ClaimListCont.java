package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.nacre.ims.admin.service.AdminClaimService;
import com.nacre.ims.admin.serviceimpl.AdminClaimServiceImpl;

/*
  @author Rohit Ubare
*/
@WebServlet("/claimList")
public class ClaimListCont extends HttpServlet {

	private static final long serialVersionUID = 1L;

	AdminClaimService adminClaim = null;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int claimNotiy = 0;
		adminClaim = new AdminClaimServiceImpl();
		PrintWriter writer = resp.getWriter();
		resp.setContentType("application/json");

		try {
			claimNotiy = adminClaim.getClaimNotification();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Gson gson = new Gson();
		String claim = gson.toJson(claimNotiy);
		writer.println(claim);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
