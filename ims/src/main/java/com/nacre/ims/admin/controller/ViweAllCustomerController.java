package com.nacre.ims.admin.controller;

import java.io.IOException;

import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;
import com.nacre.ims.bo.ViweallCustomerdetailsBO;

@WebServlet("/viewAllCustomer")

public class ViweAllCustomerController extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		pw.println("welcome katole shailendra.....");

		AdminService service = new AdminServiceImpl();

		List<ViweallCustomerdetailsBO> customerList = new ArrayList<>();
		
		try {
			customerList = service.viewAllCustomerData();
			req.setAttribute("customerList", customerList);
			RequestDispatcher rd=getServletContext().getRequestDispatcher("/admin/pages/customerViwe.jsp");
			rd.forward(req, res);
			pw.println(customerList);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
