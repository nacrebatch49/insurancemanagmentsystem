package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;
@WebServlet("/agentApprovalOrRejectionUrl")
public class AdminAgentApproveOrRejectController extends HttpServlet {
	/**
	 * G Venu
	 */ 
	private static final long serialVersionUID = 1L;
 
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		AdminService service=null;
		String result="";
		//set the response type
		res.setContentType("text/html");
		String btnType=req.getParameter("btn");
		
		String agentId[]=req.getParameterValues("agentId");
		for(String s:agentId) {
			System.out.println(s);
		}  
		int len=agentId.length;
		int agentIds[]=new int[len];
		for(int i=0;i<len;i++) {
			agentIds[i]=Integer.parseInt(agentId[i]);
		} 
		//use service
		service=new AdminServiceImpl();
		try {
			result=service.getApprovedOrReject(agentIds, btnType);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(result.contains("Records updated")) {
			res.sendRedirect("admin/pages/home.jsp");
		}
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
