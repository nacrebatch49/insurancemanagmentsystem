/**
 * 
 */
package com.nacre.ims.admin.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;

/**SSSSSS
 * @author vishal patil
 *
 */
@WebServlet("/deletelifeinsurencecontroller")
public class DeleteLifeInsurenceController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int id=Integer.parseInt(req.getParameter("id").toString());
		System.out.println(id);
		AdminService service=new AdminServiceImpl() ;
		int c=service.deleteLifeInsurance(id);
		if(c>0) {
			resp.sendRedirect("/ims/getlifeinsurencedatacontroller");
		
		}
	}

	@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			doGet(req, resp);
		}



}
