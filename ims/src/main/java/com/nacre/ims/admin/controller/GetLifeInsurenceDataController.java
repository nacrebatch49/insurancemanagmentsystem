/**
 * 
 */
package com.nacre.ims.admin.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;


/**
 * @author vishal patil
 *
 */
@WebServlet("/getlifeinsurencedatacontroller")
public class GetLifeInsurenceDataController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		AdminService service=new AdminServiceImpl();
		HttpSession session=req.getSession();
			try {
				System.out.println(service.fetchLifeInsurence());
				session.setAttribute("list",service.fetchLifeInsurence());
			resp.sendRedirect("/ims/admin/pages/lifeinsurence.jsp");
			} catch (Exception e) {
				
				e.printStackTrace();
			}	
			
		
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
