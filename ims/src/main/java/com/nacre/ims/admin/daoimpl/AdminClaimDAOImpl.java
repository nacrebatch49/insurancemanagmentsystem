package com.nacre.ims.admin.daoimpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import com.nacre.ims.admin.dao.AdminClaimDAO;
import com.nacre.ims.bo.ClaimDataBO;
import com.nacre.ims.bo.ClaimRequestDetailBO;

/*
  @author Rohit Ubare
*/

public class AdminClaimDAOImpl implements AdminClaimDAO {

	Connection con = null;
	Statement stat = null;
	PreparedStatement pst = null;
	ResultSet set = null;

	public Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/insurancepolicies", "root", "root");
		return con;
	}

	@Override
	public int getClaimNotification() throws Exception {
		int count = 0;
		con = getConnection();
		stat = con.createStatement();
		set = stat.executeQuery("select count(*) from customer_claim_request_table where customer_claim_request_table.status_id=(select status_table.status_id from status_table where status_table.status_name='CLAIMPENDING')");

		if (set.next()) {
			count = set.getInt(1);
		}
		con.close();
		return count;
	}

	@Override
	public List<ClaimRequestDetailBO> getClaimName() throws Exception {
		ClaimRequestDetailBO bo = null;
		List<ClaimRequestDetailBO> claimName = new LinkedList<>();

		con = getConnection();
		pst = con.prepareStatement(
				"select cust.customer_first_name,cust.customer_last_name,claim.register_policy_no,claim.nominies_name,claim.email,claim.relation\r\n"
						+ "from customer_claim_request_table claim,customer_registration_table cust,insurancepolicies_table inc\r\n"
						+ "where claim.status_id=(select status_table.status_id from status_table where status_table.status_name='CLAIMPENDING')\r\n"
						+ "and inc.customer_register_policy_id=claim.register_policy_no\r\n"
						+ "and cust.costomer_register_id=inc.customer_id1");
		set = pst.executeQuery();

		while (set.next()) {
			bo = new ClaimRequestDetailBO();

			bo.setfName(set.getString(1));
			bo.setlName(set.getString(2));
			bo.setPolicyNo(set.getString(3));
			bo.setNominiName(set.getString(4));
			bo.setNominiEmail(set.getString(5));
			bo.setRelation(set.getString(6));

			claimName.add(bo);
		}
		con.close();
		return claimName;
	}

	@Override
	public ClaimDataBO getClaimData(String pNo) throws Exception {

		ClaimDataBO bo = new ClaimDataBO();
		con = getConnection();

		pst = con.prepareStatement(
				"select home.nominies_name,home.nominies_email_id,home.relation ,homemtr.name, homemtr.rate_of_interest ,sum(pt.pay_amount),it.customer_id1,creq.claim_id\r\n"
						+ "from customer_register_home_insuranc_table home ,home_insurance_master_table homemtr ,payment_table pt ,insurancepolicies_table it,customer_claim_request_table creq\r\n"
						+ "where home.register_policy_no=? \r\n"
						+ "and creq.register_policy_no=home.register_policy_no\r\n"
						+ "and home.home_insurance_id=homemtr.home_insurance_id\r\n"
						+ "and it.customer_register_policy_id=home.register_policy_no\r\n"
						+ "and pt.policy_id=it.policy_id group by pt.pay_amount");
		pst.setString(1, pNo);
		set = pst.executeQuery();
		while (set.next()) {
			bo.setnName(set.getString(1));
			bo.setnEmail(set.getString(2));
			bo.setRelation(set.getString(3));
			bo.setpName(set.getString(4));
			bo.setROI(set.getString(5));
			bo.setTotalAmount(set.getString(6));
			bo.setCustID(set.getString(7));
			bo.setClaimID(set.getString(8));
		}
		return bo;
	}

	@Override
	public int getStatusID(String status) throws Exception {
		con = getConnection();
		pst = con.prepareStatement("select stat.status_id from status_table stat where stat.status_name=?");
		pst.setString(1, status);
		set = pst.executeQuery();
		if (set.next()) {
			return set.getInt(1);
		}
		return 0;
	}

	@Override
	public int setFinalClaimReport(ClaimDataBO bo) throws Exception {

		con = getConnection();
		pst = con.prepareStatement(
				"INSERT INTO `insurancepolicies`.`final_claim_approved_table` (`claim_id`, `customer_id`, `status_id1`, `description_any`, `payAmount`) VALUES (?,?,?,?,?)");

		pst.setString(1, bo.getClaimID());
		pst.setString(2, bo.getCustID());
		pst.setString(3, bo.getStatID());
		pst.setString(4, bo.getDes());
		pst.setString(5, bo.getPayAmount());

		return pst.executeUpdate();

	}

	@Override
	public int updateClaimRequest(int claimID, int pNo, int statID) throws Exception {

		con = getConnection();
		pst = con.prepareStatement(
				"UPDATE `insurancepolicies`.`customer_claim_request_table` SET `status_id`=? WHERE  `claim_id`=? and register_policy_no=?");

		pst.setInt(1, statID);
		pst.setInt(2, claimID);
		pst.setInt(3, pNo);

		int result = pst.executeUpdate();
		return result;
	}

}
