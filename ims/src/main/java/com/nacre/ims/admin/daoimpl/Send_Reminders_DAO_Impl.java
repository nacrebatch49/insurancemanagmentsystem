package com.nacre.ims.admin.daoimpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import com.nacre.ims.admin.dao.Send_Reminders_DAO;
import com.nacre.ims.bo.Send_Rem_Before_1_Day_Bo;
import com.nacre.ims.bo.Send_Rem_Dely_Pay_Bo;
import com.nacre.ims.bo.Send_Reminder_Bo;
/*
 * @ Author 
 * 
 * 
 */
public class Send_Reminders_DAO_Impl implements Send_Reminders_DAO {

	 String sql="SELECT * FROM customer_registration_table";
	 
	 String sql1="select customer_first_name,customer_mobile_no,"
	 		+ "customer_email_id,pay_amount from customer_registration_table srt"
	 		+ " ,insurancepolicies_table it ,payment_table pt where srt.costomer_register_id=it.customer_id1"
	 		+ " and it.policy_id=pt.policy_id and pay_amount=0;";
	 String sql2="SELECT * FROM customer_registration_table";
	 private static final String GET_NEW_POLICIES="SELECT * FROM "
	 		+ "CAR_INSURANCE_MASTER_TABLE";
	private static final String GET_NEW_POLICIES_OF_UPDATED_ROWS=
				"SELECT * FROM CAR_INSURANCE_MASTER_TABLE WHERE "
				+ "car_insurance_id=?";
	public Map sendReminder() throws Exception {
		Map<Integer,Send_Reminder_Bo> m=new HashMap();
		Connection con=null; 
		Statement st=null;
		ResultSet rs=null;
		con=getConnection();
		int count=0;
		st=con.createStatement();
		rs=st.executeQuery(sql);
		while(rs.next()) {
			count++;
			Send_Reminder_Bo bo=new Send_Reminder_Bo();
			bo.setName(rs.getString(2));
			bo.setMobileNo(rs.getLong(6));
			bo.setEmailId(rs.getString(4));
			m.put(count,bo);
			
		}
		System.out.println("Dao class ::"+m);
		rs=st.executeQuery(sql1);
		while(rs.next()) {
			count++;
			Send_Reminder_Bo bo=new Send_Reminder_Bo();
			bo.setName(rs.getString(1));
			bo.setMobileNo(rs.getLong(2));
			bo.setEmailId(rs.getString(3));
			m.put(count,bo);
			
		}
		System.out.println("dao class 1::"+m);
		return m;
	}
	public Connection getConnection() throws NamingException, SQLException, ClassNotFoundException {
/*
		InitialContext ic = null;
		DataSource ds = null;
		Connection con = null;
		ic = new InitialContext();
		ds = (DataSource) ic.lookup("java:/comp/env/DsJndi");
		con = ds.getConnection();
	*/
		
		Class.forName("com.mysql.jdbc.Driver");
		
		Connection con=DriverManager.getConnection(  
				"jdbc:mysql://localhost:3306/insurancepolicies","root","root");
		return con;
	}
	
	@Override
	public Map sendRemDely() throws Exception {
		Map<Integer,Send_Rem_Dely_Pay_Bo> m=new HashMap();
		Connection con=null; 
		Statement st=null;
		ResultSet rs=null;
		con=getConnection();
		int count=0;
		st=con.createStatement();
		rs=st.executeQuery(sql1);
		while(rs.next()) {
			count++;
			Send_Rem_Dely_Pay_Bo bo=new Send_Rem_Dely_Pay_Bo();
			bo.setName(rs.getString(1));
			bo.setMobileNo(rs.getLong(2));
			bo.setEmailId(rs.getString(3));
			bo.setPay_amount(rs.getDouble(4));
			m.put(count,bo);
			
		}
		System.out.println("Dao class dely payment ::"+m);
		return m;
	}
	
	@Override
	public Map sendRem1day() throws Exception {

		
		
		Map<Integer,Send_Rem_Before_1_Day_Bo> m=new HashMap();
		Connection con=null; 
		Statement st=null;
		ResultSet rs=null;
		con=getConnection();
		int count=0;
		st=con.createStatement();
		rs=st.executeQuery(sql2);
		while(rs.next()) {
			count++;
			Send_Rem_Before_1_Day_Bo bo=new Send_Rem_Before_1_Day_Bo();
			bo.setName(rs.getString(2));
			bo.setMobileNo(rs.getLong(6));
			bo.setEmailId(rs.getString(4));
			m.put(count,bo);
			
		}
		System.out.println("Dao class ::"+m);
		
		return m;
		
	}
	
	@Override
	public List getNewPolicies() throws Exception {
		List list=new ArrayList();
		Connection con=null;
		Statement st=null;
		PreparedStatement ps=null;
		int num=0;
		String str="";
		
		con=getConnection();
		System.out.println("Succ Connection");
		ps=con.prepareStatement(GET_NEW_POLICIES,Statement.RETURN_GENERATED_KEYS);
		ResultSet rs=ps.executeQuery();
		while (rs.next()) {
			num=rs.getInt(1);
		}
		System.out.println("After executing 1st Query");
		ps=con.prepareStatement(GET_NEW_POLICIES_OF_UPDATED_ROWS);
		ps.setInt(1, num);
		rs=ps.executeQuery();
		while (rs.next()) {
			list.add(rs.getString(3));
			
		}
		System.out.println("data new Policies"+list);
	

		System.out.println("Kumar new policies");
		
		return list;
	}

}
