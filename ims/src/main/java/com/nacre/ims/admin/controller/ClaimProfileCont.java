package com.nacre.ims.admin.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.admin.service.AdminClaimService;
import com.nacre.ims.admin.serviceimpl.AdminClaimServiceImpl;
import com.nacre.ims.commonsutil.SendMail;
import com.nacre.ims.dto.ClaimDataDTO;
/*
  @author Rohit Ubare
*/
@WebServlet("/claimProf")
public class ClaimProfileCont extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		/*
		 * PrintWriter writer = resp.getWriter(); resp.setContentType("text/html");
		 */
		AdminClaimService adminClaim = null;
		int result = 0;

		HttpSession session = req.getSession();
		ClaimDataDTO rdto = (ClaimDataDTO) session.getAttribute("resultDTO");
		rdto.setDes(req.getParameter("nDes"));

		adminClaim = new AdminClaimServiceImpl();
		try {
			result = adminClaim.setFinalClaimReport(rdto);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (result > 0) {
			SendMail.send("Claim Request Report", rdto.getnEmail(), "nikhilkmr214@gmail.com", "nikhilkmr214@", "Your claim request for Policy number "+rdto.getpNo()+" and for Claim ID"+rdto.getClaimID()+" has been "+rdto.getStatusId());
			resp.sendRedirect("/ims/admin/pages/ClaimRequest.jsp");
		}

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}
}
