package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;
import com.nacre.ims.dto.AgentApprovalDTO;

@WebServlet("/getApprovalUrl")
public class AdminAgentApprovalController extends HttpServlet {
	/**
	 * G Venu
	 */ 
	private static final long serialVersionUID = 1L;
	@Override 
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {	
		AdminService service=null;
		String s="";
		HttpSession session=null;
		List<AgentApprovalDTO> listdto=null;
		//create the objects
		service=new AdminServiceImpl();
		try {
			listdto=service.getApprovalData();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(listdto.isEmpty()) {
			s="No Pending Agents Found";
			session=req.getSession(true);
			session.setAttribute("pendingAgentInfo", s);
			resp.sendRedirect("admin/pages/home.jsp");
		} 
		else {
			System.out.println(listdto);
			req.setAttribute("ApprovalListDto", listdto);
			RequestDispatcher rd=req.getRequestDispatcher("admin/pages/viewAgentPendingAppovalDetails.jsp");
			rd.forward(req, resp);
		}
		
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	doGet(req, resp);
	}

}

