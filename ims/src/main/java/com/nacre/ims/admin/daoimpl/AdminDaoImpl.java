package com.nacre.ims.admin.daoimpl;

 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
 

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
 
import java.util.List;
import com.nacre.ims.admin.dao.AdminDao;
import com.nacre.ims.admin.dbutils.AdminDbUtils;
 
import com.nacre.ims.bo.AgentApprovalBO;
 
import com.nacre.ims.bo.AddCarInsuranceBO;
import com.nacre.ims.bo.AddChildCareInsurenceBO;
import com.nacre.ims.bo.AddHomeInsuranceBO;
import com.nacre.ims.bo.AddLifeInsurenceBO;
import com.nacre.ims.bo.EditChildCareInsurenceBO;
import com.nacre.ims.bo.EditHomeInsuranceBO;
import com.nacre.ims.bo.EditLifeInsurenceBO;
import com.nacre.ims.bo.GetChildCareInsurenceDataBO;
import com.nacre.ims.bo.GetHomeInsuranceBO;
import com.nacre.ims.bo.GetLifeInsurenceDataBO;
import com.nacre.ims.bo.ViweallCustomerdetailsBO;
 
import com.nacre.ims.commonsutil.ConnectionUtil;
import com.nacre.ims.dto.Agent_DataDTO;
import com.nacre.ims.dto.FixAgentMeetingDTO;

public class AdminDaoImpl implements AdminDao {
	Connection con = null;
	@Override
	public int addCarInsurance(AddCarInsuranceBO addCarInsuranceBO) {
	
		 
		con=ConnectionUtil.getConnection();
		PreparedStatement ps =null;
		int c=0;
		try {
			ps=con.prepareStatement(AdminDbUtils.addCarPolicy);
			ps.setString(1, addCarInsuranceBO.getCoverage());
			ps.setString(2, addCarInsuranceBO.getPolicyName());
			ps.setString(3, addCarInsuranceBO.getPolicyDescription());
			ps.setDouble(4, addCarInsuranceBO.getRateofInterest());
			ps.setInt(5, addCarInsuranceBO.getDuration());
			ps.setDouble(6, addCarInsuranceBO.getPaybleAmount());
			ps.setDouble(7, addCarInsuranceBO.getInitialAmount());
			c=ps.executeUpdate();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return c;
	}

	@Override
	public    List<AddCarInsuranceBO>  getAllCarPolicy() {
		  List<AddCarInsuranceBO> list=new ArrayList();
		Connection con = null;
		con=ConnectionUtil.getConnection();
		PreparedStatement ps = null;
		
		try {
			ps=con.prepareStatement(AdminDbUtils.getCarDetailsPolicy);
		   ResultSet rs=	ps.executeQuery();
		 
		   while(rs.next()) {
			   AddCarInsuranceBO bo= new AddCarInsuranceBO();
			   
			   bo.setPolicyName(rs.getString(1));
			   bo.setCoverage(rs.getString(2));
			   bo.setPolicyDescription(rs.getString(3));
			   bo.setRateofInterest(rs.getDouble(4));
			   bo.setDuration(rs.getInt(5));
			   bo.setPaybleAmount(rs.getDouble(6));
			   bo.setInitialAmount(rs.getDouble(7));
			   bo.setCarIncId(rs.getInt(8));
			   list.add(bo);
		   }
		   
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public int editCarInsurance(AddCarInsuranceBO addCarInsuranceBO) {
		Connection con = null;
		con=ConnectionUtil.getConnection();
		PreparedStatement ps =null;
		int c=0;
		try {
			ps=con.prepareStatement(AdminDbUtils.editCarInsurancePolicy);
			ps.setString(1, addCarInsuranceBO.getCoverage());
            ps.setString(2, addCarInsuranceBO.getPolicyName());
            ps.setString(3, addCarInsuranceBO.getPolicyDescription());
            ps.setDouble(4, addCarInsuranceBO.getRateofInterest());
            ps.setInt(5, addCarInsuranceBO.getDuration());
            ps.setDouble(6, addCarInsuranceBO.getPaybleAmount());
            ps.setDouble(7, addCarInsuranceBO.getInitialAmount());
            ps.setInt(8,addCarInsuranceBO.getCarIncId());
            c=ps.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return c;
	}

	@Override
	public int deleteCarInsurance(int id) {
		Connection con = null;
		con=ConnectionUtil.getConnection();
		PreparedStatement ps =null;
		int c=0;
		try {
			ps=con.prepareStatement(AdminDbUtils.DELETE_CAR_INC_POLICY);
			ps.setInt(1, id);
			c=ps.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return c;
	}
	     
	@Override
	public int addlifeinsurence(AddLifeInsurenceBO bo) throws SQLException {
		AdminDaoImpl dao=new AdminDaoImpl();
		Connection con=null;
		PreparedStatement ps=null;
		con=ConnectionUtil.getConnection();
		ResultSet rs=null;
		int count=0;
		int status_id=0;
		try{
			ps=con.prepareStatement(AdminDbUtils.SELECT_LIFE_ISURENCE_STATUS_ID_QUEERY);
			rs = ps.executeQuery();
			while (rs.next()) {
				status_id = rs.getInt(1);
		       }
		if(status_id>0){
		ps=con.prepareStatement(AdminDbUtils.INSERT_LIFE_ISURENCE_QUEERY);
	    ps.setString(1, bo.getName());
	    ps.setInt(2,bo.getDuration());
	    ps.setString(3, bo.getInsurance_policy_no());
	    ps.setDouble(4, bo.getRate_of_interest());
	    ps.setString(5, bo.getDesciption());
	    ps.setDouble(6, bo.getTotal_amount_paid());
	    ps.setString(7, bo.getLife_insurance_offers());
	    ps.setString(8, bo.getCovarage());
	    ps.setInt(9, status_id);
	    count=ps.executeUpdate();
		}
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		return count;
	}

	@Override
	public int addchildcareinsurence(AddChildCareInsurenceBO bo) throws Exception {
		AdminDaoImpl dao=new AdminDaoImpl();
		Connection con=null;
		PreparedStatement ps=null;
		ResultSet rs=null;
		con=ConnectionUtil.getConnection();
		int count=0;
		int status_id=0;
		try{
			ps=con.prepareStatement(AdminDbUtils.SELECT_LIFE_ISURENCE_STATUS_ID_QUEERY);
			rs = ps.executeQuery();
			while (rs.next()) {
				status_id = rs.getInt(1);
		       }
		if(status_id>0){
		
		ps=con.prepareStatement(AdminDbUtils.INSERT_CHILDCARE_ISURENCE_QUEERY);
		ps.setInt(1, bo.getMax_limit());
		ps.setString(2, bo.getName());
		ps.setString(3, bo.getCoverage());
		ps.setString(4, bo.getDesciption());
		ps.setDouble(5, bo.getRate_of_interest());
		ps.setDouble(6, bo.getPayable_amount());
		ps.setDouble(7, bo.getInitial_amount());
		ps.setString(8, bo.getLife_insurance_offers());
		ps.setInt(9, status_id);
		 count=ps.executeUpdate();
		}
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
			return count;
	}

	@Override
	public List<GetLifeInsurenceDataBO> fetchLifeInsurence() throws Exception {
		AdminDaoImpl dao=new AdminDaoImpl();
		List<GetLifeInsurenceDataBO> list=new ArrayList<>();
		Connection con=null;
		PreparedStatement ps=null;
		int count=0;
		con=ConnectionUtil.getConnection();
		ps=con.prepareStatement(AdminDbUtils.SELECT_LIFE_INSURENCE_QUEERY);
		ResultSet rs=ps.executeQuery();
		while(rs.next()){
			GetLifeInsurenceDataBO bo=new GetLifeInsurenceDataBO();
			bo.setName(rs.getString("insurance_name"));
			bo.setDuration(rs.getInt("duration"));
			bo.setInsurance_policy_no(rs.getString("insurance_policy_no"));
			bo.setRate_of_interest(rs.getDouble("rate_of_interest"));
			bo.setDesciption(rs.getString("desciption"));
			bo.setTotal_amount_paid(rs.getDouble("total_amount_paid"));
			bo.setLife_insurance_offers(rs.getString("life_insurance_offers"));
			bo.setCovarage(rs.getString("covarage"));
			bo.setLifeid(rs.getInt("life_isn_id"));
			
			//bo.setStatus(rs.getString("status"));
			
			list.add(bo);
		
		}
		
		return list;
	}

	@Override
	public List<GetChildCareInsurenceDataBO> fetchChildCareInsurence() throws Exception {
		AdminDaoImpl dao=new AdminDaoImpl();
		List<GetChildCareInsurenceDataBO> list=new ArrayList<>();
		Connection con=null;
		PreparedStatement ps=null;
		int count=0;
		con=ConnectionUtil.getConnection();
		ps=con.prepareStatement(AdminDbUtils.SELECT_ChilCARE_INSURENCE_QUEERY);
		ResultSet rs=ps.executeQuery();
		while(rs.next()){
			GetChildCareInsurenceDataBO bo=new GetChildCareInsurenceDataBO();
			bo.setMax_limit(rs.getInt("max_limit"));
			bo.setName(rs.getString("name"));
			bo.setCoverage(rs.getString("coverage"));
			bo.setDesciption(rs.getString("desciption"));
			bo.setRate_of_interest(rs.getDouble("rate_of_interest"));
			bo.setPayable_amount(rs.getDouble("payable_amount"));
		    bo.setInitial_amount(rs.getDouble("initial_amount"));
		    bo.setLife_insurance_offers(rs.getString("life_insurance_offers"));
		    bo.setChildid(rs.getInt("child_care_insurance_id"));
		    //bo.setStatus(rs.getString("status"));
		    list.add(bo);
		}
		
		return list;
	}

	@Override
	public int editLifeInsurence(EditLifeInsurenceBO bo) throws Exception {
		AdminDaoImpl dao=new AdminDaoImpl();
		Connection con=null;
		PreparedStatement ps=null;
		int count=0;
		con=ConnectionUtil.getConnection();
		ps=con.prepareStatement(AdminDbUtils.UPDATE_LIFE_INSURENCE_QUEERY);
	    ps.setString(1, bo.getName());
	    ps.setInt(2,bo.getDuration());
	    ps.setString(3, bo.getInsurance_policy_no());
	    ps.setDouble(4, bo.getRate_of_interest());
	    ps.setString(5, bo.getDesciption());
	    ps.setDouble(6, bo.getTotal_amount_paid());
	    ps.setString(7, bo.getLife_insurance_offers());
	    ps.setString(8, bo.getCovarage());
	    ps.setString(9, bo.getName());
	   // ps.setString(9, bo.getStatus());
	    count=ps.executeUpdate();
		return count;
	}

	@Override
	public int editchildcareinsurence(EditChildCareInsurenceBO bo) throws Exception {
		AdminDaoImpl dao=new AdminDaoImpl();
		Connection con=null;
		PreparedStatement ps=null;
		int count=0;
		con=ConnectionUtil.getConnection();
		ps=con.prepareStatement(AdminDbUtils.UPDATE_CHILDCARE_INSURENCE_QUEERY);
		ps.setInt(1, bo.getMax_limit());
		ps.setString(2, bo.getName());
		ps.setString(3, bo.getCoverage());
		ps.setString(4, bo.getDesciption());
		ps.setDouble(5, bo.getRate_of_interest());
		ps.setDouble(6, bo.getPayable_amount());
		ps.setDouble(7, bo.getInitial_amount());
		ps.setString(8, bo.getLife_insurance_offers());
		ps.setString(9, bo.getName());
		//ps.setString(9, bo.getStatus());
		 count=ps.executeUpdate();
			return count;
	}

	@Override
	public int deleteLifeInsurence(int id) {
		Connection con=null;
		PreparedStatement ps=null;
		con=ConnectionUtil.getConnection();
		int count=0;
		try{
			ps=con.prepareStatement(AdminDbUtils.DELETE_LIFE_INC_POLICY);
			ps.setInt(1, id);
			count=ps.executeUpdate();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public int deleteChildCareInsurence(int id) {
		Connection con=null;
		PreparedStatement ps=null;
		con=ConnectionUtil.getConnection();
		int count=0;
		try{
			ps=con.prepareStatement(AdminDbUtils.DELETE_CHILD_CARE_INC_POLICY);
			ps.setInt(1,id);
			count=ps.executeUpdate();
			System.out.println(count);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return count;
	}
	@Override
	public int addHomeInsurence(AddHomeInsuranceBO addHomeInsuranceBO) {
		con = ConnectionUtil.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int data = 0;
		int statusId = 0;
		try {
			ps = con.prepareStatement(AdminDbUtils.workingStatusId);
			rs = ps.executeQuery();
			while (rs.next()) {
				statusId = rs.getInt(1);
			}
			System.out.println(statusId);
			if (statusId > 0) {
				ps = con.prepareStatement(AdminDbUtils.addHomePolicy);
				ps.setString(1, addHomeInsuranceBO.getPolicyName());
				ps.setInt(2, addHomeInsuranceBO.getDuration());
				ps.setString(3, addHomeInsuranceBO.getCoverage());
				ps.setString(4, addHomeInsuranceBO.getDescription());
				ps.setDouble(5, addHomeInsuranceBO.getTotalamount());
				ps.setDouble(6, addHomeInsuranceBO.getRateOfInterest());
				ps.setDouble(7, addHomeInsuranceBO.getInitialAmount());
				ps.setString(8, addHomeInsuranceBO.getPolicyOffer());
				ps.setString(9, addHomeInsuranceBO.getPolicyNumber());
				ps.setInt(10, statusId);
				data = ps.executeUpdate();
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return data;
	}

	@Override
	public List<GetHomeInsuranceBO> getHomeInsurence() {
		con = ConnectionUtil.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		GetHomeInsuranceBO getHomeInsuranceBO = null;
		List<GetHomeInsuranceBO> listBo = null;

		try {
				ps = con.prepareStatement(AdminDbUtils.getHomePolicy);
				rs=ps.executeQuery();
				
				listBo = new ArrayList<>();
				while (rs.next()) {
					getHomeInsuranceBO = new GetHomeInsuranceBO();
					getHomeInsuranceBO.setPolicyName(rs.getString(1));
					getHomeInsuranceBO.setDuration(rs.getInt(2));
					getHomeInsuranceBO.setCoverage(rs.getString(3));
					getHomeInsuranceBO.setDescription(rs.getString(4));
					getHomeInsuranceBO.setTotalamount(rs.getDouble(5));
					getHomeInsuranceBO.setRateOfInterest(rs.getDouble(6));
					getHomeInsuranceBO.setInitialAmount(rs.getDouble(7));
					getHomeInsuranceBO.setPolicyOffer(rs.getString(8));
					getHomeInsuranceBO.setPolicyNumber(rs.getString(9));
					getHomeInsuranceBO.setHomeInsuranceId(rs.getInt(10));
					listBo.add(getHomeInsuranceBO);
				}
			
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		return listBo;
	}

	@Override
	public int editHomeInsurence(EditHomeInsuranceBO editHomeInsuranceBO) {
		con = ConnectionUtil.getConnection();
		PreparedStatement ps = null;
		int data = 0;
		try {
		ps = con.prepareStatement(AdminDbUtils.editHomePolicy);
		
		ps.setInt(1, editHomeInsuranceBO.getDuration());
		ps.setString(2, editHomeInsuranceBO.getCoverage());
		ps.setString(3, editHomeInsuranceBO.getDescription());
		ps.setDouble(4, editHomeInsuranceBO.getTotalamount());
		ps.setDouble(5, editHomeInsuranceBO.getRateOfInterest());
		ps.setDouble(6, editHomeInsuranceBO.getInitialAmount());
		ps.setString(7, editHomeInsuranceBO.getPolicyOffer());
		ps.setString(8, editHomeInsuranceBO.getPolicyNumber());
		ps.setString(9,editHomeInsuranceBO.getPolicyName());
		data = ps.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return data;
		
	}

	@Override
	public int deleteHomeInsurance(int id) {
		System.out.println(id);
		con = ConnectionUtil.getConnection();
		PreparedStatement ps = null;
		int data = 0;
		try {
			ps = con.prepareStatement(AdminDbUtils.deleteHomePolicy);
			ps.setInt(1, id);
			data = ps.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	
	@Override
	public List<ViweallCustomerdetailsBO> getAllCustomer() throws SQLException {
		ViweallCustomerdetailsBO bo=null;

		
		//get connection object
		Connection con=ConnectionUtil.getConnection();
		System.out.println(con+"...............con in dao");
		//PreparedStatement ps=con.prepareStatement("select cust.costomer_register_id, cust.customer_first_name,cust.customer_last_name,cust.customer_email_id,cust.customer_mobile_no,cust.customer_gender,cust.customer_dob, add1.local_address,add1.pincode,city.city_name,state.state_name,cou.country_name from customer_registration_table cust,address_table add1,city_master_table city,state_master_table state,country_master_table cou where add1.address_id=cust.address_id and add1.city_id=city.city_id and state.state_id=city.state_id and state.country_id=cou.country_id"); 
		PreparedStatement ps=con.prepareStatement(AdminDbUtils.FETCH_CUST0MER_DATA_BY_ADMIN);
		ResultSet rs=ps.executeQuery();
	    List<ViweallCustomerdetailsBO> customerlist=new ArrayList<>();
		
		while(rs.next()) {
			bo=new ViweallCustomerdetailsBO();
			
			bo.setCostomer_register_id(rs.getString("costomer_register_id"));
			bo.setCustomer_first_name(rs.getString("customer_first_name"));
			bo.setCustomer_last_name(rs.getString("customer_last_name"));
			bo.setCustomer_email_id(rs.getString("customer_email_id"));
			
			bo.setCustomer_mobile_no(rs.getLong("customer_mobile_no"));
			bo.setCustomer_gender(rs.getString("customer_gender"));
			bo.setCustomer_dob(rs.getDate("customer_dob"));
			bo.setLocal_address(rs.getString("local_address"));
			bo.setPincode(rs.getInt("pincode"));
			bo.setCity_name(rs.getString("city_name"));
			bo.setState_name(rs.getString("state_name"));
			bo.setCountry_name(rs.getString("country_name"));
		
			customerlist.add(bo);
		}
		
		System.out.println("customer all data is :: "+customerlist);
		return customerlist ;
	}
	/**
	 * @author shailendra katole
	 */
	
	@Override
	public int removeCustomer(int id) {
		Connection con=ConnectionUtil.getConnection();
		int count=0;
		System.out.println(con+"...............con in dao");
		try {
			PreparedStatement ps=con.prepareStatement(AdminDbUtils.REMOVE_CUSTOMER_DATA_BY_ADMIN);
			ps.setInt(1, id);
			count=ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public List<Agent_DataDTO> fetchAgentData() {

		Connection con = null;
		PreparedStatement ps = null;

		List<Agent_DataDTO> list = new ArrayList<>();

		// initialize connection class object...
		con = ConnectionUtil.getConnection();
		// create preparedStatement object
		try {
			Agent_DataDTO dto = null;

			ps = con.prepareStatement(AdminDbUtils.GET_AGENT_DATA_BY_ADMIN);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				dto = new Agent_DataDTO();

				dto.setAgent_Id(rs.getInt("agent_id"));
				dto.setAgent_name(rs.getString("agent_name"));
				dto.setAgent_qualification(rs.getString("agent_qualification"));
				dto.setGender(rs.getString("gender"));

				dto.setAgent_emai_id(rs.getString("agent_email_id"));
				dto.setMobile_Number(rs.getLong("agnt.mobile_number"));
				dto.setAdhaar_Card_No(rs.getLong("adhaar_card_no"));
				dto.setPancard_No(rs.getLong("pancard_no"));

				dto.setDate_Of_Joining(rs.getDate("date_of_joining"));
				dto.setLocation(rs.getString("location"));
				dto.setLocal_address(rs.getString("local_address"));
				dto.setPincode(rs.getLong("pincode"));

				dto.setCity_name(rs.getString("city_name"));
				dto.setState_name(rs.getString("state_name"));
				dto.setCountry_name(rs.getString("country_name"));
				dto.setCount_No_Of_Customer(rs.getInt("count(cust.agent_id)"));

				list.add(dto);
			}

			System.out.println("DTO CLASS VALUE IS :: " + dto);

		} catch (SQLException e) {
			System.out.println("Exception occured in AgentDataDao class at a time get connection......");
			e.printStackTrace();
		}

		return list;
	}

	@Override
	public int deleteAgent(int id) {

		Connection con = null;
		PreparedStatement ps = null;
		int count =0;
		// initialize connection class object...
		con = ConnectionUtil.getConnection();
		// create preparedStatement object
		try {
			ps = con.prepareStatement(AdminDbUtils.DELETE_AGENT_DATA_BY_ADMIN);
			ps.setInt(1, id);
			count = ps.executeUpdate();
			System.out.println("dao count value== "+count);
			return count;
		} catch (SQLException se) {
			se.printStackTrace();
		}
		return count;
	}

	@Override
	public int fixAgentMeeting(FixAgentMeetingDTO dto) {
		Connection con = null;
		PreparedStatement ps = null;
		int count =0;
		// initialize connection class object...
		con = ConnectionUtil.getConnection();
		// create preparedStatement object
		try {
			ps = con.prepareStatement(AdminDbUtils.FIX_AGENT_MEETING_WITH_ADMIN);
			
			int[] agentid=dto.getAgentId();
			
			for (int i = 0; i < agentid.length; i++) {
				ps.setInt(1,agentid[i]);
				ps.setString(2, dto.getVenue());
				ps.setDate(3, (java.sql.Date) dto.getDate());
				ps.setTime(4, dto.getTime());
				
				count+=ps.executeUpdate();
				
				
			}
			
			count = ps.executeUpdate();
			System.out.println("dao count value== "+count);
			return count;
		} catch (SQLException se) {
			se.printStackTrace();
		}
		return count;
	}
	@Override
	public List<AgentApprovalBO> getApprovalData() throws SQLException {
		
		Connection connection=null;
		PreparedStatement preparedStatement=null;
		ResultSet resultSet=null;
		List<AgentApprovalBO> list=null;
		//get the objects
		
		connection=ConnectionUtil.getConnection();
		preparedStatement=connection.prepareStatement(AdminDbUtils.GET_APPROVAL_DETAILS);
		preparedStatement.setInt(1, 3);
		resultSet=preparedStatement.executeQuery();
		//create the list object
		list=new ArrayList<AgentApprovalBO>();
		while(resultSet.next()) {
		 AgentApprovalBO bo=new AgentApprovalBO();
		 bo.setId(resultSet .getInt(1));
		 bo.setName(resultSet.getString(2));
		 bo.setQualification(resultSet.getString(3));
		 bo.setGender(resultSet.getString(4));
		 bo.setEmail(resultSet.getString(5));
		 bo.setLocation(resultSet.getString(6));
		 bo.setStatus_id(resultSet.getInt(7));
		list.add(bo);	
		}
		
		
		return list;
	}

	@Override
	public int getApprovedOrRejectStatus(int[] a, String btnVal) throws SQLException {
		/*
		 * G Venu 
		 */
		Connection connection=null;
		PreparedStatement preparedStatement=null;
		int len=a.length;
		int result=0;
		// convert the integerarry to string
		/*String s=Arrays.toString(a);
		s=s.substring(s.indexOf("[")+1,s.lastIndexOf("]"));
		System.out.println(s);*/
		//get the connection
		connection=ConnectionUtil.getConnection();
		if(btnVal.equals("Accept")) {
			for(int i=0;i<len;i++) {
				//create prepared statement object
				preparedStatement=connection.prepareStatement(AdminDbUtils.GET_APPROVED_OR_REJECTED_STATUS);
				//set query params 
				preparedStatement.setInt(1, 1);
				preparedStatement.setInt(2, a[i]);
				System.out.println(preparedStatement);
				//execute the query
				result=preparedStatement.executeUpdate();
			}
		}else {
			for(int i=0;i<len;i++) {
				//create prepared statement object
				preparedStatement=connection.prepareStatement(AdminDbUtils.GET_APPROVED_OR_REJECTED_STATUS);
				//set query params 
				preparedStatement.setInt(1, 2);
				preparedStatement.setInt(2, a[i]);
				System.out.println(preparedStatement);
				//execute the query
				result=preparedStatement.executeUpdate();
			}
		}
		return result;
	}
	
 
}
