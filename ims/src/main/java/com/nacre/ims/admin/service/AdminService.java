package com.nacre.ims.admin.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import com.nacre.ims.dto.AgentApprovalDTO;
import com.nacre.ims.bo.AddCarInsuranceBO;
import com.nacre.ims.dto.AddCarInsuranceDTO;
import com.nacre.ims.dto.AddChildCareInsurenceDTO;
import com.nacre.ims.dto.AddHomeInsuranceDTO;
import com.nacre.ims.dto.AddLifeInsurenceDTO;
import com.nacre.ims.dto.Agent_DataDTO;
import com.nacre.ims.dto.EditChildCareInsurenceDTO;
import com.nacre.ims.dto.EditHomeInsuranceDTO;
import com.nacre.ims.dto.EditLifeInsurenceDTO;
import com.nacre.ims.dto.FixAgentMeetingDTO;
import com.nacre.ims.dto.GetChildCareInsurenceDataDTO;
import com.nacre.ims.dto.GetHomeInsuranceDTO;
import com.nacre.ims.dto.GetLifeInsurenceDataDTO;



public interface AdminService {
	public List<AgentApprovalDTO> getApprovalData()throws SQLException;
	public String getApprovedOrReject(int i[],String btnVal)throws SQLException;
	public int addCarInsurance(AddCarInsuranceDTO addCarInsuranceDTO);
	public List<AddCarInsuranceDTO> getAllCarPolicy();
	public int editCarInsurance(AddCarInsuranceDTO addCarInsuranceDTO);
	public int deleteCarInsurance(int id);
	public int addlifeinsurence(AddLifeInsurenceDTO dto)throws Exception;
	public int addchildcareinsurence(AddChildCareInsurenceDTO dto)throws Exception;
	public List<GetLifeInsurenceDataDTO> fetchLifeInsurence()throws Exception;
	public List<GetChildCareInsurenceDataDTO> fetchChildCareInsurence() throws Exception;
	public int editLifeInsurence(EditLifeInsurenceDTO dto)throws Exception;
	public int editchildcareinsurence(EditChildCareInsurenceDTO dto)throws Exception;
	public int deleteLifeInsurance(int id);
	public int deleteChildCareInsurance(int id);
	public int addHomeInsurence(AddHomeInsuranceDTO addHomeInsuranceDTO);
	public List<GetHomeInsuranceDTO> getHomeInsurence();
	public int editHomeInsurence(EditHomeInsuranceDTO editHomeInsuranceDTO);
	public int deleteHomeInsurance(int id);
	public Map sendReminder()throws Exception;
	public Map sendRemDely()throws Exception;
	public Map sendRem1Day()throws Exception;

	public List viewAllCustomerData()throws SQLException;
	 
	public int removeCustomer(int id);
    public List<Agent_DataDTO> fetchAgentData();
	
	public int deleteAgent(int id);
	
	public int fixAgentMeeting(FixAgentMeetingDTO dto);
	
}
