package com.nacre.ims.admin.service;

import java.util.List;

import com.nacre.ims.dto.ClaimDataDTO;
import com.nacre.ims.dto.ClaimRequestDetailDTO;
/*
  @author Rohit Ubare
*/
public interface AdminClaimService {
	
	
	public int getClaimNotification() throws Exception;
	
	public List<ClaimRequestDetailDTO> getClaimName() throws Exception;

	public ClaimDataDTO getClaimResult(ClaimDataDTO ddto) throws Exception;
	
	public int setFinalClaimReport(ClaimDataDTO dto)throws Exception;
}
