package com.nacre.ims.admin.dbutils;

public class AdminDbUtils {
	public static final String GET_APPROVAL_DETAILS="SELECT * FROM agent_approved_table WHERE STATUS_ID=?";
	public static final String GET_APPROVED_OR_REJECTED_STATUS = "UPDATE "
			+ "agent_approved_table SET STATUS_ID=? WHERE agent_approved_id=?";
	
public static final String addCarPolicy="insert into car_insurance_master_table(coverage,name,description,rate_of_interest,duration,payble_amount,initial_amount,status_id) values (?,?,?,?,?,?,?,(select status_table.status_id from status_table where status_table.status_name='WORKING'))";
public static final String SELECT_LIFE_ISURENCE_STATUS_ID_QUEERY="select status_id from status_table where status_name='WORKING'";	

public static final String getCarDetailsPolicy="select car.name,car.coverage,car.description,car.rate_of_interest,car.duration,car.payble_amount,car.initial_amount, car.car_insurance_id from car_insurance_master_table car where car.status_id=(select status_table.status_id from status_table where status_table.status_name='WORKING')";
 
public static final String editCarInsurancePolicy="update car_insurance_master_table set coverage=?,name=?,description=?,rate_of_interest=?,duration=?,payble_amount=?,initial_amount=? where car_insurance_master_table.car_insurance_id=?";

public static final String DELETE_CAR_INC_POLICY="update car_insurance_master_table set car_insurance_master_table.status_id=(select status_table.status_id from status_table where status_table.status_name='NOTWORKING') where car_insurance_master_table.car_insurance_id=?";

public static final String INSERT_LIFE_ISURENCE_QUEERY="insert into life_insurance_master_table(insurance_name,duration,insurance_policy_no,rate_of_interest,desciption,total_amount_paid,life_insurance_offers,covarage,status_id)values(?,?,?,?,?,?,?,?,?)";

public static final String INSERT_CHILDCARE_ISURENCE_QUEERY="insert into child_care_insurance_master_table(max_limit,name,coverage,desciption,rate_of_interest,payable_amount,initial_amount,life_insurance_offers,status_id)values(?,?,?,?,?,?,?,?,?)";

public static final String SELECT_LIFE_INSURENCE_QUEERY="select li.insurance_name,li.duration,li.insurance_policy_no,li.rate_of_interest,li.desciption,li.total_amount_paid,li.life_insurance_offers,li.covarage, li.life_isn_id from life_insurance_master_table li where li.status_id=(select status_table.status_id from status_table where status_table.status_name='WORKING')";



public static final String SELECT_ChilCARE_INSURENCE_QUEERY="select cc.max_limit,cc.name,cc.coverage,cc.desciption,cc.rate_of_interest,cc.payable_amount,cc.initial_amount,cc.life_insurance_offers,cc.child_care_insurance_id from child_care_insurance_master_table cc where cc.status_id=(select status_table.status_id from status_table where status_table.status_name='WORKING')";


public static final String UPDATE_LIFE_INSURENCE_QUEERY="update life_insurance_master_table set insurance_name=? ,duration=?,insurance_policy_no=?,rate_of_interest=?,desciption=?,total_amount_paid=?,life_insurance_offers=?,covarage=? where insurance_name=?";
public static final String UPDATE_CHILDCARE_INSURENCE_QUEERY="update child_care_insurance_master_table set max_limit=?,name=?,coverage=?,desciption=?,rate_of_interest=?,payable_amount=?,initial_amount=?,life_insurance_offers=? where name=?";
public static final String DELETE_LIFE_INC_POLICY="update life_insurance_master_table set life_insurance_master_table.status_id=(select status_table.status_id from status_table where status_table.status_name='NOTWORKING') where life_insurance_master_table.life_isn_id=?";
public static final String DELETE_CHILD_CARE_INC_POLICY="update child_care_insurance_master_table set child_care_insurance_master_table.status_id=(select status_table.status_id from status_table where status_table.status_name='NOTWORKING') where child_care_insurance_master_table.child_care_insurance_id=?";
public  static final String workingStatusId="select status_id from status_table where status_name='WORKING' ";
public  static final String addHomePolicy="insert into home_insurance_master_table (name,max_limit,coverage,description,total_amount_paid,rate_of_interest,initial_amount,home_insurance_offer,policy_no,status_id) values (?,?,?,?,?,?,?,?,?,?)";
public static final String getHomePolicy="select name,max_limit,coverage,description,total_amount_paid,rate_of_interest,initial_amount,home_insurance_offer,policy_no,home_insurance_id from home_insurance_master_table where status_id=( select status_id from status_table where status_name='WORKING')";
public  static final String editHomePolicy="update home_insurance_master_table set  max_limit=?,coverage=?,description=?,total_amount_paid=?,rate_of_interest=?,initial_amount=?,home_insurance_offer=?,policy_no=? where name=?";
public static final String deleteHomePolicy="update home_insurance_master_table set home_insurance_master_table.status_id=(select status_table.status_id from status_table where status_table.status_name='NOTWORKING') where home_insurance_master_table.home_insurance_id=?";
public static final String FETCH_CUST0MER_DATA_BY_ADMIN="select cust.costomer_register_id, cust.customer_first_name,cust.customer_last_name,cust.customer_email_id,cust.customer_mobile_no, cust.customer_gender,cust.customer_dob, add1.local_address,add1.pincode,city.city_name,state.state_name,cou.country_name from customer_registration_table cust,address_table add1,city_master_table city,state_master_table state,country_master_table cou	where add1.address_id=cust.address_id and add1.city_id=city.city_id	and state.state_id=city.state_id and state.country_id=cou.country_id and  cust.status_id=(select status_table.status_id from status_table where status_table.status_name='OURCUSTOMER') GROUP by cust.costomer_register_id";


public static final String REMOVE_CUSTOMER_DATA_BY_ADMIN = " update customer_registration_table set customer_registration_table.status_id= (select status_table.status_id from status_table where status_table.status_name='NOTACUSTOMER') where customer_registration_table.costomer_register_id=?"; 

public static final String GET_AGENT_DATA_BY_ADMIN = "  select agnt.agent_id, \r\n" + 
		"		 		agntap.agent_name,agntap.agent_qualification,agntap.gender, \r\n" + 
		"		 		 agntap.agent_email_id,agnt.mobile_number, agnt.adhaar_card_no, agnt.pancard_no, agnt.date_of_joining,  \r\n" + 
		"		 		 agntap.location, \r\n" + 
		"		 		add1.local_address,add1.pincode,c.city_name,s.state_name, \r\n" + 
		"		 		cu.country_name,count(cust.agent_id) from agent_registration_table \r\n" + 
		"		 		agnt,agent_approved_table agntap,address_table add1,city_master_table \r\n" + 
		"		 		c,state_master_table s,country_master_table cu, \r\n" + 
		"		 		customer_registration_table cust  \r\n" + 
		"		 		where agnt.agent_approved_id=agntap.agent_approved_id  \r\n" + 
		"		 		and agnt.address=add1.address_id  \r\n" + 
		"		 		and cu.country_id=s.country_id  \r\n" + 
		"		 		and s.state_id=c.state_id  \r\n" + 
		"		 		and c.city_id=add1.city_id  \r\n" + 
		"		 		and cust.agent_id=agnt.agent_id \r\n" + 
		"		 		and agntap.status_id=(select sta.status_id from status_table sta \r\n" + 
		"		 		where sta.status_name='APPROVED') \r\n" + 
		"		 		group by agnt.agent_id   \r\n" + 
		" ";



public static final String DELETE_AGENT_DATA_BY_ADMIN = "update agent_approved_table,agent_registration_table"
		+ " set agent_approved_table.status_id="
		+ "(select status_table.status_id  from status_table "
		+ "where  status_table.status_name='NOTWORKING')"
		+ " where agent_approved_table.agent_approved_id"
		+ "=agent_registration_table.agent_approved_id and agent_registration_table.agent_id=?";

	
public static final String FIX_AGENT_MEETING_WITH_ADMIN = "INSERT INTO admin_agent_meeting"
		+ " (`agent_id`, `venue`, `date`, `time`) VALUES (?,?,?,?)";

public static final String GETTING_LOGIN_ID="select login_table.login_id FROM login_table WHERE login_table.user_username=? and login_table.user_password=?";

}
