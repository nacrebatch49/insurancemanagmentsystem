/**
 * 
 */
package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;
import com.nacre.ims.dto.AddCarInsuranceDTO;

/**
 * @author Barkha
 *
 */
@WebServlet("/EditPolicyController")
public class EditCarInsuranceController extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside Edit Servlet");
		resp.setContentType("text/html");
		PrintWriter out=resp.getWriter();
	
		AdminService adminService = null;
		try {
			int carIncId=Integer.parseInt(req.getParameter("carIncId").toString());
			String policyName= req.getParameter("policyname");
			String policyDescription=req.getParameter("policydescription");
			String coverage=req.getParameter("policycoverage");
			int  duration=Integer.parseInt(req.getParameter("policyduration"));
			double initialAmount=Double.parseDouble(req.getParameter("policyinitialamt"));
			double paybleAmount=Double.parseDouble(req.getParameter("policypaybleamt"));
			double rateofInterest=Double.parseDouble(req.getParameter("policyrate"));
			
		    AddCarInsuranceDTO dto= new AddCarInsuranceDTO();
		    dto.setCarIncId(carIncId);
		    dto.setPolicyName(policyName);
		    dto.setPolicyDescription(policyDescription);
		    dto.setCoverage(coverage);
		    dto.setDuration(duration);
		    dto.setInitialAmount(initialAmount);
		    dto.setPaybleAmount(paybleAmount);
		    dto.setRateofInterest(rateofInterest);
		    System.out.println(dto);
		    
		    AdminService service = new AdminServiceImpl();
		    int data = service.editCarInsurance(dto);
		    if(data>0) {
		    	resp.sendRedirect("/ims/getCarInsuranceDataController");
		    }
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doGet(req, resp);
	}

}
