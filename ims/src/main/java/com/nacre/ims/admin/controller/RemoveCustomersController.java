package com.nacre.ims.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nacre.ims.admin.service.AdminService;
import com.nacre.ims.admin.serviceimpl.AdminServiceImpl;

@WebServlet("/removeCustomersController")
public class RemoveCustomersController extends HttpServlet {
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		//general settings
		res.setContentType("text/html");
		PrintWriter pw=res.getWriter();
		
		String ids=req.getParameter("id");
		int id=Integer.valueOf(ids);
		
		pw.println("your id is................"+id);
		//create service class object
		AdminService service=new AdminServiceImpl();
		int count=0;
		try{
			count=service.removeCustomer(id);
		}catch (Exception e) {
			e.printStackTrace();
		}
		if(id != 0){
			res.sendRedirect("/ims/viewAllCustomer");
		}
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}

}
