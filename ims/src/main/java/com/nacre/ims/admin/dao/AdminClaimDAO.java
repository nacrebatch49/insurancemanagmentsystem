package com.nacre.ims.admin.dao;

import java.util.List;

import com.nacre.ims.bo.ClaimDataBO;
import com.nacre.ims.bo.ClaimRequestDetailBO;

/*
  @author Rohit Ubare
*/
public interface AdminClaimDAO {

	public int getClaimNotification() throws Exception;

	public List<ClaimRequestDetailBO> getClaimName() throws Exception;

	public ClaimDataBO getClaimData(String pNo) throws Exception;
	
	public int getStatusID(String status)throws Exception;
	
	public int setFinalClaimReport(ClaimDataBO bo)throws Exception;
	
	public int updateClaimRequest(int claimID,int pNo,int statID)throws Exception;

}
