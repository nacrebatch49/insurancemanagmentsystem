<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Forgot Password</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">





<link rel="stylesheet"
	href="//cdn.jsdelivr.net/bootstrap/3.2.0/css/bootstrap.min.css" />

<!-- Include FontAwesome CSS if you want to use feedback icons provided by FontAwesome -->
<link rel="stylesheet"
	href="//cdn.jsdelivr.net/fontawesome/4.1.0/css/font-awesome.min.css" />

<!-- BootstrapValidator CSS -->
<link rel="stylesheet"
	href="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css" />

<!-- jQuery and Bootstrap JS -->
<script type="text/javascript"
	src="//cdn.jsdelivr.net/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript"
	src="//cdn.jsdelivr.net/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!-- BootstrapValidator JS -->
<script type="text/javascript"
	src="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>

<script type="text/javascript" src="../js/forgotpassword2.js"></script>
</head>
<body
	style="background-image: url('../images/login.jpg'); background-repeat: no-repeat; background-size: cover;">
	<div class="jumbotron" style="height: 100px;">
		<h1
			style="text-align: center; font-size: 50px; margin-top: -20px; font-weight: bold;">INSURANCE
			POLICY</h1>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-md-5">
				<div class="jumbotron" style="margin-top: 5px; margin-left: 50px; width: 600px;">
					<h3 align="center" style="font-size: 40px; font-weight: bold;">Reset Password</h3>
					<br>
					<p style="font-size: 20px;">Reset your password here</p>
					<hr>
					
					<form class="form-group"
						action="<%=application.getContextPath()%>/ForgotPasswordController2"
						method="post" id="forgotpass2">
						<div class="form-group">
							<div>
								<label for="password" class="cols-sm-2 control-label">New
									Password</label>
							</div>
							<div class="form-group">
								<div class="input-group mb-3">
									
										<span class="input-group-addon" id="basic-addon1"><i
											class="fa fa-lock" aria-hidden="true"></i></span> <input
											type="Password" name="newpassword" class="form-control"
											placeholder="Enter New Password" aria-label=""
											aria-describedby="basic-addon1"> <span
											class="glyphicon form-control-feedback"></span> <span
											class="help-block with-errors"></span>

								</div>
							</div>
						</div>
						<div class="form-group ">
							<div>
								<label for="password" class="cols-sm-2 control-label">Confirm
									Password</label>
							</div>
							<div class="form-group">
								<div class="input-group mb-3">
									
										<span class="input-group-addon" id="basic-addon1"><i
											class="fa fa-lock" aria-hidden="true"></i></span> <input
											type="Password" name="password" class="form-control"
											placeholder="Confirm Password" aria-label=""
											aria-describedby="basic-addon1"> <span
											class="glyphicon form-control-feedback"></span> <span
											class="help-block with-errors"></span>

								
								</div>
							</div>

						</div>
						<hr>
						<div class="form-group">
							<div>
								<button type="submit" id="forgot"
									class=" btn btn-primary form-control" style="font-weight: bold; font-size: 20px; height: 40px; width: 230px; height: 40px; margin-left: 130px;">Submit</button>
							</div>
						</div>
						<%
							String email = request.getQueryString();
							session.setAttribute("email", email);
							System.out.println(email);
						%>
					</form>
				</div>
			</div>

		<div class="col-md-5"></div>
	</div>
</div>
</body>
</html>
