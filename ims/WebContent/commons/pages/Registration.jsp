<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<html lang="en">
<head>

<!-- <link href="<%=application.getContextPath() %>/commons/css/style.css" rel="stylesheet" type="text/css" /> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <!-- Website Font style -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>


<script type="text/javascript" src="../js/Registration1.js"></script>
<script type="text/javascript" src="../js/GetData.js"></script>


<link rel="stylesheet" href="../css/Registration.css">
<script type="text/javascript" src="../js/Registration.js"></script>
</head>
<body>
	<div class="container">
		<div class="row main">
			<div class="main-login main-center"
				style="background: grey; opacity: 0.9; border-radius: 20px;">
				<h2 class="title">Create an Account</h2>

				<div class="form-group">
					<label for="user" class="cols-sm-2 control-label">Type</label>
					<div class="cols-sm-10 ">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-users fa"
								aria-hidden="true"></i></span> &nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; <input type="radio"
								name="user" id="agt" value="Agent">Agent
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
								type="radio" name="user" id="cln" value="Client"> Client
						</div>
					</div>
				</div>

				<form action="<%=application.getContextPath()%>/CustomerRegistrationController" method="post" id="clientform">
					<div id="formfield">
						<div class="form-group">
							<label for="first_name" class="cols-sm-2 control-label">First Name</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="glyphicon glyphicon-user"></i></span> <input name="first_name"
										placeholder="First Name" class="form-control" type="text" id="fname">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="last_name" class="cols-sm-2 control-label">LastName</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group ">
									<span class="input-group-addon"><i
										class="fa fa-pencil fa" aria-hidden="true"></i></span> <input
										type="text" class="form-control" name="last_name" id="lname"
										placeholder="Enter your Last name" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your
								Email</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="	fa fa-send fa" aria-hidden="true"></i></span> <input
										type="text" class="form-control" name="email" id="email"
										placeholder="Enter your Email" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="mobile" class="cols-sm-2 control-label">Mobile</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="fa fa-tablet fa" aria-hidden="true"></i></span> <input
										type="text" class="form-control" name="mobile" id="mob"
										placeholder="Enter your Mobile No" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="dob" class="cols-sm-2 control-label">DOB</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="fa fa-birthday-cake fa" aria-hidden="true"></i></span> <input
										type="date" class="form-control" name="dob" id="textdob">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="adhar" class="cols-sm-2 control-label"> Adhar
								Card No.</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="	fa fa-keyboard-o fa" aria-hidden="true"></i></span> <input
										type="text" class="form-control" name="adhar" id="adhar"
										placeholder="Enter your Adhar Card No." />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="gender" class="cols-sm-2 control-label">Gender</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa"
										aria-hidden="true"></i></span> &nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<input type="Radio"
										name="gender" value="male">Male &nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
										type="radio" name="gender" value="Female">Female
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="agentno" class="cols-sm-2 control-label">Agent
								No</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="fa fa-user-secret fa" aria-hidden="true"></i></span> <input
										type="text" class="form-control" name="agentno" id="agentno"
										placeholder="Enter your Agent Number" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="address" class="cols-sm-2 control-label">Address</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
								<span class="input-group-addon"><i
										class="fa fa-home fa" aria-hidden="true"></i></span>
									<span> <textarea class="form-control" rows="1" cols="80"
											name="address" placeholder="Enter Address" id="address"></textarea>
									</span>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="country" class="cols-sm-2 control-label">Country</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="fa fa-list fa" aria-hidden="false"></i></span> <select
										class="form-control selectpicker" name="country" id="country"  >
										<option value=" ">Select country</option>
										
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="state" class="cols-sm-2 control-label">State</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-list fa"
										aria-hidden="true"></i></span> <select class="form-control selectpicker"
										name="state" id="state">
										<option value=" ">Select State</option>
										
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="city" class="cols-sm-2 control-label">City</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="fa fa-list fa" aria-hidden="true"></i></span> <select
										class="form-control selectpicker" name="city" id="city">
										<option value=" ">Select City</option>
										
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="pincode" class="cols-sm-2 control-label">Pincode</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="fa fa-home fa" aria-hidden="true"></i></span> <input
										type="text" class="form-control" name="pincode" id="pincode"
										placeholder="Enter your Pincode" />
								</div>
							</div>
						</div>

						<input type="submit" id="subclient" class="agentsb" value="Register">
					</div>
				</form>







				<form action="<%=application.getContextPath()%>/AgentApprovedController" method="post" id="agentform">
					<div id="formfield2">
						<div class="form-group">
							<label for="fullname" class="cols-sm-2 control-label">Full Name</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa"
										aria-hidden="true"></i></span> <input type="text"
										class="form-control" name="fullname" id="fname"
										placeholder="Enter yourFirst name" />
								</div>
							</div>
						</div>


						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your
								Email</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="fa fa-envelope fa" aria-hidden="true"></i></span> <input
										type="text" class="form-control" name="email" id="email"
										placeholder="Enter your Email" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="gen" class="cols-sm-2 control-label">Gender</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa"
										aria-hidden="true"></i></span> &nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<input type="Radio"
										name="gen" value="male">Male &nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
										type="radio" name="gen" value="Female">Female
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="qualification" class="cols-sm-2 control-label">
								Highest Qualification</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="fa fa-graduation-cap fa" aria-hidden="true"></i></span> <input
										type="text" class="form-control" name="qualification" id="qly"
										placeholder="Enter your Highest Qualification" />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="loc" class="cols-sm-2 control-label">
								Location</label>
							<div class="cols-sm-10 inputGroupContainer">
								<div class="input-group">
									<span class="input-group-addon"><i
										class="fa fa-map-marker fa" aria-hidden="true"></i></span> <input
										type="text" class="form-control" name="loc" id="loc"
										placeholder="Enter your Location" />
								</div>
							</div>
						</div>

						<input type="submit" id="subagent" class="clientsb" value="Request for Approval">

					</div>
				</form>
			</div>
		</div>
	</div>

  

</body>
</html>

