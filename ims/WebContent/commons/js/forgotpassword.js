 $(document).ready(function() {
    $('#forgotpassword').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	 email: {
                 validators: {
                     notEmpty: {
                         message: 'Please enter your email address'
                     },
                    
                 }
             },
        }, onSuccess:function () {
            return true;                        
        },
        onError: function(){
            return false;
        }
    })
        

		
});