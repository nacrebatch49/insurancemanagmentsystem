 $(document).ready(function() {
    $('#forgotpass2').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	newpassword: {
                validators: {
                    identical: {
                        field: 'password',
                        message: 'Confirm your password below - type same password please'
                    }

                }
            },
            password: {
                validators: {
                	identical: {
                        field: 'newpassword',
                        message: 'The password and its confirm are not the same'
                    }

                }
            },
        },onSuccess:function () {
            return true;                        
        },
        onError: function(){
            return false;
        }
    })
        
		
});