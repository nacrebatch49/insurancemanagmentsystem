 $(document).ready(function() {
    $('#clientform').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                	regexp: {
                        regexp: /^[a-zA-Z]+$/,
                        message: 'The first name can only consist of alphabets'
                    },
                        stringLength: {
                        min: 2,
                        max:15,
                        message:'first name should contain atleast 2-15 characters'
                    },
                        notEmpty: {
                        message: 'Please enter your first name'
                    }
                }
            },
             last_name: {
                validators: {
                	regexp: {
                        regexp: /^[a-zA-Z]+$/,
                        message: 'The last name can only consist of alphabets'
                    },
                     stringLength: {
                        min: 2,
                        max:15,
                        message:'last name should contain atleast 2-15 characters'
                    },
                    notEmpty: {
                        message: 'Please enter your last name'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your email address'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                }
            },
            mobile: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your mobile number'
                    },
                    phone: {
                        country: 'US',
                        message: 'Please enter a vaild phone number'
                    }
                }
            },
            dob: {
                validators: {
                    notEmpty: {
                        message: 'The date of birth is required'
                    }
                }
            },
           adhar: {
                validators: {
                	
                 stringLength: {
                	    min:12,
                        max:12,
                        message:'Adhar card number should contain 16 digit'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'The Adhar card number should contain numeric values'
                    },
                    
                        notEmpty: {
                        message: 'Please enter your adhar card number'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            agentno: {
                validators: {
                	notEmpty: {
                        message: 'Please enter agent number'
                    }
                }
            },
            address: {
                validators: {
                     stringLength: {
                        min:10,
                        max:30
           
                    },
                    notEmpty: {
                        message: 'Please supply your street address'
                    }
                }
            },
            
            country: {
                validators: {
                    notEmpty: {
                        message: 'Please select your country'
                    }
                }
            },
            state: {
                validators: {
                    notEmpty: {
                        message: 'Please select your state'
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: 'Please select your city'
                    }
                }
            },
           
            pincode: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your pincode'
                    },
                   stringLength: {
                        min:6,
                        max:6,
                        message: 'Please enter a vaild pincode'
                    }
                }
            },
		/*
	password: {
            validators: {
                identical: {
                    field: 'confirmPassword',
                    message: 'Confirm your password below - type same password please'
                }
            }
        },
        confirmPassword: {
            validators: {
                identical: {
                    field: 'password',
                    message: 'The password and its confirm are not the same'
                }
            }
         },
			*/
            
            },
            onSuccess:function () {
                return true;                        
            },
            onError: function(){
                return false;
            }
        })
    
  /* .on('success.form.bv', function(e) {
        $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
            $('#clientform').data('bootstrapValidator').resetForm();

        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $.post($form.attr('action'), $form.serialize(), function(result) {
            console.log(result);
        }, 'json');
    })*/
        

    
    $('#agentform').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fullname: {
                validators: {
                	regexp: {
                        regexp: /^[a-zA-Z]?/,
                        message:'Please enter your fullname'
                    },
                        notEmpty: {
                        message: 'Please enter your fullname'
                    }
                }
            },
             
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your email address'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                }
            },
            gen: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            qualification: {
                validators: {
                    notEmpty: {
                        message: 'The Highest Qualification is required'
                    }
                }
            },
            loc: {
                validators: {
                	notEmpty: {
                        message: 'Please enter agent number'
                    }
                }
            },
           
            
            },
            onSuccess:function () {
                return true;                        
            },
            onError: function(){
                return false;
            }
        })
       /* .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#agentform').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json',true);
        });*/

		
});


 




















/*$(document).ready(function() {
    alert("hi..");
    $('#formfield').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
           first_name: {
                message: 'The first is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The username must be more than 6 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The username can only consist of alphabetical and number'
                    },
                    different: {
                        field: 'password',
                        message: 'The username and password cannot be the same as each other'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'username',
                        message: 'The password cannot be the same as username'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must have at least 8 characters'
                    }
                }
            },
            birthday: {
                validators: {
                    notEmpty: {
                        message: 'The date of birth is required'
                    },
                    date: {
                        format: 'YYYY/MM/DD',
                        message: 'The date of birth is not valid'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            }
        }
    });
});
*/