 $(document).ready(function() {
    $('#change_password').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	newpassword: {
                validators: {
                        stringLength: {
                        min: 8,
                        max:8,
                        message:'Password should contain only 8 characters'
                    },
                        notEmpty: {
                        message: 'Please enter your Password'
                    }
                }
            },
        },
        onSuccess:function () {
            return true;                        
        },
        onError: function(){
            return false;
        }
    })
        

		
});