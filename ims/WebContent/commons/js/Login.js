$(document).ready(function() {

    $('#login').bootstrapValidator({
    	
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	username: {
                validators: {
                    notEmpty: {
                        message: 'Please enter your username'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    
                }
            },
        },
        onSuccess:function () {
            return true;                        
        },
        onError: function(){
            return false;
        }
    }) 
    
    	});