
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"
	import="java.util.*,com.nacre.ims.dto.AddCarInsuranceDTO"%>

<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>login page</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="<%=application.getContextPath()%>/admin/js/addcarinsurence.js"></script>


<style type="text/css">
.btn {
	width: 49.2%;
}
</style>

</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<div class="container">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<img style="-webkit-user-select: none;"
						src="<%=application.getContextPath()%>/images/CarInsue.jpg"
						alt="CarInsue2" width="1250" height="190">
				</div>

				<div class="item">
					<img style="-webkit-user-select: none;"
						src="<%=application.getContextPath()%>/images/CarInsue1.jpg"
						alt="CarInsue" width="1250" height="190">
				</div>

				<div class="item">
					<img style="-webkit-user-select: none;"
						src="<%=application.getContextPath()%>/images/CarInsue2.jpg"
						alt="CarInsue1" width="1250" height="190">
				</div>
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span> <span
				class="sr-only">Previous</span>
			</a> <a class="right carousel-control" href="#myCarousel"
				data-slide="next"> <span
				class="glyphicon glyphicon-chevron-right"></span> <span
				class="sr-only">Next</span>
			</a>
		</div>
	</div>


	<%-- <img style="-webkit-user-select: none;cursor: zoom-in;" 
	src="<%=application.getContextPath()%>/LifeInsue.jpg" width="1250" height="190">  --%>



	<br>

	<button type="submit" class="btn btn-success" style="width: 10%;margin-left: 80%"
	         data-toggle="modal" data-target="#myModal"> Add Policy</button>

	<br>
	<br>
	<br>


	<%
		Object obj = session.getAttribute("list");
		List<AddCarInsuranceDTO> list = (List<AddCarInsuranceDTO>) obj;
		int i = 1;
		for (AddCarInsuranceDTO dto : list) {
	%>


	<div class="container">
		<div class="row">
			<div class="col-sm-12"
				style="border: 5px solid DodgerBlue; border-radius: 20px; height: 350px">
				<img src="<%=application.getContextPath()%>/images/CarSide.png"
					width="70" height="50" align="top">
				<table>
					<tr>
						<td style="color: blue;"><h3 id="policyname"><%=dto.getPolicyName()%>&nbsp;&nbsp;&nbsp;<%=dto.getCoverage()%></h3>
						</td>
					</tr>
					<tr>
						<td style="color: blue;"><label id="poicyNumber"></label></td>
					</tr>
					<tr>
						<td><label id="description"><%=dto.getPolicyDescription()%></label></td>
					</tr>

					<tr>
						<td><b>Coverage:</b><label id="coverage"><%=dto.getCoverage()%></label></td>
					</tr>
					<tr>
						<td><b>Duration:</b><label id="duration"><%=dto.getDuration()%></label></td>
					</tr>
					<tr>
						<td><b>Initial Amount:</b><label id="initial_amount"><%=dto.getInitialAmount()%></label></td>
					</tr>
					<tr>
						<td><b>Payble Amount:</b><label id="payble_amount"><%=dto.getPaybleAmount()%></label></td>
					</tr>
					<tr>
						<td><b>Rate-of-interest:</b><label id="rate"><%=dto.getRateofInterest()%></label></td>
					</tr>
				</table>
				<button type="submit" class="btn btn-primary" id="login"
					style="width: 10%; margin-left: 70%" data-toggle="modal"
					data-target="#myModal<%=i%>">Edit Policy</button>
				<a type="submit"
					href="<%=application.getContextPath()%>/deleteCarPolicyController?id=<%=dto.getCarIncId()%>"
					class="btn btn-danger" id="login" style="width: 10%;">Delete
					Policy</a>
			</div>

		</div>
		<br> <br> <br>
	</div>

	<!-- --------------------------------------Modal2------------------------------------- -->
	<!-- Modal -->
	<div class="modal fade" id="myModal<%=i%>" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<form action="<%=application.getContextPath()%>/EditPolicyController"
				method="post" id="editcarpolicy">
				<div class="modal-content"
					style="border: 3px solid dodgerblue; background-color: #ffdab3; border-radius: 20px;">

					<div class="modal-header" align="center"
						style="background-color: #8080ff; border-radius: 20px;">
						<button style="color: black" type="button" class="close"
							data-dismiss="modal">&times;</button>
						<h1 class="modal-title">
							<b style="color: white">Edit Policy</b>
						</h1>
					</div>
					<div class="modal-body">
						<label>Policy Name:</label> <input type="text"
							class="form-control" readonly id="policyname"
							placeholder="Enter policy name" name="policyname"
							value="<%=dto.getPolicyName()%>">
					</div>
					<!-- <div class="modal-body">
           <label >Policy No:</label>
           <input type="text" class="form-control" id="policyno" placeholder="Enter password" name="policyno">
       </div> -->
					<div class="modal-body">
						<label>Policy Description:</label>
						<textarea class="form-control" id="policydescription"
							placeholder="Enter policy descrption" name="policydescription"><%=dto.getPolicyDescription()%></textarea>
					</div>
					<div class="modal-body">
						<label style="">Coverage:</label>
						<textarea class="form-control" id="policycoverage"
							placeholder="Enter policy coverage" name="policycoverage"><%=dto.getCoverage()%></textarea>
					</div>
					<div class="modal-body">
						<label>Duration :</label> <input type="number"
							class="form-control" id="policyduration" min="10" max="30"
							placeholder="Enter policy duration"
							value="<%=dto.getDuration()%>" name="policyduration">
					</div>
					<div class="modal-body">
						<label> Initial Amount:</label> <input type="number"
							class="form-control" id="policyinitialamt"
							placeholder="Enter policy initial amount"
							value="<%=dto.getInitialAmount()%>" name="policyinitialamt">
					</div>
					<div class="modal-body">
						<label> Payble Amount:</label> <input type="number"
							class="form-control" id="policypaybleamt"
							placeholder="Enter policy payble amount"
							value="<%=dto.getPaybleAmount()%>" name="policypaybleamt">
					</div>
					<div class="modal-body">
						<label>Rate of interest:</label> <input type="text"
							class="form-control" id="policyrate"
							placeholder="Enter policy rate of interate" name="policyrate"
							value=<%=dto.getRateofInterest()%>>
					</div>
					<input type="hidden" name="carIncId" value="<%=dto.getCarIncId()%>" />

					<div class="modal-footer">
						<input type="submit" class="btn btn-success" value="submit">
					</div>

				</div>
			</form>
		</div>

	</div>


	<!-- ----------------------------------/Modal2--------------------------------------- -->
	<%
		i++;
		}
	%>

	<!-- --------------------------------------Modal1------------------------------------- -->
	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<form action="<%=application.getContextPath()%>/AddPolicyController"
				method="post" id="addcarpolicy" data-toggle="validator" role="form">
				<div class="modal-content"
					style="border: 3px solid dodgerblue; background-color: #ffdab3; border-radius: 20px;">

					<div class="panel-heading" align="center"
						style="background-color: #8080ff; border-radius: 20px;">
						<button style="color: black" type="button" class="close"
							data-dismiss="modal">&times;</button>
						<h1 class="modal-title">
							<b style="color: white">Add Policy</b>
						</h1>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="control-label" for="inputName">Policy Name:</label>
							<input type="text" class="form-control" id="policyname"
								placeholder="Enter policy name" name="policyname" required>

						</div>

						<div class="form-group">
							<label class="control-label" for="inputName">Policy
								Coverage:</label> <input type="text" class="form-control"
								id="policycoverage" placeholder="Enter policy coverage"
								name="policycoverage" required>

						</div>
						<div class="form-group">
							<label class="control-label" for="inputName">Policy
								Description:</label>
							<textarea class="form-control" id="policydescription"
								placeholder="Enter policy descrption" name="policydescription"
								required></textarea>

						</div>
						<div class="form-group">
							<label class="control-label" for="inputName">Duration :</label> <input
								type="number" class="form-control" id="policyduration" min="0"
								max="120" placeholder="Enter policy duration in month"
								name="policyduration" required>

						</div>
						<div class="form-group">
							<label class="control-label" for="inputName">Initial
								Amount :</label> <input type="number" class="form-control"
								id="initialamount" min="2000" max="6000"
								placeholder="Enter policy initial amount" name="initialamount"
								required>

						</div>
						<div class="form-group">
							<label class="control-label" for="inputName">Payble
								Amount :</label> <input type="number" class="form-control"
								id="paybleamount" placeholder="Enter policy payble amount"
								name="paybleamount" required>

						</div>
						<div class="form-group">
							<label class="control-label" for="inputName">Rate of
								interest:</label> <input type="text" class="form-control"
								id="policyrate" placeholder="Enter policy rate of interate"
								name="policyrate" required>

						</div>


						<div class="modal-footer">
							<input type="submit" class="btn btn-success">
						</div>
					</div>
				</div>
			</form>
		</div>

	</div>


	<!-- ----------------------------------/Modal1--------------------------------------- -->
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>