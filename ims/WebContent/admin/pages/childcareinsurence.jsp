
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" import="java.util.*,com.nacre.ims.dto.GetChildCareInsurenceDataDTO"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html5/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>child care</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
  
  <script src="<%=application.getContextPath()%>/admin/js/addchildcareinsurence.js"></script>
  <script src="<%=application.getContextPath()%>/admin/js/editchildcareinsurence.js"></script>
<style type="text/css">
.btn {
	width: 49.2%;
}


</style>

</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container"> 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img style="-webkit-user-select: none;" 
	src="<%=application.getContextPath()%>/images/childinsurance.jpg" alt="Banner-Education" width="1250" height="190"> 
      </div>

      <div class="item">
        <img style="-webkit-user-select: none;" 
	src="<%=application.getContextPath()%>/images/Banner-Education.jpg" alt="banner-commercial-child-care" width="1250" height="190"> 
      </div>
    
      <div class="item">
         <img style="-webkit-user-select: none;" 
	src="<%=application.getContextPath()%>/images/banner-commercial-child-care.jpg" alt="childinsuranc" width="1250" height="190"> 
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
	
	
	 <%-- <img style="-webkit-user-select: none;cursor: zoom-in;" 
	src="<%=application.getContextPath()%>/LifeInsue.jpg" width="1250" height="190">  --%>
	

	
	<br>
	<button type="submit" class="btn btn-success" id="login" style="width: 10%;margin-left: 80%" data-toggle="modal" data-target="#myModal">Add Policy</button>
	<br><br><br>
	<% Object obj=session.getAttribute("list");
	List<GetChildCareInsurenceDataDTO> list=(List)obj;
	int i=1;
	for(GetChildCareInsurenceDataDTO dto:list){
	%>
	<div class="container">
		<div class="row">
		<div class="col-sm-12" style="border: 5px solid DodgerBlue; border-radius: 20px; height: 500px" >
		 <img src="<%=application.getContextPath()%>/images/LifeSide.png" width="80" height="80" align="top"> 
		<table>
		<tr>
		<td style="color: blue;"><h3 id="policyname"> <%=dto.getName()%></h3> </td>
		</tr> 
		<!-- <tr>
		<td style="color: blue;"><label id="poicyNumber"><%-- <%=dto.getInsurance_policy_no() %> --%></label> </td>
		</tr> -->
		<tr>
		<td><p><b style="color: blue;">Description:</b></p><label id="description"> <%=dto.getDesciption() %></label></td>
		</tr>
		<!-- <tr>
		<td><b>EntryAge:</b><lable id="age">18 years</lable></td>
		</tr> -->
		<tr>
		<td><b  style="color: blue;">Coverage:</b><label id="coverage"> <%=dto.getCoverage() %></label></td>
		</tr>
		<tr>
		<td><b  style="color: blue;">Duration:</b><label id="duration"> <%=dto.getMax_limit()%></label></td>
		</tr>
		<tr>
		<td><b  style="color: blue;">Amount:</b><label id="amount" style="text-align: right;"><h> &#x20a8;<h>  <%=dto.getPayable_amount() %><h>cr</h></label></td>
		</tr>
		<tr>
		<td><b  style="color: blue;">Initial Amount:</b><label id="amount"><h> &#x20a8;<h>  <%=dto.getInitial_amount() %></label></td>
		</tr>
		<tr>
		<td><b  style="color: blue;">Rate-of-interest:</b><label id="rate"> <%=dto.getRate_of_interest() %><h>%</h></label></td>
		</tr>
		 <tr>
		<td><b  style="color: blue;">Offers:</b><label id="rate">  <%=dto.getLife_insurance_offers()%></label></td>
		</tr>
		<!-- <tr>
		<td><b style="color: blue;">Status:</b><label id="status"></label></td>
		</tr> -->
		
		</table>
		<button type="submit" class="btn btn-primary" id="login" style="width: 10%;margin-left:70%" data-toggle="modal" data-target="#myModal<%=i%>">Edit Policy</button>
 		<a type="submit" href="<%=application.getContextPath() %>/deletechildcareinsurencecontroller?id=<%=dto.getChildid() %>" class="btn btn-danger" id="login" style="width: 12%;margin-left: 1%">Delete Policy</a>
 		
				</div>
				
		</div>
		<br><br><br>
	</div>
	<!-- --------------------------------------Modal1------------------------------------- -->
<!-- Modal -->
  <div class="modal fade" id="myModal<%=i %>" role="dialog" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <form action="<%=application.getContextPath() %>/editchildcareinsurencecontroller" method="post" id="editlifepolicy" >
      <div class="modal-content" style="border:3px solid dodgerblue; background-color:#ffdab3; border-radius:20px;" >
      
        <div class="panel-heading"  align="center" style="background-color:#8080ff;border-radius:20px;">
          <button style="color:black" type="button" class="close" data-dismiss="modal">&times;</button>
          <h1 class="modal-title" ><b style="color:white">Add Policy</b></h1>
        </div>
         <div class="panel-body">
          <div class="form-group">
           <label >Policy Name:</label>
           <input type="text" class="form-control" readonly id="policyname" value="<%=dto.getName() %>" placeholder="Enter policy name" name="policyname" required>
        </div>
        <!-- <div class="modal-body">
           <label >Policy No:</label>
           <input type="text" class="form-control" id="policyno" placeholder="Enter password" name="policyno">
       </div> -->
       
        <div class="form-group">
           <label >Policy Description:</label>
           <textarea class="form-control" id="policydescription"  placeholder="Enter policy descrption" name="policydescription" required><%=dto.getDesciption()%></textarea>
        </div>
        <div class="form-group">
      <label >Policy Coverage:</label>
      <textarea class="form-control" id="policycoverage" placeholder="Enter policy coverages" name="policycoverage" required><%=dto.getCoverage()%></textarea>
    </div>
        <div class="form-group">
           <label  >Duration :</label>
           <input type="number" class="form-control" id="policyduration"  value="<%=dto.getMax_limit() %>" placeholder="Enter policy duration in month" name="policyduration" required>
        </div>
        <div class="form-group">
           <label >Rate of interest:</label>
           <input type="number" class="form-control" id="policyrate" value="<%=dto.getRate_of_interest()%>" placeholder="Enter policy rate of interate" name="policyrate" required>
        </div>
       <div class="form-group">
          <label >Amount:</label>
          <input type="number" class="form-control" id="policyamount" value="<%=dto.getPayable_amount() %>" placeholder="Enter policy amount" name="policyamount" required>
        </div>
        <div class="form-group">
          <label >Initial Amount:</label>
          <input type="numbre" class="form-control" id="policyinitialamount" value="<%=dto.getInitial_amount() %>" placeholder="Enter policy amount" name="policyinitialamount" required>
        </div>
        <div class="form-group">
          <label >Offer:</label>
          <input type="text" class="form-control" id="policyoffer" value="<%=dto.getLife_insurance_offers() %>" placeholder="Enter policy offer" name="policyoffer">
        </div>
         
       <input type="hidden" name="childId" value="<%= dto.getChildid()%>">
        <div class="modal-footer">
          <input type="submit" class="btn btn-success" >
        </div>
        
      </div>
      </div>
      </form>
    </div>
    
  </div>
  

    <!-- ----------------------------------/Modal1--------------------------------------- -->
   
	
	<%
	i++;
		} %>
	<!-- --------------------------------------Modal------------------------------------- -->
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <form action="<%=application.getContextPath() %>/addchildcareinsurencecontroller" method="post" id="addlifepolicy" >
      <div class="modal-content" style="border:3px solid dodgerblue; background-color:#ffdab3; border-radius:20px;" >
      
        <div class="panel-heading"  align="center" style="background-color:#8080ff;border-radius:20px;">
          <button style="color:black" type="button" class="close" data-dismiss="modal">&times;</button>
          <h1 class="modal-title" ><b style="color:white">Add Policy</b></h1>
        </div>
        <div class="panel-body">
        <div class="form-group">
           <label >Policy Name:</label>
           <input type="text" class="form-control" id="policyname" placeholder="Enter policy name" name="policyname" required>
        </div>
        <!-- <div class="modal-body">
           <label >Policy No:</label>
           <input type="text" class="form-control" id="policyno" placeholder="Enter password" name="policyno" required>
       </div> -->
       
        <div class="form-group">
           <label>Policy Description:</label>
           <textarea class="form-control" id="policydescription" placeholder="Enter policy descrption" name="policydescription" required></textarea>
        </div>
        <div class="form-group">
      <label>Policy Coverage:</label>
      <textarea class="form-control" id="policycoverage" placeholder="Enter Coverage" name="policycoverage" required></textarea>
    </div>
        <div class="form-group">
           <label  >Duration :</label>
           <input type="number" class="form-control" id="policyduration"  min="0" max="360" placeholder="Enter policy duration in month" name="policyduration" required>
        </div>
        <div class="form-group">
           <label >Rate of interest:</label>
           <input type="number" class="form-control" id="policyrate" placeholder="Enter policy rate of interate" name="policyrate" required>
        </div>
       <div class="form-group">
          <label >Amount:</label>
          <input type="number" class="form-control" id="policytamount" placeholder="Enter policy amount company payable" name="policytamount" required>
        </div>
        <div class="form-group">
          <label >Initial Amount:</label>
          <input type="number" class="form-control" id="policyiamount" placeholder="Enter policy Initial Amount" name="policyiamount" required>
        </div>
         <div class="form-group">
          <label >Offer:</label>
          <input type="text" class="form-control" id="policyoffer" placeholder="Enter policy offer" name="policyoffer">
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-success" >
        </div>
        
      </div>
      </div>
      </form>
    </div>
    
  </div>
  

    <!-- ----------------------------------/Modal--------------------------------------- -->
   
   
   <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>