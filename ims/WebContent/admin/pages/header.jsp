<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

<style>
.navbar1 {
    overflow: hidden;
    background-color: #333;
    font-family: Arial, Helvetica, sans-serif;
}

.navbar1 a {
    float: left;
    font-size: 16px;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

.dropdown1 {
    float: left;
    overflow: hidden;
}

.dropdown1 .dropbtn {
    font-size: 16px;    
    border: none;
    outline: none;
    color: white;
    padding: 14px 16px;
    background-color: inherit;
    font-family: inherit;
    margin: 0;
}

.navbar1 a:hover, .dropdown1:hover .dropbtn {
    background-color: red;
}

.dropdown-content1 {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content1 a {
    float: none;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content1 a:hover {
    background-color: #ddd;
}

.dropdown1:hover .dropdown-content1 {
    display: block;
}
</style>
</head>
<body>
<div>
  <h1 style="text-align: center;">Insurance Policy Management</h1>
</div>
<div class="navbar1">
<a href="home.jsp">Admin Home</a>
  <div class="dropdown1">
    <button class="dropbtn">policy <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content1">
       <a href="<%=application.getContextPath()%>/getlifeinsurencedatacontroller">Life Insurance</a>
       <a href="<%=application.getContextPath()%>/getCarInsuranceDataController">Car Insurance</a>
       <a href="<%=application.getContextPath()%>/gethomeinsurancecontroller">Home Insurance</a>
       <a href="<%=application.getContextPath()%>/getchildcareinsurencedatacontroller">Child Care Insurance</a>
   </div>
  </div>
   <a href="<%=application.getContextPath()%>/get_Agent_DataController">View Agent</a> 
   <a href="<%=application.getContextPath()%>/viewAllCustomer">View Client</a>
   <a href="<%=application.getContextPath()%>/getApprovalUrl">Agent Request</a>
   <div class="dropdown1">
    <button class="dropbtn">Reminder <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content1">
       <a href="<%=application.getContextPath()%>/three">1Day Delay</a>
       <a href="<%=application.getContextPath()%>/two">Next payment</a>
       <a href="<%=application.getContextPath()%>/one">Policy Reminder</a>
   </div>
  </div>
		<a href="<%=application.getContextPath() %>/admin/pages/ClaimRequest.jsp">Claim Request <span
						class="badge badge-info" id="id_notify">0</span></a>
        <a  href="">Feedbacks</a>
       	<a  href="<%=application.getContextPath() %>/commons/pages/change.jsp">Change Password</a>
        <a   href="<%=application.getContextPath() %>/logout">Logout</a>
</div>



</body>
</html>