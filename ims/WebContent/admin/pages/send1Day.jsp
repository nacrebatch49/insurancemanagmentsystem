<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="com.nacre.ims.bo.Send_Rem_Before_1_Day_Bo"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="admin/js/send1dayjs.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
body{
background-image: url('admin/pages/business-backgrounds.jpg');
background-repeat: no-repeat;
background-position: inherit;
 background-size: 100%; 
}
table,td{
color: black;
font: bold;
}
#foot{
position: absolute;
}
footer {
   position: relative;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: gray;
   color: white;
   text-align: center;
   padding: 10px 1px 1px 1px;
}
</style>

</head>
<body>
<jsp:include page="header.jsp"/>
<h1 style="text-align: center;color:white">PAYMENT REMAINDER-1 DAY BEFORE</h1>
<div class="container">
<div class="row">
<div class="col-lg-1"></div>
<div class="col-lg-10">
<table  class="table table-bordered">
  <tr>
    <th>S.No:</th>
    <th>Name:</th>
    <th>Phone No:</th>
    <th>Email ID:</th>
  </tr>
<%
int count=1;
String s2="";
Object obj=request.getAttribute("notn3");
Map<Integer,Send_Rem_Before_1_Day_Bo> map=(Map)obj;
Set<Integer> s=map.keySet();
for(Integer i:s){
	Send_Rem_Before_1_Day_Bo bo=map.get(i);
	s2=s2+bo.getEmailId()+",";
//out.println(bo);
%>
  <tr>
  	<td><%=count %></td>
  	<td><%=bo.getName() %></td>
   	<td><%=bo.getMobileNo()%></td>
     <td><%=bo.getEmailId() %></td>
  </tr>
<%
count++;
}
%>
</table>

</div>
<div class="col-lg-1"></div>
</div>


<%-- <button onclick="return fun('<%=s2%>');" value="<%=s2%>">send</button> --%>
<center>
<button id="btn1">send</button>
</center>
<input type="hidden" id="abc" name="sname" value="<%=s2%>" >

</div>

<jsp:include page="footer.jsp"></jsp:include>
</body></html>