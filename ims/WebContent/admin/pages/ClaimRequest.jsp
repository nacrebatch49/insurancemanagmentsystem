<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Claim List</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"> 
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
 
<script src="/ims/admin/js/ClaimRequest.js"></script>
<style>
#foot{
	position: fixed;
}
</style>
<!-- <style >
input[type=text]{
	class:form-control;
}
</style> -->
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
		<div class="container table-responsive" align="center" style="padding-top: 15px">

			<table class="table table-bordered" id="tab">
				<thead class="thead-dark">
					<tr>
						<th>Name</th>
						<th>Surname</th>
						<th>Policy No</th>
						<th>Nominy</th>
						<th>Email</th>
						<th>Relation</th>
					</tr>
				</thead>
			</table>
		</div>

	<div id="detail">
		<form action="/ims/claimProf" class="container" id="id_claimProfile">
			<br>

			<div class="row jumbotron" align="center">
				<div class="col-md-6">
					<h4 >Policy Details</h4>
					<table class="table-sm">
					<tr>
					<th><label>Policy Holder</label></th><td><input type="text" class="form-control" id="pHold"  ></td>
					</tr>
					
					<tr>
					<th><label>Policy Number</label></th><td><input type="text" class="form-control" name="npNo" id="pNo"></td>
					</tr>
					
					<tr>
					<th><label>Policy Name</label></th><td><input type="text" class="form-control" id="pName"></td>
					</tr>
					
					<tr>
					<th><label>Total EMI Pay</label></th><td><input type="text" class="form-control" id="emi"></td>
					
					</tr>
					
					<tr>
					<th><label>Rate Of Intrest</label></th><td><input type="text" class="form-control" id="roi"></td>
					</tr>
					
					<tr>
					<th><label>GST</label></th><td><input type="text" class="form-control" id="gst"></td>
					</tr>
					
					<tr>
					<th><label>Policy Amount</label></th><td><input type="text" class="form-control" name="npAmnt" id="pAmnt"></td>
					</tr>
					
					<tr>
					<th><label>Nominy Name</label></th><td><input type="text" class="form-control" id="nName"></td>
					</tr>
					<tr>
					<th><label>Email</label></th><td><input type="text" class="form-control" id="nEmail"></td>
					</tr>
					<tr>
					<th><label>Relation</label></th><td><input type="text" class="form-control" id="rel"></td>
					</tr>
					
					<tr>
					<th><label>Status</label></th><td><input type="text" class="form-control" name="nStat" id="stat"></td>
					</tr>
					</table>
				</div>
				
				<div class="col-md-6 form-group">
					<label for="comment">Description</label>
					<textarea class="form-control" rows="4" id="id_Des" name="nDes" required="required"></textarea><br><br>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

				
				
			</div>

		</form>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>