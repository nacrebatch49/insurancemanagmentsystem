<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ page import="java.util.*,com.nacre.ims.dto.GetHomeInsuranceDTO" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>login page</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
    <script src="<%=application.getContextPath()%>/admin/js/addpolicy.js"></script>
      <script src="<%=application.getContextPath()%>/admin/js/editpolicy.js"></script>
<style type="text/css">
.btn {
	width: 49.2%;
}

</style>

</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<div class="container"> 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img style="-webkit-user-select: none;" 
	src="<%=application.getContextPath()%>/images/home_banner.jpg" width="1150" > 
      </div>

      <div class="item">
        <img style="-webkit-user-select: none;" 
	src="<%=application.getContextPath()%>/images/Home-Insurance-Banner.jpg" width="1250" > 
      </div>
    
      <div class="item">
         <img style="-webkit-user-select: none;" 
	src="<%=application.getContextPath()%>/images/life-insurance-banner2.jpg" width="1250" height="270px"> 
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

		<br> <br> <br> 
	
	<button type="submit" class="btn btn-success" style="width: 10%;margin-left: 80%"
	         data-toggle="modal" data-target="#myModal"> Add Policy</button>
	<br><br><br>
    <p id="addpolicy12" style="color: red;font-size: 50%; text-align: center;"></p> 
	<% Object obj=session.getAttribute("data");
		List<GetHomeInsuranceDTO> list=(List)obj;
		int i=1;
		for(GetHomeInsuranceDTO dto:list){
	%>
	
	<div class="container">
		<div class="row">
			<div class="col-sm-12"
				style="border: 5px solid DodgerBlue; border-radius: 20px;height: 550px;">
				<img src="<%=application.getContextPath()%>/images/insurance-card-logo2.png" width="80"height="80">
				<table>
				<tr>
		<td style="color: blue;"><h3 id="policyname"><%=dto. getPolicyName() %></h3> </td>
		</tr>
		<tr>
		<td><b style="color:blue">Policy Number ::</b><label id="poicyNumber"><%=dto.getPolicyNumber() %></label> </td>
		</tr>
		<tr>
		<td><p><b style="color:blue">Coverage ::</b></p><label id="coverage"><%=dto.getCoverage() %></label></td>
		</tr>
		<tr>
		<td><p><b style="color: blue;">Description ::</b></p><label id="description"><%=dto.getDescription() %></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Duration ::</b><label id="duration"><%=dto.getDuration() %>&nbsp;<h>months</h></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Initial Amount ::</b><label id="iamount"><%=dto.getInitialAmount() %>&nbsp;<h>Rupee</h></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Total Amount ::</b><label id="tamount"><%=dto.getTotalamount() %>&nbsp;<h>Rupee</h></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Rate-of-interest ::</b><label id="rate"><%=dto.getRateOfInterest() %><h>%</h></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Offers ::</b><label id="rate"><%=dto.getPolicyOffer() %></label></td>
		</tr>
	</table>
	<button type="submit"  class="btn btn-primary" id="edit policy" style="width: 10%;margin-left: 70%" data-toggle="modal" data-target="#myModal<%=i%>">Edit Policy</button>
	<a type="submit" href="<%=application.getContextPath() %>/deleteHomeInsuranceController?id=<%=dto.getHomeInsuranceId() %>" class="btn btn-danger" id="delete policy" style="width: 10%;margin-left: 1%">Delete Policy</a>
  </div>
</div>
<br><br><br>
</div>
	
	
	
	<!-- --------------------------------------Modal1------------------------------------- -->
<!-- Modal -->
  <div class="modal fade" id="myModal<%=i %>" role="dialog" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <form action="<%=application.getContextPath() %>/edithomeinsurancecontroller" method="post" id="edithomepolicy" data-toggle="validator" role="form" >
      <div class="modal-content" style="border:3px solid dodgerblue; background-color:#ffdab3; border-radius:20px;" >
      
        <div class="panel-heading" align="center" style="background-color:#8080ff;border-radius:20px;">
          <button style="color:black" type="button" class="close" data-dismiss="modal">&times;</button>
          <h1 class="modal-title" ><b style="color:white">Edit Policy</b></h1>
        </div>
        <div class="panel-body">
        <div class="form-group">
           <label class="control-label" for="inputName" >Policy Name:</label>
           <input type="text" class="form-control" id="policyname" readonly value="<%=dto.getPolicyName() %>" placeholder="Enter policy name" 
           name="policyname" required>
          
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Policy Number:</label>
           <input type="text" class="form-control" id="policynumber" readonly value="<%=dto.getPolicyNumber() %>" placeholder="Enter policy number" name="policynumber" required>
          
        </div>
       
        <div class="form-group">
           <label class="control-label" for="inputName">Policy Coverage:</label>
           <input type="text" class="form-control" id="policycoverage" value="<%=dto.getCoverage() %>" placeholder="Enter policy coverage" name="policycoverage" required>
          
        </div>
         <div class="form-group">
           <label class="control-label" for="inputName">Policy Description:</label>
           <textarea class="form-control" id="policydescription" placeholder="Enter policy descrption" name="policydescription" required><%=dto.getDescription() %></textarea>
            
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Duration :</label>
           <input type="number" class="form-control" id="policyduration"value="<%=dto.getDuration() %>" placeholder="Enter policy duration in month" name="policyduration" required>
                   
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Initial Amount :</label>
           <input type="number" class="form-control" id="initialamount" value="<%=dto.getInitialAmount() %>" min="2000" max="6000" placeholder="Enter policy initial amount" name="initialamount"  required>
          
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Total Amount :</label>
           <input type="number" class="form-control" id="totalamount" value="<%=dto.getTotalamount() %>" placeholder="Enter policy total amount" name="totalamount"  required>
                 
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Rate of interest:</label>
           <input type="text" class="form-control" id="policyrate" value="<%=dto.getRateOfInterest() %>" placeholder="Enter policy rate of interate" name="policyrate"  required>
          
        </div>
       <div class="form-group">
          <label class="control-label" for="inputName">Offer:</label>
          <input type="text" class="form-control" id="policyoffer" value="<%=dto.getPolicyOffer() %>" placeholder="Enter policy offer" name="policyoffer">
          
        </div>
        <input type="hidden" name="homeid" value="<%=dto.getHomeInsuranceId()%>">
        <div class="modal-footer">
          <input type="submit" class="btn btn-success" >
        </div>
        </div>
      </div>
      </form>
    </div>
    
  </div>
  
 
    <!-- ----------------------------------/Modal1--------------------------------------- -->
     <%
	   i++;
	   } 
	  %>
		
   <!-- --------------------------------------Modal2------------------------------------- -->
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <form action="<%=application.getContextPath() %>/addhomeinsurancecontroller" method="post" id="addhomepolicy" data-toggle="validator" role="form" >
      <div class="modal-content" style="border:3px solid dodgerblue; background-color:#ffdab3; border-radius:20px;" >
      
        <div class="panel-heading" align="center" style="background-color:#8080ff;border-radius:20px;">
          <button style="color:black" type="button" class="close" data-dismiss="modal">&times;</button>
          <h1 class="modal-title" ><b style="color:white">Add Policy</b></h1>
        </div>
        <div class="panel-body">
        <div class="form-group">
           <label class="control-label" for="inputName" >Policy Name:</label>
           <input type="text" class="form-control" id="policyname" placeholder="Enter policy name" 
           name="policyname" required>
          
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Policy Number:</label>
           <input type="text" class="form-control" id="policynumber" placeholder="Enter policy number" name="policynumber" required>
          
        </div>
       
        <div class="form-group">
           <label class="control-label" for="inputName">Policy Coverage:</label>
           <input type="text" class="form-control" id="policycoverage" placeholder="Enter policy coverage" name="policycoverage" required>
          
        </div>
         <div class="form-group">
           <label class="control-label" for="inputName">Policy Description:</label>
           <textarea class="form-control" id="policydescription" placeholder="Enter policy descrption" name="policydescription" required></textarea>
            
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Duration :</label>
           <input type="number" class="form-control" id="policyduration"  placeholder="Enter policy duration in month" name="policyduration" required>
                   
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Initial Amount :</label>
           <input type="number" class="form-control" id="initialamount" min="2000" max="6000" placeholder="Enter policy initial amount" name="initialamount"  required>
          
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Total Amount :</label>
           <input type="number" class="form-control" id="totalamount" placeholder="Enter policy total amount" name="totalamount"  required>
                 
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Rate of interest:</label>
           <input type="text" class="form-control" id="policyrate" placeholder="Enter policy rate of interate" name="policyrate"  required>
          
        </div>
       <div class="form-group">
          <label class="control-label" for="inputName">Offer:</label>
          <input type="text" class="form-control" id="policyoffer" placeholder="Enter policy offer" name="policyoffer">
          
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-success" >
        </div>
        </div>
      </div>
      </form>
    </div>
    
  </div>
  

    <!-- ----------------------------------/Modal2--------------------------------------- -->
   
   <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>