
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false" import="java.util.*,com.nacre.ims.dto.GetLifeInsurenceDataDTO"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html5/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Life Insurance</title>
<!-- <meta charset="utf-8"> -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
  
  <script src="<%=application.getContextPath()%>/admin/js/addlifeinsurence.js"></script>
  <script src="<%=application.getContextPath()%>/admin/js/editlifeinsurence.js"></script>
  

<style type="text/css">
.btn {
	width: 49.2%;
}


</style>

</head>
<body>

	<jsp:include page="header.jsp"></jsp:include>
	<div class="container"> 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img style="-webkit-user-select: none;" src="<%=application.getContextPath()%>/images/LifeInsue.jpg" alt="Life" width="1250" height="190"> 
      </div>

      <div class="item">
        <img style="-webkit-user-select: none;" 
	src="<%=application.getContextPath()%>/images/Life.jpg" alt="life-insurance-banner" width="1250" height="190"> 
      </div>
    
      <div class="item">
         <img style="-webkit-user-select: none;" 
	src="<%=application.getContextPath()%>/images/life-insurance-banner.jpg" alt="LifeInsue" width="1250" height="190"> 
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
	
	
	 <%-- <img style="-webkit-user-select: none;cursor: zoom-in;" 
	src="<%=application.getContextPath()%>/LifeInsue.jpg" width="1250" height="190">  --%>
	

	
	<br>
	
	<button type="submit" class="btn btn-success"  style="width: 10%;margin-left: 80%" data-toggle="modal" data-target="#myModal">Add Policy</button>

	<br><br><br>
	
	<% Object obj=session.getAttribute("list");
		List<GetLifeInsurenceDataDTO> list=(List)obj;
		int i=1;
		for(GetLifeInsurenceDataDTO dto:list){
			
			
	
	%>

	<div class="container">
		<div class="row">
		<div class="col-sm-12" style="border: 5px solid DodgerBlue; border-radius: 20px; height: 550px" >
		 <img src="<%=application.getContextPath()%>/images/LifeSide.png" width="80" height="80" align="top"> 
		<table>
		<tr>
		<td style="color: blue;"><h3 id="policyname"><%=dto.getName() %></h3> </td>
		</tr>
		 <tr>
		<td style="color: blue;"><label id="poicyNumber"><%=dto.getInsurance_policy_no() %></label> </td>
		</tr> 
		<tr>
		<td><p><b style="color: blue;">Coverage:</b></p><label id="coverage"><%=dto.getCovarage() %></label></td>
		</tr>
		<tr>
		<td><p><b style="color: blue;">Description:</b></p><label id="description"><%=dto.getDesciption()%></label></td>
		</tr>
		<!-- <tr>
		<td><b>EntryAge:</b><lable id="age">18 years</lable></td>
		</tr> -->
		
		<tr>
		<td><b style="color: blue;">Duration:</b><label id="duration"><%=dto.getDuration()%><h>months</h></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Amount:</b><label id="amount"><%=(int)dto.getTotal_amount_paid() %></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Rate-of-interest:</b><label id="rate"><%=dto.getRate_of_interest() %><h>%</h></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Offers:</b><label id="rate"><%=dto.getLife_insurance_offers() %></label></td>
		</tr>
		<!-- <tr>
		<td><b style="color: blue;">Status:</b><label id="status"></label></td>
		</tr> -->
		
		</table>
		<button type="submit" class="btn btn-primary" id="login" style="width: 10%;margin-left:70%" data-toggle="modal" data-target="#myModal<%=i%>">Edit Policy</button>
 		<a type="submit" href="<%=application.getContextPath() %>/deletelifeinsurencecontroller?id=<%=dto.getLifeid()%>" class="btn btn-danger" id="login" style="width: 12%;margin-left: 1%">Delete Policy</a>
 		
				</div>
				
		</div>
		<br><br><br>
	</div>
	
	
	
	<!-- --------------------------------------Modal1------------------------------------- -->
<!-- Modal -->
  <div class="modal fade" id="myModal<%=i %>" role="dialog" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <form action="<%=application.getContextPath() %>/editlifeinsurencepolicycontroller" method="post" id="editlifepolicy" data-toggle="validator" role="form">
      <div class="modal-content" style="border:3px solid dodgerblue; background-color:#D3D3D3;  border-radius:20px;" >
      
        <div class="panel-heading"  align="center" style="background-color:#8080ff;border-radius:20px;">
          <button style="color:black" type="button" class="close" data-dismiss="modal">&times;</button>
          <h1 class="modal-title" ><b style="color:white">Edit Policy</b></h1>
        </div>
        <div class="panel-body">
          <div class="form-group">
           <label class="control-label" for="inputName">Policy Name:</label>
           <input type="text" class="form-control" readonly id="policyname" value="<%=dto.getName()%>" placeholder="Enter policy name" name="policyname" required>
        </div>
         <div class="form-group">
           <label class="control-label" for="inputName">Policy No:</label>
           <input type="text" class="form-control" readonly id="policyno" placeholder="Enter policy number" value="<%=dto.getInsurance_policy_no() %>" name="policyno" required>
       </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Policy Description:</label>
           <textarea class="form-control" id="policydescription" placeholder="Enter policy descrption"  name="policydescription" required><%=dto.getDesciption()%></textarea>
        </div>
        <div class="form-group">
      <label class="control-label" for="inputName">Policy Coverage:</label>
      <textarea class="form-control" id="policycoverage" placeholder="Enter policy coverage" name="policycoverage" required><%=dto.getCovarage()%></textarea>
       </div>
        <div class="form-group">
           <label class="control-label" for="inputName" >Duration :</label>
           <input type="number" class="form-control" id="policyduration" min="10" max="360" placeholder="Enter policy duration in months" value="<%=dto.getDuration()%>" name="policyduration" required>
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName">Rate of interest:</label>
           <input type="number" class="form-control" id="policyrate" placeholder="Enter policy rate of intereast" value="<%=dto.getRate_of_interest()%>" name="policyrate" required>
        </div>
        <div class="form-group">
           <label class="control-label" for="inputName" >Amount:</label>
           <input type="number" class="form-control" id="policyamount" placeholder="Enter policy  Amount" value="<%=dto.getTotal_amount_paid()%>" name="policyamount" required>
        </div>
       <div class="form-group">
          <label class="control-label" for="inputName" >Offer:</label>
          <input type="text" class="form-control" id="policyoffer" placeholder="Enter policy offer" value="<%=dto.getLife_insurance_offers()%>" name="policyoffer">
        </div>
        <input type="hidden" name="lifeId" value="<%= dto.getLifeid()%>">
        <div class="modal-footer">
          <input type="submit" class="btn btn-success" >
        </div>
       
        </div>
      </div>
      </form>
    </div>
    
  </div>
  

    <!-- ----------------------------------/Modal1--------------------------------------- -->
	
	<%
	i++;
		} %>
	
	
	<!-- --------------------------------------Modal------------------------------------- -->
<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <form action="<%=application.getContextPath() %>/addlifepolicycontroller" method="post" id="addlifepolicy" >
      <div class="modal-content" style="border:3px solid dodgerblue; background-color:#D3D3D3;  border-radius:20px;" >
      
        <div class="panel-heading"  align="center" style="background-color:#8080ff;border-radius:20px;">
          <button style="color:black" type="button" class="close" data-dismiss="modal">&times;</button>
          <h1 class="modal-title" ><b style="color:white">Add Policy</b></h1>
        </div>
        <div class="panel-body">
        <div class="form-group">
           <label >Policy Name:</label>
           <input type="text" class="form-control" id="policyname" placeholder="Enter policy name" name="policyname" required>
        </div>
         <div class="form-group">
           <label >Policy No:</label>
           <input type="text" class="form-control" id="policyno" placeholder="Enter policy Number" name="policyno" required>
       </div> 
        <div class="form-group">
           <label >Policy Description:</label>
           <textarea class="form-control" id="policydescription" placeholder="Enter policy descrption" name="policydescription" required></textarea>
        </div>
        <div class="form-group">
      <label >Policy Coverage:</label>
      <textarea class="form-control" id="policycoverage" placeholder="Enter policy coverage" name="policycoverage" required></textarea>
       </div>
        <div class="form-group">
           <label >Duration :</label>
           <input type="number" class="form-control" id="policyduration" min="10" max="30" placeholder="Enter policy duration" name="policyduration" required>
        </div>
        <div class="form-group">
           <label >Rate of interest:</label>
           <input type="number" class="form-control" id="policyrate" placeholder="Enter policy rate of interate" name="policyrate"required>
        </div>
        <div class="form-group">
           <label >Amount:</label>
           <input type="number" class="form-control" id="policyamount" placeholder="Enter policy Amount" name="policyamount" required>
        </div>
       <div class="form-group">
          <label >Offer:</label>
          <input type="text" class="form-control" id="policyoffer" placeholder="Enter policy offer" name="policyoffer">
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-success" >
        </div>
        </div>
      </div>
      </form>
      
    </div>
    
    
  </div>
  
  

    <!-- ----------------------------------/Modal--------------------------------------- -->
   
	<jsp:include page="footer.jsp"></jsp:include>
   

	</body>
</html>