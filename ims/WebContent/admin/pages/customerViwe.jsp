<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"
	import="com.nacre.ims.bo.ViweallCustomerdetailsBO,java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
#body1 {
	background-color: #f2f2f2;
	background-image: #;
	margin-top: 40px;
}
 
#container1 {
	height: 400px;
	width: 1600px;
	margin-left: 70px;
	border: 2px solid #1a000d;
	background-color:#ccccff;
	margin-top: 10px;
	
}

#foot{
position: absolute;
}

#jumbo {
	width: 1800px;
	margin-left: 0px;
	height: 50px;
}

#h1 {
	font-size: 30px;
	text-align: center;
	margin-top: -25px;
	margin-right: 110px;
	color: #1a000d;
}

#td1 {
	color: black;
	text-align: center;
	font-family:monospace;
    background-color: #ffcccc;
}

#td2 {
	color: Black;
	text-align: center;
	background-color: #d9d9d9;
}

#td3 {
	color: Black;
	text-align: center;
	background-color: white;
}

#table1 {
	margin-right: 350px;

 

}
#login{
 witdh:15px;
 height:35px;

}
#myInput{
 margin-left:5.5%;

}
/* #formid{

background-image: url("admin/pages/bck.jpg");


} */
/*#h2{
  width:30px;
  height:60px;
  font-size: 30px%;
  margin-left:200%;
  background-color: yellow;
*/

}

</style>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
</head>
<body id="body1" style="overflow:hidden">

<jsp:include page="header.jsp"></jsp:include>


<div class="container-fluid">
<div class="row">


<h1 id="hh1" align="center">View Customer</h1>
	
	
	<form action="viewAllCustomer" method="post" id="formid">
		
		<input type="text" id="myInput" align="right" placeholder="Search for Customer...." title="Type in a name">
	
		<div id="container1" style="width:1200px">
       

			<table class="table table-hover" id="table1">
				<thead>
				
				
					<tr id="tr1">
						<th id="td1">FirstName</th>
						<th id="td1">LastName</th>
						<th id="td1">EmailId</th>

						<th id="td1">MobileNo</th>
						<th id="td1">Gender</th>
						<th id="td1">DOB</th>
						<th id="td1">Address</th>

						<th id="td1">Pin</th>
						<th id="td1">City</th>
						<th id="td1">State</th>
						<th id="td1">Country</th>
						<th id="td1"></th>
						<th id="td1"></th>
				
					
						
					</tr>
				</thead>
				<%
				Object obj=request.getAttribute("customerList");
				List<ViweallCustomerdetailsBO> list=(List)obj;
				//out.println(list);
				for(ViweallCustomerdetailsBO bo:list){
				
				%>
				
				<tbody id="myTable">
					<tr id="tr2">
						<td id="td2"><%=bo.getCustomer_first_name()%></td>
						<td id="td2"><%=bo.getCustomer_last_name()%></td>
						<td id="td2"><%=bo.getCustomer_email_id()%></td>
						<td id="td2"><%=bo.getCustomer_mobile_no()%></td>
						<td id="td2"><%=bo.getCustomer_gender() %></td>
						<td id="td2"><%=bo.getCustomer_dob()%></td>

						<td id="td2"><%=bo.getLocal_address()%></td>
						<td id="td2"><%=bo.getPincode()%></td>
						<td id="td2"><%=bo.getCity_name() %></td>
						<td id="td2"><%=bo.getState_name() %></td>
						<td id="td2"><%=bo.getCountry_name()%></td>
						<td id="td2"></td>
						

						<td><a type="submit"
							href="<%=application.getContextPath()%>/removeCustomersController?id=<%=bo.getCostomer_register_id() %>"
							class="btn btn-danger" id="login"  style="width: 100%; margin-left: 2%">Remove Customer</a></td>
						
					</tr>
				</tbody>
				<%} %>
			</table>
			
			
		
		</div>
		
	</form>
	<jsp:include page="footer.jsp" ></jsp:include>
	</div>
	</div>
</body>
</html>





