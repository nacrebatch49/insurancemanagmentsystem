
<%@page import="java.util.List"%>
<%@page import="com.nacre.ims.dto.AgentApprovalDTO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style>
* {
	box-sizing: border-box;
}  

#myInput {
	background-image: url('/css/searchicon.png');
	background-position: 10px 10px;
	background-repeat: no-repeat;
	width: 100%;
	font-size: 16px;
	padding: 12px 20px 12px 40px;
	border: 1px solid #ddd;
	margin-bottom: 12px;
}

#myTable {
	border-collapse: collapse;
	width: 100%;
	border: 1px solid #ddd;
	font-size: 18px;
}

#myTable th, #myTable td {
	text-align: left;
	padding: 12px;
}

#myTable tr {
	border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
	background-color: #f1f1f1;
}
</style>

</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<form action="<%=application.getContextPath()%>/agentApprovalOrRejectionUrl"
		method="post">
		<div class="container">
			<h1>ViewPendingAgents</h1>

			<!-- <input type="text" id="myInput" onkeyup="myFunction()"
			placeholder="Search for names.." title="Type in a name"> -->

			<!--  <h2>Client Table...</h2>
        <input type="text" id="myInput" onkeyup="myFunction()"
            placeholder="Search for client names.." title="Type in a name"> -->


			<!-- <div align="right">
				<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search.." title="Type in a name">
				<button type="button" class="btn">Search</button>
			</div> -->
			<div class="table-responsive">
				<table class="table" id="myTable">

					<tr>
						<th><button type="button" id='check_all'>Check All</button></th>
						<th>Agent Id</th>
						<th>Agent Name</th>
						<th>Agent Qualification</th>
						<th>Gender</th>
						<th>Mail Id</th>
						<th>Agent Location</th>
						<!-- <th></th> -->

					</tr>



					<%
						Object obj = request.getAttribute("ApprovalListDto");
						List<AgentApprovalDTO> ldata = (List<AgentApprovalDTO>) obj;
						System.out.println(ldata);
						if (ldata != null) {

							for (AgentApprovalDTO agent : ldata) {
					%>
					<tr>
						<td><input type="checkbox" name="agentId"
							value="<%=agent.getId()%>"></td>
						<td><%=agent.getId()%></td>
						<td><%=agent.getName()%></td>
						<td><%=agent.getQualification()%></td>
						<td><%=agent.getGender()%></td>
						<td><%=agent.getEmail()%></td>
						<td><%=agent.getLocation()%></td>
						<!-- <td></td> -->
					</tr>
					<%
						}
						}
					%>
				</table>
				<div class="row" align="center">
					<input type="submit" class="btn btn-info btn-lg" name="btn"
						data-toggle="modal" data-target="#myModal" value="Accept">
					<input type="submit" class="btn btn-info btn-lg" name="btn"
						data-toggle="modal" data-target="#myModal" value="Reject">
				</div>
			</div>
		</div>
		</div>
	</form>
</body> 
</html>

<script>
	$('#check_all').click(function() {
		$('input[name=vehicle]').prop('checked', false);
	});

	$('#check_all').click(function() {
		$('input[name=vehicle]').prop('checked', true);
	});
</script>
<!-- <script>
	function myFunction() {
		var input, filter, table, tr, td, i;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		table = document.getElementById("myTable");
		tr = table.getElementsByTagName("tr");
		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td")[1];
			if (td) {
				if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
			}
		}
	}
</script> -->
