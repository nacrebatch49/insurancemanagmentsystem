<!DOCTYPE html>
<html lang="en">
<head>
<title>Admin Home</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"> -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<!-- <script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script> -->

<script src="/ims/admin/js/ClaimList.js"></script>
<style>
.footer {
	position: fixed;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: gray;
	color: white;
	text-align: center;
}
</style>
</head>
<body>

	<!-- Links -->
	<nav class="navbar navbar-expand-md bg-dark navbar-dark">
		<a class="navbar-brand" href="#">Logo</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="collapsibleNavbar">
			<!-- Links -->
			<ul class="navbar-nav">
				
				<li class="nav-item"><a class="nav-link"
					href="../pages/AdminHome.jsp">Home</a></li>
				
				<!-- Dropdown -->
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbardrop"
					data-toggle="dropdown"> Policy </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="<%=application.getContextPath()%>/getlifeinsurencedatacontroller">Life Insurance</a> 
						<a class="dropdown-item" href="<%=application.getContextPath()%>/getCarInsuranceDataController">Car Insurance</a>
						<a class="dropdown-item" href="<%=application.getContextPath()%>/gethomeinsurancecontroller">Home Insurance</a>
						<a class="dropdown-item" href="<%=application.getContextPath()%>/getchildcareinsurencedatacontroller">Child Insurance</a>
					</div></li>
				
					
				<li class="nav-item"><a class="nav-link"
					href="<%=application.getContextPath()%>/get_Agent_DataController">View Agent</a></li>
					
				<li class="nav-item"><a class="nav-link"
					href="<%=application.getContextPath()%>/viewAllCustomer">View Client</a></li>
					
				<li class="nav-item"><a class="nav-link"
					href="<%=application.getContextPath()%>/getApprovalUrl">Agent Request <span
						class="badge badge-info" id="id_Agnotify">0</span>
				</a></li>
				
				<!-- Dropdown -->
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle" href="#" id="navbardrop"
					data-toggle="dropdown"> Reminder </a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="#">1 Day Delay</a> <a
							class="dropdown-item" href="#">Next payment</a> <a
							class="dropdown-item" href="#">Policy Reminder</a>
					</div></li>

				<li class="nav-item"><a class="nav-link"
					href="../pages/ClaimRequest.jsp">Claim Request <span
						class="badge badge-info" id="id_notify">0</span>
				</a></li>

				<li class="nav-item"><a class="nav-link"
					href="#">Change Password</a></li>
					
				<li class="nav-item"><a class="nav-link"
					href="#">Logout</a></li>
			</ul>
		</div>
	</nav>
	<br>

	
	<div class="footer">
		<div class="container">Copyright &copy; 2016 Nacre Software
			Services</div>
	</div>
</body>
</html>
