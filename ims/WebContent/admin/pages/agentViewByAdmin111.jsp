<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"
	import="com.nacre.ims.dto.Agent_DataDTO,java.util.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>

<script type="text/javascript" src="../js/viewcustomerdatatable.js"></script>
<style>
* {
	box-sizing: border-box;
}

.footer {
	left: 0;
	bottom: 0;
	width: 100%;
	text-align: center;
}
#foot{
position: absolute;
}

#myInput {
	background-image: url('/css/searchicon.png');
	background-position: 10px 10px;
	background-repeat: no-repeat;
	width: 100%;
	font-size: 16px;
	padding: 12px 20px 12px 40px;
	border: 1px solid #ddd;
	margin-bottom: 12px;
}

input[type=text] {
	display: block;
	text-align: left;
	size: 59px;
}

th, td {
	text-align: left;
	padding: 8px;
}

th {
	background-color: #4CAF50;
	color: white;
}

j1 {
	width: auto;
}

table {
	display: block !important;
	overflow-x: auto !important;
}

.modal-body {
	background-image: url('../../images/meetingimage2.jpg');
	background-repeat: no-repeat;
	background-size: cover;
	height: 300px;
}

.modal-footer {
	background-image: url('../../images/meetingfooter1.jpg');
	background-repeat: no-repeat;
	background-size: cover;
	height: 75px;
}
</style>

<script>
	$(document).ready(
			function() {
				//alert("hello");

				$("#fmeeting").click(function() {
							//alert("hello1");
							var a="";
							$("input[name='agentid']:checked").each(function() {
												a=a+"<input type='hidden' name='agentids' id='agentids' value='"+ $(this).val()+ "'>";
							//alert($(this).val());
											});
				$("#appendHiddenData").html(a)			
				//alert($("#appendHiddenData").text());
							
						});
			});
			
</script>

<title>Agent Table........</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>

	<form
		action="<%=application.getContextPath()%>/fixAgentMeetingController"
		id="registrationForm" method="post">
	<div id="appendHiddenData"></div>
		<div class="container">

			<div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<h3>MEETING SCHEDULE FOR AGENTS</h3>
							<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						</div>

						<div class="modal-body">
							<div class="row">
								<div class="form-group">
									<label class="col-md-3 control-label">Venue</label>
									<div class="col-md-8">
										<input type="text" class="form-control" name="venue"
											placeholder="Write Venue">
									</div>
								</div>
								<!-- <div class="form-group">
									<label class="col-md-3 control-label">Description</label>
									<div class="col-md-8">
										<textarea class="form-control" name="description" rows="3"
											col="40"></textarea>
									</div>
								</div> -->

								<div class="form-group">
									<label class="col-sm-3 control-label">Date</label>
									<div class="col-sm-8">
										<input type="date" class="form-control" name="date"
											placeholder="YYYY/MM/DD" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Time</label>
									<div class="col-sm-8">
										<input type="time" class="form-control" name="time"
											placeholder="00:00:00" />
									</div>
								</div>
							</div>
						</div>


						<div class="modal-footer">
							<div class="form-group">
								<div class="col-sm-9 col-sm-offset-3" align="center">
									
									<!-- Do NOT use name="submit" or id="submit" for the Submit button -->
									<button type="submit" class="btn btn-primary">Send</button>
									
									
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Close</button>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</form>	
			<h1>View Agents</h1>
			<input type="text" id="myInput" align="right"
				placeholder="Search for Agents...." title="Type in a name">
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th id="th"><h4>Select Agent</h4></th>
							<!-- <th id="th"><h4>Agent_Id</h4></th> -->
							<th id="th"><h4>Agent Name</h4></th>
							<th id="th"><h4>Qualification</h4></th>
							<th id="th"><h4>Gender</h4></th>
							<th id="th"><h4>Email</h4></th>
							<th id="th"><h4>Mobile No</h4></th>
							<th id="th"><h4>Aadhar card</h4></th>
							<!-- <th id="th"><h4>Pancard</h4></th> -->
							<th id="th"><h4>Joining</h4></th>
							<th id="th"><h4>Location</h4></th>
							<th id="th"><h4>Address</h4></th>
							<th id="th"><h4>Customers</h4></th>
							<th id="th"><h4>Delete</h4></th>

						</tr>
					</thead>
					<tbody  id="myTable">
						<%
							Object obj = session.getAttribute("agentData");
							List<Agent_DataDTO> list = (List<Agent_DataDTO>) obj;

							if (list.isEmpty() != true) {
								for (Agent_DataDTO dto : list) {
						%>

						<tr class="warning">
							<td><input type="checkbox" id="agentid" name="agentid"
								value="<%=dto.getAgent_Id()%>"></td>
							<%-- <td><%=dto.getAgent_Id()%></td> --%>
							<td><%=dto.getAgent_name()%></td>
							<td><%=dto.getAgent_qualification()%></td>
							<td><%=dto.getGender()%></td>
							<td><%=dto.getAgent_emai_id()%></td>
							<td><%=dto.getMobile_Number()%></td>
							<td><%=dto.getAdhaar_Card_No()%></td>
							<%-- <td><%=dto.getPancard_No()%></td> --%>
							<td><%=dto.getDate_Of_Joining()%></td>
							<td><%=dto.getLocation()%></td>
							<%
								String add = dto.getLocal_address() + ", " + dto.getCity_name() + ", " + dto.getState_name() + ", "
												+ dto.getCountry_name() + ", " + dto.getPincode();
							%>
							<td><%=add%></td>
							<td><%=dto.getCount_No_Of_Customer()%></td>

							<td><a type="submit"
								href="<%=application.getContextPath()%>/deleteAgentController?id=<%=dto.getAgent_Id()%>"
								class="btn btn-danger" id="login"
								style="width: 100%; margin-left: 1%">Delete Agent</a></td>
						</tr>
						<%
							} //for loop
							} //if condition
							else {
								System.out.println("hay i am here... only......");
						%>

						<h4 class="text-danger" style="font-style: italic;" align="center">Agent
							Not Found in DataBase.......</h4>
						<%
							}
						%>
					</tbody>
				</table>

				<div  align="center">
					<button type="button" class="btn btn-info btn-lg" id="fmeeting"
						data-toggle="modal" data-target="#myModal">Fix Meeting
						Schedule</button>
				</div>

				<script>
					$('#check_all').click(function() {
						$('input[name=vehicle]').prop('checked', false);
					});

					$('#check_all').click(function() {
						$('input[name=vehicle]').prop('checked', true);
					});
				</script>
				<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

			</div>
		</div>
	
<br><br><br>


	<jsp:include page="footer.jsp"></jsp:include>

</body>
</html>