$(document).ready(function() {
   // alert("hi..");
    $('#registrationForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          
          
        	date: {
                validators: {
                    notEmpty: {
                        message: 'The Meeting date is required'
                    },
                    date: {
                        format: 'YYYY/MM/DD',
                        message: 'Thes Meeting date is not valid'
                    }
                }
            },
            
            venue: {
                validators: {
                    notEmpty: {
                        message: 'The Venue is required and cannot be empty'
                    },
                    stringLength: {
                        max: 200,
                        message: 'The Venue must be less than 200 characters long'
                    }
                }
            }//venue
           
           
        }
       
    });
});

