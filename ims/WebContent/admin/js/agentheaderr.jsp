<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>Navbar Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <style>
      body {
        padding-top: 20px;
        padding-bottom: 20px;
      }
      .navbar {
        margin-bottom: 20px;
      }
    </style>
    
    <!-- Styles for avoiding jQuery -->
    <link rel="stylesheet" href="/ims/admin/css/nav.css">
    <link rel="stylesheet" href="/ims/admin/js/nav.js">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  <div class="header">
  <h1 style="text-align: center;"><a href="#default" class="logo">Insurance Policy Management</a>     </h1>
  <!-- <ul class="nav navbar-top-links navbar-right">
  <li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#"></a></li>
  </ul> -->
</div>

    <div >

      <!-- Static navbar -->
      <nav class="navbar navbar-default" >
        <div class="container-fluid" style="background-color: black;width: 100%" >
          <div class="navbar-header" >
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
           
          </div>
          <div id="navbar" class="navbar-collapse collapse">
          
            <ul class="nav navbar-nav">
            
            <li>  <a  href="#">AgentHome</a></li>
             <li>  <a  href="#">Profile</a></li>
              
              <li class="dropdown">
                <div href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Policy<span class="caret"></span></div>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="<%=application.getContextPath()%>/getlifeinsurencedatacontroller">Life Insurance</a></li>
                  <li><a href="#">Car Insurance</a></li>
                  <li><a href="#">Home Insurance</a></li>
                   <li><a href="<%=application.getContextPath()%>/getchildcareinsurencedatacontroller">Child Care Insurance</a></li>
                </ul>
              </li>
                <li><a  href="#">MyClient</a></li>
              <li> <a  href="#">Add Client</a></li>
              <li> <a  href="#">Update Meeting</a></li>
           
                <li><a  href="#">Feedback</a>
         </li>
         <li><a  href="#">Notification</a>
         </li>
          <li><a  href="#">Change Password</a>
         </li>
         
         <li><a   href="#">Logout</a></li>
              </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main component for a primary marketing message or call to action -->
    <!-- Navbar JavaScript -->
    <script src="navbar.js"></script>
  </body>
</html>
</body>
</html>