$(document).ready(function() {
    $('#editlifepolicy').bootstrapValidator({
        
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	policyname: {
                validators: {
                    notEmpty: {
                        message: 'The full name is required and cannot be empty'
                    }
                }
            },
           /* policyno: {
                validators: {
                    notEmpty: {
                        message: 'The policy number is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The email address is not valid'
                    }
                }
            },*/
            policycoverage: {
                validators: {
                    notEmpty: {
                        message: 'The policy coverage is required and cannot be empty'
                    }/*,
                    emailAddress: {
                        message: 'The email address is not valid'
                    }*/
                }
            },
            policydescription: {
                validators: {
                    notEmpty: {
                        message: 'The policy description is required and cannot be empty'
                    }/*,
                    stringLength: {
                        max: 100,
                        message: 'The title must be less than 100 characters long'
                    }*/
                }
            },
            policyduration: {
                validators: {
                    notEmpty: {
                        message: 'The policy duration is required and cannot be empty'
                    }/*,
                    stringLength: {
                        max: 500,
                        message: 'The content must be less than 500 characters long'
                    }*/
                }
            },
            policyrate: {
                validators: {
                    notEmpty: {
                        message: 'The policy rate of interest is required and cannot be empty'
                    }/*,
                    stringLength: {
                        max: 500,
                        message: 'The content must be less than 500 characters long'
                    }*/
                }
            },
            policyamount: {
                validators: {
                    notEmpty: {
                        message: 'The policy rate of interest is required and cannot be empty'
                    }/*,
                    stringLength: {
                        max: 500,
                        message: 'The content must be less than 500 characters long'
                    }*/
                }
            }
           /* ,
            policyoffer: {
                validators: {
                    notEmpty: {
                        message: 'The  is required and cannot be empty'
                    },
                    stringLength: {
                        max: 500,
                        message: 'The content must be less than 500 characters long'
                    }
                }
            }*/
        }
    });
});