
$(document).ready(function() {

    $('#addcarpolicy').bootstrapValidator({
      
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	policyname: {
                validators: {
                    notEmpty: {
                        message: 'The full name is required and cannot be empty'
                    }
                }
            },
           
            policydescription: {
                validators: {
                    notEmpty: {
                        message: 'The policy description is required and cannot be empty'
                    }
                    
                }
            },
            policycoverage: {
                validators: {
                    notEmpty: {
                        message: 'The policy coverage is required and cannot be empty'
                    }
                }
            },
            policyduration: {
                validators: {
                    notEmpty: {
                        message: 'The policy duration is required and cannot be empty'
                    }
                }
            },
            initialamount: {
                validators: {
                    notEmpty: {
                        message: 'The policy initial amount is required and cannot be empty'
                    },
                stringvalue: {
                min:2000,
                max: 10000,
                message: 'The policy initial amount is must be geter than Rs.2000 and less than Rs.10000 '
                }
                }
            },
          
            paybleamount: {
                validators: {
                    notEmpty: {
                        message: 'The policy total amount is required and cannot be empty'
                    }
                }
            },
            policyrate: {
                validators: {
                    notEmpty: {
                        message: 'The policy rate of interest is required and cannot be empty'
                    }
                }
            }
          
        }
    });
});