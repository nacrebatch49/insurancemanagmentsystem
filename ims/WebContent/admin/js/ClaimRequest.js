$(document).ready(function() {
	$("#detail").hide();
	
	var data = "<table class='table table-bordered' id='tab'><thead class='thead-dark'><tr><th>Name</th>"+
				"<th>Surname</th><th>Policy No</th><th>Nominy</th><th>Email</th><th>Relation</th><th></th></tr></thead>";
	$.ajax({
		type : "Get",
		url : "../../claimReq",
		dataType : "json",
		success : function(response) {
			
			$.each(response,function(k,v){
				
				data= data+"<tr><td>"+v.fName+"</td><td>"+v.lName+"</td><td>"+v.policyNo+"</td><td>"+v.nominiName+"</td><td>"+v.nominiEmail+"</td><td>"+v.relation+"</td><td><input type='button' class='appv btn btn-primary' value='Approve' onClick='return app("+v.policyNo+",\""+v.nominiName+"\",\""+v.nominiEmail+"\",\""+v.relation+"\",\""+v.fName+"\",\""+v.lName+"\");'></td></tr>";
				
				
			});
			$("#tab").html(data);

		},
		error : function(e) {
			alert(e)
		}
	});

});


 function app(no,nName,nEmail,relation,fName,lName){
	 
	 $.ajax({
			type : "Get",
			url : "../../claimApp",
			dataType : "json",
			data :{ "no":no,"nName":nName,"nEmail":nEmail,"relation":relation},
			
			success : function(response) {
				
				$("#detail").show();
				
				$("#pHold").val(fName+" "+lName);
				$("#pNo").val(no);
				$("#pName").val(response.pName);
				$("#emi").val(response.totalAmount);
				$("#roi").val(response.ROI);
				$("#gst").val(response.GST);
				$("#pAmnt").val(response.payAmount);
				$("#nName").val(nName);
				$("#nEmail").val(nEmail);
				$("#rel").val(relation);
				$("#stat").val(response.statusId);
				
				
			},
			error : function(e) {
				alert(e)
			}
		});

 }