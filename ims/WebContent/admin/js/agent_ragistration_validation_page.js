/*$(document).ready(function() {
    alert("hi..");
    $('#registrationForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                message: 'The agentname is not valid',
                validators: {
                    notEmpty: {
                        message: 'The agentname is required and cannot be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The agentname must be more than 6 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The agentname can only consist of alphabetical and number'
                    },
                    different: {
                        field: 'password',
                        message: 'The agentname and password cannot be the same as each other'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                }
            },
           
            birthday: {
                validators: {
                    notEmpty: {
                        message: 'The date of birth is required'
                    },
                    date: {
                        format: 'YYYY/MM/DD',
                        message: 'Thes date of birth is not valid'
                    }
                }
            },
            
            gender: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            birth: {
                validators: {
                    notEmpty: {
                        message: 'The date of joining is required'
                    },
                    date: {
                        format: 'YYYY/MM/DD',
                        message: 'Thes date of joining is not valid'
                    }
                }
            },
            content: {
                validators: {
                    notEmpty: {
                        message: 'The address is required and cannot be empty'
                    },
                    stringLength: {
                        max: 500,
                        message: 'The address must be less than 500 characters long'
                    }
                }
            },
            
            
        }
        
    });
});*/