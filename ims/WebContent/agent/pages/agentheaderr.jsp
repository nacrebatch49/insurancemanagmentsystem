<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.navbar1 {
    overflow: hidden;
    background-color: #333;
    font-family: Arial, Helvetica, sans-serif;
}

.navbar1 a {
    float: left;
    font-size: 16px;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}

.dropdown1 {
    float: left;
    overflow: hidden;
}

.dropdown1 .dropbtn {
    font-size: 16px;    
    border: none;
    outline: none;
    color: white;
    padding: 14px 16px;
    background-color: inherit;
    font-family: inherit;
    margin: 0;
}

.navbar1 a:hover, .dropdown1:hover .dropbtn {
    background-color: red;
}

.dropdown-content1 {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content1 a {
    float: none;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content1 a:hover {
    background-color: #ddd;
}

.dropdown1:hover .dropdown-content1 {
    display: block;
}
</style>
</head>
<body>
<div id="jumbo">
  <h1 style="text-align: center;" id="h1">Insurance Policy Management</h1>
</div>
<div class="navbar1">
<a href="home.jsp">Agent Home</a>
  <div class="dropdown1">
    <button class="dropbtn">policy <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content1">
         <a href="<%=application.getContextPath()%>/viewagentlifeinsurencedatacontroller">Life Insurance</a>
         <a href="<%=application.getContextPath()%>/getCarInsuranceDataControllerByAgent">Car Insurance</a>
         <a href="<%=application.getContextPath()%>/viewHomeInsuranceByAgentController">Home Insurance</a>
         <a href="<%=application.getContextPath()%>/viewagent_childinsurencedatacontroller">Child Care Insurance</a>
      </div>
  </div>
    <a href="<%=application.getContextPath()%>/MyCustomerDetailsByAgentControllerurl">View Client</a> 
   <a  href="<%=application.getContextPath()%>/commons/pages/change.jsp">Change Password</a>
   <a   href="<%=application.getContextPath() %>/logout">Logout</a>
</div>
</body>
</html>