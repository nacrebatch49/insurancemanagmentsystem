<%@page import="com.nacre.ims.bean.AgentViewProfileBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AgentRegister
</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<!-- 
<script type="text/javascript" src="../js/agentUpdateProfile.js"></script> -->



<link rel="stylesheet" href="../css/agentEditProfile.css"
	type="text/css">
<script type="text/javascript"
	src="<%=application.getContextPath()%>/commons/js/GetData.js"></script>
	
	<script type="text/javascript"
	src="<%=application.getContextPath()%>/agent/js/checkstatus.js"></script>
	
	
</head>


<body>
	<div class="contanier">
		<h1 style="color: red">Agent Registration</h1>


		<div class="row">
			<div class="col-lg-2"></div>



			<div class="col-lg-8">

				<form id="agentEditForm"
					action="<%=application.getContextPath()%>/AgentRegisterController">
					<table class="background">
						<tr>
							<td><b>Name</b></td>
							<td><input type="text" disabled="disabled" class="form-control" id="name"
								name="name" ><span
								id="fn"></span></td>
							<td><b>Mobile No</b></td>
							<td><input type="text" class="form-control" id="mobileno"
								name="mobileno" ><span
								id="mobno"></span></td>

						</tr>

						<tr>
							<td><b>Email Id</b></td>
							<td><input type="text" disabled="disabled" id="email"
								class="form-control" ></td>
							<td><b>Gender</b></td>
							<td><input type="radio" name="gender" value="male">male
							<input type="radio" name="gender" value="female">female
							

						</tr>

						<tr>
							<td><b>DOB</b></td>
							<td><input type="date" class="form-control" name="dob"
								></td>
							<td><b>DOJ</b></td>
							<td><input type="date" class="form-control" name="doj"
								></td>

						</tr>

						<tr>
							<td><b>Adhar No</b></td>
							<td><input type="text" class="form-control" name="adhar"
								></td>

							<td><b>Pan No</b></td>
							<td><input type="text" class="form-control" name="pan"
								></td>
						</tr>
						<tr>
							<td><b style="color: orange">Educational Details:<b></td>
						</tr>
						<tr>
							<td><b>Qualification</b></td>
							<td><input type="text" class="form-control" id="qualification"
								name="qualification" disabled="disabled"><span
								id="qual"></span></td>

							<td><b>YOP</b></td>
							<td><input type="text" class="form-control" id="yop"
								name="yop" ><span
								id="year"></span></td>
						</tr>
						<tr>
							<td><b>Percentage(%)</b></td>
							<td><input type="text" class="form-control" id="percentage"
								name="percentage1" ><span
								id="per"></span></td>

						</tr>
						
						<tr>
							<td><b style="color: orange">Address Details:</b></td>
						</tr>
						<tr>
							<td><b>Country</b></td>
							<td><select id="country" class="form-control" name="country">
									
							</select><span id="country"></span></td>

							<td><b>State</b></td>
							<td><select id="state" class="form-control" name="state">
									
							</select><span id="state"></span></td>
						</tr>
						<tr>
							<td><b>city</b></td>
							<td><select id="city" class="form-control" name="city">
									
							</select><span id="city"></span></td>

							<td><b>Address</b></td>
							<td><input type="text" class="form-control" name="address"
								id="address" ><span
								id="add"></span></td>
						</tr>
						<tr>
							<td><b>pincode</b></td>
							<td><input type="text" class="form-control" name="pincode"
								id="pincode" ><span
								id="pin"></span></td>
						</tr>
						<tr>
						<td><input type="hidden" value="<%=request.getParameter("approveid")%>" name="approveid"/></td>
						</tr>

						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td align="right"><input type="submit" value="register"
								class="button" /></td>
						</tr>

					</table>



				</form>
			</div>
			<div class="col-lg-2"></div>

		</div>

	</div>
	
</body>
</html>