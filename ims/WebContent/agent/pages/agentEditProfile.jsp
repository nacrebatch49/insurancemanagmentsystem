<%@page import="com.nacre.ims.bean.AgentViewProfileBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AgentEditProfile</title>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript" src="../js/agentUpdateProfile.js"></script>





<link rel="stylesheet" href="../css/agentEditProfile.css"
	type="text/css">
<script type="text/javascript"
	src="<%=application.getContextPath()%>/commons/js/GetData.js"></script>
</head>
<%
	Object obj = session.getAttribute("viewinfo");
	AgentViewProfileBean bean = (AgentViewProfileBean) obj;
%>
<body>
<jsp:include page="agentheaderr.jsp"></jsp:include>
	<div class="contanier">
		<h1 style="color: red">Update Profile</h1>


		<div class="row">
			<div class="col-lg-2"></div>



			<div class="col-lg-8">

				<form id="agentEditForm"
					action="<%=application.getContextPath()%>/agentEditController">
					<table class="background">
						<tr>
							<td><b>Name</b></td>
							<td><input type="text" class="form-control" id="name"
								name="name" value="<%=bean.getAgent_name()%>"><span
								id="fn"></span></td>
							<td><b>Mobile No</b></td>
							<td><input type="text" class="form-control" id="mobileno"
								name="mobileno" value="<%=bean.getMobile_number()%>"><span
								id="mobno"></span></td>

						</tr>

						<tr>
							<td><b>Email Id</b></td>
							<td><input type="text" disabled="disabled"
								class="form-control" value="<%=bean.getAgent_email_id()%>"></td>
							<td><b>Gender</b></td>
							<td><input type="text" class="form-control"
								disabled="disabled" value="<%=bean.getGender()%>"></td>

						</tr>

						<tr>
							<td><b>DOB</b></td>
							<td><input type="date" class="form-control"
								disabled="disabled" value="<%=bean.getDob()%>"></td>
							<td><b>DOJ</b></td>
							<td><input type="date" class="form-control"
								disabled="disabled" value="<%=bean.getDate_of_joining()%>"></td>

						</tr>

						<tr>
							<td><b>Adhar No</b></td>
							<td><input type="text" class="form-control"
								disabled="disabled" value="<%=bean.getAdhaar_card_no()%>"></td>

							<td><b>Pan No</b></td>
							<td><input type="text" class="form-control"
								disabled="disabled" value="<%=bean.getPancard_no()%>"></td>
						</tr>
						<tr>
							<td><b style="color: orange">Educational Details:<b></td>
						</tr>
						<tr>
							<td><b>Qualification</b></td>
							<td><input type="text" class="form-control" id="btech"
								name="qualification" value="<%=bean.getQualification()%>"><span
								id="qual"></span></td>

							<td><b>YOP</b></td>
							<td><input type="text" class="form-control" id="yop"
								name="yop" value="<%=bean.getYear_of_passout()%>"><span
								id="year"></span></td>
						</tr>
						<tr>
							<td><b>Percentage(%)</b></td>
							<td><input type="text" class="form-control" id="percentage"
								name="percentage" value="<%=bean.getPercentage()%>"><span
								id="per"></span></td>

						</tr>
						<tr>
							<td><b style="color: orange">Address Details:</b></td>
						</tr>
						<tr>
							<td><b>Country</b></td>
							<td><select id="country" class="form-control" name="country">
									
							</select><span id="country"></span></td>

							<td><b>State</b></td>
							<td><select id="state" class="form-control" name="state">
									
							</select><span id="state"></span></td>
						</tr>
						<tr>
							<td><b>city</b></td>
							<td><select id="city" class="form-control" name="city">
									
							</select><span id="city"></span></td>

							<td><b>Address</b></td>
							<td><input type="text" class="form-control" name="address"
								id="address" value="<%=bean.getLocal_address()%>"><span
								id="add"></span></td>
						</tr>
						<tr>
							<td><b>pincode</b></td>
							<td><input type="text" class="form-control" name="pincode"
								id="pincode" value="<%=bean.getPincode()%>"><span
								id="pin"></span></td>
						</tr>

						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td align="right"><input type="submit" value="Update"
								class="button" /></td>
						</tr>

					</table>



				</form>
			</div>
			<div class="col-lg-2"></div>

		</div>

	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>