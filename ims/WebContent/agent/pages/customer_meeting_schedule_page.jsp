<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1">
<title>Meeting Schedule</title>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../css/customermeeting.css">
<script type="text/javascript"src="../js/custoner_meeting_schedule_validetion_page.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2" style="border: 3px solid cyan; border-radius:20px;">
				<div class="page-header">
					<h2>Meeting Schedule Page</h2>
				</div>
				<form action="<%=application.getContextPath()%>/agent_Meeting_Schedule_To_Customer" id="registrationForm"  method="post" class="form-horizontal">
					<div class="form-group">
						<label class="col-md-3 control-label">Venue</label>
						<div class="col-md-6">
							<textarea class="form-control"name="venue" rows="5"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Description</label>
						<div class="col-md-6">
							<textarea class="form-control" name="description" rows="5"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Date</label>
						<div class="col-sm-5">
							<input type="date" class="form-control" name="date" placeholder="YYYY/MM/DD" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Time</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="time" placeholder="12:30:01" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3" align="center">
							<!-- Do NOT use name="submit" or id="submit" for the Submit button -->
							<button type="submit" class="btn btn-primary" >Send</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>