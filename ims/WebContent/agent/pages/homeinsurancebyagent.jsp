<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ page import="java.util.*,com.nacre.ims.dto.GetHomeInsuranceDTO" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>login page</title>
<meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
    
<style type="text/css">
.btn {
	width: 49.2%;
}

</style>

</head>
<body>
     <jsp:include page="agentheaderr.jsp"></jsp:include>
	<div class="container"> 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img style="-webkit-user-select: none;" 
	src="<%=application.getContextPath()%>/images/home_banner.jpg" width="1150" > 
      </div>

      <div class="item">
        <img style="-webkit-user-select: none;" 
	src="<%=application.getContextPath()%>/images/Home-Insurance-Banner.jpg" width="1250" > 
      </div>
    
      <div class="item">
         <img style="-webkit-user-select: none;" 
	src="<%=application.getContextPath()%>/images/life-insurance-banner2.jpg" width="1250" height="270px"> 
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>


	<br><br><br>
	<% Object obj=session.getAttribute("data");
		List<GetHomeInsuranceDTO> list=(List)obj;
		for(GetHomeInsuranceDTO dto:list){
	%>
	
	<div class="container">
		<div class="row">
			<div class="col-sm-12"
				style="border: 5px solid DodgerBlue; border-radius: 20px;height: 550px;">
				<img src="<%=application.getContextPath()%>/images/insurance-card-logo2.png" width="80"height="80">
				<table>
				<tr>
		<td style="color: blue;"><h3 id="policyname"><%=dto. getPolicyName() %></h3> </td>
		</tr>
		<tr>
		<td><b style="color:blue">Policy Number ::</b><label id="poicyNumber"><%=dto.getPolicyNumber() %></label> </td>
		</tr>
		<tr>
		<td><p><b style="color:blue">Coverage ::</b></p><label id="coverage"><%=dto.getCoverage() %></label></td>
		</tr>
		<tr>
		<td><p><b style="color: blue;">Description ::</b></p><label id="description"><%=dto.getDescription() %></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Duration ::</b><label id="duration"><%=dto.getDuration() %>&nbsp;<h>months</h></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Initial Amount ::</b><label id="iamount"><%=dto.getInitialAmount() %>&nbsp;<h>Rupee</h></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Total Amount ::</b><label id="tamount"><%=dto.getTotalamount() %>&nbsp;<h>Rupee</h></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Rate-of-interest ::</b><label id="rate"><%=dto.getRateOfInterest() %><h>%</h></label></td>
		</tr>
		<tr>
		<td><b style="color: blue;">Offers ::</b><label id="rate"><%=dto.getPolicyOffer() %></label></td>
		</tr>
	</table>
	
  </div>
</div>
<br><br><br>
</div>
 <%

	   } 
	  %>	
	
   
    <jsp:include page="footer.jsp"></jsp:include> 
</body>
</html>