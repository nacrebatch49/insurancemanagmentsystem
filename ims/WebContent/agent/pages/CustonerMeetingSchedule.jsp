<%@page import="java.util.List"%>
<%@page import="com.nacre.ims.bean.MyCustomerDetailsByAgent"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>

<script type="text/javascript" src="../js/custoner_meeting_schedule_validetion_page.js"></script>
<style>
* {
	box-sizing: border-box;
}

#myInput {
	background-image: url('/css/searchicon.png');
	background-position: 10px 10px;
	background-repeat: no-repeat;
	width: 100%;
	font-size: 16px;
	padding: 12px 20px 12px 40px;
	border: 1px solid #ddd;
	margin-bottom: 12px;
}

#myTable {
	border-collapse: collapse;
	width: 100%;
	border: 1px solid #ddd;
	font-size: 18px;
}

#myTable th, #myTable td {
	text-align: left;
	padding: 12px;
}

#myTable tr {
	border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
	background-color: #f1f1f1;
}

.modal-header {
	background-image: url('../ImageM/meetingheader1.jpg');
	background-repeat: no-repeat;
	background-size: cover;
	height: 110px;
}

.modal-body {
	background-image: url('../ImageM/meetingimage2.jpg');
	background-repeat: no-repeat;
	background-size: cover;
	height: 300px;
}

.modal-footer {
	background-image: url('../ImageM/meetingfooter1.jpg');
	background-repeat: no-repeat;
	background-size: cover;
	height: 75px;
}
</style>
<title>Client Table........</title>
</head>
<body>
	
<form
		action="<%=application.getContextPath()%>/MyCustomerDetailsByAgentControllerurl" id="registrationForm"
		method="post">



		<div class="container">
           x
		
			<h1>ViewMyCustomer</h1>
			<input type="text" id="myInput" onkeyup="myFunction()"
				placeholder="Search for client names.." title="Type in a name">
			<div class="table-responsive">
				<table class="table" id="myTable">

					<tr class="header">
					
						<!-- <div class="row" align="right">
<form class="example" action="/action_page.php" style="margin:auto;max-width:300px">
  <input type="text" placeholder="Search.." name="search2">
  <input type="submit" class="fa fa-search"  value="Search" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
</form>
  </div> -->

						<!-- <th><input type="checkbox">All</th> -->
						<th><button type="button" id='check_all'>Check All</button></th>
						<th>ClientName</th>
						<th>Email</th>
						<th>Mobile</th>
						<th>Address</th>
						<th>Policies</th>
						<th>Gender</th>
						<th>Dob</th>
					</tr>

					<%
                    Object obj=session.getAttribute("mycustomer");
				    List<MyCustomerDetailsByAgent> ldata=(List<MyCustomerDetailsByAgent> )obj;
                    if(ldata!=null)
                    {
                    for(MyCustomerDetailsByAgent agent:ldata){    %>
                   
                   <tr>
					<td><input type="checkbox" name="vehicle"></td>
						<td><h5><%=agent.getFirst_name() %></h5></td>
						<td><h5><%=agent.getEmail() %></h5></td>
						<td><h5><%=agent.getMobile_no()%></h5></td>
						<td><h5><%=agent.getAddress() %></h5></td>
						<td><h5><%=agent.getPolicies() %></h5></td>
						<td><h5><%=agent.getGender() %></h5></td>
						<td><h5><%=agent.getDob() %></h5></td>
						
				</tr>
							 <%} }%>
				    
						
				</table>
				<script>
					$('#check_all').click(function() {
						$('input[name=vehicle]').prop('checked', false);
					});

					$('#check_all').click(function() {
						$('input[name=vehicle]').prop('checked', true);
					});
				</script>
				<script>
					function myFunction() {
						var input, filter, table, tr, td, i;
						input = document.getElementById("myInput");
						filter = input.value.toUpperCase();
						table = document.getElementById("myTable");
						tr = table.getElementsByTagName("tr");
						for (i = 0; i < tr.length; i++) {
							td = tr[i].getElementsByTagName("td")[1];
							if (td) {
								if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
									tr[i].style.display = "";
								} else {
									tr[i].style.display = "none";
								}
							}
						}
					}
				</script>
				<div class="row" align="center">
					<button type="button" class="btn btn-info btn-lg"
						data-toggle="modal" data-target="#myModal">Fix Meeting
						Schedule</button>
					<!-- <input type="submit" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" value="Fix Meeting Schedule"> -->
				</div>
			</div>
	</form>
</body>
</html>