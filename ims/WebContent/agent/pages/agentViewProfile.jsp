<%@page import="com.nacre.ims.bean.AgentViewProfileBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AgentViewProfile</title>
<link rel="stylesheet" href="../css/agentViewProfile.css"
	type="text/css">
	
<!-- <link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"> -->

</head>
<% Object obj=session.getAttribute("viewinfo");
	AgentViewProfileBean bean=(AgentViewProfileBean)obj;
%>
<body>
<jsp:include page="agentheaderr.jsp"></jsp:include>
	<div class="contanier">
		<h1 style="color:maroon;">View Profile</h1>

		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-4">
			
				<table class="background">
				<tr>
						<td><b>Name</b></td>
						<td><%=bean.getAgent_name() %></td>
					</tr>
					
						<tr>
						<td><b>Mobile No</b></td>
						<td><%=bean.getMobile_number() %></td>
					</tr>
					<tr>
						<td><b>Email Id</b></td>
						<td><%=bean.getAgent_email_id()%></td>
					</tr>
					<tr>
						<td><b>Gender</b></td>
						<td><%=bean.getGender()%></td>
					</tr>
					<tr>
						<td><b>DOB</b></td>
						<td><%=bean.getDob()%></td>
					</tr>
						<tr>
						<td><b>Adhar No</b></td>
						<td><%=bean.getAdhaar_card_no()%></td>
					</tr>
					<tr>
						<td><b>Pan No</b></td>
						<td><%=bean.getPancard_no()%></td>
					</tr>
					<tr>
						<td><b>DOJ</b></td>
						<td><%=bean.getDate_of_joining()%></td>
					</tr>
					<tr>
					<td style="border-bottom: 1px solid #D1CDCD; margin: 0px 10px;"></td>
					<td style="border-bottom: 1px solid #D1CDCD; margin: 0px 10px;"></td>
					</tr>
				
					<tr>
						<td><b>Qualification</b></td>
						<td><%=bean.getQualification()%></td>
					</tr>
				<!-- 	
					<tr>
						<td><b>University Name</b></td>
						<td>QisIt</td>
					</tr> -->
					<tr>
						<td><b>YOP</b></td>
						<td><%=bean.getYear_of_passout()%></td>
					</tr>
					<tr>
						<td><b>Percentage(%)</b></td>
						<td><%=bean.getPercentage()%></td>
					</tr>
					<tr>
					<td style="border-bottom: 1px solid #D1CDCD; margin: 0px 10px;"></td>
					<td style="border-bottom: 1px solid #D1CDCD; margin: 0px 10px;"></td>
					</tr>
				<tr>
						<td><b>Country</b></td>
						<td><%=bean.getCountry_name()%></td>
					</tr>
						<tr>
						<td><b>State</b></td>
						<td><%=bean.getState_name() %></td>
					</tr>
						<tr>
						<td><b>city</b></td>
						<td><%=bean.getCity_name() %></td>
					</tr>
						<tr>
						<td><b>Address</b></td>
						<td><%=bean.getLocal_address() %></td>
					</tr>
					<tr>
						<td><b>pincode</b></td>
						<td><%=bean.getPincode() %></td>
					</tr>
				
					
					<tr>
						<td></td>
						<td align="right"><a href="../pages/agentEditProfile.jsp"><input
								type="submit" id="btnedd" value="Edit" class="button" /></a></td>
						
						
					</tr>
				</table>

			</div>


			<div class="col-lg-4"></div>
		</div>




	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>