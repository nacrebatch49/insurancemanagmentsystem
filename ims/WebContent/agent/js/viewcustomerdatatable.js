$(document)
		.ready(
				function() {
					$('#registrationForm')
							.bootstrapValidator(
									{
										message : 'This value is not valid',
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},

										fields : {
											date : {
												validators : {
													notEmpty : {
														message : 'The Meeting date is required'
													},
													date : {
														format : 'YYYY/MM/DD',
														message : 'Thes Meeting date is not valid'
													}
												}
											},

											time : {
												validators : {
													notEmpty : {
														message : 'The Meeting time is required'
													},
													time : {
														message : 'Thes Meeting time is not valid'
													}
												}
											},
											description : {
												validators : {
													notEmpty : {
														message : 'The description is required and cannot be empty'
													},
													stringLength : {
														max : 500,
														message : 'The description must be less than 500 characters long'
													}
												}
											},
											venue : {
												validators : {
													notEmpty : {
														message : 'The Venue is required and cannot be empty'
													},
													stringLength : {
														max : 200,
														message : 'The Venue must be less than 200 characters long'
													}
												}
											}
										// venue

										}

									});
				});
