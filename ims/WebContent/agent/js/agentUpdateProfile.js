$(document).ready(function() {
    $('#agentEditForm').bootstrapValidator({
    
        fields: {
        	name: {
        		  container: '#fn',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    },
                    regexp: {
                        regexp:/^[a-zA-Z]+$/,
                        message: 'Enter only characters'
                    }
                }
            },
         
            mobileno: {
            	container: '#mobno',
            
                validators: {
                	  
                
                    notEmpty: {
                        message: 'The mobileno is required and cannot be empty'
                    },
                    regexp: {
                        regexp:/^\d{10}$/ && /^[7-9][0-9]{9}$/,
                        message: 'Enter only 10 digit number'
                    }
                }
            },
            
            qualification: {
            	  container: '#qual',
                validators: {
                    notEmpty: {
                        message: 'The qualification is required and cannot be empty'
                    },
                    regexp: {
                        regexp:/^[a-zA-Z]+$/,
                        message: 'Enter only characters do not use any special characters'
                    }
                }
            },
            
            yop: {
            	  container: '#year',
            
                validators: {
                    notEmpty: {
                        message: 'The yop is required and cannot be empty'
                    },
                    regexp: {
                        regexp:/^\d{4}$/ && /^[0-9]{4}$/,
                        message: 'Enter valid passed out year'
                    }
                }
            },
            percentage: {
            	container: '#per',
            
                validators: {
                    notEmpty: {
                        message: 'The percentage is required and cannot be empty'
                    },
                    regexp: {
                    
                        regexp:/^([0-9]{1,2}([\.][0-9]{1,})?$|100([\.][0]{1,})?)$/,
                        message: 'Enter valid percentage'
                    }
                }
            },
            country: {
            	container: '#cou',
                validators: {
                    notEmpty: {
                        message: 'country selection required'
                    }
                }
            },
            state: {
            	container: '#st',
                validators: {
                    notEmpty: {
                        message: 'state selection required'
                    }
                }
            },
            city: {
            	container: '#ct',
                validators: {
                    notEmpty: {
                        message: 'city selection required'
                    }
                }
            },
            address: {
            	container: '#add',
                validators: {
                    notEmpty: {
                        message: 'The address is required and cannot be empty'
                    }
                }
            },
            pincode: {
            	container: '#pin',
            
                validators: {
                    notEmpty: {
                        message: 'The pincode is required and cannot be empty'
                    },
            regexp: {
                regexp:/^\d{6}$/ && /^[0-9]{6}$/,
                message: 'Enter proper Number with 6 digit'
            }
            
                }
            }
    
                }
    });
});