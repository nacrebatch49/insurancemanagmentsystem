<%@page import="com.nacre.ims.bean.CustomerViewProfileBean"%>
<%@page import="com.nacre.ims.bean.AgentViewProfileBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CustomerEditProfile</title>
<link rel="stylesheet" href="../css/customerEditProfile.css"
	type="text/css">

<!-- <link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">


<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 

<link rel="stylesheet"
	href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css" />
<script type="text/javascript"
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js">
	
</script>
<script type="text/javascript" src="<%=application.getContextPath()%>/customer/js/customerUpdateProfile.js"></script>
<script type="text/javascript"
	src="<%=application.getContextPath()%>/commons/js/GetData.js"></script>

</head>
<%
	Object obj = session.getAttribute("viewinfo");
	CustomerViewProfileBean bean=(CustomerViewProfileBean)obj;
%>
<body>
<jsp:include page="cutomerheader.jsp"></jsp:include>
	<div class="contanier">
		<h1 style="color: blue">Update Profile</h1>


		<div class="row">
			<div class="col-lg-2"></div>



			<div class="col-lg-8">

				<form id="customerEditForm"
					action="<%=application.getContextPath()%>/CustomerEditProfileController"
					method="POST">
					<table class="background">
						<tr>
							<td><b>First Name</b></td>
							<td><input type="text" class="form-control" id="fname"
								name="firstname" value=<%=bean.getCustomer_first_name() %>><span id="fn"></span></td>
							<td><b>Last Name</b></td>
							<td><input type="text" class="form-control" id="lname"
								name="lastname" value=<%=bean.getCustomer_last_name() %>><span id="ln"></span></td>
						</tr>
						<tr>
							<td><b>Mobile No</b></td>
							<td><input type="text" class="form-control" id="mobileno"
								name="mobileno" value=<%=bean.getCustomer_mobile_no() %>><span id="mobno"></span></td>

							<td><b>Email Id</b></td>
							<td><input type="text" disabled="disabled"
								class="form-control" value=<%=bean.getCustomer_email_id() %>></td>

						</tr>
						<tr>
							<td><b>Gender</b></td>
							<td><input type="text" class="form-control"
								disabled="disabled" value=<%=bean.getCustomer_gender() %>></td>

							<td><b>Adhar No</b></td>
							<td><input type="text" class="form-control"
								disabled="disabled" value=<%=bean.getAdhar_no() %>></td>

						</tr>
						<tr>
							<td><b>Dob</b></td>
							<td><input type="date" class="form-control"
								disabled="disabled" value=<%=bean.getCustomer_dob() %>></td>
						</tr>

						<tr>
							<td><b style="color: orange">Address Details:</b></td>
						</tr>

						<tr>
							<td><b>Country</b></td>
							<td><select id="country" class="form-control" name="country" required="required">

							</select><span id="country"></span></td>
							<td><b>State</b></td>
							<td><select id="state" class="form-control" name="state" required="required">

							</select><span id="state"></span></td>
						</tr>
						<tr>
							<td><b>city</b></td>
							<td><select id="city" class="form-control" name="city" required="required">
									
							</select><span id="city"></span></td>

							<td><b>Address</b></td>
							<td><input type="text" class="form-control" name="address"
								id="address" value=<%=bean.getLocal_address() %>><span id="add"></span></td>
						</tr>
						<tr>
							<td><b>pincode</b></td>
							<td><input type="text" class="form-control" name="pincode"
								id="pincode" value=<%=bean.getPincode() %>><span id="pin"></span></td>
						</tr>



						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td align="right"><input type="submit" id="btnedd"
								value="Update" class="button" /></td>
						</tr>

					</table>
				</form>
			</div>


			<div class="col-lg-2"></div>
		</div>




	</div>
		<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>