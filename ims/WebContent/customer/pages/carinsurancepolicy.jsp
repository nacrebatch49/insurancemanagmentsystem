<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LifeInsuranceRegistration</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css" />
<script type="text/javascript"
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../css/car.css">
<script src="<%=application.getContextPath()%>/customer/js/carinsurancevalidation.js"></script>


</head>
<body>
<form action="<%=application.getContextPath()%>/carinsuarncepolicyurl" id="lifeform">
	<h1 Style="text-align: center;color: red;">Car Insurance Form</h1>
	<div class="container-fluid">
		<div class="row-md-6">
			<div class="col-md-4"></div>
			<div class="col-md-4">
			<form id="lifeform" method="post">
					<div class="form-group row">
						<table class="table table-hover">
							<tbody>
								<tr>
									<td>Insurance Policy No</td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control"  name="insurancePolicyNo"
												id="litype">
										</div>
									</td>
								</tr>
								<tr>
									<td>Car No</td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control" name="carNo"
												id="maxlimit">
										</div>
									</td>
								</tr>
								<tr>
									<td>Purchase Date</td>
									<td>
										<div class="col-xs-12">
											<input type="date" class="form-control" name="purchaseDate">
										</div>
									</td>
								</tr>
								<tr>

									<td>R/C No</td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control" name="rcNo">
										</div>
									</td>
								</tr>
								<tr>
									<td>Nominee Name </td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control"
												name="nomineeName">
										</div>
									</td>
								</tr>
								<tr>
									<td>Nominee Mobile No </td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control"
												name="nomineeMobileNo">
										</div>
									</td>
								</tr>
								<tr>
									<td>Nominee Relation </td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control"
												name="nomineeRelation">
										</div>
									</td>
								</tr>
								<tr>
									<td>Max Limit </td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control"
												name="maxLimit">
										</div>
									</td>
								</tr>
								<!--<tr>
									<td>EMI Option Type </td>
									<td>
										<div class="col-sm-12">
											<select name="pay">
												<option value="1">Monthly</option>
												<option value="2">Quaterly</option>
												<option value="3">Half yearly</option>
												<option value="4">Annually</option>
											</select>
										</div>
									</td>

								</tr>
								<tr>
									<td>Customer id </td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control"
												name="customerid">
										</div>
									</td>
								</tr>
								<tr>
									<td>Car Insurance Master Id </td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control"
												name="carInsuranceMasterId">
										</div>
									</td>
								</tr>-->

								<tr>
								<td colspan="2"><span id="messages"></span></td>
								</tr>
								
								<tr>
									<td></td>
									<td>
										<div class="col-xs-12">
											<input type="submit" class="form-control" value="submit">
										</div>
									</td>
								</tr>
								
							</tbody>

						</table>
					</div>
				</form>
							</div>
			<div class="col-md-4">	
			</div>
		</div>

	</div>
	</form>
</body>
</html>