<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="com.nacre.ims.bo.CustomerViewCarInsuranceBO"%>
<%@page import="com.nacre.ims.bo.CustomerViewChildCareInsuranceBO"%>
<%@page import="com.nacre.ims.bo.CustomerViewMyHomeInsurance"%>
<%@page import="com.nacre.ims.bo.CustomerViewMyLifeInsurance"%>
<%@ page import ="java.util.List"%>


<link
	href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="customer/css/viewmypoliciesbycustomer.css">
<script
	src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrasp.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<body
	style="align-content: center; background-image: url('<%=application.getContextPath()%>/images/ima.jpg'); background-repeat: no-repeat; background-size: cover;">
	<jsp:include page="cutomerheader.jsp"></jsp:include>
		<%
	List obj =(List) request.getAttribute("ViewMyPolicies");
	List<CustomerViewMyLifeInsurance> first = (List) obj.get(0);
	List<CustomerViewMyHomeInsurance> second = (List) obj.get(1);
	List<CustomerViewCarInsuranceBO> third = (List) obj.get(2);
	List<CustomerViewChildCareInsuranceBO> fourth = (List) obj.get(3);
	%>
	<div class="container-fluid">
		<h1>My Policies</h1>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<c:if test="<%=(!first.isEmpty())%>">
					<h1>LIFE INSURANCE</h1>
							<table  border="3" >
								<tr>
									<th>InsuranceName</th>
									<th>Maximum months to complete Insurance</th>
									<th>Nominee Name</th>
									<th>Nominee Email</th>
									<th>Nominee Relation</th>
									<th>register policy number</th>
									<th>Date of policy</th>
									<th>Payment type</th>
									<th>Total Amount</th>
									<th></th>
								</tr>
								
									<%
										for (CustomerViewMyLifeInsurance l : first) {
									%>
									<tr>
									<td><%=l.getInsuranceName()%></td>
									<td><%=l.getMaxLimit()%></td>
									<td><%=l.getNomineeName()%></td>
									<td><%=l.getEmailId()%></td>
									<td><%=l.getNomineeRelation()%></td>
									<td><%=l.getRegisterPolicyNumber()%></td>
									<td><%=l.getDateOfPolicy()%></td>
									<td><%=l.getEmiType()%></td>
									<td><%=l.getTotalAmt()%></td>
									<td><input type="button" value="pay"></td>	</tr>
									<%
										}
									%>

							
							</table>
						</c:if>

						<c:if test="<%=(!second.isEmpty())%>">
					<h1>HOME INSURANCE</h1>
							<table border="3px">
								<tr>
									<th>Register Number</th>
									<th>Home Address</th>
									<th>Maximum Duration of Policy</th>
									<th>Nominee Name</th>
									<th>Nominee Email</th>
									<th>Nominee Relation</th>
									<th>Date of policy</th>
									<th>EMI Options</th>
									<th>Total Amount</th>
									<th></th>
								</tr>
								<tr>
									<%
										for (CustomerViewMyHomeInsurance h : second) {
									%>
									<td><%=h.getRegisterNumber()%></td>
									<td><%=h.getHomeAdd()%></td>
									<td><%=h.getMaxDur()%></td>
									<td><%=h.getNominies_name()%></td>
									<td><%=h.getNominies_email_id()%></td>
									<td><%=h.getRelation()%></td>
									<td><%=h.getDate_of_policies()%></td>
									<td><%=h.getEmi_option_type1()%></td>
									<td><%=h.getTotalAmt()%></td>
									<td><input type="button" value="pay"></td>
									<%
										}
									%>

								</tr>
							</table>
							</c:if>
						<c:if test="<%=(!third.isEmpty())%>">
					<h1>CAR INSURANCE</h1>
							<table border="3px">
								<tr>
									<th>Insurance Policy Number</th>
									<th>Car Number</th>
									<th>Purchase Date</th>
									<th>RC Number</th>
									<th>Nominee Name</th>
									<th>Nominee MobileNo</th>
									<th>Nominee Relation</th>
									<th>Maximum months for policy completion</th>
									<th>Payment Type</th>
									<th>Total Amount</th>
								</tr>
								<tr>
									<%
										for (CustomerViewCarInsuranceBO c : third) {
									%>
									<td><%=c.getInsurance_policy_no()%></td>
									<td><%=c.getCar_number()%></td>
									<td><%=c.getPurchage_date()%></td>
									<td><%=c.getRc_no()%></td>
									<td><%=c.getNominies_name()%></td>
									<td><%=c.getNominies_mobile_no()%></td>
									<td><%=c.getNominies_relation()%></td>
									<td><%=c.getMax_limit()%></td>
									<td><%=c.getEmi_option_type()%></td>
						            <td><%=c.getTotalAmt()%></td>
									
									<td><input type="button" value="pay"></td>
									<%
										}
									%>

								</tr>
							</table>
						</c:if> 
						<c:if test="<%=(!fourth.isEmpty())%>">
						<h1>CHILD CURE INSURANCE</h1>
							<table border="3px">
								<tr>
									<th>Child Name</th>
									<th>Child Age</th>
									<th>BirthCertificate Number</th>
									<th>Parents Name</th>
									<th>Date of insurance</th>
									<th>Payment type</th>
									<th>Total Amount</th>
									<th>Register Policy</th>
									<th></th>
								</tr>
								<tr>
									<%
										for (CustomerViewChildCareInsuranceBO c : fourth) {
									%>
									<td><%=c.getChild_name()%></td>
									<td><%=c.getChild_age()%></td>
									<td><%=c.getBirthcertificate_no()%></td>
									<td><%=c.getParents_name()%></td>
									<td><%=c.getDate_of_insurance()%></td>
									<td><%=c.getPayment_type()%></td>
									<td><%=c.getTotalAmt()%></td>
									<td><%=c.getRegisterpolicy()%></td>
									<td><input type="button" value="pay"></td>
									<%
										}
									%>

								</tr>
							</table>
						</c:if>
					</div>
					</div>
					</div>
					</div>
					</div>
					<jsp:include page="footer.jsp"></jsp:include>
					</body>
					</html>