<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>LifeInsuranceRegistration</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>


<link rel="stylesheet" href="<%=application.getContextPath()%>/customer/css/Life.css">

<script src="<%=application.getContextPath()%>/customer/js/life.js"></script>


</head>
<body>
<jsp:include page="cutomerheader.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row-md-6">
							<h2>Life Insurance Registration form</h2>
		
			<div class="col-md-3"></div>
			<div class="col-md-7">
			<form id="lifeform" action="<%=application.getContextPath()%>/customerLifeReg" method="post">
					<div class="form-group row">
					
						<table class="table table-hover">
							<tbody>
								<tr>
									<td>Life Insurance Type</td>
									<td>
										<div class="col-xs-12">
										    <input type="hidden" name="id" value="<%=request.getParameter("id")%>">
											<input type="text" class="form-control"  name="litype">
										<span id="litype"></span>
										</div>
									</td>
								</tr>
								<tr>
									<td>Maximum months required to cover whole Insurance?</td>
									<td>
										<div class="col-xs-12">
										
											<input type="text" class="form-control" name="maxlimit"
												id="maxlimit">
												<span id="smax"></span>
										</div>
									</td>
								</tr>
								<tr>
									<td>Nominee Name</td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control" name="nomineename">
										</div>
									</td>
								</tr>
								<tr>

									<td>Nominee Email Address</td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control" name="nomineeEmail">
										</div>
									</td>
								</tr>
								<tr>
									<td>Nominee Relation</td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control"
												name="nomineerelation">
										</div>
									</td>
								</tr>
								<tr>
									<td>Payment Type</td>
									<td>
										<div class="col-sm-12">
											<select name="pay">
												<option value="1">Monthly</option>
												<option value="2">Quaterly</option>
												<option value="3">Half yearly</option>
												<option value="4">Annually</option>
											</select>
										</div>
									</td>

								</tr>
							
								
								<tr>
									<td></td>
									<td>
										<div class="col-xs-12">
											<input type="submit" class="form-control" value="submit">
										</div>
									</td>
								</tr>
								
							</tbody>

						</table>
					</div>
				</form>
						<h1><jsp:include page="footer.jsp"></jsp:include>	</h1>	
				
							</div>
			<div class="col-md-2">
			</div>
		</div>
				

	</div>

</body>
</html>