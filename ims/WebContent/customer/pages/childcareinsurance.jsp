
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ChildCareInsuranceRegistration</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css" />
<script type="text/javascript"
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>

<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- <link rel="stylesheet" href="../customer/css/childcareinsuranceregister.css"> -->
<script src="<%=application.getContextPath() %>/customer/js/childcareinsurancevalidation.js"></script>
<style type="text/css">
</style>

</head>
<body>
<jsp:include page="cutomerheader.jsp"/>
<form action="<%=application.getContextPath()%>/childcareinsuranceurl" id="childInssurance" >
	<h1 style="text-align:center;color: red;">CHILD CARE INSURANCE REGISTRATION FORM</h1>
	<div class="container-fluid">
		<div class="row-md-6">
			<div class="col-md-4"></div>
			<div class="col-md-4">
			<form id="childInssurance" method="post" >
					<div class="form-group row">
						<table class="table table-hover">
							<tbody>
								<tr>
									<td>Child Name</td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control"  name="childName"
												id="childId">
										</div>
									</td>
								</tr>
								<tr>
									<td>Child Age</td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control" name="childAge">
										</div>
									</td>
								</tr>
								<tr>
									<td>Birth  Certificate</td>
									<td>
										<div class="col-xs-12">
											<input type="text" class="form-control" name="birthCertificate">
										</div>
									</td>
								</tr>
								<tr>
									<td>Parent Name</td>
									<td>
											<div class="col-xs-12">
											<input type="text" class="form-control" name="parentName" id="nomineeRelation">
										</div>
									</td>
								</tr>
								<tr>
									<td>Date of Insurance</td>
									<td>
										<div class="col-xs-12">
											<input type="date" class="form-control" name="dateInsurance">
										</div>
									</td>
								</tr>
								<tr>
									<td>Payment Type</td>
									<td>
										<div class="col-sm-12">
											<select name="pay">
												<option value="1">Monthly</option>
												<option value="2">Quaterly</option>
												<option value="3">Half yearly</option>
												<option value="4">Annually</option>
											</select>
										</div>
									</td>

								</tr>
								<tr>
								<td colspan="2"><span id="messages"></span></td>
								</tr>
								<tr>
								  <td></td>
								<td>
								<div class="col-xs-12">
											<input type="hidden" class="form-control" name="childcareid" value="<%=request.getParameter("id") %>">
										</div>
								
								</tr>
								
								<tr>
									<td></td>
									<td>
										<div class="col-xs-12">
											<input type="submit" class="form-control" value="submit">
										</div>
									</td>
								</tr>
								
								
								
							</tbody>

						</table>
					</div>
				</form>
							</div>
			<div class="col-md-4">	
			</div>
		</div>

	</div>
	</form>
	<jsp:include page="footer.jsp"/>
</body>
</html>