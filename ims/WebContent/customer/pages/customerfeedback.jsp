<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.10.2.js"></script>
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css" />
<script type="text/javascript"
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"> </script>
	<script src="<%=application.getContextPath() %>/customer/js/feedbackvalidation.js"></script>
	<style type= "text/css" >
	body {
	background-repeat:repeat-x;
    background-size:cover;
    
  }
	input[type=text]:focus {
    border: 3px solid #555;
}  

	</style>
	
</head>
<jsp:include page="cutomerheader.jsp"></jsp:include>
<body background="<%=application.getContextPath()%>/customer/pages/nicefd.jpg">
<div>
<h1 style="color:red;text-align:center"><b>Give Your Feedback</b></h1>

</div>
	<form  action="<%=application.getContextPath()%>/customerfeedbackcontroller" id="contactForm" method="get" class="form-horizontal">
    
    <div  class="container">
    <div class="row">
    <div  class="col-sm-4"></div>
    <div class ="col-sm-5">
    <div class ="panel panel-default" style ="border: 3px solid dodgerblue; background-color: #ffdab3;border-radius: 20px;" 
                              align="center" hight="60%" width="40%">
    <div class="panel-body">
    <div class ="modal-body">
    
        
        <div class="form-group" >
        <label for="col-md-6 control-label" ><b>Email</b></label><br>
        <div class="input-group">
            <input type="email" class="form-control" name="email" id="email" />
        </div>
    </div>
    
    
     <div class="form-group" >
        <label for="col-md-6 control-label"><b>Feedback</b></label>
        
        <div class="input-group">
           <textarea class="form-control" name="description" id="description" rows="5"></textarea>
        </div>
    </div>
    
    <!-- #messages is where the messages are placed inside -->
    <div class="form-group" >
        <div class="col-md-6 col-md-offset-6">
            <div id="messages"></div>
        </div>
    </div> 
    <div class="form-group" align="center">
        <div class="col-md-9 col-md-offset-3">
            <!-- <button type="clear" class="btn btn-default" >Clear</button>&nbsp &nbsp -->
            <button type="submit" class="btn btn-default"><b>Send</b></button>
            
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
</form>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>