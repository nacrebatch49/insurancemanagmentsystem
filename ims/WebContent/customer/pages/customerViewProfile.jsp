<%@page import="com.nacre.ims.bean.CustomerViewProfileBean"%>
<%@page import="com.nacre.ims.bean.AgentViewProfileBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> --%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>customerViewProfile</title>
<link rel="stylesheet" href="../css/customerViewProfile.css"
	type="text/css">

</head>
<% Object obj=session.getAttribute("viewinfo");
	CustomerViewProfileBean bean=(CustomerViewProfileBean)obj;
%>	
<body>

<jsp:include page="cutomerheader.jsp"></jsp:include>
	<div class="contanier">
		<h1 style="color:maroon;">View Profile</h1>

		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-4">
			
				<table class="background">
				<tr>
						<td><b>First Name</b></td>
						<td><%=bean.getCustomer_first_name() %></td>
					</tr>
					
					<tr>
						<td><b>last Name</b></td>
						<td><%=bean.getCustomer_last_name() %></td>
					</tr>
					<tr>
						<td><b>Email Id</b></td>
						<td><%=bean.getCustomer_email_id() %></td>
					</tr>
					<tr>
						<td><b>password</b></td>
						<td><%=bean.getCustomer_gender() %></td>
					</tr>
					
					
						<tr>
						<td><b>Mobile No</b></td>
						<td><%=bean.getCustomer_mobile_no() %></td>
					</tr>
					
					<tr>
						<td><b>Gender</b></td>
						<td><%=bean.getCustomer_gender() %></td>
					</tr>
					<tr>
						<td><b>DOB</b></td>
						<td><%=bean.getCustomer_dob()%></td>
					</tr>
						<tr>
						<td><b>Adhar No</b></td>
						<td><%=bean.getAdhar_no() %></td>
					</tr>
					
					<tr>
					<td style="border-bottom: 1px solid #D1CDCD; margin: 0px 10px;"></td>
					<td style="border-bottom: 1px solid #D1CDCD; margin: 0px 10px;"></td>
					</tr>
				
					
						<td><b>Country</b></td>
						<td><%=bean.getCountry_name() %></td>
					</tr>
						<tr>
						<td><b>State</b></td>
						<td><%=bean.getState_name() %></td>
					</tr>
						<tr>
						<td><b>city</b></td>
						<td><%=bean.getCity_name() %></td>
					</tr>
						<tr>
						<td><b>Address</b></td>
						<td><%=bean.getLocal_address() %></td>
					</tr>
					<tr>
						<td><b>pincode</b></td>
						<td><%=bean.getPincode() %></td>
					</tr>
				
					
					<tr>
						<td></td>
						<td align="right"><a href="../pages/customerEditProfile.jsp"><input
								type="submit" id="btnedd" value="Edit" class="button" /></a></td>
						
						
					</tr>
				</table>

			</div>


			<div class="col-lg-4"></div>
		</div>




	</div>
		<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>