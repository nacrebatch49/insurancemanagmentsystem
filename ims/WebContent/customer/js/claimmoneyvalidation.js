$(document).ready(function(){
	alert("hi");
	$('#claimMoneyByCustomer').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
        	policyNo: {
                validators: {
                    notEmpty: {
                        message: 'The register policy  number is required and cannot be empty'
                    },
                   /* regexp:{
                        regexp: /^[A-Z a-z]+$/i,
                        message:'please enter only letters'
                        }*/
                    regexp:{
                        regexp: /^[0-9]+$/i,
                        message:'please enter only numbers'
                        },
                }
            },
            nomineeName: {
                validators: {
                    notEmpty: {
                        message: 'The full name is required and cannot be empty'
                    },
                    regexp:{
                        regexp: /^[A-Z a-z]+$/i,
                        message:'please enter only letters'
                        }
                }
            },
            nomineeEmail: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The email address is not valid'
                    }
                }
            },
            nomineeRelation:{
            	validators:{
            		notEmpty:{
            			message:'The nominee realation is required and cannot be empty'
            		},
            		regexp:{
                        regexp: /^[A-Z a-z]+$/i,
                        message:'please enter only letters'
                        }
            	}
            },
            nomineeReason:{
            	validators:{
            		notEmpty:{
            			message:'The reason is required and cannot be empty'
            		},
            		regexp:{
                        regexp: /^[A-Z a-z]+$/i,
                        message:'please enter only letters'
                        }
            	}
            }
        }
    });
});