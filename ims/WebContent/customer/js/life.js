$(document).ready(function(){
	$('#lifeform').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            litype: {
            	container: '#litype',
                validators: {
                    notEmpty: {
                        message: 'The life insurance type is required and cannot be empty'
                    },
                    regexp:{
                        regexp: /^[A-Z a-z]+$/i,
                        message:'please enter only letters'
                        }
                }
            },
            maxlimit: {
                validators: {
                    notEmpty: {
                        message: 'Please fill the maximum months'
                    },
                    regexp:{
                    regexp: /^[0-9]+$/i,
                    message:'please enter only numbers'
                    },
                  
                }
            },
            nomineename: {
                validators: {
                    notEmpty: {
                        message: 'The full name is required and cannot be empty'
                    },
                    regexp:{
                        regexp: /^[A-Z a-z]+$/i,
                        message:'please enter only letters'
                        }
                }
            },
            
            
            nomineeEmail: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    },
                    emailAddress: {
                        message: 'The email address is not valid'
                    }
                }
            },
            nomineerelation: {
                validators: {
                    notEmpty: {
                        message: 'The nominee relation status is required'
                    },
                    regexp:{
                        regexp: /^[A-Z a-z]+$/i,
                        message:'please enter only letters'
                        }
                }
            },
        }
    });
	
	$("#maxlimit").change(function(){
		var value=0;
		var data="";
	    value=$(this).val();
			$.ajax({
				url : "getDob",
				dataType : "json",
				success : function(response) {
				var value2=0;
				value2=response;
				var result=0;
				result=parseInt(value)+parseInt(value2);
				alert(result);
				    if(value<120){
				    	data="Minimum months are 120, please keep 120+ months";
				    }
					if(result>600){
					data="Minimum gap should be 10 years from your registered age, you cannot register, your age is to high for this policy";
				}
					$("#smax").text(data);
				},
				error : function(error) {
					alert("error");
				}
			});	
			});
	
	
});
	