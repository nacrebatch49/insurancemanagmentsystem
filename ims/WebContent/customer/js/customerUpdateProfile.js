$(document).ready(function() {
    $('#customerEditForm').bootstrapValidator({
    
        fields: {
        	firstname: {
        		  container: '#fn',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and cannot be empty'
                    },
                    regexp: {
                        regexp:/^[a-zA-Z]+$/,
                        message: 'Enter only characters'
                    }
                }
            },
           lastname: {
            	  container: '#ln',
                validators: {
                    notEmpty: {
                        message: 'The last name is required and cannot be empty'
                    },
                    regexp: {
                        regexp:/^[a-zA-Z]+$/,
                        message: 'Enter only characters'
                    }
                }
            },
            mobileno: {
            	container: '#mobno',
            
                validators: {
                	  
                
                    notEmpty: {
                        message: 'The mobileno is required and cannot be empty'
                    },
                    regexp: {
                        regexp:/^\d{10}$/ && /^[7-9][0-9]{9}$/,
                        message: 'Enter only 10 digit number'
                    }
                }
            },
            
          /* country: {
            	container: '#cou',
                validators: {
                    notEmpty: {
                        message: 'country selection required'
                    }
                }
            },
            state: {
            	container: '#st',
                validators: {
                    notEmpty: {
                        message: 'state selection required'
                    }
                }
            },
            city: {
            	container: '#ct',
                validators: {
                    notEmpty: {
                        message: 'city selection required'
                    }
                }
            },*/
            address: {
            	container: '#add',
                validators: {
                    notEmpty: {
                        message: 'The address is required and cannot be empty'
                    }
                }
            },
            pincode: {
            	container: '#pin',
            
                validators: {
                    notEmpty: {
                        message: 'The pincode is required and cannot be empty'
                    },
            regexp: {
                regexp:/^\d{6}$/ && /^[0-9]{6}$/,
                message: 'Enter proper Number with 6 digit'
            }
            
                }
            }
    
                }
    });
});