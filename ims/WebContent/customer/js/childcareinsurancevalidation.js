$(document).ready(function(){
	$('#childInssurance').bootstrapValidator({
		container:'#messages',
		feedbackIcons:{
			valid:'glyphicon glyphicon-ok',
			invalid:'glyphicon glyphicon-remove',
			validating:'glyphicon glyphicon-refresh'
				
		},
		fields:{
			childName:{
				validators:{
					notEmpty:{
						message:'childName is required and cannot be empty'
					},
					regexp:{
						regexp:/^[A-Za-z]+$/i,
						message:'please enter only letter'
					}
					
				}
			},

				childAge:{
					validators:{
						notEmpty:{
							message:'child Age is required and cannot be empty'
						},
						/*regexp:{
        					regexp: /^[1-9][0-9]{5,18}$/,
        					message:'please enter only number'
        				},	*/
						 callback: {
		                        message: 'Age must be greater than 4 and less than or equal to 18',
		                        callback: function(value, validator, $field) {
		                            if (value >17) {
		                                return false;
		                             }
		                            if(value<2){
		                            	return false;
		                            }
		                            return true;
		                            }
		                    }
					}
				},
		
					birthCertificate:{
						validators:{
							notEmpty:{
								message:'Birth Certification is required and cannot be empty'
							},
							regexp:{
								regexp:/^[A-Z0-9]+$/i,
								message:'please enter only birthcertification'
							}
						}
					},
	
						parentName:{
							validators:{
								notEmpty:{
									message:'Parent Name is required and cannot be empty'
								},
								regexp:{
									regexp:/^[A-Za-z]+$/i,
									message:'please enter only letter'
											
								}
							}
						},
							dateInsurance:{
								validators:{
									notEmpty:{
										message:'Date Insurance is required and cannot be empty'
									},
								
									}
								},
							}
	});
});