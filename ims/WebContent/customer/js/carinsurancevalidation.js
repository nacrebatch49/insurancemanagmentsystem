
$(document).ready(function(){
	//alert("hi")
	$('#lifeform').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
        	insurancePolicyNo:{
        		validators:{
        			notEmpty:{
        				message:'Insurance Policy Number Required'
        			},
        			regexp:{
        				regexp:/^[0-9]+$/i,
        				message:'please enter only number'
        			}
        		}
        	},
        		carNo:{
        			validators:{
        				notEmpty:{
        					message:'Car Number is Required'
        				},
        				regexp:{
        					regexp:/^[A-Z0-9]+$/i,
        					message:'please enter valid CarNo number'
        				}
        			}
        		},
        		purchaseDate:{
        			validators:{
        				notEmpty:{
        					message:'Purchase Date is Required'
        				},
        				
        			}
        		},
        		rcNo:{
        			validators:{
        				notEmpty:{
        					message:'R/C No is Required'
        				},
        				regexp:{
        					regexp:/^[A-Z0-9]+$/i,
        					message:'please enter valid rc number'
        				}
        			}
        		},
        		nomineeName:{
        			validators:{
        				notEmpty:{
        					message:'Nominee Name is Required'
        				},
        				regexp:{
        					regexp:/^[A-Z a-z]+$/i,
        					message:'please enter only letters'
        				}
        			}
        		},
        		nomineeMobileNo:{
        			validators:{
        				notEmpty:{
        					message:'Nominee Mobile No is Required'
        				},
        				regexp:{
        					regexp: /^[789]\d{9}$/,
        					message:'please enter only number'
        				},	
        						
        			}
        		},
        		
        		nomineeRelation:{
        			validators:{
        				notEmpty:{
        					message:'Nominee Relation is Required'
        				},
        				regexp:{
        					regexp:/^[A-Z a-z]+$/i,
        					message:'please enter only letters'
        				}
        			}
        		},
        		maxLimit:{
        			validators:{
        				notEmpty:{
        					message:'Max Limit is Required'
        				},
        				regexp:{
        					regexp:/^[0-9]+$/i,
        					message:'please enter only number'
        				}
        			}
        		},
        		/*maxLimit:{
        			validators:{
        				notEmpty:{
        					message:'EMI Option is Required'
        				},
        			}
        		},
        		customerid:{
        			validators:{
        				notEmpty:{
        					message:'Customer Id is Required'
        				},
        				regexp:{
        					regexp:/^[A-Z0-9]+$/i,
        					message:'please enter customer Id'
        				}
        			}
        		},
        		carInsuranceMasterId:{
        			validators:{
        				notEmpty:{
        					message:'Car Insurance Master Id is Required'
        				},
        				regexp:{
        					regexp:/^[A-Z0-9]+$/i,
        					message:'please enter Car Insurance Master Id'
        				}
        			}
        		},		*/
        }
    });
});